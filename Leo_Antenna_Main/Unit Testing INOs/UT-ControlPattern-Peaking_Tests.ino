//**************************************************************************************
//**************************************************************************************
#if isUnitTesting

#line 3 "UT-PeakingTests"
//#include "StandardFunctions_Array.h"
StandardFunctions_Array StandFunc;


//Simple Set Test
test(UT_CP_Peaking_Test0_ResultingSetCentrePosition)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_PeakingClass NewPeakingPattern;		NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************
//**************************************************************************************

//Simple Position Test
// Tests the New Targets are step size positions away from the Centre Position
//   First position test - Pos A
test(UT_CP_Peaking_Test1_ResultingPosition_A)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Find "A" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az] - AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El] + ElStepSize, 2));
}

//**************************************************************************************
//**************************************************************************************

//Simple Position Test
// Tests the New Targets are step size positions away from the Centre Position
//   First position test - Pos B
test(UT_CP_Peaking_Test2_ResultingPosition_B)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = A;		//Find "B" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El] + ElStepSize, 2));
}

//**************************************************************************************
//**************************************************************************************

//Simple Position Test
// Tests the New Targets are step size positions away from the Centre Position
//   First position test - Pos C
test(UT_CP_Peaking_Test3_ResultingPosition_C)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = B;		//Find "C" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El] - ElStepSize, 2));
}


//**************************************************************************************
//**************************************************************************************

//Simple Position Test
// Tests the New Targets are step size positions away from the Centre Position
//   First position test - Pos D
test(UT_CP_Peaking_Test4_ResultingPosition_D)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = C;		//Find "D" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az] - AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El] - ElStepSize, 2));
}


//**************************************************************************************
//**************************************************************************************

//Counter test
//  - Ensures the Current Pattern Position increases after each call of Next Position
test(UT_CP_Peaking_Test5_IteratingCurrentPatternPosition)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = A;		//Find "D" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, B);
	
}


//**************************************************************************************

//Counter test
//  - Ensures the Current Pattern Position increases after each call of Next Position
test(UT_CP_Peaking_Test6_IteratingCurrentPatternPositionComplete)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = A;		//Find "D" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, D);
	
}

//**************************************************************************************

//Counter test
//  - Ensures the Current Pattern Position increases after each call of Next Position
test(UT_CP_Peaking_Test6_IteratingCurrentPatternPositionInitial)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Find "D" Position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);			if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, D);

	
}




//**************************************************************************************

//Counter test
//  - Ensures the Current Pattern Position increases after each call of Next Position
test(UT_CP_Peaking_Test6_IteratingCurrentPatternPositionFinal)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
			ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Loop complete pattern round to a new A position	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);	NewPeakingPattern.GetNewSkyTarget(1500, NewSkyTarget);		if(DebugPrint)	{		Serial.print("+,Centre Pos;");		StandFunc.Print(SpoofCentrePosition);				Serial.print(" \ New Target Pos;");		StandFunc.Print(NewSkyTarget);				Serial.print("\n");	}	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A);

	
}

//**************************************************************************************
//**************************************************************************************

//Saving Values Test
//  - Ensures the Current Pattern Position increases after each call of Next Position saving values correctly
test(UT_CP_Peaking_Test7_SignalSavedValues)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[4] = {300, 600, 1200, 5};		ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);		// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Force Pattern to "A" Position before running complete pattern	NewPeakingPattern.GetNewSkyTarget(0, NewSkyTarget); // NewSkyTarget = A Position		//New signal values for each position of complete pattern	NewPeakingPattern.GetNewSkyTarget(savedSignals[A], NewSkyTarget); // Saves A signal value, NewSkyTarget = B Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B], NewSkyTarget);// Saves B signal value, NewSkyTarget = C Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C], NewSkyTarget);// Saves C signal value, NewSkyTarget = D Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D], NewSkyTarget);// Saves D signal value, NewSkyTarget = A Position		assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A);
	
	assertEqual(NewPeakingPattern._SavedPeakingSignalsAtPoint[A], savedSignals[A]);
	assertEqual(NewPeakingPattern._SavedPeakingSignalsAtPoint[B], savedSignals[B]);
	assertEqual(NewPeakingPattern._SavedPeakingSignalsAtPoint[C], savedSignals[C]);
	assertEqual(NewPeakingPattern._SavedPeakingSignalsAtPoint[D], savedSignals[D]);

	
}


//**************************************************************************************
//**************************************************************************************
//Failed Peaking Test
// Check issignallost flag
test(UT_CP_Peaking_Test8_RestartSearchCondition)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[4] = {0, 0, 0, 5};		ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);	NewPeakingPattern._signalLostThreshold = 1200;		// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Force Pattern to "A" Position before running complete pattern	NewPeakingPattern.GetNewSkyTarget(0, NewSkyTarget); // NewSkyTarget = A Position		//New signal values for each position of complete pattern	NewPeakingPattern.GetNewSkyTarget(savedSignals[A], NewSkyTarget); // Saves A signal value, NewSkyTarget = B Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B], NewSkyTarget);// Saves B signal value, NewSkyTarget = C Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C], NewSkyTarget);// Saves C signal value, NewSkyTarget = D Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D], NewSkyTarget);// Saves D signal value, NewSkyTarget = A Position
	
	assertTrue(NewPeakingPattern._isSignalLost);

	
}




//**************************************************************************************
//**************************************************************************************
//Failed Peaking Test
// - On isSignalLost the next position should be the last good centre position to restart the search from
test(UT_CP_Peaking_Test8_RestartSearchConditionCentreAsNewStart)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[4] = {0, 0, 0, 5};		ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, 0, 0);	NewPeakingPattern._signalLostThreshold = 1200;		// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Force Pattern to "A" Position before running complete pattern	NewPeakingPattern.GetNewSkyTarget(0, NewSkyTarget); // NewSkyTarget = A Position		//New signal values for each position of complete pattern	NewPeakingPattern.GetNewSkyTarget(savedSignals[A], NewSkyTarget); // Saves A signal value, NewSkyTarget = B Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B], NewSkyTarget);// Saves B signal value, NewSkyTarget = C Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C], NewSkyTarget);// Saves C signal value, NewSkyTarget = D Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D], NewSkyTarget);// Saves D signal value, NewSkyTarget = A Position	
	
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, SignalLost);
	
	assertTrue(NewPeakingPattern._isSignalLost);


	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El], 2));
	
}



//**************************************************************************************
//**************************************************************************************
//Complete Peaking Test
// - Signal Strength Values entered to produce change in Centre Position
//  - Values compared with Excel sheet to ensure accurate results
test(UT_CP_Peaking_Test9_NewCentrePosition)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	float KAz = 0.001; float KEl = 0.001;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[4] = {1200, 1800, 1500, 1200};		ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.SetPeakingSizes(AzStepSize,ElStepSize, KAz, KEl);	NewPeakingPattern._signalLostThreshold = 1000;		// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Force Pattern to "A" Position before running complete pattern	NewPeakingPattern.GetNewSkyTarget(0, NewSkyTarget); // NewSkyTarget = A Position		//New signal values for each position of complete pattern	NewPeakingPattern.GetNewSkyTarget(savedSignals[A], NewSkyTarget); // Saves A signal value, NewSkyTarget = B Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B], NewSkyTarget);// Saves B signal value, NewSkyTarget = C Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C], NewSkyTarget);// Saves C signal value, NewSkyTarget = D Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D], NewSkyTarget);// Saves D signal value, NewSkyTarget = A Position		float newCentreCalculated[3];
	
	// Get newly calculated Centre Sky Position once peaking pattern has run a complete pattern
	NewPeakingPattern.GetCentreSkyPosition(newCentreCalculated);
	
	// Check peaking pattern complete
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A);
	// Check no errors with reading signal
	assertFalse(NewPeakingPattern._isSignalLost);


	// Compare New values with expected
	assertTrue(StandFunc.CompareFloats(newCentreCalculated[Az], 120.9, 2)); // Expected Values from Excel Sheet - check drive
	assertTrue(StandFunc.CompareFloats(newCentreCalculated[El], 30.3, 2)); // Expected Values from Excel Sheet
	
}


//**************************************************************************************
//**************************************************************************************
//Complete Peaking Test
// - Signal Strength Values entered to produce change in Centre Position
test(UT_CP_Peaking_Test_10_FinalTestRefactored)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 5.0; float ElStepSize = 5.0;
	float KAz = 0.001; float KEl = 0.001;
	int signalLostThreshold = 1000;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[4] = {1200, 1800, 1500, 1200};		ControlPattern_PeakingClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup(AzStepSize,ElStepSize,KAz,KEl,signalLostThreshold);		NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);			// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Force Pattern to "A" Position before running complete pattern	NewPeakingPattern.GetNewSkyTarget(0, NewSkyTarget); // NewSkyTarget = A Position		//New signal values for each position of complete pattern	NewPeakingPattern.GetNewSkyTarget(savedSignals[A], NewSkyTarget); // Saves A signal value, NewSkyTarget = B Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B], NewSkyTarget);// Saves B signal value, NewSkyTarget = C Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C], NewSkyTarget);// Saves C signal value, NewSkyTarget = D Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D], NewSkyTarget);// Saves D signal value, NewSkyTarget = A Position		float newCentreCalculated[3];
	
	// Get newly calculated Centre Sky Position once peaking pattern has run a complete pattern
	NewPeakingPattern.GetCentreSkyPosition(newCentreCalculated);
	
	// Check peaking pattern complete
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A);
	// Check no errors with reading signal
	assertFalse(NewPeakingPattern._isSignalLost);


	// Compare New values with expected
	assertTrue(StandFunc.CompareFloats(newCentreCalculated[Az], 120.9, 2)); // Expected Values from Excel Sheet - check drive
	assertTrue(StandFunc.CompareFloats(newCentreCalculated[El], 30.3, 2)); // Expected Values from Excel Sheet
	
}

#endif