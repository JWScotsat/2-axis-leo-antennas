#if isUnitTesting
//**************************************************************************************
//**************************************************************************************
#line 3 "UT-PeakingTests"
//#include "StandardFunctions_Array.h"
StandardFunctions_Array StandFunc;


//Simple Set Test
test(UT_CP_PeakingAltCircleGrad_Test0_ResultingSetCentrePosition)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_PeakingAltCircleGradientClass NewPeakingPattern;		NewPeakingPattern.Setup();	NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************
//**************************************************************************************

//Complete Peaking Test
// - Signal Strength Values entered to produce change in Centre Position
test(UT_CP_PeakingAltCircleGrad_Test_1_FinalTestRefactored)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 0.25; float ElStepSize = 0.25;
	float KAz = 0.001; float KEl = 0.001;
	int signalLostThreshold = 1800;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[8] = {2000, 2100, 1500, 1200, 1500, 2000};		ControlPattern_PeakingAltCircleGradientClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup(AzStepSize,ElStepSize,KAz,KEl,signalLostThreshold);		NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);			// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		//Force Pattern to "A" Position before running complete pattern	NewPeakingPattern.GetNewSkyTarget(0, NewSkyTarget); // NewSkyTarget = A Position		//New signal values for each position of complete pattern	NewPeakingPattern.GetNewSkyTarget(savedSignals[A1], NewSkyTarget); // Returns B Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B1], NewSkyTarget);// Returns C Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C1], NewSkyTarget);// Returns D Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D1], NewSkyTarget);// Returns E Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[A2], NewSkyTarget);// Returns F Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[B2], NewSkyTarget);// Returns G Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[C2], NewSkyTarget);// Returns H Position	NewPeakingPattern.GetNewSkyTarget(savedSignals[D2], NewSkyTarget);// Returns New Centre A Position		float newCentreCalculated[3];
	
	// Get newly calculated Centre Sky Position once peaking pattern has run a complete pattern
	NewPeakingPattern.GetCentreSkyPosition(newCentreCalculated);
	
	// Check peaking pattern complete
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A1);
	// Check no errors with reading signal
	assertFalse(NewPeakingPattern._isSignalLost);


// 	// Compare New values with expected
// 	assertTrue(StandFunc.CompareFloats(newCentreCalculated[Az], 120.9, 2)); // Expected Values from Excel Sheet - check drive
// 	assertTrue(StandFunc.CompareFloats(newCentreCalculated[El], 30.3, 2)); // Expected Values from Excel Sheet
// 	
}



//Complete Peaking Test
// - Signal Strength Values entered to produce change in Centre Position
test(UT_CP_PeakingAltCircleGrad_Test_1_Printout)
{
	boolean DebugPrint = false;
	
	// Peaking Pattern Step Sizes
	float AzStepSize = 0.25; float ElStepSize = 0.25;
	float KAz = 0.001; float KEl = 0.001;
	int signalLostThreshold = 1800;
	
	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		float savedSignals[9] = {0, 2000, 2100, 1500, 1200, 1500, 2000, 1000, 2000};		ControlPattern_PeakingAltCircleGradientClass NewPeakingPattern;		// Setup and initial values	NewPeakingPattern.Setup(AzStepSize,ElStepSize,KAz,KEl,signalLostThreshold);		NewPeakingPattern.SetCentreSkyPosition(SpoofCentrePosition);			// Test forcing CurrentPatternPosition to starting point	NewPeakingPattern._CurrentPatternTargetPositionCounter = NONE;		float newCentreCalculated[3];		//New signal values for each position of complete pattern	for(int i = 0; i < 9; i++)	{	NewPeakingPattern.GetNewSkyTarget(savedSignals[i], NewSkyTarget); // Returns B Position	NewPeakingPattern.GetCentreSkyPosition(newCentreCalculated);		Serial.print("Pattern Val,"); Serial.print(NewPeakingPattern._CurrentPatternTargetPositionCounter);	Serial.print(", El,"); Serial.print(NewSkyTarget[El]);	Serial.print(", Az,"); Serial.print(NewSkyTarget[Az]);	Serial.print(", Centre El"); Serial.print(newCentreCalculated[El]);	Serial.print(", Centre Az"); Serial.print(newCentreCalculated[Az]);	Serial.println("");	}		
	
	// Get newly calculated Centre Sky Position once peaking pattern has run a complete pattern
	NewPeakingPattern.GetCentreSkyPosition(newCentreCalculated);
	
	// Check peaking pattern complete
	assertEqual(NewPeakingPattern._CurrentPatternTargetPositionCounter, A1);
	// Check no errors with reading signal
	assertFalse(NewPeakingPattern._isSignalLost);

	
}

#endif