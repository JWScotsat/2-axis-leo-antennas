#if isUnitTesting

// **************************************************************************************
// **************************************************************************************
// 
// Simple First Run Test
//  - Expected same results as inputs due to no Compass
test(AC_FindYaw_Test1_ReturnAzNoCorrection)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {-30.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0}; 
	float CompassYaw = 0.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;
	

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(CurrentAxisPos[Az], resultingYaw,2));

	
}
// 
// 
// **************************************************************************************
// **************************************************************************************
// 
// Simple First Run Test
//  - Expected same results Compass due to no Az Encoder value
test(AC_FindYaw_Test2_AzAtZeroCompassVal)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {0.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0}; 
	float CompassYaw = 30.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(30.0, resultingYaw,2));

	
}
// 
// 
// **************************************************************************************
// **************************************************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test3_AzAndCompassVal)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {-30.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0}; 
	float CompassYaw = 30.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;
	
	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(0.0, resultingYaw,2));

	
}
// 
// **************************************************************************************
// **************************************************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test3_AzAndCompassValReversed)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {30.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0}; 
	float CompassYaw = 270.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;
	
	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(-60.0, resultingYaw,2));

	
}
// 
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test4_ExtremeAz)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {350.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = 20.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(10.0, resultingYaw,2));

	
}
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test4_ExtremeAzNegative)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {-340.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = 20.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(40.0, resultingYaw,2));

	
}
// 
// ***************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test5_ShortestDistance)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {-160.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = -160.0;
	float resultingYaw;
	
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;
	
	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(40.0, resultingYaw,2));

	
}
// 
// ***************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test5_ShortestDistanceReversed)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {160.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = 160.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;
	

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(-40.0, resultingYaw,2));

	
}
// 
// ***************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test5_ShortestDistanceCompassCheck)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {160.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = 200.0;
	float resultingYaw;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;
	

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw,0.0);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		StandardFunctions.Print(CurrentAxisPos, 3);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(0.0, resultingYaw,2));

	
}
// 
// 
// ***************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test6_0_ManualOffset)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {0.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = 0.0;
	float resultingYaw;
	float mountingOffset = -10.0;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw, mountingOffset);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
			Serial.print(CurrentAxisPos[Az]);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
				Serial.print("\t MountingOffset;");
				Serial.print(mountingOffset);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(-10.0, resultingYaw,2));

	
}
// 
// 
// 
// ***************************************************
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test6_1_ManualOffsetAz)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {20.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = 0.0;
	float resultingYaw;
	float mountingOffset = -10.0;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw, mountingOffset);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		Serial.print(CurrentAxisPos[Az]);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t MountingOffset;");
		Serial.print(mountingOffset);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(10.0, resultingYaw,2));

	
}
// 
// ***************************************************
// 
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test6_2_ManualOffsetAz)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {20.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = -10.0;
	float resultingYaw;
	float mountingOffset = -10.0;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw, mountingOffset);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		Serial.print(CurrentAxisPos[Az]);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t MountingOffset;");
		Serial.print(mountingOffset);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(0.0, resultingYaw,2));

	
}
// 
// 
// ***************************************************
// 
// 
// Simple First Run Test
//  - Expected combination result
test(AC_FindYaw_Test6_3_ManualOffsetAzExtreme)
{
	const boolean DebugPrint = true;
	float CurrentAxisPos[] = {20.0, 20.0, 164.0}; //Az Encoder,  El Encoder
	float IMUYawPitchRoll[] = {0.0,0.0,0.0};
	float CompassYaw = -10.0;
	float resultingYaw;
	float mountingOffset = -220.0;
	
	AntennaCalculations_TargetAdjustmentClass AxisControl;

	resultingYaw = AxisControl.CalculateYaw(CurrentAxisPos[Az],CompassYaw, mountingOffset);
	
	if(DebugPrint)
	{
		
		Serial.print(" Current Pos;");
		Serial.print(CurrentAxisPos[Az]);
		Serial.print("\t Compass Val;");
		Serial.print(CompassYaw);
		Serial.print("\t MountingOffset;");
		Serial.print(mountingOffset);
		Serial.print("\t Returned Yaw;");
		Serial.print(resultingYaw);
		Serial.println("");
	}
	

	assertTrue(StandardFunctions.CompareFloats(150.0, resultingYaw,2));

	
}

#endif