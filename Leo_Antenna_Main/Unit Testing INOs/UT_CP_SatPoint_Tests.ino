#if isUnitTesting


#line 1 "UT_CP_SatPoint_Tests.ino"
//**************************************************************************************
//**************************************************************************************
#define MaxPhysical_Elevation 85.0
#define MinPhysical_Elevation -10.0
#define MaxPhysical_Azimuth 360.01
#define MinPhysical_Azimuth -0.01



//StandardFunctions_Array StandardFunctions;

//Simple Get Set Test
//   Ensure we have functions for reading and setting GPS - first float point
//    Functions created - SetGPS, returnGPS
test(CP_SatPoint_Test1_returnGPS)
{
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);
	
	float ReturnedGPS[2];
	
	ControlPattern_SatPoint.returnGPS(ReturnedGPS);
	
	if(DebugPrint)
	{
		Serial.print("Office Lat Lng;"); Serial.print(OfficeGPSLat, 6); Serial.print("\t"); Serial.print(OfficeGPSLng, 6);
		
		Serial.print("\nReturned Lat Lng;"); Serial.print(ReturnedGPS[Lat], 6); Serial.print("\t"); Serial.print(ReturnedGPS[Lng], 6);
		Serial.println("");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedGPS[Lat], OfficeGPSLat, 5));
	assertTrue(StandardFunctions.CompareFloats(ReturnedGPS[Lng], OfficeGPSLng, 5));

}


//************************************************************************************************
//************************************************************************************************

//Calculate Az
//  First Calculation Function
//	Find Az for given Sat and GPS - Tests Function Calculate Sat Point
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test2_CalculateOfficeAz)
{
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(IS907Lng,VerticalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 208.60, 2));

}


//************************************************************************************************
//************************************************************************************************

//Calculate Az
//  First Calculation Function
//	Find El for given Sat and GPS - Tests Function Calculate Sat Point
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test3_CalculateOfficeEl)
{
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(IS907Lng, VerticalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 22.7, 1));

}

//************************************************************************************************
//************************************************************************************************

//Calculate Pol
//  First Calculation Function
//	Find Pol for given Sat and GPS - Tests Function Calculate Sat Point
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test3_CalculateOfficePol)
{
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(IS907Lng, VerticalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180+15.5, 0));

}

//************************************************************************************************
//************************************************************************************************

//Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test4_CalculateIS901Complete)
{
	const float IS901Lng = 360-18;
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(IS901Lng, HorizontalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 197.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 24.9, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90+9.8, 0));

}

//************************************************************************************************
//************************************************************************************************


//Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test5_0_CalculateGPSSouthernHemisphComplete)
{
	// Random Location in the Indian Ocean
	const float IndianOceanLat = -13.706792;
	const float IndianOceanLng = 102.341467;
	
	const float SES7 = 108.2; // SES-7 Sat 108.2E
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(IndianOceanLat,IndianOceanLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(SES7, HorizontalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 23.4, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 72.5, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90+22.7, 0));
	// Test resulted in negative Pol - issue fixed by adding 180 when Pol result is less than 0
}

//************************************************************************************************
//************************************************************************************************


//Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test5_1_CalculateGPSEasternComplete)
{
	// Random Location in the Chinese Ocean
	const float SouthernChinaLat = 20.217499;
	const float SouthernChinaLng = 114.295131;
	
	const float SES7 = 108.2; // SES-7 Sat 108.2E
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(SouthernChinaLat,SouthernChinaLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(SES7, VerticalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 197.2, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 65.3, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180+16.1, 0));
	
}



//************************************************************************************************
//************************************************************************************************


//Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test5_2_CalculateGPSWesternComplete)
{
	// Random Location in the Alaska Ocean
	const float AlaskaCoastLat = 50.370870;
	const float AlaskaCoastLng = -131.506397;
	
	const float TKSAT1 = 360-87.3; // TKSAT-1 Sat 87.3W
	const boolean DebugPrint = false;
	
	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(AlaskaCoastLat,AlaskaCoastLng);
	
	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(TKSAT1, VerticalPol, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 128.3, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 19.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180-30, 0));
	
}


//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
// test(CP_SatPoint_Test5_3_CalculateGPSWesternComplete)
// {
// 	// Random Location in the Alaska Ocean
// 	const float AlaskaCoastLat = 50.370870;
// 	const float AlaskaCoastLng = -131.506397;
//
// 	const float TKSAT1 = 87.3; // TKSAT-1 Sat 87.3W
// 	const boolean DebugPrint = true;
//
// 	ControlPattern_SatPointClass ControlPattern_SatPoint;
////	ControlPattern_SatPoint.setGPS(AlaskaCoastLat,AlaskaCoastLng);
//
// 	float ReturnedPos[3];
//
// 	ControlPattern_SatPoint.CalculateSatPoint(TKSAT1, VerticalPol, ReturnedPos);
//
// 	if(DebugPrint)
// 	{
// 		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
// 	}
//
// 	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 128.3, 1));
// 	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 19.0, 1));
// 	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180-30, 0));
//
// 	// Test Proves that Satellite LNG MUST be given in terms of East. We could create a market E/W to solve for.
// }


//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test5_4_CalculateGPSSouthWrnComplete)
{
	// Random Location in the Alaska Ocean
	const float SouthWestLat = -35.980945;
	const float SouthWestLng = -49.020159;

	const float TKSAT1 = 360-87.3; // TKSAT-1 Sat 87.3W
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	ControlPattern_SatPoint.setGPS(SouthWestLat,SouthWestLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSAT1, HorizontalPol, ReturnedPos);

	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 306.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 32.1, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90-40.5, 0));
}



//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test6_0_SatDetailsStructTest)
{
	// Random Location in the Alaska Ocean
	const float SouthWestLat = -35.980945;
	const float SouthWestLng = -49.020159;

	const float TKSAT1 = 360-87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(SouthWestLat,SouthWestLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 306.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 32.1, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90-40.5, 0));
}




//************************************************************************************************



// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test6_1_NegativeSatLng)
{
	// Random Location in the Alaska Ocean
	const float SouthWestLat = -35.980945;
	const float SouthWestLng = -49.020159;

	const float TKSAT1 = -87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(SouthWestLat,SouthWestLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 306.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 32.1, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90-40.5, 0));
}

//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test6_2_PositiveGPSLng)
{
	// Random Location in the Alaska Ocean
	const float SouthWestLat = -35.980945;
	const float SouthWestLng = 360.0-49.020159;

	const float TKSAT1 = -87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(SouthWestLat,SouthWestLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 306.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 32.1, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90-40.5, 0));
}


//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test6_3_EastGPS)
{
	// Random Location
	const float GPSLat = -35.980945;
	const float GPSLng = 49.020159;

	const float TKSAT1 = -87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 58.4, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], -10.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 46.61, 0));
}

//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test6_4_ReversedEastGPS)
{

	const float GPSLat = -35.980945;
	const float GPSLng = -360+49.020159;

	const float TKSAT1 = -87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 58.4, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], -10.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 46.61, 0));
}


//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test7_SatCustomSkew)
{
	// Random Location in the Alaska Ocean
	const float SouthWestLat = -35.980945;
	const float SouthWestLng = -49.020159;

	const float TKSAT1 = 360-87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 12.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(SouthWestLat,SouthWestLng);

	float ReturnedPos[3];

	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 306.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 32.1, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90-40.5+TKSATSkew, 0));
}
//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test8_0_MaxElBoundary)
{
	// Random Location in the Alaska Ocean
	const float SouthWestLat = -1.081989;
	const float SouthWestLng = -87.821570;

	const float TKSAT1 = 360-87.3; // TKSAT-1 Sat 87.3W
	const float TKSATSkew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails TKSATTest = {TKSAT1, HorizontalPol, TKSATSkew};
	ControlPattern_SatPoint.setGPS(SouthWestLat,SouthWestLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(TKSATTest, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
			Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 25.7, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], MaxPhysical_Elevation, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90+25.7+TKSATSkew, 0));
}

//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test8_1_MinElBoundary)
{


	const float MEXSAT3 = 360-114.8; // MEXSAT3 at 114.8W
	const float MEXSAT3Skew = 0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails MEXSAT3SatDet = {MEXSAT3, HorizontalPol, MEXSAT3Skew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(MEXSAT3SatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
			Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 108.2, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], MinPhysical_Elevation, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 90+32.1+MEXSAT3Skew, 0));
}



//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test9_0_PolBoundary)
{


	const float MEXSAT3 = 360-114.8; // MEXSAT3 at 114.8W
	const float MEXSAT3Skew =180.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails MEXSAT3SatDet = {MEXSAT3, VerticalPol, MEXSAT3Skew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(MEXSAT3SatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
			Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 108.2, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], MinPhysical_Elevation, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180+MEXSAT3Skew+32.1 - MaxPhysical_Pol, 0));
}

//************************************************************************************************
//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test9_1_PolBoundary)
{
	//Extreme Pol Out of Bounds

	const float MEXSAT3 = 360-114.8; // MEXSAT3 at 114.8W
	const float MEXSAT3Skew =350.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails MEXSAT3SatDet = {MEXSAT3, VerticalPol, MEXSAT3Skew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(MEXSAT3SatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
			Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 108.2, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], MinPhysical_Elevation, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 201.88, 0));
}

//************************************************************************************************
//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test9_2_PolBoundary)
{
	//Extreme Pol Out of Bounds Negative

	const float MEXSAT3 = 360-114.8; // MEXSAT3 at 114.8W
	const float MEXSAT3Skew =-350.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails MEXSAT3SatDet = {MEXSAT3, VerticalPol, MEXSAT3Skew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(MEXSAT3SatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}

	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 108.2, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], MinPhysical_Elevation, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 41.88, 0));
}

//************************************************************************************************
//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_0_FailedSatData)
{
	 	//Data out of bounds. Failed Sat Data

	const float FailedSat = 410.0; 
	const float FailedSatSkew =0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails FailedSatDet = {FailedSat, VerticalPol, FailedSatSkew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(FailedSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}

//************************************************************************************************


// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_1_FailedSatDataNegSat)
{
	//Data out of bounds. Failed Sat Data

	const float FailedSat = -410.0;
	const float FailedSatSkew =0.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails FailedSatDet = {FailedSat, VerticalPol, FailedSatSkew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(FailedSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}

//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_2_FailedSatDataSkew)
{
	//Data out of bounds. Failed Sat Data

	const float FailedSat = 10.0;
	const float FailedSatSkew =390.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails FailedSatDet = {FailedSat, VerticalPol, FailedSatSkew};
	ControlPattern_SatPoint.setGPS(OfficeGPSLat,OfficeGPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(FailedSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}

//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_3_FailedGPSLng)
{
	//Data out of bounds. Failed Sat Data

	const float RandomSat = 10.0;
	const float RandomSatSkew =90.0;
	const float GPSLng = 380.0;
	const float GPSLat = 10.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails RandomSatDet = {RandomSat, VerticalPol, RandomSatSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(RandomSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}

//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_4_FailedGPSLngNeg)
{
	//Data out of bounds. Failed Sat Data

	const float RandomSat = 10.0;
	const float RandomSatSkew =90.0;
	const float GPSLng = -380.0;
	const float GPSLat = 10.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails RandomSatDet = {RandomSat, VerticalPol, RandomSatSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(RandomSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}

//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_5_FailedGPSLat)
{
	//Data out of bounds. Failed Sat Data

	const float RandomSat = 10.0;
	const float RandomSatSkew =90.0;
	const float GPSLng = 80.0;
	const float GPSLat = 310.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails RandomSatDet = {RandomSat, VerticalPol, RandomSatSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(RandomSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}


//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test10_6_FailedGPSLatNeg)
{
	//Data out of bounds. Failed Sat Data

	const float RandomSat = 10.0;
	const float RandomSatSkew =90.0;
	const float GPSLng = 80.0;
	const float GPSLat = -310.0;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails RandomSatDet = {RandomSat, VerticalPol, RandomSatSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(RandomSatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue((ControlPattern_SatPoint._isError > 0));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 0.0, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180.0, 1));
}


//************************************************************************************************
//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test11_0_GPSCrossOverPoints)
{
	//Data out of bounds. Failed Sat Data

	const float BorderPacificLat = 3.057886;
	const float BorderPacificLng = 179.998323;
	const float AMC7Lng = -137; // AMC-7 137W
	
	const float SatLng = AMC7Lng;
	const float SatSkew =0.0;
	const float GPSLng = BorderPacificLng;
	const float GPSLat = BorderPacificLat;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails SatDet = {SatLng, VerticalPol, SatSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(SatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 93.3, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 40.3, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180-85.5, 1));
}

//************************************************************************************************

// Calculate Az El Pol
// Results checked through http://www.dishpointer.com/
test(CP_SatPoint_Test11_1_GPSCrossOverPointsNeg)
{
	//Data out of bounds. Failed Sat Data

	const float BorderPacificLat = 3.057886;
	const float BorderPacificLng = -179.7926;
	const float AMC7Lng = -137; // AMC-7 137W
	
	const float SatLng = AMC7Lng;
	const float SatSkew =0.0;
	const float GPSLng = BorderPacificLng;
	const float GPSLat = BorderPacificLat;
	
	const boolean DebugPrint = false;

	ControlPattern_SatPointClass ControlPattern_SatPoint;
	
	SatelliteDetails SatDet = {SatLng, VerticalPol, SatSkew};
	ControlPattern_SatPoint.setGPS(GPSLat,GPSLng);

	float ReturnedPos[3];
	
	ControlPattern_SatPoint.CalculateSatPoint(SatDet, ReturnedPos);
	
	if(DebugPrint)
	{
		if(ControlPattern_SatPoint._isError != 0)
		Serial.print(ControlPattern_SatPoint._StatusMessage);
		
		Serial.print("+,Returned Az El Pol;"); StandardFunctions.Print(ReturnedPos); Serial.print("\n");
	}
	
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Az], 93.3, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[El], 40.5, 1));
	assertTrue(StandardFunctions.CompareFloats(ReturnedPos[Pol], 180-85.5, 1));
}

#endif