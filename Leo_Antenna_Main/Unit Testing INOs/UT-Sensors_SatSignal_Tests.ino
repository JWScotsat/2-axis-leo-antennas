#if isUnitTesting

#ifndef ModemSigStrPin
#define ModemSigStrPin A13
#endif

#line 5 "UT_Sensors_SatSignal_Tests.ino"
// //**************************************************************************************
// //**************************************************************************************
//StandardFunctions_Array StndFncts;
//
//Simple First Run Test
// Ensure Signal Pin is correctly defined
test(Sensors_SatSignalTests_Test1_Setup)
{
	Sensors_SatSignalClass NewSatSignal;
	
	NewSatSignal.setup(ModemSigStrPin,1500,3);
	
	assertTrue (NewSatSignal._AnalogueReadPin == ModemSigStrPin);
	
}


//Simple First Run Test
// Ensure spoof return signal
test(Sensors_SatSignalTests_Test2_SignalReadSpoof)
{
	Sensors_SatSignalClass NewSatSignal;
	
	int spoofValue = 1500;
	
	NewSatSignal.setup(ModemSigStrPin,1500,3);
	NewSatSignal._spoofSignalVal = spoofValue;
	NewSatSignal.isSignalDebug = true;
	
	int newSignalVal = NewSatSignal.read();
	
	assertEqual(newSignalVal, spoofValue);
	
}



//Simple First Run Test
// Check spoof signal is lock
test(Sensors_SatSignalTests_Test3_SignalisLock)
{
	Sensors_SatSignalClass NewSatSignal;
	
	int spoofValue = 1500;
	
	NewSatSignal.setup(ModemSigStrPin,400,3);
	
	NewSatSignal._spoofSignalVal = spoofValue;
	NewSatSignal.isSignalDebug = true;
	
	int newSignalVal = NewSatSignal.read();
	
	boolean isLock = NewSatSignal._isLock;
	
	assertTrue(isLock);
	
}

//Simple First Run Test
// Check spoof signal is lock
test(Sensors_SatSignalTests_Test3_SignalisNOTLock)
{
	Sensors_SatSignalClass NewSatSignal;
	
	int spoofValue = 1500;
	
	NewSatSignal.setup(ModemSigStrPin,1800,3);
	
	NewSatSignal._spoofSignalVal = spoofValue;
	NewSatSignal.isSignalDebug = true;
	
	int newSignalVal = NewSatSignal.read();
	
	boolean isLock = NewSatSignal._isLock;
	
	assertFalse(isLock);
	
}


#endif