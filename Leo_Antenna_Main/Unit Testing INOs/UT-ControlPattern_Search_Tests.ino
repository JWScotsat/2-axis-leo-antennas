#if isUnitTesting

#line 1 "UT-SimpleTests.ino"
#define Az 0
#define El 1
#define Pol 2
#define Cr 3

ControlPattern_SearchClass NewSearchPattern;

//Simple Addition Test
// Tests the New Targets are size point additions of the Starting Position
class customTest_SimpleAdditionTest : public TestOnce
{
	public:
	// constructor must name test

	customTest_SimpleAdditionTest(const char *name)
	: TestOnce(name)
	{
		// lightweight constructor, since
		// this test might be skipped.
		// you can adjust verbosity here

		verbosity = (TEST_VERBOSITY_TESTS_ALL|TEST_VERBOSITY_ASSERTIONS_FAILED);
	}


	void setup()
	{
	}

	void once()
	{
		
		// Setup Function must be called by every test to ensure initial conditions of the class
		NewSearchPattern.setup();
		
		float SatTarget[3] = {0.0,0.0,0};
		float CurrentPos[3] = {0.0,0.0,0};
		float NewPos[3] = {0.0,0.0,0};
		
		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize;


		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		assertTrue((NewPos[Az] == SatTarget[Az] + pointSize));
	}
	

};

customTest_SimpleAdditionTest Test1("Test1: Simple Starting Point Addition");

//**************************************************************************************
//**************************************************************************************


//Simple Addition Test
// Tests the New Targets are size point additions of the Current Position
test(CP_Search_Test2_CurrentPosAzAddition)
{
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize;
	SatTarget[Az] = 10.0;
	
	CurrentPos[Az] = 40.0;		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	assertTrue((NewPos[Az] == CurrentPos[Az] + pointSize));
}

//**************************************************************************************
//**************************************************************************************

//Simple Pattern Boundary Test
// Tests the New Targets are within the correct boundary size of sweeping search
test(CP_Search_Test3_CurrentPosMaxAz)
{
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize;	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary;
	
	SatTarget[Az] = 10.0;
	
	CurrentPos[Az] = SatTarget[Az] + maxSweep;		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	assertTrue((NewPos[Az] == SatTarget[Az] + maxSweep));
}

//**************************************************************************************
//**************************************************************************************

//Simple Addition Test
// Tests the New Targets Elevation to ensure it's changing correctly at an Azimuth Corner of the spiral
test(CP_Search_Test4_ElMoveAtMaxAz)
{
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize;	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary;
	
	SatTarget[Az] = 10.0;
	
	CurrentPos[Az] = SatTarget[Az] + maxSweep;		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	assertTrue((NewPos[El] == SatTarget[El] + pointSize));
}

//**************************************************************************************
//**************************************************************************************

//Simple Logic Addition Test
// Tests the New Target Elevation doesn't change when the Azimuth position isn't in a corner of the spiral
test(CP_Search_Test5_ElStoppedForAz)
{
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize;	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary;
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	SatTarget[Az] = 10.0;
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	assertTrue((NewPos[El] == CurrentPos[El]));
	assertTrue((NewPos[Az] != CurrentPos[Az]));
}

//**************************************************************************************
//**************************************************************************************

//Simple Revrse Direction Test
// Tests the New Target Azimuth reduces when elevation is at the right hand corner
test(CP_Search_Test6_AzReversedOnElStop)
{
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt
		CurrentPos[Az] = SatTarget[Az] + maxSweep;	CurrentPos[El] = SatTarget[El] + NewSearchPattern.SearchElevation._maxCounterBoundary;// 10.0		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	// 	Serial.print("New Pos;");
	// 	Array_Print(NewPos);
	// 	Serial.print("\t Current Pos;");
	// 	Array_Print(CurrentPos);
	// 	Serial.println("");
	
	assertTrue((NewPos[El] == CurrentPos[El]));
	assertTrue((NewPos[Az] == CurrentPos[Az] - pointSize));

}

//**************************************************************************************
//**************************************************************************************

//Half Pattern Test
// Tests the New Target moves in an expected pattern by updating the current position with the most recent New Position
test(CP_Search_Test7_HalfPatternTest)
{
	const boolean debugPrint = false;
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt
		//CurrentPos[Az] = SatTarget[Az] + maxSweep;	//CurrentPos[El] = SatTarget[El] + NewSearchPattern.SearchElevation._maxCounterBoundary;// 10.0		for(int i = 0; i<8; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	assertTrue((NewPos[El] > SatTarget[El]));
	assertTrue((NewPos[Az] >= SatTarget[Az]));

}

//**************************************************************************************
//**************************************************************************************

//Simple Reverse Elevation Direction Test
// Tests the New Target Elevation reduces when Current Position is at the right hand corner
test(CP_Search_Test8_ReverseElevation)
{
	const boolean debugPrint = false;
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	for(int i = 0; i<16; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	

	assertTrue((NewPos[El] == SatTarget[El] - pointSize));

}
//**************************************************************************************
//**************************************************************************************

//Simple Reverse Elevation, Forward Azimuth Direction Test
// Tests the New Target Elevation reduces when Current Position is at the left hand corner and the azimuth
// moves in the forward direction
test(CP_Search_Test9_RevElForwardAzComp)
{
	const boolean debugPrint = false;
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};	float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	for(int i = 0; i<25; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewPos[El] == SatTarget[El] - pointSize && (NewPos[Az] > SatTarget[Az]))
		break;
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	assertTrue((NewPos[El] == SatTarget[El] - pointSize));
	assertTrue((NewPos[Az] >= SatTarget[Az]));

}

//**************************************************************************************
//**************************************************************************************

//Simple Expanding Pattern Loop Test
// Tests the Expanding Elevation Pattern. Final Position should be Az0, El15
test(CP_Search_Test11_ExpandingElBoundary)
{
	const boolean debugPrint = false;
	int n = 32;
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		//Main Test Criteria - Set the size of the elevation increasing	float ExpandingElFactor = 5.0;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	// Run the pattern for n iterations
	for(int i = 0; i<n; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
		
		if(NewPos[El] == SatTarget[El] + pointSize + ExpandingElFactor)
		break;
	}
	
	assertTrue((NewPos[El] == SatTarget[El] + pointSize + ExpandingElFactor));
	//assertTrue((NewPos[Az] == SatTarget[Az]));

}

//**************************************************************************************
//**************************************************************************************

//Further Expanding Pattern Loop Test
// Further Tests the Expanding Elevation Pattern. Final Position should be Az40, El-40
test(CP_Search_Test12_ExpandingElBoundary)
{
	const boolean debugPrint = false;
	int n = 104; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		//Main Test Criteria - Set the size of the elevation increasing	float ExpandingElFactor = 10.0;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	// Run the pattern for n iterations
	for(int i = 0; i<n; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewPos[El] >= 40.0 && NewPos[Az] >= 40.0 )
		break;
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	assertTrue((NewPos[El] == SatTarget[El] + pointSize + (ExpandingElFactor*3)));
	assertTrue((NewPos[Az] == SatTarget[Az] + maxSweep));

}

//**************************************************************************************
//**************************************************************************************

//Further Expanding Pattern Loop Test
// Further Tests the Expanding Elevation Pattern. Final Position should be Az40, El40
// Tests the Pattern has a final maximum boundary that it won't go outside of for large n iterations
test(CP_Search_Test13_ExpandingElMaxBoundary)
{
	const boolean debugPrint = false;
	int n = 304; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		//Main Test Criteria - Set the size of the elevation increasing	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern._AbsMaxPatternElevationBoundary = 40.0;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	// Run the pattern for n iterations
	for(int i = 0; i<n; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
		
		assertTrue((abs(NewPos[El]) <= SatTarget[El] + AbsMaxEl));
	}
	
	
	//assertTrue((NewPos[Az] <= SatTarget[Az] - maxSweep));

}

//**************************************************************************************
//**************************************************************************************

//Pattern Test - Sweeping Complete Az On Start
// Further Tests the pattern to ensure that the Search Object can be reconfigured
// at the start to produce a sweep in azimuth before beginning the spiral search pattern
test(CP_Search_Test14_SweepingStart)
{
	const boolean debugPrint = false;
	int n = 14; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		//Main Test Criteria - Set the size of the elevation increasing	float ExpandingElFactor = 0.0;	float AbsMaxEl = 0.0;	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	// Run the pattern for n iterations
	for(int i = 0; i<n; i++)	{		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	assertTrue((abs(NewPos[El]) == 0.0));
	assertTrue((abs(NewPos[Az]) <= SatTarget[Az] + maxSweep));

}

//**************************************************************************************
//**************************************************************************************

//Pattern Test - Sweeping Complete Az On Start, Stopping at repeated position
// Further Tests the pattern to ensure that the Search Object can be reconfigured
// at the start to produce a sweep in azimuth before beginning the spiral search pattern
test(CP_Search_Test15_SweepingStartThenStop)
{
	const boolean debugPrint = false;
	int n = 304; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		//Main Test Criteria - Set the size of the elevation increasing	float ExpandingElFactor = 0.0;	float AbsMaxEl = 0.0;	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	// Run the pattern for n iterations
	for(int i = 0; i<n; i++)	{		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Search Flag Complete!");
			break;
		}				NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		

		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	assertTrue((abs(NewPos[El]) == 0.0));
	assertTrue((abs(NewPos[Az]) <= SatTarget[Az] + maxSweep));
	assertTrue(NewSearchPattern._isSearchComplete)

}


//**************************************************************************************
//**************************************************************************************

//Pattern Test - Sweeping Complete Az On Start, Stopping at repeated position & Moving to Correct Search
// Further Tests the pattern to ensure that the Search Object can be reconfigured
// at the start to produce a sweep in azimuth then beginning the spiral search pattern
test(CP_Search_Test16_SweepingCompleteSearch)
{
	const boolean debugPrint = false;
	int n = 304; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		//Main Test Criteria - Set the size of the elevation increasing	float ExpandingElFactor = 0.0;	float AbsMaxEl = 0.0;	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;		float pointSize = NewSearchPattern.SearchAzimuth._stepCountSize; //10.0	float maxSweep = NewSearchPattern.SearchAzimuth._maxCounterBoundary; //40.0 defualt

	// Run the pattern for n iterations of sweeping search
	for(int i = 0; i<n; i++)	{		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Sweep Search Flag Complete!");
			break;
		}				NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		

		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	// Test to ensure sweeping search has run correctly
	assertTrue((abs(NewPos[El]) == 0.0));
	assertTrue((abs(NewPos[Az]) <= SatTarget[Az] + maxSweep));
	assertTrue(NewSearchPattern._isSearchComplete)
	
	// Reconfigure and run spiral search from sweeping's final position
	ExpandingElFactor = 5.0;	AbsMaxEl = 40.0;	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 10.0;
	

	// Run the pattern for n iterations of spiral search
	for(int i = 0; i<n; i++)	{				NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);

		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		

		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Spiral Search Flag Complete!");
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	// Test to ensure sweeping search has run correctly
	assertTrue((abs(NewPos[El]) == 40.0));
	assertTrue((abs(NewPos[Az]) <= SatTarget[Az] + maxSweep));
	assertTrue(NewSearchPattern._isSearchComplete)
}



//**************************************************************************************
//**************************************************************************************

//Pattern Test - Complete Search Finishing On centre for restart
// Further Tests the pattern to ensure that the Search Object can be reconfigured
// at the start to sweep by constricting the starting elevation - without mid-reconfigure of NewSearch Object
test(CP_Search_Test19_CompleteSearchSetVariables)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {220.0,45.0,0};
	float CurrentPos[3] = {190.0,15.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 12.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 55.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search from sweeping's final position
	float ExpandingElFactor = 5.0;	float AbsMaxEl = 20.0;	NewSearchPattern.SearchElevation._stepCountSize = 2.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	
	int i = 0;
	// Run the pattern for n iterations of spiral search
	for(i = 0; i<n; i++)	{		// Main function Call for Next Pattern Position		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		// Break Out after Completed Searck
		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Spiral Search Flag Complete!");
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
		
		
	}
	
	assertTrue(i < n);
	
	// Final position to see restart of search after a Complete Search
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	// Debug Print
	if(debugPrint)
	{
		Serial.print("i:"); Serial.print("Final");
		Serial.print("\tNew Pos;");
		Array_Print(NewPos);
		Serial.print("\t Current Pos;");
		Array_Print(CurrentPos);
		Serial.println("");
	}
	
	// Test to ensure sweeping search has run correctly
	assertTrue((abs(NewPos[El]) == SatTarget[El]));
	assertTrue((abs(NewPos[Az]) == SatTarget[Az]));
	
}



//**************************************************************************************
//**************************************************************************************
#define MaxPhysical_Elevation 85.0
#define MinPhysical_Elevation -10.0
#define MaxPhysical_Azimuth 360.01
#define MinPhysical_Azimuth -0.01

//Pattern Test - Complete Search Finishing On centre for restart.
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360
test(CP_Search_Test20_MaxPhyBoundary)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {320.0,55.0,0};
	float CurrentPos[3] = {320.0,55.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search from sweeping's final position
	float ExpandingElFactor = 10.0;	float AbsMaxSearchEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxSearchEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	
	int i = 0;
	// Run the pattern for n iterations of spiral search
	for(i = 0; i<n; i++)	{		// Main function Call for Next Pattern Position		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
			
			
		}
		
		// Break Out after Completed Searck
		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Spiral Search Flag Complete!");
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
		
		// Test to ensure sweeping search has run correctly
		assertTrue(NewPos[El] <= MaxPhysical_Elevation);
		assertTrue(NewPos[Az] <= MaxPhysical_Azimuth);
		
		assertTrue(NewPos[El] >= MinPhysical_Elevation);
		assertTrue(NewPos[Az] >= MinPhysical_Azimuth);
		
		if(NewSearchPattern._isError && debugPrint)
		{
			Serial.println(NewSearchPattern._StatusMessage);
			NewSearchPattern._isError = 0;
		}
	}
	
	assertTrue(i < n);
	
	// Final position to see restart of search after a Complete Search
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	// Debug Print
	if(debugPrint)
	{
		Serial.print("i:"); Serial.print("Final");
		Serial.print("\tNew Pos;");
		Array_Print(NewPos);
		Serial.print("\t Current Pos;");
		Array_Print(CurrentPos);
		Serial.println("");
	}
	
	// Test to ensure spiral search has run correctly and finished at sat pointed
	assertTrue((abs(NewPos[El]) == SatTarget[El]));
	assertTrue((abs(NewPos[Az]) == SatTarget[Az]));
	
}


//**************************************************************************************
//**************************************************************************************


//Pattern Test - Complete Search Finishing On centre for restart.
// Further Tests the pattern to ensure that the allgorithim correctly handles current positions outside of the
// pattern
test(CP_Search_Test21_CurrentPosOutofBounds)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {340.0,10.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search from sweeping's final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	
	int i = 0;
	// Run the pattern for n iterations of spiral search
	for(i = 0; i<n; i++)	{		// Main function Call for Next Pattern Position		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		// Break Out after Completed Searck
		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Spiral Search Flag Complete!");
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
		
		// Test to ensure not moving outside of max boundary
		assertTrue(NewPos[El] < MaxPhysical_Elevation);
		assertTrue(abs(NewPos[Az]) < MaxPhysical_Azimuth);
	}
	
	assertTrue(i < n);
	
	// Final position to see restart of search after a Complete Search
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	// Debug Print
	if(debugPrint)
	{
		Serial.print("i:"); Serial.print("Final");
		Serial.print("\tNew Pos;");
		Array_Print(NewPos);
		Serial.print("\t Current Pos;");
		Array_Print(CurrentPos);
		Serial.println("");
	}
	
	// Test to ensure spiral search has run correctly and finished at sat pointed
	assertTrue(((NewPos[El]) == SatTarget[El]));
	assertTrue(((NewPos[Az]) == SatTarget[Az]));
	
}


//**************************************************************************************
//**************************************************************************************


//Pattern Test - Complete Search Finishing On centre for restart.
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test22_AzRollOver0to360Rev)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 1.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 1.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(i == 4)
		{
			assertTrue(NewPos[El] == 1.0);
			assertTrue(NewPos[Az] == 359.0);
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}
//**************************************************************************************
//**************************************************************************************

//Pattern Test - Complete Search Finishing On centre for restart.
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test22_AzRollOver359ElDownRev)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 1.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 1.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(i == 5)
		{
			assertTrue(NewPos[El] == 0.0);
			assertTrue(NewPos[Az] == 359.0);
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}

//**************************************************************************************
//**************************************************************************************

//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test23_AzRollOver359AzUpRev)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 1.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 1.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(i == 8)
		{
			assertTrue(NewPos[El] == -1.0);
			assertTrue(NewPos[Az] == 0.0);
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}


//**************************************************************************************
//**************************************************************************************

//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test24_AzRollOverCompleteRev)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,0.0,0};
	float CurrentPos[3] = {0.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 5.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 2.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		assertTrue(abs(CurrentPos[Az]-NewPos[Az]) <= 1.0 ||  abs(CurrentPos[Az]-NewPos[Az]) == 359.0)
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}



//**************************************************************************************
//**************************************************************************************

//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test25_AzRollOver359Forw)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {359.0,0.0,0};
	float CurrentPos[3] = {359.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 1.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 1.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(i == 0)
		{
			assertTrue(NewPos[El] == 0.0);
			assertTrue(NewPos[Az] == 0.0);
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}

//***********************************************************
//***********************************************************


//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test26_AzRollOverUpperBndsForw)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {359.0,0.0,0};
	float CurrentPos[3] = {359.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 1.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 1.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(i == 2)
		{
			assertTrue(NewPos[El] == 1.0);
			assertTrue(NewPos[Az] == 0.0);
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}


//***********************************************************
//***********************************************************


//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test27_AzRollOverRevFrwd)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {359.0,0.0,0};
	float CurrentPos[3] = {359.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 1.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 1.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(i == 3)
		{
			assertTrue(NewPos[El] == 1.0);
			assertTrue(NewPos[Az] == 359.0);
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}




//***********************************************************
//***********************************************************


//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test28_AzRollOverCompleteFrwd)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {359.0,0.0,0};
	float CurrentPos[3] = {359.0,0.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 1.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 5.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 1.0;	float AbsMaxEl = 2.0;	NewSearchPattern.SearchElevation._stepCountSize = 1.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 1.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}

		assertTrue(abs(CurrentPos[Az]-NewPos[Az]) <= 1.0 ||  abs(CurrentPos[Az]-NewPos[Az]) == 359.0)
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}

	
}



//***********************************************************
//***********************************************************

//Pattern Test - Az Roll Over. Handling 0-360 boundary situations
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test29_0_CompleteQuad)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {350.0,60.0,0};
	float CurrentPos[3] = {350.0,60.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			// 			Serial.print("i:"); Serial.print(i);
			// 			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			// 			Serial.print("\t Current Pos;");
			// 			Array_Print(CurrentPos);
			Serial.println("");
		}

		assertTrue(abs(CurrentPos[Az]-NewPos[Az]) <= pointSize ||  abs(CurrentPos[Az]-NewPos[Az]) == 350.0)
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	assertTrue(NewSearchPattern._isSearchComplete)
	
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	// Debug Print
	if(debugPrint)
	{
		Serial.print("i:"); Serial.print("Final");
		Serial.print("\tNew Pos;");
		Array_Print(NewPos);
		Serial.print("\t Current Pos;");
		Array_Print(CurrentPos);
		Serial.println("");
	}
	
	assertTrue(NewPos[El] == SatTarget[El]);
	assertTrue(NewPos[Az] == SatTarget[Az]);

	
}


//}


//***********************************************************
//***********************************************************

//Pattern Test - Ensure upper boundary doesn't cause an offset in the elevation pattern
// Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test30_UpperBoundaryHandle)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {350.0,60.0,0};
	float CurrentPos[3] = {350.0,60.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}

		if(CurrentPos[El] == MaxPhysical_Elevation && NewSearchPattern.SearchAzimuth._atBoundary)
		{
			assertTrue(NewPos[El] == 80);
		}
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	

	
}






//***********************************************************
//***********************************************************

//Pattern Test - Ensure upper Elevation boundary doesn't cause an offset in the elevation pattern
//	Checks to ensure the upper Elevation boundary holds when the stepSize coincides with the axismax
//Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test30_1_UpperBoundaryHandle)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {350.0,65.0,0};
	float CurrentPos[3] = {350.0,65.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}

		if(CurrentPos[El] == MaxPhysical_Elevation && CurrentPos[Az] == SatTarget[Az] - maxSweep)
		{
			assertTrue(NewPos[El] == 75.0);
		}
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	

	
}


//***********************************************************
//***********************************************************

//Pattern Test - Ensure lower Elevation boundary doesn't cause an offset in the elevation pattern
//Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85 -10, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test30_2_LowerBoundaryHandle)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {180.0,5.0,0};
	float CurrentPos[3] = {180.0,65.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}

		if(CurrentPos[El] == MinPhysical_Elevation && CurrentPos[Az] == SatTarget[Az] + maxSweep)
		{
			assertTrue(NewPos[El] == -5.0);
		}
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	

	
}

//***********************************************************
//***********************************************************

//Pattern Test - Ensure lower Elevation boundary doesn't cause an offset in the elevation pattern
//	Checks to ensure the lower Elevation boundary holds when the stepSize coincides with the axisMin
//Further Tests the pattern to ensure that the New Position never goes out of bounds of the physical antenna
// maximum boundaries - El Range 85 -10, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test30_3_LowerBoundaryHandle)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {180.0,0.0,0};
	float CurrentPos[3] = {180.0,65.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}

		if(CurrentPos[El] == MinPhysical_Elevation && CurrentPos[Az] == SatTarget[Az] + maxSweep)
		{
			assertTrue(NewPos[El] == 0.0);
		}
		
		if(NewSearchPattern._isSearchComplete)
		break;
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	

	
}

//***********************************************************
//***********************************************************

//Pattern Test - Ensure hitting the maximum boundary doesn't cause the search to end early
// maximum boundaries - El Range 85 -10, Az 360 & handles the cross over point of 0 and 360
test(CP_Search_Test31_UpperBoundaryHandleSearchComplete)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {180.0,75.0,0};
	float CurrentPos[3] = {180.0,75.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	boolean CompletedSearch = false;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewSearchPattern._isSearchComplete)
		{
			assertTrue(NewSearchPattern.SearchElevation._maxCounterBoundary == AbsMaxEl);
			CompletedSearch = true;
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	if(CompletedSearch)
	{
		assertTrue(NewPos[El] == SatTarget[El]);
		assertTrue(NewPos[Az] == SatTarget[Az]);
		
		
	}

	assertTrue(CompletedSearch);
	
}



//***********************************************************
//***********************************************************

//Pattern Test - Check handling of current position out of sync with Target and New
test(CP_Search_Test32_0_CurrentPosOutofSync)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {0.0,25.0,0};
	float CurrentPos[3] = {180.0,75.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 default
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	boolean CompletedSearch = false;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewSearchPattern._isSearchComplete)
		{
			assertTrue(NewSearchPattern.SearchElevation._maxCounterBoundary == AbsMaxEl);
			CompletedSearch = true;
			break;
		}
		
		if(i == 0)
		{
			assertTrue(NewPos[El] == SatTarget[El]);
			assertTrue(NewPos[Az] == SatTarget[Az]);
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	if(CompletedSearch)
	{
		assertTrue(NewPos[El] == SatTarget[El]);
		assertTrue(NewPos[Az] == SatTarget[Az]);
		
		
	}

	assertTrue(CompletedSearch);
	
}


//***********************************************************
//***********************************************************

//Pattern Test - Check handling of current position out of sync with Target and New
test(CP_Search_Test32_1_CurrentPosOutofSync)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {180.0,75.0,0};
	float CurrentPos[3] = {0.0,25.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 default
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	boolean CompletedSearch = false;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewSearchPattern._isSearchComplete)
		{
			assertTrue(NewSearchPattern.SearchElevation._maxCounterBoundary == AbsMaxEl);
			CompletedSearch = true;
			break;
		}
		
		if(i == 0)
		{
			assertTrue(NewPos[El] == SatTarget[El]);
			assertTrue(NewPos[Az] == SatTarget[Az]);
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	if(CompletedSearch)
	{
		assertTrue(NewPos[El] == SatTarget[El]);
		assertTrue(NewPos[Az] == SatTarget[Az]);
		
		
	}

	assertTrue(CompletedSearch);
	
}


//***********************************************************
//***********************************************************

//Pattern Test - Check handling of current position out of sync with Target and New
test(CP_Search_Test32_2_CurrentPosOutofSync)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {350.0,-5.0,0};
	float CurrentPos[3] = {60.0,65.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 default
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	boolean CompletedSearch = false;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewSearchPattern._isSearchComplete)
		{
			assertTrue(NewSearchPattern.SearchElevation._maxCounterBoundary == AbsMaxEl);
			CompletedSearch = true;
			break;
		}
		
		if(i == 0)
		{
			assertTrue(NewPos[El] == SatTarget[El]);
			assertTrue(NewPos[Az] == SatTarget[Az]);
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	if(CompletedSearch)
	{
		assertTrue(NewPos[El] == SatTarget[El]);
		assertTrue(NewPos[Az] == SatTarget[Az]);
		
		
	}

	assertTrue(CompletedSearch);
	
}

//***********************************************************
//***********************************************************

//Pattern Test - Check handling of current position out of sync with Target and New
test(CP_Search_Test33_0_TestAntennaManager)
{
	const boolean debugPrint = false;
	int n = 310; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setup();
	
	float SatTarget[3] = {350.0,55.0,0};
	float CurrentPos[3] = {60.0,35.0,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 10.0;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 40.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 default
	
	// Reconfigure and run spiral search final position
	float ExpandingElFactor = 10.0;	float AbsMaxEl = 40.0;	NewSearchPattern.SearchElevation._stepCountSize = 10.0; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	NewSearchPattern.SearchAzimuth._AxisBoundaryRollOver = true;
	
	boolean CompletedSearch = false;
	
	for (int i = 0; i < n; i++)
	{
		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			//			Serial.print("i:"); Serial.print(i);
			//			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			// 			Serial.print("\t Current Pos;");
			// 			Array_Print(CurrentPos);
			Serial.println("");
		}
		
		if(NewSearchPattern._isSearchComplete)
		{
			assertTrue(NewSearchPattern.SearchElevation._maxCounterBoundary == AbsMaxEl);
			CompletedSearch = true;
			break;
		}
		
		if(i == 0)
		{
			assertTrue(NewPos[El] == SatTarget[El]);
			assertTrue(NewPos[Az] == SatTarget[Az]);
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
	}
	
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	if(CompletedSearch)
	{
		assertTrue(NewPos[El] == SatTarget[El]);
		assertTrue(NewPos[Az] == SatTarget[Az]);
		
		
	}
	
	// Debug Print
	if(debugPrint)
	{
		//			Serial.print("i:"); Serial.print(i);
		//			Serial.print("\tNew Pos;");
		Array_Print(NewPos);
		// 			Serial.print("\t Current Pos;");
		// 			Array_Print(CurrentPos);
		Serial.println("");
	}

	assertTrue(CompletedSearch);
	
}



//**************************************************************************************
//**************************************************************************************

//Pattern Test -Testing isue with Elevation returning to "Centre" position instead of upper values
test(CP_Search_Test34_ElevationUpperError)
{
	const boolean debugPrint = false;
	int n = 9000; //104
	
	// Setup Function must be called by every test to ensure initial conditions of the class
	NewSearchPattern.setupDefaultAntennaSearch();
	
	float SatTarget[3] = {208.59,22.66,0};
	float CurrentPos[3] = {266.17,23.66,0};	float NewPos[3] = {0.0,0.0,0};		float pointSize = 0.75;	NewSearchPattern.SearchAzimuth._stepCountSize = pointSize; //10.0	float maxSweep = 10.0;
	NewSearchPattern.SearchAzimuth._maxCounterBoundary = maxSweep; //40.0 defualt
	
	// Reconfigure and run spiral search from sweeping's final position
	float ExpandingElFactor = 0.2;	float AbsMaxEl = 15.0;	NewSearchPattern.SearchElevation._stepCountSize = 0.75; //10.0	NewSearchPattern._AbsMaxPatternElevationBoundary = AbsMaxEl;	NewSearchPattern._ElExpandingBoundaryFactor = ExpandingElFactor;	NewSearchPattern.SearchElevation._maxCounterBoundary = 0.0;
	
	NewSearchPattern._PointDelay = 0;
	
	int i = 0;
	// Run the pattern for n iterations of spiral search
	for(i = 0; i<n; i++)	{		// Main function Call for Next Pattern Position		NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
		
		// Debug Print
		if(debugPrint)
		{
			Serial.print("i:"); Serial.print(i);
			Serial.print("\tNew Pos;");
			Array_Print(NewPos);
			Serial.print("\t Current Pos;");
			Array_Print(CurrentPos);
			Serial.println("");
			
			if(NewSearchPattern._isError)
			{
				Serial.print(NewSearchPattern._StatusMessage);
			}
		}
		
		// Break Out after Completed Searck
		if(NewSearchPattern._isSearchComplete)
		{
			if(debugPrint)
			Serial.println("Spiral Search Flag Complete!");
			break;
		}
		
		Array_SetEquals(NewPos,CurrentPos,3);
		
		assertLessOrEqual(NewSearchPattern._isError, 0);
		
		
	}
	
	assertTrue(i < n);
	
	// Final position to see restart of search after a Complete Search
	NewSearchPattern.NextPosition(SatTarget,CurrentPos,NewPos);
	
	// Debug Print
	if(debugPrint)
	{
		Serial.print("i:"); Serial.print("Final");
		Serial.print("\tNew Pos;");
		Array_Print(NewPos);
		Serial.print("\t Current Pos;");
		Array_Print(CurrentPos);
		Serial.println("");
	}
	
	// Test to ensure sweeping search has run correctly
	assertTrue((abs(NewPos[El]) == SatTarget[El]));
	assertTrue((abs(NewPos[Az]) == SatTarget[Az]));
	
}

//**************************************************************************************
//**************************************************************************************



void Array_Print(float InputArray[])
{
	
	Serial.print(" {");
		
		//for (int i = 0; i < sizeof(InputArray)/sizeof(float); i++)
		for (int i = 0; i < 3; i++)
		{
			Serial.print(InputArray[i]);
			Serial.print(",");
		}
		
	Serial.print("}");
	
}

void Array_SetEquals(float InputArray[], float *OutputArray, int ArraySize)
{
	for (int i = 0; i < ArraySize; i++)
	{
		OutputArray[i] = InputArray[i];
	}
	
	
}

void Array_Randomise(float *ArrayRandomised)
{
	for (int i = 0; i < 3; i++)
	{
		long randomUpper = random(0, 1000);
		long randomLower = random(0, 1000);
		float randomMapped = map(randomUpper, 0, 1000, -500, 500) +
		map(randomUpper, 0, 1000, -500, 500)/100.0;
		ArrayRandomised[i] = randomMapped;
	}
}


#endif
//**************************************************************************************//**************************************************************************************
//**************************************************************************************//**************************************************************************************