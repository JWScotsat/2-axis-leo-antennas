#if isUnitTesting

#line 1 "UT_AM_AMC_Tests.ino"
// //**************************************************************************************
// //**************************************************************************************
StandardFunctions_Array StndFncts;
//
//Simple First Run Test
// Ensure Antenna Mode is returned correctly
test(Z_AntennaManager_ModeControl_Test1_ReturnCurrentAntennaMode)
{
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModeTest, 0, CurrentSkyPos, NewSkyPos);
	

	assertEqual(AntennaMode,AntennaModeTest);
	
}

// //**************************************************************************************

//Simple First Run Test
// Ensure Antenna Mode is returned correctly
test(Z_AntennaManager_ModeControl_Test2_PointingClassTest)
{
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	

	assertEqual(AntennaMode,AntennaModePointing);
	
}


// //**************************************************************************************

//Simple First Run Test
// Ensure Antenna Mode is returned correctly
test(Z_AntennaManager_ModeControl_Test2_PointingClassTestNewSkyTarget)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.70;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	if(DebugPrint)
	{
		
		Serial.print(" CurrentSkyPos;");
		StndFncts.Print(CurrentSkyPos, 3);
		Serial.print("\t NewSkyPos;");
		StndFncts.Print(NewSkyPos,3);
		Serial.println("");
	}

	assertEqual(AntennaMode,AntennaModePointing);
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],Az907OfficePoint,1));

	assertTrue(StndFncts.CompareFloats(NewSkyPos[El],El907OfficePoint,1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Pol],Pol907OfficePoint,1));
}


// //**************************************************************************************

//Simple First Run Test
// Ensure Antenna Mode is returned correctly
test(Z_AntennaManager_ModeControl_Test2_PointingClassTestStartSearch)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.70;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	if(DebugPrint)
	{
		
		Serial.print(" CurrentSkyPos;");
		StndFncts.Print(CurrentSkyPos, 3);
		Serial.print("\t NewSkyPos;");
		StndFncts.Print(NewSkyPos,3);
		Serial.println("");
	}

	assertEqual(AntennaMode,AntennaModeSearching);

}



// //**************************************************************************************

//Simple First Run Test
// Ensure Antenna Mode is returned correctly
test(Z_AntennaManager_ModeControl_Test3_SearchClassNextPosTestAfterPointing)
{
	const boolean DebugPrint = true;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.70;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	NewAntennaManager._SearchPattern.setupDefaultAntennaSearch();
	
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);

	if(DebugPrint)
	{
		
		Serial.print(" CurrentSkyPos;");
		StndFncts.Print(CurrentSkyPos, 3);
		Serial.print("\t NewSkyPos;");
		StndFncts.Print(NewSkyPos,3);
		Serial.println("");
	}

	assertEqual(AntennaMode,AntennaModeSearching);
	
	assertTrue(StndFncts.CompareFloats(NewSkyPos[El],El907OfficePoint,1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],Az907OfficePoint +  NewAntennaManager._SearchPattern.SearchAzimuth._stepCountSize,1));
}




// //**************************************************************************************

//Complex Test
//  Point antenna based on sat details, run search, complete search and return to repoint the antenna
test(Z_AntennaManager_ModeControl_Test3_SearchClassSearchComplete)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	// Pointing Az El Pol for IS907 based on Hanover St Office GPS
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.66;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	NewAntennaManager._SearchPattern.setupDefaultAntennaSearch();
	NewAntennaManager._SearchPattern._PointDelay = 0;
	
	// Initial Point at Sat
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	// Loop for search. Search will update NewSkyPos for ~870 counts to complete search pattern.
	// Break within loop based on "_SearchComplete" flag
	for (int i = 0; i < 1600; i++)
	{
		StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
		
		AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);

		if(DebugPrint)
		{
			Serial.print("i;");
			Serial.print(i);
			Serial.print("Is Search Complete;");
			Serial.print(NewAntennaManager._SearchPattern._isSearchComplete);
			Serial.print(" CurrentSkyPos;");
			StndFncts.Print(CurrentSkyPos, 3);
			Serial.print("\t NewSkyPos;");
			StndFncts.Print(NewSkyPos,3);
			Serial.println("");
		}
		
		if(NewAntennaManager._SearchPattern._isSearchComplete)
		break;
		
	}
	
	// Completed Search Positions
	assertTrue(StndFncts.CompareFloats(NewSkyPos[El], El907OfficePoint + NewAntennaManager._SearchPattern.SearchElevation._maxCounterBoundary,1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],Az907OfficePoint +  NewAntennaManager._SearchPattern.SearchAzimuth._maxCounterBoundary,1));

	// After Search Complete the Antenna Mode should go to Sat Point and repoint at Satellite
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	
	assertTrue(NewAntennaManager._SearchPattern._isSearchComplete);
	
	assertEqual(AntennaMode,AntennaModePointing);
	
	assertTrue(StndFncts.CompareFloats(NewSkyPos[El],El907OfficePoint,1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],Az907OfficePoint,1));
	
}


// //**************************************************************************************

//Ensure peaking pattern runs and updates New Sky Pos
test(Z_AntennaManager_ModeControl_Test4_PeakingClassInitialTest)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	// Pointing Az El Pol for IS907 based on Hanover St Office GPS
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.66;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	NewAntennaManager._SearchPattern.setupDefaultAntennaSearch();
	
	// Initial Point at Sat
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	// Set Peaking Pattern for signal Lock point
	NewAntennaManager._PeakingPattern.SetCentreSkyPosition(NewSkyPos);
	
	AntennaMode = AntennaModePeaking;
	// Next call will be to peaking pattern, should result in postion "A"
	NewAntennaManager.AntennaModeControl(AntennaMode, 2100, CurrentSkyPos, NewSkyPos);

	if(DebugPrint)
	{
		Serial.print("\Pointed Sky Pos;");
		StndFncts.Print(CurrentSkyPos, 3);
		Serial.print("\t NewSkyPos;");
		StndFncts.Print(NewSkyPos,3);
		Serial.println("");
	}
	
	

	
	// Check Position A Details
	assertTrue(StndFncts.CompareFloats(NewSkyPos[El], El907OfficePoint + NewAntennaManager._PeakingPattern._ElStepSize,1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],Az907OfficePoint -  NewAntennaManager._PeakingPattern._AzStepSize,1));
	
	assertEqual(AntennaMode,AntennaModePeaking);

}


// //**************************************************************************************

//Ensure peaking pattern runs and updates New Sky Pos
// Run test to check pointing is reset if the peaking class loses signal
test(Z_AntennaManager_ModeControl_Test4_PeakingClassFailedSignal)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	// Pointing Az El Pol for IS907 based on Hanover St Office GPS
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.66;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	NewAntennaManager._isTesting = true;
	
	NewAntennaManager._SearchPattern.setupDefaultAntennaSearch();
	
	// Initial Point at Sat
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	// Set Peaking Pattern for signal Lock point
	NewAntennaManager._PeakingPattern.SetCentreSkyPosition(NewSkyPos);
	
	AntennaMode = AntennaModePeaking;

	
	// Next call will be to peaking pattern, should result in postion "A"
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 100, CurrentSkyPos, NewSkyPos);

	AntennaMode =	NewAntennaManager.AntennaModeControl(AntennaMode, 1100, CurrentSkyPos, NewSkyPos);

	AntennaMode =	NewAntennaManager.AntennaModeControl(AntennaMode, 1100, CurrentSkyPos, NewSkyPos);
	
	AntennaMode =	NewAntennaManager.AntennaModeControl(AntennaMode, 1100, CurrentSkyPos, NewSkyPos);
	
	
	if(DebugPrint)
	{
		Serial.print("\Pointed Sky Pos;");
		StndFncts.Print(CurrentSkyPos, 3);
		Serial.print("\t NewSkyPos;");
		StndFncts.Print(NewSkyPos,3);
		Serial.println("");
	}
	
	// Check correctly flagged signal lost
	assertTrue(NewAntennaManager._PeakingPattern._isSignalLost);
	
	// Check Position Signal Lost Details
	assertTrue(StndFncts.CompareFloats(NewSkyPos[El], El907OfficePoint,1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],Az907OfficePoint,1));
	
	assertEqual(AntennaMode,AntennaModePointing);

}


// //**************************************************************************************

//Ensure Manual Position correctly updates NewSkyPos despite any current movements or AntennaModes
test(Z_AntennaManager_ModeControl_Test5_ManualPosition)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	// Pointing Az El Pol for IS907 based on Hanover St Office GPS
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.66;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	NewAntennaManager._SearchPattern.setupDefaultAntennaSearch();
	
	// Initial Point at Sat
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	//Override Sat Point with Manual Position
	AntennaMode = AntennaModeManualPos;
	
	float NewManualPos[3] = {-10, 33, 25};
	StndFncts.SetEquals(NewManualPos,NewAntennaManager._StoreManualPosition, 3);
	
	NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	
	
	if(DebugPrint)
	{
		Serial.print(" Current Sky Pos;");
		StndFncts.Print(CurrentSkyPos, 3);
		Serial.print("\t NewSkyPos;");
		StndFncts.Print(NewSkyPos,3);
		Serial.println("");
	}
	
	// Check Manual Position Used
	assertTrue(StndFncts.CompareFloats(NewSkyPos[El], NewManualPos[El],1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],NewManualPos[Az],1));
	assertTrue(StndFncts.CompareFloats(NewSkyPos[Pol],NewManualPos[Pol],1));
	
	assertEqual(AntennaMode,AntennaModeManualPos);

}


// //**************************************************************************************

//Ensure Pausing the Antenna can continue after a pause
//  i.e. Pause during search then continue after a certain amount of iterations
//  During pause the Current Position and New Sky Position should stay the same
test(Z_AntennaManager_ModeControl_Test6_AntennaModePaused)
{
	const boolean DebugPrint = false;
	
	AntennaManagerClass NewAntennaManager;
	
	NewAntennaManager.setup();
	
	float CurrentSkyPos[3]; float NewSkyPos[3];
	
	// Pointing Az El Pol for IS907 based on Hanover St Office GPS
	float Az907OfficePoint = 208.6;
	float El907OfficePoint = 22.66;
	float Pol907OfficePoint = 195.4;
	
	NewAntennaManager._CurrentSat = {IS907Lng, VerticalPol, 0};
	NewAntennaManager._CurrentGPSLatLng[Lat] = OfficeGPSLat;
	NewAntennaManager._CurrentGPSLatLng[Lng] = OfficeGPSLng;
	
	NewAntennaManager._SearchPattern.setupDefaultAntennaSearch();
	
	NewAntennaManager._SearchPattern._PointDelay = 0;
	
	// Initial Point at Sat
	int AntennaMode = NewAntennaManager.AntennaModeControl(AntennaModePointing, 0, CurrentSkyPos, NewSkyPos);
	
	
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	// Antenna Goes Into Searching
	// Antenna is paused in the middle of searching and then returns to normal
	
	//Will start the antenna to searching. Next Sky Pos target will be away from starting point
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);
	StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
	
	for (int i = 0; i < 800; i++)
	{
		AntennaMode = NewAntennaManager.AntennaModeControl(AntennaMode, 0, CurrentSkyPos, NewSkyPos);


		if(DebugPrint)
		{
			Serial.print("+,i;");
			Serial.print(i);
			Serial.print(" Current Sky Pos;");
			StndFncts.Print(CurrentSkyPos, 3);
			Serial.print("\t NewSkyPos;");
			StndFncts.Print(NewSkyPos,3);
			Serial.println("");
		}
		
		// Pause Antenna Mode - searching should pause
		if(i > 100 && i < 300)
		NewAntennaManager._AntennaModePaused = true;
		
		// During Pause the NewSkyPos and CurrentSkyPos should stay the same
		if(i > 101 && i < 300)
		{
			assertTrue(StndFncts.CompareFloats(NewSkyPos[El], CurrentSkyPos[El],1));
			assertTrue(StndFncts.CompareFloats(NewSkyPos[Az],CurrentSkyPos[Az],1));
			assertTrue(StndFncts.CompareFloats(NewSkyPos[Pol],CurrentSkyPos[Pol],1));
		}
		
		// While Searching the Current Position should never reach the New Sky Target position unless at Search
		//  Pattern Corners
		if(NewAntennaManager._AntennaModePaused == false)
		{
			int axisChangeInPos = 0;
			static int cornerPauseInPos = 0;
					
					if(!StndFncts.CompareFloats(NewSkyPos[El], CurrentSkyPos[El],1))
					axisChangeInPos++;
					if(!StndFncts.CompareFloats(NewSkyPos[Az],CurrentSkyPos[Az],1))
					axisChangeInPos++;
					
					if(!StndFncts.CompareFloats(NewSkyPos[Pol],CurrentSkyPos[Pol],1))
					axisChangeInPos++;
					
					if(axisChangeInPos == 0)
					cornerPauseInPos++;
					else
					cornerPauseInPos = 0;
					
					assertTrue(cornerPauseInPos < 2);
		}
		
		// Continue Antenna Mode - should continue from the same place is paused
		if(i > 300)
		{
		NewAntennaManager._AntennaModePaused = false;
		}
		
		
		StndFncts.SetEquals(NewSkyPos,CurrentSkyPos,3);
		
		
	}
	

}

#endif