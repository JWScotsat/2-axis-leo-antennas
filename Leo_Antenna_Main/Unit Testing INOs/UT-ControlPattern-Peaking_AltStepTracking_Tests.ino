#if isUnitTesting
//**************************************************************************************
//**************************************************************************************
#line 3 "UT-PeakingTests"
//#include "StandardFunctions_Array.h"
StandardFunctions_Array StandFunc;


//Simple Set Test
test(UT_CP_Peaking_AltStepTracking_Test0_ResultingSetCentrePosition)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		NewPeakingPattern.Setup();	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 0);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************

//Simple Step to Position A Test
test(UT_CP_Peaking_AltStepTracking_Test1_CircleTestA)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 0);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El]+0.25, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************

//Simple Step to Position B Test
test(UT_CP_Peaking_AltStepTracking_Test1_CircleTestB)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 0);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Reutrns Position B	
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************

//Simple Step to Position C Test
test(UT_CP_Peaking_AltStepTracking_Test1_CircleTestC)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 0);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Reutrns Position B	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Reutrns Position C	
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El] - ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************

//Simple Step to Position D Test
test(UT_CP_Peaking_AltStepTracking_Test1_CircleTestD)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 0);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Reutrns Position B	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Reutrns Position C	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Reutrns Position D	
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az] - AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}

//**************************************************************************************

//**************************************************************************************

//Simple Step to Position A Repeated Test
test(UT_CP_Peaking_AltStepTracking_Test2_ContinueInA)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1950,NewSkyTarget); // Returns New centre position A	NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], SpoofCentrePosition[El]+ElStepSize+ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El]+ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}


//**************************************************************************************

//Simple Step to Position A Then Pos B After checking new A
test(UT_CP_Peaking_AltStepTracking_Test2_ContinueInBFromNewA)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1950,NewSkyTarget); // Returns New centre position A	NewPeakingPattern.GetNewSkyTarget(1900,NewSkyTarget); // Returns New centre position B		NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El]+ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}
//**************************************************************************************



//**************************************************************************************

//Simple Step to Position A,A,B,C,D,D is new Max Point
test(UT_CP_Peaking_AltStepTracking_Test2_ContinueInBFromNewACompleteToD)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1950,NewSkyTarget); // Returns New centre position A	NewPeakingPattern.GetNewSkyTarget(1900,NewSkyTarget); // Returns position B	NewPeakingPattern.GetNewSkyTarget(1900,NewSkyTarget); // Returns position C	NewPeakingPattern.GetNewSkyTarget(1900,NewSkyTarget); // Returns position D	NewPeakingPattern.GetNewSkyTarget(1980,NewSkyTarget); // Returns position D New Centre			NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az] - AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El]+ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az] - AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], SpoofCentrePosition[Pol], 2));
}


//**************************************************************************************

//Simple Step to Position A,B,C,D,D,A is new Max Point
test(UT_CP_Peaking_AltStepTracking_Test3_CompleteCircle)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position B	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position C	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position D	NewPeakingPattern.GetNewSkyTarget(2000,NewSkyTarget); // Returns position New Centre D	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position A from New Centre			NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El]+ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az] - AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));
}
//**************************************************************************************


//**************************************************************************************

//Simple Step to Position A,B,C,D,Centre is new Max Point
test(UT_CP_Peaking_AltStepTracking_Test4_CompleteCircleNoImprovSignal)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position B	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position C	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position D	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position Centre	//NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget);			NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));
}
//**************************************************************************************



//**************************************************************************************

//Simple Step to Position A,B,B,C,D,A,Centre
test(UT_CP_Peaking_AltStepTracking_Test4_CompleteCircleNoImprovSignalAdv)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position B	NewPeakingPattern.GetNewSkyTarget(2000,NewSkyTarget); // Returns position B New Centre	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position C	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position D	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position A	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position Centre		NewPeakingPattern.GetCentreSkyPosition(CurrentPos);		// Check back to centre correctly	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));		// Check Pos A Next	NewPeakingPattern.GetNewSkyTarget(2000,NewSkyTarget); // Returns position A
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El] + ElStepSize, 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));
	
	//Check Continue to Pos B
	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position B
	
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az] +AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));
}
//**************************************************************************************

//**************************************************************************************

//Simple Step to Position A,B,C,D,Centre,A,B is new Max Point
test(UT_CP_Peaking_AltStepTracking_Test4_CompleteCircleImprovSignalLesser)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position B	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position C	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position D	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns position Centre	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Reutrns Position A	NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Reutrns Position B	NewPeakingPattern.GetNewSkyTarget(1850,NewSkyTarget); // Returns Position B New Centre			NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az] + AzStepSize, 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));
	
}



//**************************************************************************************

//Simple Step to Position A,B,C,D,Centre,A, SigLost
test(UT_CP_Peaking_AltStepTracking_Test5_SignalLost)
{

	// Resulting Target Positions
	float NewSkyTarget[3];
	float CurrentPos[3];
	// Current Centre Positon - spoof value
	float SpoofCentrePosition[3] = {120.0,30.0,164.0};
		ControlPattern_Peaking_AltStepTrackingClass NewPeakingPattern;		float ElStepSize = 0.25;	float AzStepSize = 0.25;	int LockThreshold = 1800;		NewPeakingPattern.Setup();	NewPeakingPattern.Setup(ElStepSize,AzStepSize,LockThreshold);	NewPeakingPattern.SetStartPosition(SpoofCentrePosition, 1900);	NewPeakingPattern.GetCentreSkyPosition(NewSkyTarget);		NewPeakingPattern.GetNewSkyTarget(1800,NewSkyTarget); // Returns Position A	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns position B	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns position C	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns position D	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns position Centre	assertFalse(NewPeakingPattern._isSignalLost);	NewPeakingPattern.GetNewSkyTarget(0,NewSkyTarget); // Returns position Centre - Sig Lost			NewPeakingPattern.GetCentreSkyPosition(CurrentPos);
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Az], CurrentPos[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[El], CurrentPos[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[El], SpoofCentrePosition[El], 2));
	assertTrue(StandFunc.CompareFloats(CurrentPos[Az], SpoofCentrePosition[Az], 2));
	assertTrue(StandFunc.CompareFloats(NewSkyTarget[Pol], CurrentPos[Pol], 2));
	assertTrue(NewPeakingPattern._isSignalLost);
}
//**************************************************************************************

#define PrintingTest false

#if PrintingTest

#endif
#endif