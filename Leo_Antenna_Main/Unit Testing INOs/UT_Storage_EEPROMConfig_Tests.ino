#line 1 "ST_EEPROMConfig_Tests"

#if isUnitTesting

Storage_EEPROMConfigClass NewEEPROMClass;

//***************************************************************************************
// Check initial default values
test(ST_EEPROMConfig_Test1_InitialValues)
{
	NewEEPROMClass.setup();
	
	assertEqual(NewEEPROMClass.CurrentConfig.ConfigVersion, NewEEPROMClass.DefaultConfig.ConfigVersion);
}

//***************************************************************************************
// Check ability to adjust current values
test(ST_EEPROMConfig_Test2_ChangeCurrentConfig)
{
	
	NewEEPROMClass.CurrentConfig.ConfigVersion = 0XA2;
	

	assertNotEqual(NewEEPROMClass.DefaultConfig.ConfigVersion, NewEEPROMClass.CurrentConfig.ConfigVersion);
}

//***************************************************************************************
// Read stored EEPROM data with new current data stored in
test(ST_EEPROMConfig_Test3_ReadBackNewConfig)
{
	const boolean TestPrint = false;
	
	NewEEPROMClass.CurrentConfig.ConfigVersion = 0XA2;
	
	NewEEPROMClass.SaveCurrent();
	NewEEPROMClass.Read();
	
	NewEEPROMClass.CurrentConfig.ConfigName = _CurrentConfigName;
	
	assertEqual(NewEEPROMClass.CurrentConfig.ConfigVersion, NewEEPROMClass.StoredConfig.ConfigVersion);
	assertNotEqual(NewEEPROMClass.DefaultConfig.ConfigVersion, NewEEPROMClass.StoredConfig.ConfigVersion);
	
	if(TestPrint)
	{
		NewEEPROMClass.PrintCurrentConfig();
		NewEEPROMClass.PrintStoredConfig();
		Serial.print("Current Size of Config; "); Serial.print(sizeof(NewEEPROMClass.CurrentConfig));
		Serial.print(", Stored Size of Config;"); Serial.print(sizeof(NewEEPROMClass.StoredConfig));
		Serial.println("");
	}
	
	assertEqual(sizeof(NewEEPROMClass.CurrentConfig),sizeof(NewEEPROMClass.StoredConfig));
}


//***************************************************************************************
// Adjust config version number after each save
test(ST_EEPROMConfig_Test4_IncreaseConfigVersionOnSave)
{
	const boolean TestPrint = false;
	
	NewEEPROMClass.CurrentConfig.ConfigVersion = 0x02;
	NewEEPROMClass.CurrentConfig.ConfigName = _CurrentConfigName;
	
	NewEEPROMClass.SaveCurrent();
	NewEEPROMClass.Read();
	
	
	
	
	
	if(TestPrint)
	{
		NewEEPROMClass.PrintCurrentConfig();
		NewEEPROMClass.PrintStoredConfig();
		Serial.println("");
	}
	
	assertEqual(NewEEPROMClass.CurrentConfig.ConfigVersion, 0x03);
}

//***************************************************************************************
// Check roll over of version number after each save
test(ST_EEPROMConfig_Test5_IncreaseConfigVersionAlwaysGreaterEqualFactory)
{
	const boolean TestPrint = false;
	
	NewEEPROMClass.CurrentConfig.ConfigVersion = 0xFE;
	NewEEPROMClass.CurrentConfig.ConfigName = _CurrentConfigName;
	
	NewEEPROMClass.SaveCurrent();
	NewEEPROMClass.Read();
	
	if(TestPrint)
	{
		NewEEPROMClass.PrintCurrentConfig();
		NewEEPROMClass.PrintStoredConfig();
		Serial.println("");
	}
	
	assertMoreOrEqual(NewEEPROMClass.CurrentConfig.ConfigVersion, NewEEPROMClass.DefaultConfig.ConfigVersion);
}

//***************************************************************************************
//Ensure setup handles missing config data and sets up default eeprom data
// **Caution do not run on configured antenna

// test(ST_EEPROMConfig_Test6_SetupTest)
// {
// 	const boolean TestPrint = true;
// 	
// 	NewEEPROMClass.CurrentConfig.ConfigVersion = 0x10;
// 	NewEEPROMClass.CurrentConfig.ConfigName = _CurrentConfigName;
// 	NewEEPROMClass.CurrentConfig.EndMemoryChar = 0x00;
// 	
// 	NewEEPROMClass.SaveCurrent();
// 	
// 	if(TestPrint)
// 	{
// 		NewEEPROMClass.PrintCurrentConfig();
// 		NewEEPROMClass.PrintStoredConfig();
// 		Serial.println("");
// 	}
// 	
// 	NewEEPROMClass.setup();
// 	
// 	if(TestPrint)
// 	{
// 		Serial.print("+,Test 6 - After Setup\n");
// 		NewEEPROMClass.PrintCurrentConfig();
// 		NewEEPROMClass.PrintStoredConfig();
// 		Serial.println("");
// 	}
// 	
// 	assertEqual(NewEEPROMClass.CurrentConfig.ConfigVersion, NewEEPROMClass.DefaultConfig.ConfigVersion);
// }

#endif