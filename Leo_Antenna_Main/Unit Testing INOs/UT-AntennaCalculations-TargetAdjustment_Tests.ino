// #if isUnitTesting
// 
// #line 1 "UT_AC_ATA_Tests.ino"
// // //**************************************************************************************
// // //**************************************************************************************
// StandardFunctions_Array StandardFunctions;
// //
// // // Simple First Run Test
// // //  - Expected same results as inputs due to no Yaw,Pitch,Roll
// test(MC_ACTAdj_Test1_ReturnAzElPolNoCorrection)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {0.0, 20.0, 164.0};
// 	float IMUPitchRoll[] = {0.0,0.0,0.0};
// 	
// 	float AntennaYaw = 0.0;
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\t Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\t IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\t Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.println("");
// 	}
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Az], ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[El], ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple Pitch-Elevation return
// test(MC_ACTAdj_Test2_ReturnAzElPolPitchCorrection)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {0.0, 0.0, 164.0};
// 	float IMUPitchRoll[] = {0.0,0.0,0.0};
// 	
// 	IMUPitchRoll[Pitch] = -30.0;
// 	
// 	float AntennaYaw = 0.0;
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.println("");
// 	}
// 
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Az], ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(-1*IMUPitchRoll[Pitch], ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple
// test(MC_ACTAdj_Test2_ReturnAzElPolPitchCorrectionWithAzPos)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {180.0, 0.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,0.0,0.0}; // Yaw, Pitch, Roll
// 	
// 	IMUPitchRoll[Pitch] = -30.0;
// 	
// 	float AntennaYaw = 0.0;
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.println("");
// 	}
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Az], ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(IMUPitchRoll[Pitch], ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple
// test(MC_ACTAdj_Test2_ReturnAzElPolPitchCorrectionWithYawPos)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {0.0, 0.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,0.0,0.0}; // Yaw, Pitch, Roll
// 	
// 	IMUPitchRoll[Pitch] = 0.0;
// 	
// 	float AntennaYaw = 90.0;
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.println("");
// 	}
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(-1*AntennaYaw,ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(IMUPitchRoll[Pitch], ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple
// test(MC_ACTAdj_Test3_0_CompareWithExcel)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {90.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {90.0,-10.0,0.0}; // Yaw, Pitch, Roll
// 	
// 	float AntennaYaw = 90.0;
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.println("");
// 	}
// 	
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(0.0, ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(20.0, ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	
// }
// 
// 
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple
// test(MC_ACTAdj_Test3_1_CompareWithExcel)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {125.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AntennaYaw = 65.0;
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.println("");
// 	}
// 	
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(60.15, ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(15.61, ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	
// }
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple
// test(MC_ACTAdj_Test4_CompleteTest)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {125.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AzEncPos = 20.0;
// 	float CompassHeading = 60.0;
// 	float MountingOffset = 10.0;
// 	
// 	float AntennaYaw = AntennaCalculations_TargetAdjustment.CalculateYaw(AzEncPos, CompassHeading, MountingOffset);
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	float NewTargetsAzEl[] = {AzEncPos + ReturnedAzElPol[Az], ReturnedAzElPol[El]};
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.print("\ MotorTargets;");
// 		StandardFunctions.Print(NewTargetsAzEl, 2);
// 		Serial.println("");
// 	}
// 	
// 	
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(34.61, ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(15.12, ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	assertTrue(StandardFunctions.CompareFloats(NewTargetsAzEl[El], 15.12,2));
// 	assertTrue(StandardFunctions.CompareFloats(NewTargetsAzEl[Az], 54.61,2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple First Run Test
// //  - Expected simple
// test(MC_ACTAdj_Test4_ClassCompleteTest)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {125.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AzEncPos = 20.0;
// 	float CompassHeading = 60.0;
// 	float MountingOffset = 10.0;
// 	
// 	float AntennaYaw = AntennaCalculations_TargetAdjustment.CalculateYaw(AzEncPos, CompassHeading, MountingOffset);
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	float NewTargetsAzEl[] = {AzEncPos + ReturnedAzElPol[Az], ReturnedAzElPol[El]};
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.print("\ MotorTargets;");
// 		StandardFunctions.Print(NewTargetsAzEl, 2);
// 		Serial.println("");
// 	}
// 	
// 	
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(34.61, ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(15.12, ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	assertTrue(StandardFunctions.CompareFloats(NewTargetsAzEl[El], 15.12,2));
// 	assertTrue(StandardFunctions.CompareFloats(NewTargetsAzEl[Az], 54.61,2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// // Simple Full Test
// //  Expected Values generated by Excel Sheet - Stabilised Antenna Maths Updated 20-8-15
// test(MC_ACTAdj_Test5_MaxResultingAzTarget)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {260.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AzEncPos = 20.0;
// 	float CompassHeading = 60.0;
// 	float MountingOffset = 10.0;
// 	
// 	float AntennaYaw = AntennaCalculations_TargetAdjustment.CalculateYaw(AzEncPos, CompassHeading, MountingOffset);
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	float CompleteAzCorrectedTarget = AzEncPos + ReturnedAzElPol[Az];
// 	
// 	if (CompleteAzCorrectedTarget > 180.0)
// 	CompleteAzCorrectedTarget -= 360.0;
// 	else if (CompleteAzCorrectedTarget < -180.0)
// 	CompleteAzCorrectedTarget += 360.0;
// 	
// 	
// 	float NewTargetsAzEl[] = {CompleteAzCorrectedTarget, ReturnedAzElPol[El]};
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.print("\ MotorTargets;");
// 		StandardFunctions.Print(NewTargetsAzEl, 2);
// 		Serial.println("");
// 	}
// 	
// 	
// 	
// 
// 	assertTrue(StandardFunctions.CompareFloats(170.94, ReturnedAzElPol[Az],2));
// 	assertTrue(StandardFunctions.CompareFloats(7.93, ReturnedAzElPol[El],2));
// 	assertTrue(StandardFunctions.CompareFloats(AxisTarget[Pol], ReturnedAzElPol[Pol],2));
// 	assertTrue(StandardFunctions.CompareFloats(NewTargetsAzEl[El], 7.93,2));
// 	assertTrue(StandardFunctions.CompareFloats(NewTargetsAzEl[Az], -169.06,2));
// 	
// }
// 
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// //Reverse Correction Test
// //  - Find Current Az El Uncorrected
// test(MC_ACTAdj_Test6_CurrentPosUncorrected)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {260.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AzEncPos = 20.0;
// 	float ElEncoderPos = 10.0;
// 	float CompassHeading = 60.0;
// 	float MountingOffset = 10.0;
// 	
// 	float AntennaYaw = AntennaCalculations_TargetAdjustment.CalculateYaw(AzEncPos, CompassHeading, MountingOffset);
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	float CompleteAzCorrectedTarget = AzEncPos + ReturnedAzElPol[Az];
// 	
// 	if (CompleteAzCorrectedTarget > 180.0)
// 	CompleteAzCorrectedTarget -= 360.0;
// 	else if (CompleteAzCorrectedTarget < -180.0)
// 	CompleteAzCorrectedTarget += 360.0;
// 	
// 	
// 	float NewTargetsAzEl[] = {CompleteAzCorrectedTarget, ReturnedAzElPol[El]};
// 	
// 	
// 	float UncorrectedAzElPol[3];
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateUncorrectedCurrentPosition(CompleteAzCorrectedTarget,ReturnedAzElPol[El],NewTargetsAzEl,AxisTarget, UncorrectedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.print("\ MotorTargets;");
// 		StandardFunctions.Print(NewTargetsAzEl, 2);
// 		
// 		Serial.print("\ ReversedTargets;");
// 		StandardFunctions.Print(UncorrectedAzElPol, 2);
// 		Serial.println("");
// 	}
// 	
// 	
// 	
// 	assertTrue(StandardFunctions.CompareFloats(UncorrectedAzElPol[Az], AxisTarget[Az]-360.0,2));
// 	assertTrue(StandardFunctions.CompareFloats(UncorrectedAzElPol[El], AxisTarget[El],2));
// 	
// }
// 
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// //Reverse Correction Test
// //  - Find Current Az El Uncorrected
// test(MC_ACTAdj_Test7_FurtherCurrentPosUncorrected)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {260.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AzEncPos = -160.0;
// 
// 	float CompassHeading = 60.0;
// 	float MountingOffset = 10.0;
// 	
// 	float AntennaYaw = AntennaCalculations_TargetAdjustment.CalculateYaw(AzEncPos, CompassHeading, MountingOffset);
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	float CompleteAzCorrectedTarget = AzEncPos + ReturnedAzElPol[Az];
// 	
// 	if (CompleteAzCorrectedTarget > 180.0)
// 	CompleteAzCorrectedTarget -= 360.0;
// 	else if (CompleteAzCorrectedTarget < -180.0)
// 	CompleteAzCorrectedTarget += 360.0;
// 	
// 	
// 	float NewTargetsAzEl[] = {CompleteAzCorrectedTarget, ReturnedAzElPol[El]};
// 	
// 	
// 	float UncorrectedAzElPol[3];
// 	
// 	float ElEncoderPos = NewTargetsAzEl[El];
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateUncorrectedCurrentPosition(AzEncPos,ElEncoderPos,NewTargetsAzEl,AxisTarget, UncorrectedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.print("\ MotorTargets;");
// 		StandardFunctions.Print(NewTargetsAzEl, 2);
// 		
// 		Serial.print("\ ReversedTargets;");
// 		StandardFunctions.Print(UncorrectedAzElPol, 2);
// 		Serial.println("");
// 	}
// 	
// 	float reversedTargetForAz = AxisTarget[Az]-360.0-(NewTargetsAzEl[Az]-AzEncPos);
// 	
// 	assertTrue(StandardFunctions.CompareFloats(UncorrectedAzElPol[Az], reversedTargetForAz,2));
// 	assertTrue(StandardFunctions.CompareFloats(UncorrectedAzElPol[El], AxisTarget[El],2));
// 	
// }
// 
// 
// //**************************************************************************************
// //**************************************************************************************
// 
// //Reverse Correction Test
// //  - Find Current Az El Uncorrected
// test(MC_ACTAdj_Test7_FurtherCurrentPosUncorrectedNegativeElBoundary)
// {
// 	const boolean DebugPrint = false;
// 	float AxisTarget[] = {260.0, 10.0, 164.0}; //Az,El,Pol
// 	float IMUPitchRoll[] = {0.0,-2.9,4.8}; // Yaw, Pitch, Roll
// 	
// 	float AzEncPos = -160.0;
// 	float ElEncoderPos = MinPhysical_Elevation;
// 	float CompassHeading = 60.0;
// 	float MountingOffset = 10.0;
// 	
// 	float AntennaYaw = AntennaCalculations_TargetAdjustment.CalculateYaw(AzEncPos, CompassHeading, MountingOffset);
// 	
// 	float ReturnedAzElPol[3] = {1.0,2.0,3.0};
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateTargetAdjustment(IMUPitchRoll, AntennaYaw, AxisTarget,ReturnedAzElPol);
// 	
// 	float CompleteAzCorrectedTarget = AzEncPos + ReturnedAzElPol[Az];
// 	
// 	if (CompleteAzCorrectedTarget > 180.0)
// 	CompleteAzCorrectedTarget -= 360.0;
// 	else if (CompleteAzCorrectedTarget < -180.0)
// 	CompleteAzCorrectedTarget += 360.0;
// 	
// 	
// 	float NewMotorTargetsAzEl[] = {CompleteAzCorrectedTarget, ReturnedAzElPol[El]};
// 	
// 	
// 	float UncorrectedAzElPol[3];
// 	
// 	AntennaCalculations_TargetAdjustment.CalculateUncorrectedCurrentPosition(AzEncPos,ElEncoderPos,NewMotorTargetsAzEl, AxisTarget, UncorrectedAzElPol);
// 	
// 	if(DebugPrint)
// 	{
// 		
// 		Serial.print("\ Target Pos;");
// 		StandardFunctions.Print(AxisTarget, 3);
// 		Serial.print("\ Yaw Val;");
// 		Serial.print(AntennaYaw);
// 		Serial.print("\ IMU Vals;");
// 		StandardFunctions.Print(IMUPitchRoll, 3);
// 		Serial.print("\ Returned Vals;");
// 		StandardFunctions.Print(ReturnedAzElPol, 3);
// 		Serial.print("\ MotorTargets;");
// 		StandardFunctions.Print(NewMotorTargetsAzEl, 2);
// 		
// 		Serial.print("\ ReversedTargets;");
// 		StandardFunctions.Print(UncorrectedAzElPol, 2);
// 		Serial.println("");
// 	}
// 	
// 	float reversedTargetForAz = AxisTarget[Az]-360.0-(NewMotorTargetsAzEl[Az]-AzEncPos);
// 	
// 	assertTrue(StandardFunctions.CompareFloats(UncorrectedAzElPol[Az], reversedTargetForAz,2));
// 	assertTrue(StandardFunctions.CompareFloats(UncorrectedAzElPol[El], MinPhysical_Elevation,2));
// 	
// }
// 
// #endif