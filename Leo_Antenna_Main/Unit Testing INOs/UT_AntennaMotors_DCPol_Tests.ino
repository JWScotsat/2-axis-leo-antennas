#if isUnitTesting
#line 2 "UT_AM_DCPol_Tests"

#define EncoderConnected false

//Pol Minimum Test - trim in negative direction
test(Test1_DCMotorTests_LessThanMin)
{
	boolean debugPrint = false;
	
	_AntMotorPol._isUnitTesting = true;
	
	if(debugPrint)
	Serial.print("Current Pos Before Trim;" + String(_AntMotorPol.getAxisPos()) + "\n");
	
	_AntMotorPol._axisTrim = -140.0;
	
	if(debugPrint)
	Serial.print("Current Pos After Trim;" + String(_AntMotorPol.getAxisPos()) + "\n");
	
	assertMoreOrEqual(_AntMotorPol.getAxisPos(),MinPhysical_Pol);


}

//Pol Minimum Test - trim in negative direction
test(Test2_DCMotorTests_TargetChange)
{
	boolean debugPrint = false;
	
	_AntMotorPol._isUnitTesting = false;


	_AntMotorPol._axisTarget = 0.0;
	
	if(debugPrint)
	Serial.print("Current Target Before Trim;" + String(_AntMotorPol._axisTargetTrimmed) + "\n");
	
	
	_AntMotorPol._axisTrim = -140.0;
	_AntMotorPol.moveToTarget();
	
	if(debugPrint)
	Serial.print("Current Target After Trim;" + String(_AntMotorPol._axisTargetTrimmed) + "\n");
	

	if(EncoderConnected)
	assertMoreOrEqual(_AntMotorPol.getAxisPos(),MinPhysical_Pol);
	
	assertTrue(_AntMotorPol._axisTargetTrimmed > 0.0);

}

//Pol Minimum Test - trim in negative direction
test(Test3_DCMotorTests_TargetChangeCurrentInPosition)
{
	boolean debugPrint = false;
	
	_AntMotorPol._isUnitTesting = true;
	_AntMotorPol._axisTrim = -140.0;

	_AntMotorPol._axisTarget = 0.0;
	_AntMotorPol._axisValUnitSpoof = 0.0;
	
	_AntMotorPol._isDCAxisTargetBoundMidWay = false;
	
	if(debugPrint)
	{
		Serial.print("Current Target;" + String(_AntMotorPol._axisTarget) + "\n");
		Serial.print("Current Target Trimmed;" + String(_AntMotorPol._axisTargetTrimmed) + "\n");
		Serial.print("Current Pos;" + String(_AntMotorPol.getAxisPos()) + "\n");
	}
	
	_AntMotorPol.moveToTarget();
	
	_AntMotorPol._axisValUnitSpoof = 220.0;
	_AntMotorPol.moveToTarget();
	
	
	if(debugPrint)
	Serial.print("Current Pos Updated;" + String(_AntMotorPol.getAxisPos()) + "\n");
	

	assertTrue(_AntMotorPol._axisTargetTrimmed > 0.0);
	assertTrue(_AntMotorPol._axisInPosition);
}



//Pol Max Test - trim in positive direction
test(Test4_DCMotorTests_TargetChangeCurrentInPosition)
{
	boolean debugPrint = false;
	
	_AntMotorPol._isUnitTesting = true;
	_AntMotorPol._axisTrim = 140.0;

	_AntMotorPol._axisTarget = 360.0;
	
	_AntMotorPol.moveToTarget();
	
	if(debugPrint)
	{
		Serial.print("Current Target;" + String(_AntMotorPol._axisTarget) + "\n");
		Serial.print("Current Target Trimmed;" + String(_AntMotorPol._axisTargetTrimmed) + "\n");
		Serial.print("Current Pos;" + String(_AntMotorPol.getAxisPos()) + "\n");
	}
	
	_AntMotorPol.moveToTarget();
	
	_AntMotorPol._axisValUnitSpoof = 140.0;
	_AntMotorPol.moveToTarget();
	
	
	if(debugPrint)
	Serial.print("Current Pos Updated;" + String(_AntMotorPol.getAxisPos()) + "\n");
	

	assertTrue(_AntMotorPol._axisTargetTrimmed == 140.0);
	assertTrue(_AntMotorPol._axisInPosition);
}


//Axis Trim Max Min
test(Test5_DCMotorTests_MaxMinTrim)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = true;
	_AntMotorPol._axisTrim = 440.0;

	_AntMotorPol._axisTarget = 360.0;
	
	_AntMotorPol.moveToTarget();
	
	if(debugPrint)
	Serial.print("Max Targert;" + String(_AntMotorPol._axisTargetTrimmed) + "\n");
	
	assertTrue(_AntMotorPol._axisTargetTrimmed < _AntMotorPol._axisMaxBoundary + 0.2);
	
	_AntMotorPol._axisTrim = -440.0;

	_AntMotorPol._axisTarget = 0.0;
	
	_AntMotorPol.moveToTarget();

	if(debugPrint)
	Serial.print("Min Targert;" + String(_AntMotorPol._axisTargetTrimmed) + "\n");
	
	
	assertTrue(_AntMotorPol._axisTargetTrimmed > _AntMotorPol._axisMinBoundary - 0.2);
	
}


//Axis Init Test
// Requires connected encoder
test(Test6_DCMotorTests_InitMax)
{
	if(EncoderConnected)
	{
		boolean debugPrint = false;
		
		_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
		_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
		
		_AntMotorPol._isUnitTesting = false;
		_AntMotorPol._axisTrim = 140.0;

		_AntMotorPol._axisTarget = 360.0;
		
		while(_AntMotorPol.getAxisPos() > 310.0)
		{
			Serial.print("Current Pos; " + String(_AntMotorPol.getAxisPos())+"\n");
			delay(250);
		}
		
		_AntMotorPol.initialise();
	}
}

//Axis Init Test
// Requires connected encoder
test(Test7_DCMotorTests_InitMin)
{
	if(EncoderConnected)
	{
		boolean debugPrint = false;
		
		_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
		_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
		
		_AntMotorPol._isUnitTesting = false;
		_AntMotorPol._axisTrim = -140.0;

		_AntMotorPol._axisTarget = 360.0;
		
		while(_AntMotorPol.getAxisPos() > 25.0)
		{
			Serial.print("Current Pos; " + String(_AntMotorPol.getAxisPos())+"\n");
			delay(250);
		}
		
		assertTrue(_AntMotorPol.initialise() == AxisInitialised);
		
		Serial.print("Final Pos;" + String(_AntMotorPol.getAxisPos()) + "\n");
	}
}


//New Dev - Adding 0-180 border technique for limiting large pol movements

//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test8_DCMotorTests_TargetOutBoundary)
{
	_AntMotorPol._isDCAxisTargetBoundMidWay = true;
	
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = 0;

	_AntMotorPol._axisTarget = 330;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (330-180.0)) < 0.1);

	
}

//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test8_DCMotorTests_TargetUnderBoundary)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = 0;

	_AntMotorPol._axisTarget = 20;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (20+180.0)) < 0.1);

	
}


//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test8_DCMotorTests_TargetUnderBoundaryWithTrim)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = 60;

	_AntMotorPol._axisTarget = 20;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (260.0)) < 0.1);

	
}

//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test8_DCMotorTests_TargetUnderBoundaryWithTrim2)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = 60;

	_AntMotorPol._axisTarget = 85;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (85+180.0+60.0)) < 0.1);
	assertTrue((_AntMotorPol._axisTargetTrimmed < (270.0 + 60.0)));
	
}

//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test9_DCMotorTests_TargetOverBoundaryWithTrim)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = 60;

	_AntMotorPol._axisTarget = 330;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (210.0)) < 0.1);
	assertTrue((_AntMotorPol._axisTargetTrimmed < (270.0 + 60.0)));
	
}

//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test9_DCMotorTests_TargetOverBoundaryWithTrim2)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = 330;

	_AntMotorPol._axisTarget = 330;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (120)) < 0.1);
	assertTrue((_AntMotorPol._axisTargetTrimmed < (360.0)));
	
}


//Target testing to reproduce positions within a 180 degree arc. Horizontal/Vertical pol means that every position in 360 can be reperesented in a 180 arc.
test(Test10_DCMotorTests_TargetUnderMinBoundaryWithTrim2)
{
	boolean debugPrint = false;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
	
	_AntMotorPol._isUnitTesting = false;
	_AntMotorPol._axisTrim = -330;

	_AntMotorPol._axisTarget = 20;
	
	_AntMotorPol.moveToTarget();
	
	assertTrue(abs(_AntMotorPol._axisTargetTrimmed - (230)) < 0.1);
	assertTrue((_AntMotorPol._axisTargetTrimmed < (360.0)));
	assertTrue((_AntMotorPol._axisTargetTrimmed > (0.0)));
}

#endif