#include "AntennaManager.h"
//******************************************************************************
//Main Antenna Mode Logic
//  Switch statement chooses sub function based on current Antenna Mode
// Outputs Antenna Mode as well as any NewSkyPos Az El Pol
//Pause before switch allows the functionality to be paused without losing store states
int AntennaManagerClass::AntennaModeControl(int AntennaMode, float CurrentSignal, float CurrentSkyPos[], float* NewSkyPos)
{
	
	if(!_AntennaModePaused)
	{
		switch(AntennaMode)
		{
			// Pointing Case: Pass In latest GPS Values, Calculate New Sky Sat Point, Flag _HasPointed so on completetion flag AntennaMode as Searching
			case AntennaModePointing : AntennaMode = AntennaModeControl_SatPoint(AntennaMode,NewSkyPos);
			break;
			
			// Searching Case: Outputs new position for searching
			case AntennaModeSearching : AntennaMode = AntennaModeControl_Searching(AntennaMode,NewSkyPos,CurrentSkyPos);
			break;

			// Peaking Mode - Continue Peaking unless Class Flag "Peaking Lost"
			case AntennaModePeaking : AntennaMode = AntennaModeControl_Peaking(AntennaMode,NewSkyPos,(int)CurrentSignal);
			break;
			
			// Manual Position - New Position is set to Stored Manual Position
			case AntennaModeManualPos : StndFncs.SetEquals(_StoreManualPosition,NewSkyPos,3);
			break;
			
			// First Test Case - Unit Test
			case AntennaModeTest : Serial.print("+,Antenna Manager - Antenna Mode Test;"+ String(AntennaMode) + "\n"); break;
			
			// Error Case
			default : Serial.print("@,Antenna Manager Error - Antenna Mode unrecognised;" + String(AntennaMode) + "\n"); AntennaMode = AntennaModeManualPos; break;
			
		}
	}else
	StndFncs.SetEquals(CurrentSkyPos,NewSkyPos,3);


	return AntennaMode;
}

//**********************************************************************************
// Main Case Function for Sat Pointing
//  Uses Sat Point class to calculate NewSkyPos Az El Pol based on GPS Input
//  Waits for pointing to complete (repeated call of Sat Point) to change logic over to Searching
int AntennaManagerClass::AntennaModeControl_SatPoint(int AntennaMode, float* NewSkyPos)
{
	if(_hasPointed && StndFncs.CompareFloats(_CurrentSat.SatLng,_SatPoint._CurrentSat.SatLng, 1) && _CurrentSat.SatPolHV == _SatPoint._CurrentSat.SatPolHV)
	{
		// Change mode to searching
		AntennaMode = AntennaModeSearching;
		_hasPointed = false;
		
		//Ensure clear Search Pattern
		float BlankArray[3];
		_SearchPattern.ResetSearchComplete(NewSkyPos,NewSkyPos,BlankArray);
		
		return AntennaMode;
	}
	
	_CurrentGPSLatLng[Lat] = _CurrentGPS.Latitude;
	_CurrentGPSLatLng[Lng] = _CurrentGPS.Longitude;
	
	_SatPoint.setGPS(_CurrentGPSLatLng[Lat], _CurrentGPSLatLng[Lng]);
	_SatPoint.CalculateSatPoint(_CurrentSat,NewSkyPos);
	_hasPointed = true;
	StndFncs.SetEquals(NewSkyPos, _SatSkyTarget, 3);
	
	return AntennaMode;
}



//**********************************************************************************
// Main Case Function for Searching
//  Uses SearchPattern class to output NewSkyPos Az El in a search spiral pattern
//  if Searching Completes then antenna logic is changed over to re-point the antenna
int AntennaManagerClass::AntennaModeControl_Searching(int AntennaMode, float* NewSkyPos, float CurrentSkyPos[])
{
	//Pause delay for next search position. Adds a timed delay between each NextPosition to allow the azimuth to settle before moving to next pos
	if(millis() -  _SearchPattern._LastUpdateTime >= _SearchPattern._PointDelay || _isTesting)
	{
		
		//Handle GPS CoG current status for going to 360 sweep or tighter accurate heading sweep
		_SearchHandleSearchSize();
		
		//Calculate next position
		_SearchPattern.NextPosition(_SatSkyTarget,CurrentSkyPos,NewSkyPos);
		
		if(_SearchPattern._isError != 0)
		Serial.println(_SearchPattern._StatusMessage);
		
		// Repoint antenna on failed search
		if(_SearchPattern._isSearchComplete)
		{
			_SearchPattern.addSearchAttempt();
			
			AntennaMode = AntennaModePointing;
		}
	}
	
	return AntennaMode;
}

//**********************************************************************************
// Main Case Function for Peaking
// Uses PeakingPattern class to out put next position based on box style peaking pattern
//  if peaking fails then flags antenna mode to repoint at the Sat
int AntennaManagerClass::AntennaModeControl_Peaking(int AntennaMode, float* NewSkyPos, int CurrentSignal)
{
	static unsigned long polRepointTimer = millis();

	switch(_CurrentConfig->PeakingMode)
	{
		
		case _PeakingModeDynamic :
		if(millis() - _PeakingPatternDynamicDelta._LastUpdateTime >= _PeakingPatternDynamicDelta._PointDelayMin || _isTesting)
		{
			_PeakingPatternDynamicDelta.GetNewSkyTarget(CurrentSignal,NewSkyPos);
			
			if(_PeakingPatternDynamicDelta._isSignalLost)
			AntennaMode = AntennaModePointing;
			
			//_SatPoint.setAzOffset(_LastGoodHeading);
			if(_PeakingPatternDynamicDelta._CurrentPatternTargetPositionCounter > A)
			_TargetAdjustments.CalculateOffsetFromSat(_PeakingPatternDynamicDelta.getCentreAz(), _HeadingAvg.getCurrentAvg());//Recalculate new heading from sat
			
			
			
			
			_PeakingPatternDynamicDelta._LastUpdateTime = millis();
		}
		break;
		
		
		case _PeakingModeStepTracking:
		if(millis() - _PeakingPatternStepTracking._LastUpdateTime >= _PeakingPatternStepTracking._PointDelay || _isTesting)
		{
			_PeakingPatternStepTracking.GetNewSkyTarget(CurrentSignal,NewSkyPos);
			
			if(_PeakingPatternStepTracking._isSignalLost)
			AntennaMode = AntennaModePointing;
			
			_PeakingPatternStepTracking._LastUpdateTime = millis();
		}
		break;
		
		
		case _PeakingModeCircGrad:
		if(millis() - _PeakingPatternCircGrad._LastUpdateTime >= _PeakingPatternCircGrad._PointDelay || _isTesting)
		{
			_PeakingPatternCircGrad.GetNewSkyTarget(CurrentSignal,NewSkyPos);
			
			if(_PeakingPatternCircGrad._isSignalLost)
			AntennaMode = AntennaModePointing;
			
			_PeakingPatternCircGrad._LastUpdateTime = millis();
		}
		break;
		
		default:
		if(millis() - _PeakingPattern._LastUpdateTime >= _PeakingPattern._PointDelay || _isTesting)
		{
			_PeakingPattern.GetNewSkyTarget(CurrentSignal,NewSkyPos);
			
			if(_PeakingPattern._isSignalLost)
			AntennaMode = AntennaModePointing;
			
			_PeakingPattern._LastUpdateTime = millis();
		}
		break;
		
	}
	
	/*Repoint Polarisation when peaking for a long period of time to prevent Polarisation skew from effecting signal quality
	*/
	if(millis() - polRepointTimer > 120*1000)
	{
		float _PointedGPSLatLong[2];
		_SatPoint.returnGPS(_PointedGPSLatLong);//Retreive pointed GPS Lat Long to determine if terminal has traveled far enough to warrant a reposition of the pol
		if(abs(_PointedGPSLatLong[Lat] - _CurrentGPS.Latitude) > 0.5 || abs(_PointedGPSLatLong[Lng] - _CurrentGPS.Longitude) > 0.5)
		{
			_SatPoint.setGPS( _CurrentGPS.Latitude, _CurrentGPS.Longitude);
			_SatPoint.CalculateSat_PolUpdate(NewSkyPos);
			StndFncs.SetEquals(NewSkyPos, _SatSkyTarget, 3);
		}
		
		polRepointTimer = millis();
	}
	
	return AntennaMode;
}


//Search handler for CoG logic when relying soley on CoG for heading
// Adjusts the search pattern to a wide 360 Degree search of the sky when CoG is below threshold
// Adjusts the search pattern to standard short 20 degree search of the sky when CoG is trustable

void AntennaManagerClass::_SearchHandleSearchSize()
{
	static boolean previousSearchStatus = _isWideSearch;
	
	// 	if(GPSCompass.GetSpeedStatus())
	// 	_isWideSearch = false;
	// 	else
	// 	_isWideSearch = true;

	if(_SearchPattern.getSearchAttempt() == 0)
	{
		_SearchPattern.resetFirstEverSearchAttempt();
		_isWideSearch = true;
	}
	else if (_SearchPattern.getSearchAttempt() > 1)
	_isWideSearch = true;
	else
	_isWideSearch = false;
	
	if(_isWideSearch)
	{
		_SearchPattern.SearchAzimuth._maxCounterBoundary = 179.4;
		double SearchSweepSize = 1.0;
		int divisor = (_SearchPattern.SearchAzimuth._maxCounterBoundary / SearchSweepSize) + 1;
		_SearchPattern.SearchAzimuth._stepCountSize = (_SearchPattern.SearchAzimuth._maxCounterBoundary/divisor);
		//MotorController._AntMotorCross._axisError = 1.25;
		_SearchPattern._PointDelay = 110;
	}
	else
	{
		_SearchPattern.SearchAzimuth._maxCounterBoundary = _CurrentConfig->SearchAzimuth_maxCounterBoundary;
		// 		_SearchPattern.SearchAzimuth._stepCountSize = _CurrentConfig->SearchAzimuth_stepCountSize;
		// 		//MotorController._AntMotorCross._axisError = _CurrentConfig->Cross_axisError;
		// 		_SearchPattern._PointDelay = _CurrentConfig->_SearchPatternPointDelayTime;
		double SearchSweepSize = 1.0;
		int divisor = (_SearchPattern.SearchAzimuth._maxCounterBoundary / SearchSweepSize) + 1;
		_SearchPattern.SearchAzimuth._stepCountSize = SearchSweepSize;//(_SearchPattern.SearchAzimuth._maxCounterBoundary/divisor);
		//MotorController._AntMotorCross._axisError = 1.25;
		_SearchPattern._PointDelay = 110;
	}
	
}
//**********************************************************************************//**********************************************************************************
//**********************************************************************************//**********************************************************************************
