#include "AntennaManager.h"

boolean AntennaManagerClass::AntennaComms_SerialCommandHandle()
{
	boolean isNewBlockingCommand = false;
	
	if(USBSerial.Read())
	{
		
		int CommandReceived = USBSerial.parseCommand();
		
		switch(CommandReceived)
		{
			//***************
			
			case Command_PiInitialisation : AntennaComms_Command_PiInitialisation(); isNewBlockingCommand = true; _USBConnectionStatus = true; break;
			
			//***************
			case Command_CompassUpdate : _CurrentCompassHeading = USBSerial.parseFloat(PacketPosition1);
			
			if(_PrintDebug_CompassUSB)
			{
				Serial.print("+,43," + String(_CurrentCompassHeading) + ",");
				Serial.print(millis()); Serial.println(",");
				_debugCompassTimer = millis();
			}
			break;
			
			//***************
			case Command_PointAtSat : AntennaComms_Command_NewSatPoint(); isNewBlockingCommand = true; break;
			
			//***************
			
			//***************
			case Command_UpdateDBSignal : SatSignalRecv1.setDBSignal(USBSerial.parseFloat(PacketPosition1)); break;
			
			//***************
			
			case Command_ManualPosition :AntennaComms_Command_NewManualPos(); isNewBlockingCommand = true; break;
			
			//***************
			
			case Command_PauseAntennaMode : _AntennaModePaused = !_AntennaModePaused;
			
			if(_CurrentConfig->_PrintUSBDebug)
			Serial.print("+,USB Comms - Antenna Mode Pause;" + String(_AntennaModePaused) + "\n");break;
			
			//***************
			case Command_AllStop :AntennaComms_Command_AllStop(); isNewBlockingCommand = true; break;
			
			
			//***************
			case Command_ManualStartSearch : AntennaComms_Command_ManualSearch(); isNewBlockingCommand = true; break;
			
			//***************

			case Command_ManualStartPeaking : AntennaComms_Command_ManualPeaking(); isNewBlockingCommand = true; break;
			
			

			//***************
			
			case Command_AdjustPolAcc : _CurrentConfig->Pol_axisError = USBSerial.parseFloat(PacketPosition1); MotorController._AntMotorPol._axisError = _CurrentConfig->Pol_axisError;
			
			if(_CurrentConfig->_PrintUSBDebug)
			Serial.print("+,USB Comms - New Pol Axis Error/Acc;" + String(MotorController._AntMotorPol._axisError) + "\n");
			
			break;
			
			//***************
			
			
			case Command_AdjustPolTrim : _CurrentConfig->Pol_axisTrim = USBSerial.parseFloat(PacketPosition1); MotorController._AntMotorPol._axisTrim = _CurrentConfig->Pol_axisTrim;
			
			if(_CurrentConfig->_PrintUSBDebug)
			Serial.print("+,USB Comms - New Pol Axis Trim;" + String(MotorController._AntMotorPol._axisTrim) + "\n");
			
			_EEPROMConfig.SaveCurrent();
			
			break;
			
			//***************
			
			case Command_AdjustAzTrim : MotorController._AntMotorAz._axisTrim = _CurrentConfig->Az_axisTrim + USBSerial.parseFloat(PacketPosition1);
			
			if(_CurrentConfig->_PrintUSBDebug)
			Serial.print("+,USB Comms - New Az Offset + Trim;" + String( MotorController._AntMotorAz._axisTrim) + "\n");

			
			break;
			
			//***************
			
			case Command_ManualGPSSet : AntennaComms_Command_ManualGPS(); break;
			
			//***************
			
			case Command_AutoGPSSet : GPSDecoder._isGPSManual = false; GPSCompass.ClearGPSManual();
			
			if(_CurrentConfig->_PrintUSBDebug)
			Serial.print("+,USB Comms - GPS Set to Auto;" + String(GPSDecoder._isGPSManual) + "\n");break;
			
			//***************
			
			case Command_GetGPSTime : AntennaComms_Command_GetGPSTime(); break;
			
			//***************
			
			//***************

			
			
			//Advanced Settings Commands
			//***************
			
			case Command_Adv_PeakingSettings: AntennaComms_Advanced_PeakingSettings(); break;
			
			case Command_Adv_PeakingSettingsDynamic: AntennaComms_Advanced_PeakingSettingsDynamic(); break;
			
			case Command_Adv_PeakingSettingsStepTracking: AntennaComms_Advanced_PeakingSettingsStepTracking(); break;
			
			case Command_Adv_SearchSettings: AntennaComms_Advanced_SearchSettings(); break;
			
			case Command_Adv_MotorDisable: AntennaComms_Advanced_MotorsDisable(); break;
			
			case Command_Adv_MotorLock: AntennaComms_Advanced_MotorsLock(); break;
			
			case Command_Adv_MiscSettings: AntennaComms_Advanced_MiscSettings(); break;
			
			case Command_Adv_SatSignalSettings: AntennaComms_Advanced_SatSigSettings(); break;
			
			case Command_Adv_TargetAdj: AntennaComms_Advanced_TargetAdj(); break;
			
			case Command_Adv_IMUSettings: AntennaComms_Advanced_IMUSettings(); break;
			
			case Command_Adv_UseOnboardGyroCompass: _CurrentConfig->UseOnBoardGyroWithCompass = USBSerial.parseInt(PacketPosition1);Serial.print("+, USB Comms - Onboard Gyro Compass Use;" + String(_CurrentConfig->UseOnBoardGyroWithCompass) + "\n"); break;
			
			case Command_Adv_CompFiltKVal: _CurrentConfig->KGyroCompFilter = USBSerial.parseFloat(PacketPosition1); _YawCompFilter.setup(_CurrentConfig->KGyroCompFilter); Serial.print("+, USB Comms - Complimentary Compass Filter K Value Change;" + String(_CurrentConfig->KGyroCompFilter) + "\n");
			constrain(_CurrentConfig->KGyroCompFilter, 0.00001, 0.99999);
			break;
			
			case Command_Adv_SwitchStabilisationMethod: _CurrentConfig->UseSimplifiedStabilisation = USBSerial.parseInt(PacketPosition1);Serial.print("+, USB Comms - Use Simplified Stabilisation Method;" + String(_CurrentConfig->UseSimplifiedStabilisation) + "\n"); break;
			
			case Command_Adv_ChangeAnyConfig: AntennaComms_Advanced_ConfigAnySetting();break;

			case Command_Adv_PrintAnyConfig: AntennaComms_Advanced_ConfigPrintSetting();break;

			case Command_Adv_MotorSettings: AntennaComms_Advanced_ConfigMotorSpeedSettings(); break;

			case Command_Adv_AzPIDConfig: AntennaComms_Advanced_ConfigAzPID(); break;
			//***************
			//***************
			
			//Debug Commands
			
			case Command_Debug_PeakingPatternPointDelayChange: _CurrentConfig->_PeakingPatternPointDelayTime = USBSerial.parseInt(PacketPosition1); _PeakingPattern._PointDelay = _CurrentConfig->_PeakingPatternPointDelayTime; break;
			
			case Command_Debug_PeakingPatternModeChange: _CurrentConfig->PeakingMode = USBSerial.parseInt(PacketPosition1); Serial.print("+,USB Comms - New Peaking Mode Chosen;" + String(_CurrentConfig->PeakingMode) + "\n"); break;
			
			case Command_Debug_PeakingPatternPrintSettings: AntennaComms_Debug_PrintPeakingSettings(); break;
			
			case Command_Debug_SearchPatternPointDelayChange: _CurrentConfig->_SearchPatternPointDelayTime =  USBSerial.parseInt(PacketPosition1); _SearchPattern._PointDelay = _CurrentConfig->_SearchPatternPointDelayTime; break;
			
			case Command_Debug_FrameIMUDebugEnable: _CurrentConfig->_FrameIMUEnabled = USBSerial.parseInt(PacketPosition1); break;
			
			case Command_Debug_ToggleMotorDataOutput: _CurrentConfig->_UpdatePi_MotorData_Enable = USBSerial.parseInt(PacketPosition1);
			if(_CurrentConfig->_UpdatePi_MotorData_Enable)
			_CurrentConfig->_UpdatePiRate_MotorData = USBSerial.parseInt(PacketPosition2); //max(USBSerial.parseInt(PacketPosition2), 20);
			
			break;
			
			case Command_Debug_PrintCurrentAxisData: Serial.print("/,Current Axis Data; El Encoder,"); Serial.print(MotorController._AntMotorEl.getAxisPos());
			Serial.print(", Az Encoder,"); Serial.print(MotorController._AntMotorAz.getAxisPos());
			Serial.print(", Cross IMU,"); Serial.print(MotorController._AntMotorCross.getAxisPos());
			Serial.print(", Pol Encoder,"); Serial.print(MotorController._AntMotorPol.getAxisPos());
			Serial.print(", Compass Val,"); Serial.print(_CurrentCompassHeading);
			Serial.print("\n");
			break;
			
			case Command_Ack_InitFail: PIACKFailedInit = true; Serial.print("+,USB Comms - Pi Init Fail Acknolowedged! \n"); break;
			
			case Command_Debug_PrintUSBReplies: _CurrentConfig->_PrintUSBDebug = USBSerial.parseInt(PacketPosition1); if(_CurrentConfig->_PrintUSBDebug) Serial.print("+,USB Comms - USB Debug Printing\n"); break;
			
			case Command_Debug_PrintCompassReplies: USBSerial.parseInt(PacketPosition1); if(_CurrentConfig->_PrintUSBDebug) Serial.print("+,USB Comms - USB Compass Debug Printing\n"); break;
			
			case Command_Debug_PrintLoopData: _PrintDebug_Loop = USBSerial.parseInt(PacketPosition1); if(_CurrentConfig->_PrintUSBDebug) Serial.print("+,USB Comms - Loop Debug Printing\n"); break;
			
			case Command_Debug_InitByPass: _DebugInitBypass = true; Serial.print("+,USB Comms - Debug Initialisation bypass on \n"); break;
			
			case Command_Debug_PrintFWVersion: Serial.print("+,USB Comms - FW Version;" + _ProjectCodeVersion + "\n"); break;

			case Command_Debug_EnableLoopTimer:  _LoopDebugEnable = USBSerial.parseInt(PacketPosition1); Serial.println("+,USB Comms - Loop Timer Enable:" + String(_LoopDebugEnable)); break;
			
			case Command_Debug_IMUStabilityChoice:  _IMUStabilityChoice = USBSerial.parseInt(PacketPosition1); Serial.print("+,USB Comms - Debug IMU Stability Choice now;"); if(_IMUStabilityChoice) Serial.println("Frame");else Serial.println("Cross");break;
			
			case Command_Debug_GyroOffset:  _GyroOffset = USBSerial.parseFloat(PacketPosition1); Serial.print("+,USB Comms - Debug IMU Gyro Offset now;"); Serial.println(_GyroOffset,3);break;
			
			
			case Command_SwitchStabilityMethod: AntennaComms_Advanced_SwitchStabilityMethod(); break;
			
			//Main Reset Commands
			
			case Command_Reset_Reinitialise: Serial.print("+,USB Comms - ReInitialise Command Received!\n");
			if(!_InitRunning)
			{
				Serial.print("+,Running Initialisation sequence;\n"); _AntennaStatusWorking = 0; _isInitilisation_Passed = false; MotorController.AllStop(); break;
			}else
			{
				Serial.print("~,USB Comms - Command to run initialisation when one is currently running. Infinite loop error. Command Ignored.\n");
			}
			break;
			
			case Command_Reset_SystemRestart: Serial.print("+,USB Comms - System Reset!\n"); delay(200); MotorController.AllStop(); CPU_RESTART; break;
			
			case Command_ID_Response: Serial.println("+,Antenna," + _ProjectCodeVersion + ", "); break; // Command response for query of code name and version
			
			case Command_Reset_PMReset: Serial.println(F("+,USB Comms - Power Cycle Reset Call! Sending Reset to PM circuit\n")); //PM_Reset.SetPin(HIGH); delay(500); 
			break;//Power management reset command. Toggle Power reset
			
			// EEPROM Storage Commands
			
			case Command_Storage_SaveCurrentConfig: Serial.print("+,USB Comms - Saving Current Config to EEPROM\n"); _EEPROMConfig.SaveCurrent(); break;
			
			case Command_Storage_RestoreFactoryConfig: Serial.print("+,USB Comms - Restoring Default Config to EEPROM\n");_EEPROMConfig.FactoryReset(); break;
			
			case Command_Storage_PrintConfigs: _EEPROMConfig.PrintCurrentConfig(); _EEPROMConfig.PrintStoredConfig(); break;
			
			case Command_Storage_PrintCurrentConfigPacket: _EEPROMConfig.PrintCurrentConfigPacket(); break;
			
			case Command_Storage_PrintStoredConfigPacket: _EEPROMConfig.PrintStoredConfigPacket(); break;

			//Debug Spoof commands
			
			case Command_Debug_SpoofSignalValueChange: SatSignalRecv1._spoofSignalVal = USBSerial.parseInt(PacketPosition1); break;
			
			//***************
			//***************
			default: Serial.print("~,USB Comms - Comms Serial Command Unrecognised! Command Received;" + String(CommandReceived) + "\n"); break;
			
		}

		// 		if(CommandReceived > 30 && CommandReceived <= 210)
		// 			_EEPROMConfig.SaveCurrent();
	}
	
	return isNewBlockingCommand;

}

//***********************************************************************************
//Sat Point Command
//  Changes stored current sat and sets antenna mode to repoint
void AntennaManagerClass::AntennaComms_Command_NewSatPoint()
{
	const int CommandExpectedNoValues = 4;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		_CurrentSat = {USBSerial.parseFloat(PacketPosition1), USBSerial.parseInt(PacketPosition2), USBSerial.parseFloat(PacketPosition3)};
		_AntennaModePaused = false;
		_CurrentAntennaMode = AntennaModePointing;
		
		if(_CurrentConfig->_PrintUSBDebug)
		Serial.print("+,USB Comms - New Sat Point Data Received! New Sat;" + String(_CurrentSat.SatLng) + ", "+ String(_CurrentSat.SatPolHV) + ", " + String(_CurrentSat.SatCustomSkew) + "\n");
	}else
	{
		Serial.println("@,USB Comms - New Sat Point Invalid No. Values. Expected 3, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}

//***********************************************************************************
// New Manual Az El Pol Command
//  Sets variables for manually pointing to usb packet Az El Pol
void AntennaManagerClass::AntennaComms_Command_NewManualPos()
{

	_AntennaModePaused = 0;
	
	_CurrentAntennaMode = AntennaModeManualPos;
	
	float NewManualPos[] = {USBSerial.parseFloat(PacketPosition2),USBSerial.parseFloat(PacketPosition1),USBSerial.parseFloat(PacketPosition3)};
	
	NewManualPos[Az] -= _TargetAdjustments.getOffsetFromSat();
	
	StndFncs.Bound(NewManualPos[Az],-0.001,360.01);
	
	StndFncs.SetEquals(NewManualPos,_StoreManualPosition,3);
	
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - New Manual Pos; Az" + String(_StoreManualPosition[Az]) + " El" + String(_StoreManualPosition[El]) + " Pol" + String(_StoreManualPosition[Pol]) + "\n");

}

//***********************************************************************************
// Manual Search Command Toggle
//  Used to turn on a manual search or stop the antenna on point
void AntennaManagerClass::AntennaComms_Command_ManualSearch()
{
	
	if(USBSerial.parseInt(PacketPosition1) == 1)
	{
		_AntennaModePaused = false;
		
		_CurrentAntennaMode = AntennaModeSearching;
		
		if(_CurrentAntennaMode != AntennaModeManualPos)
		{
			GetCurrentSkyMotorPositions();
			StndFncs.SetEquals(_CurrentSkyMotorsPos,_NewSkyPos,3);
		}
		else
		StndFncs.SetEquals(_StoreManualPosition,_NewSkyPos,3);
		
		StndFncs.SetEquals(_NewSkyPos,_SatSkyTarget,3);
		_SearchPattern.ResetSearchComplete(_NewSkyPos,_NewSkyPos,_NewSkyPos);
		
		if(_CurrentConfig->_PrintUSBDebug)
		{
			Serial.print("+,USB Comms - Manual Searching On\n");
			Serial.print("+,New Sky pos;"); StndFncs.Print(_NewSkyPos,3);
		}
	}else
	{
		_CurrentAntennaMode = AntennaModeManualPos;
		
		GetCurrentSkyMotorPositions();
		
		StndFncs.SetEquals(_CurrentSkyMotorsPos,_StoreManualPosition,3);
		
		if(_CurrentConfig->_PrintUSBDebug)
		Serial.print("+,USB Comms - Manual Searching Off.Stopping on point\n");
	}
}
//***********************************************************************************
// Manual Peaking Toggle
//  Used to turn on a manual peaking or stop the antenna at a point
void AntennaManagerClass::AntennaComms_Command_ManualPeaking()
{
	if(USBSerial.parseInt(PacketPosition1) == 1)
	{
		_AntennaModePaused = false;
		
		GetCurrentSkyMotorPositions();
		
		StndFncs.SetEquals(_CurrentSkyMotorsPos,_NewSkyPos,3);
		StndFncs.SetEquals(_CurrentSkyMotorsPos,_SatSkyTarget,3);
		
		_CurrentAntennaMode = AntennaModePeaking;
		
		//Alt Peaking
		_PeakingPattern.SetCentreSkyPosition(_CurrentSkyMotorsPos);
		_PeakingPatternDynamicDelta.SetCentreSkyPosition(_CurrentSkyMotorsPos);
		_PeakingPatternStepTracking.SetStartPosition(_CurrentSkyMotorsPos, SatSignalRecv1.read());
		_PeakingPatternCircGrad.SetCentreSkyPosition(_CurrentSkyMotorsPos);
		
		if(_CurrentConfig->_PrintUSBDebug)
		Serial.print("+,USB Comms - Manual Peaking On\n");
	}
	else
	{
		_CurrentAntennaMode = AntennaModeManualPos;
		
		GetCurrentSkyMotorPositions();
		
		StndFncs.SetEquals(_CurrentSkyMotorsPos,_StoreManualPosition,3);
		
		if(_CurrentConfig->_PrintUSBDebug)
		Serial.print("+,USB Comms - Manual Peaking off. Stopping at point\n");
	}
}

//***********************************************************************************
// All Stop Command used to force the antenna to stop on point
void AntennaManagerClass::AntennaComms_Command_AllStop()
{
	_AntennaModePaused = false;
	_CurrentAntennaMode = AntennaModeManualPos;
	
	GetCurrentSkyMotorPositions();

	StndFncs.SetEquals(_CurrentSkyMotorsPos,_NewSkyPos,3);
	StndFncs.SetEquals(_NewSkyPos,_StoreManualPosition,3);
	StndFncs.SetEquals(_NewSkyPos,_SatSkyTarget,3);
	
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Motors All Stop! \n");
}

//***********************************************************************************
//Pi Initialisation Packet Received
//   Sets initial conditions for antenna functionality
void AntennaManagerClass::AntennaComms_Command_PiInitialisation()
{
	const int CommandExpectedNoValues = 13;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		if(_CurrentConfig->_PrintUSBDebug)
		Serial.print("+,USB Comms - Pi Init packet received! Setting Init values\n");
		
		// Manual Position Az El Pol
		_StoreManualPosition[El] = USBSerial.parseFloat(1);
		_StoreManualPosition[Az] = USBSerial.parseFloat(2);
		_StoreManualPosition[Pol] = USBSerial.parseFloat(3);
		
		// Pol Axis Trim/Offset
		_CurrentConfig->Pol_axisTrim =  USBSerial.parseFloat(4);
		MotorController._AntMotorPol._axisTrim = _CurrentConfig->Pol_axisTrim;
		
		
		// Last Pi Heading
		_CurrentCompassHeading = USBSerial.parseFloat(5);
		
		// Azimuth Axis Trim/Offset
		double AzOffset =  USBSerial.parseFloat(6);
		MotorController._AntMotorAz._axisTrim = _CurrentConfig->Az_axisTrim + AzOffset;
		
		// Current Sat Details *Note missing custom skew
		_CurrentSat = {USBSerial.parseFloat(7), USBSerial.parseInt(8)};
		
		// BUC power state
		//_CurrentConfig->_BucPower = USBSerial.parseInt(9);
		//BucPower.SetPin(_CurrentConfig->_BucPower);
		
		// Manual GPS Details
		GPSLatLng TempManualGPS = {USBSerial.parseFloat(11), USBSerial.parseFloat(12)};
		GPSDecoder.SetGPSManual(USBSerial.parseInt(10), TempManualGPS);
		GPSCompass.SetGPSManual(USBSerial.parseInt(10),TempManualGPS);
		
		// If Sat LNG isn't 999 then point at stored sat details, otherwise manually point
		if(_CurrentSat.SatLng != 999)
		_CurrentAntennaMode = AntennaModePointing;
		else
		_CurrentAntennaMode = AntennaModeManualPos;
		
		_CurrentConfig->ConfigVersion -= 1;
		_EEPROMConfig.SaveCurrent();
	}else
	{
		Serial.println("@,USB Comms - Initialisation Packet Invalid No. Values. Expected 13, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}
//***********************************************************************************
// Set Manual GPS Conditions
void AntennaManagerClass::AntennaComms_Command_ManualGPS()
{
	GPSLatLng TempLatLng = {USBSerial.parseFloat(1), USBSerial.parseFloat(2)};
	
	GPSDecoder.SetGPSManual(true,TempLatLng);

	GPSCompass.SetGPSManual(true,TempLatLng);
	
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - New Manual GPS; Lat;" + String(GPSDecoder._ManualLatLng.Latitude) + " Lng;"  + String(GPSDecoder._ManualLatLng.Longitude) + "\n");
}
//***********************************************************************************
// Return Current GPS Time Data
void AntennaManagerClass::AntennaComms_Command_GetGPSTime()
{
	GPSTime TempTime;
	GPSDecoder.GetGPSTime(&TempTime);
	
	Serial.print("^,");
	Serial.print(TempTime.Day); Serial.print(",");
	Serial.print(TempTime.Month);Serial.print(",");
	Serial.print(TempTime.Year);Serial.print(",");
	Serial.print(TempTime.Hr);Serial.print(",");
	Serial.print(TempTime.Min);Serial.print(",");
	Serial.print(TempTime.Sec);Serial.print(",");
	Serial.print(TempTime.TimeValid);Serial.print(",");
	Serial.print("c\n");
	
	
}
//***********************************************************************************
//Print Current Peaking Pattern Settings
void AntennaManagerClass::AntennaComms_Debug_PrintPeakingSettings()
{
	
	Serial.print("+, Command Print Peaking Settings. Peaking Mode;" + String(_CurrentConfig->PeakingMode) + "\n");
	
	switch(_CurrentConfig->PeakingMode)
	{
		case _PeakingModeDynamic: _PeakingPatternDynamicDelta.PrintSettings(); break;
		
		default: Serial.print("+,Standard Peaking Mode. See Config file \n");
	}
}


//**********************************************************************************//**********************************************************************************
//**********************************************************************************//**********************************************************************************
