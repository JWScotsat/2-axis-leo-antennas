#include "AntennaManager.h"

//**********************************************************************************
// Get Current Positions of Motors relative to the sky
void AntennaManagerClass::GetCurrentSkyMotorPositions()
{
	float AzEncoderPos = MotorController._AntMotorAz.getAxisPos();
	float ElEncoderPos = MotorController._AntMotorEl.getAxisPos();
	
	_TargetAdjustments.CalculateUncorrectedCurrentPosition(AzEncoderPos, ElEncoderPos, _CorrectedMotorTargetPos, _NewSkyPos, _CurrentSkyMotorsPos, _SatPoint.getSatAzPos());
	
}



//IMU Frame Get Latest Yaw/Pitch/Roll
void AntennaManagerClass::FrameIMUUpdate(float *LatestVals)
{
	//Frame IMU Read - get latest Yaw Pitch Roll
	_YEIFrame.getEularAngles();
	
	float IMUYaw = 0.0;
	
	
	//if(_IMUStabilityChoice == IMU_Frame)
	//Running Averages for Frame value results
	_FramePitchAvg.addValue(-1.0*_YEIFrame._Pitch);
	_FrameRollAvg.addValue(-1.0*_YEIFrame._Roll);
	IMUYaw = _YEIFrame._Yaw;

	// IMU Data store - Yaw Pitch Roll - Values reversed
	float FrameIMUYawPitchRoll[3] = {IMUYaw,_FramePitchAvg.getCurrentAvg(),_FrameRollAvg.getCurrentAvg()};
	
	if(_CurrentConfig->_FrameIMUEnabled)
	StndFncs.SetEquals(FrameIMUYawPitchRoll,LatestVals,3);
}


//***********************************************************************************
//IMU Yaw onboard GYRO code
// Uses complimentary filter with input Compass, Encoder and IMU Yaw values
// Returns estimated Compass Heading filtered
float AntennaManagerClass::OnboardGyroHeadingFiltered()
{
	static unsigned long validHeadingTimer = millis();//Timer for allowing a brief period of time with gryo data without lock
	static float IMUPreviousYaw = 0.0; //holder of previous IMU yaw value for getting IMU yaw delta on updates
	static float previousAzEncoder = 0.0; //holder of previous az encoder value for getting encover delta on updates
	static float yawOffset = 0.0;
	static boolean validHeading = false; //flag for valid heading data
	static float AntennaYaw = 0.0;
	
	static boolean firstTrack = true; //First time getting heading with lock during wide search
	static float azFirstPeakOffset = 0.0; //offset from sat producing heading
	
	float currentAzEncoder = 0.0; //updated current encoder value

	if(_CurrentAntennaMode == AntennaModeSearching)
	firstTrack = true;
	
	//Decision logic for when IMU data is usable
	if(_CurrentAntennaMode == AntennaModePeaking)
	{
		if(firstTrack)
		{
			azFirstPeakOffset = _TargetAdjustments.getOffsetFromSat();
			_EstimatedHeading = _TargetAdjustments.getOffsetFromSat();
			IMUTrackedHeading = _TargetAdjustments.getOffsetFromSat();
			_HeadingAvg.setValue(_TargetAdjustments.getOffsetFromSat());
			firstTrack = false;
			yawOffset = AntennaYaw;
		}
		
		
		validHeading = true;
		validHeadingTimer = millis();
	}
	else
	{
		if(millis() - validHeadingTimer > 4*1000UL && validHeading == true)
		{
			_LastGoodHeading = _HeadingAvg.getCurrentAvg();
			validHeading = false;
		}
	}
	
	//Only update heading imu data when IMU has refreshed
	if(MotorController._AntMotorCross._imuSensor->_isNewData)
	{
		_HeadingAvg.addValue(_EstimatedHeading);
		
		IMUYawPitchRoll[Yaw] = StndFncs.Bound(360 - MotorController._AntMotorCross._imuSensor->_Yaw, -0.01, 360.01); //get latest imu yaw value in 0-360 range
		currentAzEncoder = MotorController._AntMotorAz.getAxisPos(); // get latest az encoder value
		float deltaIMUYaw = IMUYawPitchRoll[Yaw] - IMUPreviousYaw; // difference of imu from last update - shows current movement experienced
		float deltaAzEncoder = currentAzEncoder - previousAzEncoder;  // difference of encoder from last update - shows intended current azimuth movement
		
		//update previous values with latest values for next loop
		previousAzEncoder = currentAzEncoder;
		IMUPreviousYaw = IMUYawPitchRoll[Yaw];
		
		// Fix for crossing the 0-360 boundaries
		if(deltaIMUYaw > 180.01)
		deltaIMUYaw -= 360.0;
		if(deltaIMUYaw < -180.01)
		deltaIMUYaw += 360.0;
		
		if(deltaAzEncoder > 180.01)
		deltaAzEncoder -= 360.0;
		if(deltaAzEncoder < -180.01)
		deltaAzEncoder += 360.0;
		
		//Combine IMU and Encoder data to show only the "unintended" movement of the IMU not due to motor movement
		IMUTrackedHeading += deltaIMUYaw - deltaAzEncoder;
		
		IMUTrackedHeading = StndFncs.Bound(IMUTrackedHeading, -0.01, 360.01);
		

		
		
		//If heading is considered valid then use high trust value for imu data in complimentary filter with a running average - only works under peaking
		if(validHeading == true)
		{
			const float k = 0.9999000000;
			//IMUTrackedHeading = (IMUTrackedHeading*(k)) + (_HeadingAvg.getCurrentAvg()*(1.0-k));
			
			//get difference in two heading values incase of 360 boundary cross over
			float headingdifference = IMUTrackedHeading - _TargetAdjustments.getOffsetFromSat();
			
			//Handling when two headings cross 360 boundary at different times
			if(abs(headingdifference) > 180.0)
			{
				float TargetHeading = _TargetAdjustments.getOffsetFromSat();
				
				if(TargetHeading > 180.0 && IMUTrackedHeading < 180.0)
				TargetHeading -= 360.0;
				
				if(IMUTrackedHeading > 180.0 && TargetHeading < 180.0)
				TargetHeading += 360.0;
				
				IMUTrackedHeading = (IMUTrackedHeading*(k)) + (TargetHeading*(1.0-k));
			}else // normal tracking when values are in similar boundary positions
			IMUTrackedHeading = (IMUTrackedHeading*(k)) + (_TargetAdjustments.getOffsetFromSat()*(1.0-k));
			
			IMUTrackedHeading = StndFncs.Bound(IMUTrackedHeading, -0.01, 360.01);

		}
		//otherwise use last known good heading to try and hold the imu drift and allow for heading tracking for a brief period without peaking
		else
		{
			const float k = 0.99900;
			//get difference in two heading values incase of 360 boundary cross over
			float headingdifference = IMUTrackedHeading - _LastGoodHeading;
			
			//Handling when two headings cross 360 boundary at different times
			if(abs(headingdifference) > 180.0)
			{
				float TargetHeading = _LastGoodHeading;
				
				if(TargetHeading > 180.0 && IMUTrackedHeading < 180.0)
				TargetHeading -= 360.0;
				
				if(IMUTrackedHeading > 180.0 && TargetHeading < 180.0)
				TargetHeading += 360.0;
				
				IMUTrackedHeading = (IMUTrackedHeading*(k)) + (TargetHeading*(1.0-k));
			}else // normal tracking when values are in similar boundary positions
			IMUTrackedHeading = (IMUTrackedHeading*(k)) + (_LastGoodHeading*(1.0-k));
			
			IMUTrackedHeading = StndFncs.Bound(IMUTrackedHeading, -0.01, 360.01);
		}
		
	}
	
	//Update latest heading
	if(validHeading == true || !firstTrack)
	{
		if(abs(IMUTrackedHeading - _EstimatedHeading) > 0.250)
		_EstimatedHeading = IMUTrackedHeading;
	}
	

	
	//float AntennaYaw = StndFncs.Bound(_EstimatedHeading - azFirstPeakOffset, -0.01, 360.01);
	AntennaYaw = StndFncs.Bound(_EstimatedHeading + _TargetAdjustments.getPeakingOffset() - _TargetAdjustments.getFirstOffset() + yawOffset, -0.01, 360.01);
	//AntennaYaw = StndFncs.Bound(_EstimatedHeading - _TargetAdjustments.getPeakingOffset(), -0.01, 360.01);
	
	return AntennaYaw;
}


//***********************************************************************************
//Motor Positional Check - check if we've reached the target position on all motors
// Returns result of checks
boolean AntennaManagerClass::CheckMotorsAreInPosition()
{
	return MotorController.isMotorsInPosition();
}
AntennaManagerClass AntennaManager;
//**********************************************************************************//**********************************************************************************
//**********************************************************************************//**********************************************************************************
