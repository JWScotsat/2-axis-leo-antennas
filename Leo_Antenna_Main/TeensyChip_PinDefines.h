// TeensyChip_PinDefines.h

#ifndef _TEENSYCHIP_PINDEFINES_h
#define _TEENSYCHIP_PINDEFINES_h

//			Pin				Arduino
#define		ChipPin_A4		33
#define		ChipPin_A5		24
#define		ChipPin_A12		3
#define		ChipPin_A13		4
#define		ChipPin_B0		16
#define		ChipPin_B1		17
#define		ChipPin_B2		19
#define		ChipPin_B3		18
#define		ChipPin_B16		0
#define		ChipPin_B17		1
#define		ChipPin_B18		32
#define		ChipPin_B19		25
#define		ChipPin_C0		15
#define		ChipPin_C1		22
#define		ChipPin_C2		23
#define		ChipPin_C3		9
#define		ChipPin_C4		10
#define		ChipPin_C5		13
#define		ChipPin_C6		11
#define		ChipPin_C7		12
#define		ChipPin_C8		28
#define		ChipPin_C9		27
#define		ChipPin_C10		29
#define		ChipPin_C11		30
#define		ChipPin_D0		2
#define		ChipPin_D1		14
#define		ChipPin_D2		7
#define		ChipPin_D3		8
#define		ChipPin_D4		6
#define		ChipPin_D5		20
#define		ChipPin_D6		21
#define		ChipPin_D7		5
#define		ChipPin_E0		31
#define		ChipPin_E1		26
#define		ChipPin_A0_DP	A10
#define		ChipPin_A0_DM	A11
#define		ChipPin_A1_DP	A12
#define		ChipPin_A1_DM	A13

#endif

