// AntennaManager.h

#ifndef _ANTENNAMANAGER_h
#define _ANTENNAMANAGER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "AntennaProject_Defines.h"

#include "Control Classes/Supporting Libs/StandardFunctions_ComplFilter.h"
#include "Control Classes/Supporting Libs/StandardFunctions_Array.h"
#include "Control Classes/Supporting Libs/StandardFunctions_RnAvg.h"

#include "EEPROMEx/EEPROMex.h"


#include "Control Classes/ControlPattern_Search.h"
#include "Control Classes/ControlPattern_SatPoint.h"
#include "Control Classes/ControlPattern_Peaking.h"
#include "Control Classes/ControlPattern_Peaking_AltDyDelta.h"
#include "Control Classes/ControlPattern_Peaking_AltStepTracking.h"
#include "Control Classes/ControlPattern_PeakingAltCircleGradient.h"

#include "Sensor Classes/Sensors_SatSignal.h"
#include "Sensor Classes/Sensors_GPSDecoder.h"
#include "Sensor Classes/Sensors_ScotsatGPSCompass.h"

#include "Motor Control/AntennaMotorManager.h"
#include "AntennaCalculations_TargetAdjustment.h"


#include "Sensor Classes/Supporting Libs/Comms_SerialExtended.h"



#include "HW_Pin.h"
#include "Storage_EEPROMConfig.h"

#include "LED Lib/RGBTools.h"


#define _DebugSpoofSignal false

class AntennaManagerClass
{
	protected:
	
	//*****************************************************************************
	//Main positional values
	
	float _SatSkyTarget[PosArraySize];  // Satellite Target Position Az El Pol
	float _CurrentSkyMotorsPos[PosArraySize]; // Antenna Current Position Az El Pol Uncorrected	float _NewSkyPos[PosArraySize]; // New Target Position Az El Pol
	float _CorrectedMotorTargetPos[PosArraySize]; // Corrected Target Position for Motors based on _NewSkyPos
	
	float _AntennaYaw = 0.0;
	
	//*****************************************************************************
	// Main setup for antenna subclasses and functions
	
	void setup_RetrieveStoredData();
	void setup_AntennaMotorManager();
	void setup_ControlPatterns();
	void setup_SubClasses();
	
	//*****************************************************************************
	//Main Initialisation Functionality
	
	//ErrorInitList _InitList;
	ErrorTracker _InitListArray[InitListSize];
	String _InitialisationPacket = "#,0,0,0,0,0,0,0,0,0,0,\n";
	boolean Initialise_CheckForErrors();
	void Intialise_PiReportError();
	void Initialise_PiReportComplete();
	void Initialise_WaitforPiResponse();
	void Initialise_UpdateInitPacket();
	void Initialise_UpdateFailedPacket();
	
	

	
	//*****************************************************************************
	// Control Pattern Functions. Called by AntennaModeControl
	
	int AntennaModeControl_SatPoint(int AntennaMode, float* NewSkyPos);
	int AntennaModeControl_Searching(int AntennaMode, float* NewSkyPos, float CurrentSkyPos[]);
	int AntennaModeControl_Peaking(int AntennaMode, float* NewSkyPos, int CurrentSignal);
	
	
	// Logic control for deciding if the SatPoint object is completed
	boolean _hasPointed;
	
	//Logic control for wide search during low CoG Speed
	boolean _isWideSearch = true;
	void _SearchHandleSearchSize(); //Handler for CoG logic effecting current search pattern
	
	//*****************************************************************************
	// GPS Object for reading and decoding GPS sentences received through Serial port
	
	Sensors_GPSDecoderClass GPSDecoder;
	GPSLatLng _CurrentGPS;

	//Scotsat GPS Compass Support
	Sensors_ScotsatGPSCompass GPSCompass;

	//*****************************************************************************
	// Main USB Serial Comms
	
	Comms_SerialExtendedClass USBSerial;
	PacketLayout USBPacketLayout = {':', '\n', ','};
	boolean _USBConnectionStatus = false;
	void AntennaComms_Command_NewManualPos();
	void AntennaComms_Command_ManualPeaking();
	void AntennaComms_Command_ManualSearch();
	void AntennaComms_Command_NewSatPoint();
	void AntennaComms_Command_AllStop();
	void AntennaComms_Command_PiInitialisation();
	void AntennaComms_Command_ManualGPS();
	void AntennaComms_Command_GetGPSTime();
	void AntennaComms_Advanced_PeakingSettings();
	void AntennaComms_Advanced_PeakingSettingsDynamic();
	void AntennaComms_Advanced_PeakingSettingsStepTracking();
	void AntennaComms_Advanced_SearchSettings();
	void AntennaComms_Advanced_MotorsLock();
	void AntennaComms_Advanced_MotorsDisable();
	void AntennaComms_Advanced_MiscSettings();
	void AntennaComms_Advanced_SatSigSettings();
	void AntennaComms_Advanced_TargetAdj();
	void AntennaComms_Advanced_IMUSettings();
	void AntennaComms_Advanced_ConfigAnySetting();
	void AntennaComms_Advanced_ConfigPrintSetting();

	void AntennaComms_Advanced_ConfigMotorSpeedSettings();

	//New stability switching command
	void AntennaComms_Advanced_SwitchStabilityMethod();
	//Antenna Motor Az PID Debug Testing
	void AntennaComms_Advanced_ConfigAzPID();

	//Output Comms
	
	void AntennaComms_Debug_PrintPeakingSettings();
	
	void AntennaComms_UpdatePi_MasterPacket();
	void AntennaComms_Debug_PrintOutputPacket(float *InputData);
	unsigned long _CommsLastMasterStamp;
	

	//Loop Debug Variables
	void AntennaComms_Debug_PrintLoopPacket(unsigned long loopTimer);

	unsigned long _counter = 0;
	unsigned long _LoopsRecorded = 0;
	unsigned long _HighestLoop = 0;
	unsigned long _LowestLoop = 0;
	unsigned long _TotalTime = 0;
	unsigned long _previousLoop = 0;
	boolean _LoopDebugEnable = false;
	boolean _MotorDebugPrinted = false;

	//Pi Acknowledge failed initialisation - USB Command
	boolean PIACKFailedInit = false;

	// Keep track of failed Init attempts
	int _InitCounter = 1;
	boolean _InitRunning = false;


	void AntennaComms_UpdatePi_MotorPacket();
	unsigned long _CommsLastMotorStamp;
	//*****************************************************************************
	// Target Adjustment usage

	float _CurrentCompassHeading = 0.0;
	void GetCurrentSkyMotorPositions();

	//*****************************************************************************
	//Signal reading for Modem going from searching to peaking
	Sensors_SatSignalClass SatSignalRecv1;
	Sensors_SatSignalClass SatSignalRecv2;
	int CheckModemLock();
	float _ModemSignalReading = 0.0;

	//*****************************************************************************

	//IMU Running Average Values
	StandardFunctions_RnAvgClass _FramePitchAvg;
	StandardFunctions_RnAvgClass _FrameRollAvg;

	//*****************************************************************************
	//IMU Data Retrieval

	void FrameIMUUpdate(float *LatestVals);

	//IMU Yaw Tracking
	float _DeltaIMUEncoder;
	float _HIPassIMU;
	unsigned long _IMUYawTimer;
	boolean _StabilityIMUGyroReset;
	float _GyroOffset = -0.00;
	
	StandardFunctions_RnAvgClass _HeadingAvg;
		

	//Complimentary Filter Compass-IMU
	StandardFunctions_ComplFilterClass _YawCompFilter;
	StandardFunctions_RnAvgClass _CompassHeadingRnAvg;

	float OnboardGyroHeadingFiltered();
	float _EstimatedHeading = 0.0;
	float IMUTrackedHeading = 0.0;
	float _LastGoodHeading = 0.0;
	
	float _RollCompAzResult = 0.0;
	float IMUYawPitchRoll[3] = {0.0,0.0,0.0};
		
	//*****************************************************************************
	//Stability settings swap over
	void newStabilitySettings();
	void oldStabilitySettings();
	void newPeakingOldStabilitySettings();

	//*****************************************************************************

	// Motor Control logic
	boolean CheckMotorsAreInPosition();

	//*****************************************************************************

	//Toggle Hardware Pin boolean stores
	HW_PinClass ModemTXMute;

	//RGB LED On-board for visual debugging
	RGBTools _RGBLED;

	//*****************************************************************************
	//Live Status Checking

	boolean StatusCheck();
	void Status_ErrorCondition(String ErrorMessage);
	
	
	//*****************************************************************************
	//Live Status Checking

	HW_PinClass LED_USB;
	HW_PinClass LED_Compass;
	
	//*****************************************************************************
	//Power Reset Pin handler
	HW_PinClass PM_Reset;
	

	//*****************************************************************************

public:
//*****************************************************************************
//Debug Usage

	String _ProjectCodeVersion = "V1.01";

	boolean _UseGPSCompass = false;
	int _GPSCompassBaud = 115200;
	boolean _DebugInitBypass = false;
	boolean _PrintDebug_USB = false;
	boolean _PrintDebug_CompassUSB = false;
	boolean _PrintDebug_Loop = false;
	boolean _isTesting = false;

	int _AntennaStatusWorking = 1;
	
	const int  IMU_Cross = 0;
	const int IMU_Frame = 1;
	int _IMUStabilityChoice = 1;

	//*****************************************************************************
	//Storage of Config Values
	Storage_EEPROMConfigClass _EEPROMConfig;
	Configuration *_CurrentConfig;


	//*****************************************************************************
	//Motor Class
	AntennaMotorManager MotorController; // Motor Manager for handling all motors and related sensors

	//*****************************************************************************
	//Supporting Classes

	StandardFunctions_Array StndFncs; // Standard Array function shortcuts

	AntennaCalculations_TargetAdjustmentClass _TargetAdjustments; // Mathematical functions for stabilisation for frame IMU changes

	// IMU Used on the Frame for Boat motion corrections
	YEISensor _YEIFrame;

	//*****************************************************************************
	//Control Pattern Classes

	#define _PeakingModeDynamic 1
	#define _PeakingModeStepTracking 2
	#define _PeakingModeCircGrad 3

	//Antenna Logical Control Functionality - decides between different states; pointing, peaking etc
	int AntennaModeControl(int AntennaMode, float CurrentSignal, float CurrentSkyPos[], float* NewSkyPos); // Main logical control for antenna functions pointing,searching,peaking etc.
	int _CurrentAntennaMode;

	// Control pattern classes for producing different logical patterns for antenna movement
	ControlPattern_SearchClass _SearchPattern; // Control Pattern - Search - produces the search pattern spiral
	ControlPattern_SatPointClass _SatPoint; // Control Pattern - Satellite Point - produces the Az El Pol position for given Satellite Details and GPS Position


	//Peaking Patterns & Alternatives
	ControlPattern_PeakingClass _PeakingPattern;
	ControlPattern_Peaking_AltDyDeltaClass _PeakingPatternDynamicDelta;
	ControlPattern_Peaking_AltStepTrackingClass _PeakingPatternStepTracking;
	ControlPattern_PeakingAltCircleGradientClass _PeakingPatternCircGrad;

	//*****************************************************************************
	//Sensor Data and Comms Storage

	SatelliteDetails _CurrentSat; // Struct for Satellite Details {Lng, Pol, Skew}

	float _CurrentGPSLatLng[2];

	boolean _AntennaModePaused; // AntennaModeControl bool for pausing current functionality

	float _StoreManualPosition[3]; // Serial input manual Az El Pol
	//*****************************************************************************


	void run(); // Main Run

	void setup(); // Object Setups
	void Initialise(); // Initialise axis sensors and motors

	boolean _isInitilisation_Passed = false;



	// Main handler of Antenna Commands through USB serial
	boolean AntennaComms_SerialCommandHandle();

	//DebugTimer
	unsigned long _debugCompassTimer;


};

extern AntennaManagerClass AntennaManager;

#endif

