#include "AntennaManager.h"

/*
Antenna Manager Setup
Created By: Jack Wells

Setup code for Antenna Manager. Retrieves stored config data from EEPROM and applies settings to all sub classes.

Returns: Nothing, no current checks for successful setup

*/


/* Main Antenna Setup
- Retrieve CurrentConfig from EEPROM.
- Setup all functionality for 3 Axis Antenna
- Returns: Nothing
*/
void AntennaManagerClass::setup()
{
	
	//Get EEPROM saved Config Data
	setup_RetrieveStoredData();
	
    //RGB LED for visual debugging
    _RGBLED.setup(LEDRedPin,LEDGreenPin,LEDBBluePin, COMMON_CATHODE);
    _RGBLED.setColor(LEDColour_White);

	// Motor Controller
	setup_AntennaMotorManager();
	
	//Setup subclasses for Antenna Control
	setup_SubClasses();

	_hasPointed = false;
	_AntennaModePaused = false;
	
	// Setup 12 bit ADC for analogue readings
	analogReadResolution(12);
	
	//Setup Control Patterns Pointing,Searching,Peaking for Antenna Positional Control
	setup_ControlPatterns();


	Serial.print("+,Antenna Manager Setup Complete\n");
}

//**********************************************************************************
//**********************************************************************************

/* Setup - Retreive Stored Data
- Retrieve CurrentConfig from EEPROM.
- Returns: Nothing
*/
void AntennaManagerClass::setup_RetrieveStoredData()
{
	Serial.print("+,Retreiving Stored Config Data\n");

	_EEPROMConfig.setup();
	
	_CurrentConfig = &_EEPROMConfig.CurrentConfig;
	
}

//**********************************************************************************
//**********************************************************************************

/* Setup - Motor Manager
- Apply EEPROM Config settings to Motor Manager
- Setup handles Motors and Sensors settings
- Returns: Nothing
*/
void AntennaManagerClass::setup_AntennaMotorManager()
{
	
	
	MotorController.setup();
	
	//Setup Antenna Motors Config Data - Adds things like Axis Trim from startup
	MotorController._AntMotorEl.setupConfigValues(_CurrentConfig->El_axisMoveReverse,_CurrentConfig->El_axisReadReverse, _CurrentConfig->El_axisTrim, _CurrentConfig->El_axisError,_CurrentConfig->El_axisEnable,_CurrentConfig->El_axisPowerDown,_CurrentConfig->El_axisMaxBoundary,_CurrentConfig->El_axisMinBoundary);
	MotorController._AntMotorAz.setupConfigValues(_CurrentConfig->Az_axisMoveReverse,_CurrentConfig->Az_axisReadReverse, _CurrentConfig->Az_axisTrim, _CurrentConfig->Az_axisError,_CurrentConfig->Az_axisEnable,_CurrentConfig->Az_axisPowerDown,_CurrentConfig->Az_axisMaxBoundary,_CurrentConfig->Az_axisMinBoundary);
	MotorController._AntMotorCross.setupConfigValues(_CurrentConfig->Cross_axisMoveReverse,_CurrentConfig->Cross_axisReadReverse, _CurrentConfig->Cross_axisTrim, _CurrentConfig->Cross_axisError,_CurrentConfig->Cross_axisEnable,_CurrentConfig->Cross_axisPowerDown,_CurrentConfig->Cross_axisMaxBoundary,_CurrentConfig->Cross_axisMinBoundary);
	MotorController._AntMotorPol.setupConfigValues(_CurrentConfig->Pol_axisMoveReverse,_CurrentConfig->Pol_axisReadReverse, _CurrentConfig->Pol_axisTrim, _CurrentConfig->Pol_axisError,_CurrentConfig->Pol_axisEnable,_CurrentConfig->Pol_axisPowerDown,_CurrentConfig->Pol_axisMaxBoundary,_CurrentConfig->Pol_axisMinBoundary);

	MotorController.setup_MotorStatusTimeouts(_CurrentConfig->_AzMotorTimeout,_CurrentConfig->_ElMotorTimeout, _CurrentConfig->_CrossMotorTimeout, _CurrentConfig->_PolMotorTimeout);
}

//**********************************************************************************
//**********************************************************************************

/* Setup - Control Pattern Classes
- Apply EEPROM Config settings to Control Patterns
- Initialise Control Pattern arrays with 0 values
- Returns: Nothing
*/
void AntennaManagerClass::setup_ControlPatterns()
{
	
	// Setup/Init the search pattern for initial values
	_SearchPattern.setupConfig(_CurrentConfig->_ElExpandingBoundaryDelta,
	_CurrentConfig->_AbsMaxPatternElevationBoundary,
	_CurrentConfig->SearchElevation_stepCountSize,
	_CurrentConfig->SearchElevation_axisMax,
	_CurrentConfig->SearchElevation_axisMin,
	_CurrentConfig->SearchElevation_AxisBoundaryRollOver,
	_CurrentConfig->SearchAzimuth_stepCountSize,
	_CurrentConfig->SearchAzimuth_maxCounterBoundary,
	_CurrentConfig->SearchAzimuth_axisMax,
	_CurrentConfig->SearchAzimuth_axisMin,
	_CurrentConfig->SearchAzimuth_AxisBoundaryRollOver,
	_CurrentConfig->_SearchPatternPointDelayTime);
	
	
	//Setup/Init Satellite Pointing
	_SatPoint.setup();
	
	//Setup/Init Peaking Pattern
	_PeakingPattern.Setup(_CurrentConfig->DeltaAz,_CurrentConfig->DeltaEl,_CurrentConfig->KAz,_CurrentConfig->KEl,_CurrentConfig->PeakingLockThreshold);
	_PeakingPattern._PointDelay = _CurrentConfig->_PeakingPatternPointDelayTime;
	
	_PeakingPatternDynamicDelta.Setup(_CurrentConfig->_DyDeltaAzStart,_CurrentConfig->_DyDeltaElStart,_CurrentConfig->_DyCentreKAz,_CurrentConfig->_DyCentreKEl,
	_CurrentConfig->_DyPeakingLockThreshold, _CurrentConfig->_DyDeltaAzMax, _CurrentConfig->_DyDeltaElMax, _CurrentConfig->_DyKEl, _CurrentConfig->_DyKAz, _CurrentConfig->_DySigStrOffset,
	_CurrentConfig->_AutoDyOffset);
	_PeakingPatternDynamicDelta._PointDelayMin = _CurrentConfig->_DyPeakingPatternPointDelayTime;
	
	_PeakingPatternStepTracking.Setup(_CurrentConfig->DeltaEl, _CurrentConfig->DeltaAz, _CurrentConfig->PeakingLockThreshold, 30);
	_PeakingPatternStepTracking._PointDelay = _CurrentConfig->_PeakingPatternPointDelayTime;
	
	_PeakingPatternCircGrad.Setup(_CurrentConfig->DeltaAz,_CurrentConfig->DeltaEl,_CurrentConfig->KAz,_CurrentConfig->KEl,_CurrentConfig->_DyPeakingLockThreshold);
	_PeakingPatternCircGrad._PointDelay = _CurrentConfig->_PeakingPatternPointDelayTime;
	
	// Clear initial values.
	StndFncs.Clear(_SatSkyTarget, PosArraySize);
	StndFncs.Clear(_CurrentSkyMotorsPos, PosArraySize);
	StndFncs.Clear(_NewSkyPos, PosArraySize);
	
	Serial.print("+,Antenna Manager Setup - Control Patterns Complete\n");
}

//**********************************************************************************
//**********************************************************************************

/* Setup - Control Pattern Classes
- Apply EEPROM Config settings to further sub classes
- Returns: Nothing
*/
void AntennaManagerClass::setup_SubClasses()
{
	//Setup SerialUSB for Packet Data transfer
	USBSerial.setup(USBPacketLayout,10);
	
	//Hardware Pin Setups
	PM_Reset.setup(PM_Input1);
	
	//GPS Compass Selection - 1 - GPS Compass - 0 - Standard GPS Module
	_UseGPSCompass = _CurrentConfig->UseGPSCompass;
	
	//Setup GPS Object
	if(_UseGPSCompass)
	GPSCompass.setup(&GPSSerial,_CurrentConfig->GPSCompassBaud, _CurrentConfig->_GPSCompassDir);
	else
	GPSDecoder.setup(&GPSSerial,GPSSerialBaud);
	
	//Setup Frame IMU
	_YEIFrame.begin(&ImuFrameSerial, ImuBaudRate, "Frame Sensor");
	_YEIFrame.setupConfig(_CurrentConfig->Frame_PitchTrim,_CurrentConfig->Frame_RollTrim,_CurrentConfig->Frame_YEIUpdateRate);
	
	//Compensation Data Averaging
	_FramePitchAvg.setup(_CurrentConfig->_FramePitchNumbAvg);
	_FrameRollAvg.setup(_CurrentConfig->_FrameRollNumbAvg);
	
	_FramePitchAvg._ValueBuffer = _CurrentConfig->_FramePitchDefaultBuffer;
	_FrameRollAvg._ValueBuffer = _CurrentConfig->_FrameRollDefaultBuffer;
	
	//Compass Data Filtering
	_YawCompFilter.setup(_CurrentConfig->KGyroCompFilter);
	_CompassHeadingRnAvg.setup(20);
	_HeadingAvg.setup(40);
	
	//Add Config Data to Cross Sensor
	MotorController._YEICross.setupConfig(_CurrentConfig->Cross_PitchTrim,_CurrentConfig->Cross_RollTrim,_CurrentConfig->Cross_YEIUpdateRate);
	
	//New IMU filter mode setup - TODO: Make settings adjustable as part of the current config
	AntennaManager._YEIFrame._filterMode = 4;
	AntennaManager._YEIFrame._YEIUpdateRate = 2;
	
	AntennaManager.MotorController._AntMotorCross._imuSensor->_filterMode = 4;
	AntennaManager.MotorController._AntMotorCross._imuSensor->_YEIUpdateRate = 2;
	
	
	//Setup Sat Signal analog reading object
	SatSignalRecv1.setup(ModemSigStrPin,_CurrentConfig->_LockThreshold,_CurrentConfig->_NumberofReadAverages);
	SatSignalRecv2.setup(ModemSigStrPin2,_CurrentConfig->_LockThreshold,_CurrentConfig->_NumberofReadAverages);
	
	SatSignalRecv1.setup(_CurrentConfig->_useDBviaUSB, _CurrentConfig->_DBThreshold);
	SatSignalRecv2.setup(_CurrentConfig->_useDBviaUSB, _CurrentConfig->_DBThreshold);
	
	
	
	Serial.print("+,Antenna Manager Setup - Comms SubClasses Complete\n");
	
}


//************************************************************/\************************************************************\\