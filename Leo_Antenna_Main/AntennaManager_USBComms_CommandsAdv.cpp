#include "AntennaManager.h"

//********************************************************************
//Comms Advanced Commands
// -Effects config and object settings

//Peaking Settings
void AntennaManagerClass::AntennaComms_Advanced_PeakingSettings()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,New Peaking Settings Received\n");

	const int CommandExpectedNoValues = 11;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{

		if(USBSerial.parseInt(1) != 999)
		_PeakingPattern._PointDelay = USBSerial.parseInt(1);
		_CurrentConfig->_PeakingPatternPointDelayTime = _PeakingPattern._PointDelay;
		
		if(USBSerial.parseInt(2) != 999)
		_PeakingPattern._ElStepSize = USBSerial.parseFloat(2);
		_CurrentConfig->DeltaEl = _PeakingPattern._ElStepSize;
		
		if(USBSerial.parseInt(3)  != 999)
		_PeakingPattern._AzStepSize = USBSerial.parseFloat(3);
		_CurrentConfig->DeltaAz = _PeakingPattern._AzStepSize;
		
		if(USBSerial.parseInt(4) != 999)
		_PeakingPattern._KEl = USBSerial.parseFloat(4);
		_CurrentConfig->KEl = _PeakingPattern._KEl;
		
		if(USBSerial.parseInt(5) != 999)
		_PeakingPattern._KAz = USBSerial.parseFloat(5);
		_CurrentConfig->KAz = _PeakingPattern._KAz;
		
		if(USBSerial.parseInt(6) != 999)
		_PeakingPattern._isConstantPeaking = USBSerial.parseInt(6);
		
		if(USBSerial.parseInt(7) != 999)
		_PeakingPattern._isStopOnPeaked = USBSerial.parseInt(7);
		_CurrentConfig->PeakingPauseOnUpperThreshold = _PeakingPattern._isStopOnPeaked;
		
		if(USBSerial.parseInt(8)!= 999)
		_PeakingPattern._PeakedUpperThreshold = USBSerial.parseInt(8);
		_CurrentConfig->PeakingPauseThreshold = _PeakingPattern._PeakedUpperThreshold;
		
		if(USBSerial.parseInt(9) != 999)
		_PeakingPattern._signalLostThreshold = USBSerial.parseInt(9);
		_CurrentConfig->PeakingLockThreshold = _PeakingPattern._signalLostThreshold;
		
		if(USBSerial.parseInt(10) != 999)
		_PeakingPattern._PeakedLowerThreshold = USBSerial.parseInt(10);
		_CurrentConfig->PeakingUnPauseThreshold = _PeakingPattern._PeakedLowerThreshold;
	}else
	{
		Serial.println("@,USB Comms - New Peaking Settings Invalid No. Values. Expected 11, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}
//***********************************************************************************



//Peaking Settings
void AntennaManagerClass::AntennaComms_Advanced_PeakingSettingsDynamic()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,New Dynamic Peaking Settings Received\n");
	
	const int CommandExpectedNoValues = 12;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{

		if(USBSerial.parseInt(1)  != 999)
		_PeakingPatternDynamicDelta._DeltaElMax = USBSerial.parseFloat(1);
		_CurrentConfig->_DyDeltaElMax = _PeakingPatternDynamicDelta._DeltaElMax;
		
		if(USBSerial.parseInt(2) != 999)
		_PeakingPatternDynamicDelta._DeltaAzMax = USBSerial.parseFloat(2);
		_CurrentConfig->_DyDeltaAzMax = _PeakingPatternDynamicDelta._DeltaAzMax;
		
		if(USBSerial.parseInt(3) != 999)
		_PeakingPatternDynamicDelta._ElStepSize = USBSerial.parseFloat(3);
		_CurrentConfig->_DyDeltaElStart = _PeakingPatternDynamicDelta._ElStepSize;
		
		if(USBSerial.parseInt(4) != 999)
		_PeakingPatternDynamicDelta._AzStepSize = USBSerial.parseFloat(4);
		_CurrentConfig->_DyDeltaAzStart = _PeakingPatternDynamicDelta._AzStepSize;
		
		if(USBSerial.parseInt(5) != 999)
		_PeakingPatternDynamicDelta._DyKEl = USBSerial.parseFloat(5);
		_CurrentConfig->_DyKEl = _PeakingPatternDynamicDelta._DyKEl;
		
		if(USBSerial.parseInt(6) != 999)
		_PeakingPatternDynamicDelta._DyKAz = USBSerial.parseFloat(6);
		_CurrentConfig->_DyKAz = _PeakingPatternDynamicDelta._DyKAz;
		
		if(USBSerial.parseInt(7) != 999)
		_PeakingPatternDynamicDelta._DySigStrOffset = USBSerial.parseFloat(7);
		_CurrentConfig->_DySigStrOffset = _PeakingPatternDynamicDelta._DySigStrOffset;
		
		if(USBSerial.parseInt(8) != 999)
		_PeakingPatternDynamicDelta._AutoDyOffset = USBSerial.parseInt(8);
		_CurrentConfig->_AutoDyOffset = _PeakingPatternDynamicDelta._AutoDyOffset;
		
		if(USBSerial.parseInt(9) != 999)
		_PeakingPatternDynamicDelta._KEl = USBSerial.parseFloat(9);
		_CurrentConfig->_DyCentreKEl = _PeakingPatternDynamicDelta._KEl;
		
		if(USBSerial.parseInt(10) != 999)
		_PeakingPatternDynamicDelta._KAz = USBSerial.parseFloat(10);
		_CurrentConfig->_DyCentreKAz = _PeakingPatternDynamicDelta._KAz;
		
		if(USBSerial.parseInt(11) != 999)
		_PeakingPatternDynamicDelta._signalLostThreshold = USBSerial.parseInt(11);
		_CurrentConfig->_DyPeakingLockThreshold = _PeakingPatternDynamicDelta._signalLostThreshold;
		
		if(USBSerial.parseInt(12) != 999)
		_PeakingPatternDynamicDelta._PointDelayMin = USBSerial.parseInt(12);
		_CurrentConfig->_DyPeakingPatternPointDelayTime = _PeakingPatternDynamicDelta._PointDelayMin;
		
		_CurrentConfig->PeakingMode = _PeakingModeDynamic;
	}else
	{
		Serial.println("@,USB Comms - New Dyanmic Peaking Settings Invalid No. Values. Expected 12, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}

//***********************************************************************************



//Peaking Settings
void AntennaManagerClass::AntennaComms_Advanced_PeakingSettingsStepTracking()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,New Step Tracking Peaking Settings Received\n");
	
	const int CommandExpectedNoValues = 7;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{

		if(USBSerial.parseInt(1) != 999)
		_PeakingPatternStepTracking._PointDelay = USBSerial.parseInt(1);

		
		if(USBSerial.parseInt(2) != 999)
		_PeakingPatternStepTracking._DeltaEl = USBSerial.parseFloat(2);

		
		if(USBSerial.parseInt(3)  != 999)
		_PeakingPatternStepTracking._DeltaAz = USBSerial.parseFloat(3);
		
		
		if(USBSerial.parseInt(4) != 999)
		_PeakingPatternStepTracking._signalLostThreshold = USBSerial.parseInt(4);

		
		if(USBSerial.parseInt(5) != 999)
		_PeakingPatternStepTracking._PointDelay = USBSerial.parseInt(5);

		if(USBSerial.parseInt(6) != 999)
		_PeakingPatternStepTracking._NewSigBufferError = USBSerial.parseInt(6);
		
		_CurrentConfig->PeakingMode = _PeakingModeStepTracking;

	}else
	{
		Serial.println("@,USB Comms - New Step Peaking Settings Invalid No. Values. Expected 7, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}


//***********************************************************************************

void AntennaManagerClass::AntennaComms_Advanced_SearchSettings()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Search Settings Received\n");
	const int CommandExpectedNoValues = 5;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{


		if(USBSerial.parseInt(1) != 999)
		_SearchPattern._ElExpandingBoundaryFactor = USBSerial.parseFloat(1);
		_CurrentConfig->_ElExpandingBoundaryDelta = _SearchPattern._ElExpandingBoundaryFactor;
		
		if(USBSerial.parseInt(2) != 999)
		_SearchPattern.SearchAzimuth._maxCounterBoundary = USBSerial.parseFloat(2);
		_CurrentConfig->SearchAzimuth_maxCounterBoundary = _SearchPattern.SearchAzimuth._maxCounterBoundary;
		
		if(USBSerial.parseInt(3) != 999)
		_SearchPattern.SearchAzimuth._stepCountSize = USBSerial.parseFloat(3);
		_CurrentConfig->SearchAzimuth_stepCountSize = _SearchPattern.SearchAzimuth._stepCountSize;
		
		if(USBSerial.parseInt(4) != 999)
		SatSignalRecv1._LockThreshold = USBSerial.parseInt(4);
		_CurrentConfig->_LockThreshold = SatSignalRecv1._LockThreshold;

	}else
	{
		Serial.println("@,USB Comms - New Search Settings Invalid No. Values. Expected 5, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}
//***********************************************************************************

void AntennaManagerClass::AntennaComms_Advanced_MotorsLock()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,Locking Motors\n");
	
	const int CommandExpectedNoValues = 5;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{

		if(USBSerial.parseInt(1) != 999)
		MotorController._AntMotorEl._axisEnable = !(boolean)USBSerial.parseInt(1);
		
		if(USBSerial.parseInt(2) != 999)
		MotorController._AntMotorAz._axisEnable = !(boolean)USBSerial.parseInt(2);
		
		if(USBSerial.parseInt(3) != 999)
		MotorController._AntMotorCross._axisEnable = !(boolean)USBSerial.parseInt(3);
		
		if(USBSerial.parseInt(4) != 999)
		MotorController._AntMotorPol._axisEnable = !(boolean)USBSerial.parseInt(4);
		
		if(!MotorController._AntMotorPol._axisEnable)
		MotorController._AntMotorPol._DCMotor->Disable();

	}else
	{
		Serial.println("@,USB Comms - Locking Motors Invalid No. Values. Expected 5, Received-" + String(USBSerial._FinalSeperatorCount));
	}
	
}
//***********************************************************************************

void AntennaManagerClass::AntennaComms_Advanced_MotorsDisable()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,Disabling Motors\n");
	
	const int CommandExpectedNoValues = 5;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{

		if(USBSerial.parseInt(1) != 999)
		MotorController._AntMotorEl.motorPowerControl((boolean)USBSerial.parseInt(1));
		
		if(USBSerial.parseInt(2) != 999)
		MotorController._AntMotorAz.motorPowerControl((boolean)USBSerial.parseInt(2));
		
		if(USBSerial.parseInt(3) != 999)
		MotorController._AntMotorCross.motorPowerControl((boolean)USBSerial.parseInt(3));
		
		if(USBSerial.parseInt(4) != 999)
		MotorController._AntMotorPol.motorPowerControl((boolean)USBSerial.parseInt(4));
		
		if(!MotorController._AntMotorPol._axisPowerDown)
		MotorController._AntMotorPol._DCMotor->Disable();

	}else
	{
		Serial.println("@,USB Comms - Disable Motors Invalid No. Values. Expected 5, Received-" + String(USBSerial._FinalSeperatorCount));
	}
	
}
//***********************************************************************************
//Command 200 - Misc Settings
void AntennaManagerClass:: AntennaComms_Advanced_MiscSettings()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Misc Settings Received\n");
	
	const int CommandExpectedNoValues = 11;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		// IMU, Encoder buffers
		if(abs(USBSerial.parseInt(1)) != 999)//New El Axis Buffer/Error
		MotorController._AntMotorEl._axisError = USBSerial.parseFloat(1);
		_CurrentConfig->El_axisError = MotorController._AntMotorEl._axisError;
		
		if(abs(USBSerial.parseInt(2)) != 999)//New Cross Axis Buffer/Error
		MotorController._AntMotorCross._axisError = USBSerial.parseFloat(2);
		_CurrentConfig->Cross_axisError = MotorController._AntMotorCross._axisError;
		
		if(abs(USBSerial.parseInt(3)) != 999)//New Az Axis Buffer/Error
		MotorController._AntMotorAz._axisError = USBSerial.parseFloat(3);
		_CurrentConfig->Az_axisError = MotorController._AntMotorAz._axisError;

		// El, Cross, Roll Triming values for IMUs and Encoder
		
		if(abs(USBSerial.parseInt(4)) != 999)//New Cross Axis Trim
		MotorController._AntMotorCross._axisTrim = USBSerial.parseFloat(4);
		_CurrentConfig->Cross_axisTrim = MotorController._AntMotorCross._axisTrim;
		
		if(abs(USBSerial.parseInt(5)) != 999)//New Frame IMU Roll Trim
		_YEIFrame._RollTrim = USBSerial.parseFloat(5);
		_CurrentConfig->Frame_RollTrim = _YEIFrame._RollTrim;

		if(abs(USBSerial.parseInt(6)) != 999)//New Frame IMU Pitch Trim
		_YEIFrame._PitchTrim = USBSerial.parseFloat(6);
		_CurrentConfig->Frame_PitchTrim = _YEIFrame._PitchTrim;
		
		if(abs(USBSerial.parseInt(7)) != 999)//New El Axis Trim
		MotorController._AntMotorEl._axisTrim = USBSerial.parseFloat(7);
		_CurrentConfig->El_axisTrim = MotorController._AntMotorEl._axisTrim;
		//
		// 	// El, Az Weighting Factors for Frame IMU
		// 	if((int)ReceivedCommandPacket[i] != 999 && ReceivedCommandPacket[i] >= 0.00001 )//Send Elevation Pitch Weighting Factor
		// 	Advanced_Sensor_Settings.ElAdjustmentWeighting = ReceivedCommandPacket[i];
		//
		// 	i++;
		//
		// 	if((int)ReceivedCommandPacket[i] != 999 && ReceivedCommandPacket[i] >= 0.00001)//Send Az Roll Weighting Factor
		// 	Advanced_Sensor_Settings.AzRollWeighting = ReceivedCommandPacket[i];
		//
		// 	i++;
		//
		// 	if((int)ReceivedCommandPacket[i] != 999 && ReceivedCommandPacket[i] >= 0.00001 )//Send Az Pitch Weighting Factor
		// 	Advanced_Sensor_Settings.AzPitchWeighting = ReceivedCommandPacket[i];
		//
		// 	i++;
		
		//Elevation Max and Min Limit Positions
		if(abs(USBSerial.parseInt(8)) != 999)//New El Top Boundary
		MotorController._AntMotorEl._axisMaxBoundary = USBSerial.parseFloat(8);
		_CurrentConfig->El_axisMaxBoundary = MotorController._AntMotorEl._axisMaxBoundary;
		
		if(abs(USBSerial.parseInt(9)) != 999)//New El Lower Boundary
		MotorController._AntMotorEl._axisMinBoundary = USBSerial.parseFloat(9);
		_CurrentConfig->El_axisMinBoundary = MotorController._AntMotorEl._axisMinBoundary;

		if(abs(USBSerial.parseInt(10)) != 999)//New Code Loop Update Time
		_CurrentConfig->_LoopUpdateTime = USBSerial.parseInt(10);

	}else
	{
		Serial.println("@,USB Comms - Misc Settings Invalid No. Values. Expected 11, Received-" + String(USBSerial._FinalSeperatorCount));
	}
	

}
//***********************************************************************************

void AntennaManagerClass::AntennaComms_Advanced_SatSigSettings()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - SatModem Sig Settings Received\n");
	
	const int CommandExpectedNoValues = 3;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		if(USBSerial.parseInt(1) != 999)
		_CurrentConfig->_LockThreshold = USBSerial.parseInt(1);
		
		if(USBSerial.parseInt(2) != 999)
		_CurrentConfig->_NumberofReadAverages = USBSerial.parseInt(2);
		
		SatSignalRecv1._LockThreshold = _CurrentConfig->_LockThreshold;
		SatSignalRecv1._NumberofReadAverages = _CurrentConfig->_NumberofReadAverages;
	}else
	{
		Serial.println("@,USB Comms - Sat Sig Settings Invalid No. Values. Expected 3, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}
//***********************************************************************************

void AntennaManagerClass::AntennaComms_Advanced_TargetAdj()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Target Adjustment Settings Received\n");
	
	const int CommandExpectedNoValues = 7;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		if(USBSerial.parseInt(1) != 999)
		_CurrentConfig->ElTargetAdjustmentDampening = USBSerial.parseFloat(1);
		
		if(USBSerial.parseInt(2) != 999)
		_CurrentConfig->ElPitchRollCompBuffer = USBSerial.parseFloat(2);
		
		if(USBSerial.parseInt(3) != 999)
		_CurrentConfig->_FramePitchNumbAvg = USBSerial.parseInt(3);
		
		if(USBSerial.parseInt(4) != 999)
		_CurrentConfig->_FrameRollNumbAvg = USBSerial.parseInt(4);
		
		if(USBSerial.parseInt(5) != 999)
		_CurrentConfig->_FramePitchDefaultBuffer = USBSerial.parseFloat(5);
		
		if(USBSerial.parseInt(6) != 999)
		_CurrentConfig->_FrameRollDefaultBuffer = USBSerial.parseFloat(6);
		
		_FramePitchAvg._NumberOfAverages = _CurrentConfig->_FramePitchNumbAvg;
		_FrameRollAvg._NumberOfAverages = _CurrentConfig->_FrameRollNumbAvg;
		
		_FramePitchAvg._ValueBuffer = _CurrentConfig->_FramePitchDefaultBuffer;
		_FrameRollAvg._ValueBuffer = _CurrentConfig->_FrameRollDefaultBuffer;
		
	}else
	{
		Serial.println("@,USB Comms - Target Adjustment Settings Invalid No. Values. Expected 7, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}
//***********************************************************************************

void AntennaManagerClass:: AntennaComms_Advanced_IMUSettings()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - IMU Settings Received\n");
	
	const int CommandExpectedNoValues = 8;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		if(USBSerial.parseInt(1) != 999)
		_CurrentConfig->_FrameIMUEnabled = USBSerial.parseInt(1);
		
		if(USBSerial.parseInt(2) != 999)
		_CurrentConfig->Frame_RollTrim = USBSerial.parseFloat(2);
		
		if(USBSerial.parseInt(3) != 999)
		_CurrentConfig->Frame_PitchTrim = USBSerial.parseFloat(3);
		
		if(USBSerial.parseInt(4) != 999)
		_CurrentConfig->Frame_YEIUpdateRate = USBSerial.parseInt(4);
		
		if(USBSerial.parseInt(5) != 999)
		_CurrentConfig->Cross_RollTrim = USBSerial.parseFloat(5);
		
		if(USBSerial.parseInt(6) != 999)
		_CurrentConfig->Cross_PitchTrim = USBSerial.parseFloat(6);
		
		if(USBSerial.parseInt(7) != 999)
		_CurrentConfig->Cross_YEIUpdateRate = USBSerial.parseInt(7);
		
		_YEIFrame.setupConfig(_CurrentConfig->Frame_PitchTrim, _CurrentConfig->Frame_RollTrim, _CurrentConfig->Frame_YEIUpdateRate);
		MotorController._YEICross.setupConfig(_CurrentConfig->Cross_PitchTrim, _CurrentConfig->Cross_RollTrim, _CurrentConfig->Cross_YEIUpdateRate);
	}else
	{
		Serial.println("@,USB Comms -IMU Settings Invalid No. Values. Expected 8, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}


//**********************************************************************************
void AntennaManagerClass:: AntennaComms_Advanced_ConfigAnySetting()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Adjust Config Value\n");
	
	const int CommandExpectedNoValues = 4;

	if(USBSerial._FinalSeperatorCount >= CommandExpectedNoValues)
	{
		int ConfigIndex = USBSerial.parseInt(PacketPosition1);

		boolean ConfigIndexValid = true;

		switch(ConfigIndex)
		{

			//TODO: Finish adding functions
			case 0: _CurrentConfig->PeakingMode = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - Peaking Mode  Changed to; " + String(_CurrentConfig->PeakingMode)); break;

			case 1: _CurrentConfig->DeltaEl = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - Peaking Delta El  Changed to; " + String(_CurrentConfig->DeltaEl)); break;
			case 2: _CurrentConfig->DeltaAz = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - Peaking Delta Az  Changed to; " + String(_CurrentConfig->DeltaAz)); _PeakingPatternCircGrad._CircRadi = _CurrentConfig->DeltaAz; break;
			case 3: _CurrentConfig->KEl = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - Peaking KEl  Changed to; " + String(_CurrentConfig->KEl)); _PeakingPatternCircGrad._KEl =_CurrentConfig->KEl; break;
			case 4: _CurrentConfig->KAz = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - Peaking KAz  Changed to; " + String(_CurrentConfig->KAz)); _PeakingPatternCircGrad._KAz =_CurrentConfig->KAz; break;
			case 5: _CurrentConfig->PeakingLockThreshold = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - Peaking Lock Threshold  Changed to; " + String(_CurrentConfig->PeakingLockThreshold)); break;
			case 6: _CurrentConfig->PeakingPauseOnUpperThreshold = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println("+,USB Comms - PeakingPauseOnUpperThreshold  Changed to; " + String(_CurrentConfig->PeakingPauseOnUpperThreshold)); break;
			case 7: _CurrentConfig->PeakingPauseThreshold = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - PeakingPauseThreshold  Changed to; ") + String(_CurrentConfig->PeakingPauseThreshold)); break;
			case 8: _CurrentConfig->PeakingUnPauseThreshold = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - PeakingUnPauseThreshold  Changed to; ") + String(_CurrentConfig->PeakingUnPauseThreshold)); break;
			case 9: _CurrentConfig->_PeakingPatternPointDelayTime = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _PeakingPatternPointDelayTime  Changed to; ") + String(_CurrentConfig->_PeakingPatternPointDelayTime)); _PeakingPatternCircGrad._PointDelay = _CurrentConfig->_PeakingPatternPointDelayTime;break;

			case 10: _CurrentConfig->_DyDeltaElStart = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyDeltaElStart  Changed to; ") + String(_CurrentConfig->_DyDeltaElStart));_PeakingPatternDynamicDelta._DeltaEl = _CurrentConfig->_DyDeltaElStart; break;
			case 11: _CurrentConfig->_DyDeltaAzStart = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyDeltaAzStart  Changed to; ") + String(_CurrentConfig->_DyDeltaAzStart)); _PeakingPatternDynamicDelta._DeltaAz = _CurrentConfig->_DyDeltaAzStart; break;
			case 12: _CurrentConfig->_DyCentreKAz = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyCentreKAz  Changed to; ") + String(_CurrentConfig->_DyCentreKAz)); _PeakingPatternDynamicDelta._KAz = _CurrentConfig->_DyCentreKAz; break;
			case 13: _CurrentConfig->_DyCentreKEl = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyCentreKEl  Changed to; ") + String(_CurrentConfig->_DyCentreKEl)); _PeakingPatternDynamicDelta._KEl = _CurrentConfig->_DyCentreKEl; break;
			case 14: _CurrentConfig->_DyDeltaAzMax = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyDeltaAzMax  Changed to; ") + String(_CurrentConfig->_DyDeltaAzMax)); _PeakingPatternDynamicDelta._DeltaAzMax = _CurrentConfig->_DyDeltaAzMax; break;
			case 15: _CurrentConfig->_DyDeltaElMax = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyDeltaElMax  Changed to; ") + String(_CurrentConfig->_DyDeltaElMax)); _PeakingPatternDynamicDelta._DeltaElMax = _CurrentConfig->_DyDeltaElMax; break;
			case 16: _CurrentConfig->_DyKEl = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyKEl  Changed to; ") + String(_CurrentConfig->_DyKEl)); _PeakingPatternDynamicDelta._DyKEl = _CurrentConfig->_DyKEl; break;
			case 17: _CurrentConfig->_DyKAz = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyKAz  Changed to; ") + String(_CurrentConfig->_DyKAz)); _PeakingPatternDynamicDelta._DyKAz = _CurrentConfig->_DyKAz; break;
			case 18: _CurrentConfig->_DySigStrOffset = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DySigStrOffset  Changed to; ") + String(_CurrentConfig->_DySigStrOffset));_PeakingPatternDynamicDelta._DySigStrOffset = _CurrentConfig->_DySigStrOffset; break;
			case 19: _CurrentConfig->_AutoDyOffset = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _AutoDyOffset  Changed to; ") + String(_CurrentConfig->_AutoDyOffset));_PeakingPatternDynamicDelta._AutoDyOffset = _CurrentConfig->_AutoDyOffset; break;
			case 20: _CurrentConfig->_DyPeakingLockThreshold = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyPeakingLockThreshold  Changed to; ") + String(_CurrentConfig->_DyPeakingLockThreshold)); _PeakingPatternDynamicDelta._signalLostThreshold = _CurrentConfig->_DyPeakingLockThreshold; break;
			case 21: _CurrentConfig->_DyPeakingPatternPointDelayTime = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DyPeakingPatternPointDelayTime Minimum  Changed to; ") + String(_CurrentConfig->_DyPeakingPatternPointDelayTime)); _PeakingPatternDynamicDelta._PointDelayMin = _CurrentConfig->_DyPeakingPatternPointDelayTime; break;

			case 22: _CurrentConfig->_LockThreshold = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Modem Read _LockThreshold  Changed to; ") + String(_CurrentConfig->_LockThreshold)); SatSignalRecv1._LockThreshold = _CurrentConfig->_LockThreshold; break;
			case 23: _CurrentConfig->_NumberofReadAverages = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Modem Read _NumberofReadAverages  Changed to; ") + String(_CurrentConfig->_NumberofReadAverages)); SatSignalRecv1._NumberofReadAverages = _CurrentConfig->_NumberofReadAverages; break;

			case 24: _CurrentConfig->_ElExpandingBoundaryDelta = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search _ElExpandingBoundaryDelta  Changed to; ") + String(_CurrentConfig->_ElExpandingBoundaryDelta)); _SearchPattern._ElExpandingBoundaryFactor = _CurrentConfig->_ElExpandingBoundaryDelta;break;
			case 25: _CurrentConfig->_AbsMaxPatternElevationBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search _AbsMaxPatternElevationBoundary  Changed to; ") + String(_CurrentConfig->_AbsMaxPatternElevationBoundary));  _SearchPattern._AbsMaxPatternElevationBoundary = _CurrentConfig->_AbsMaxPatternElevationBoundary;break;
			case 26: _CurrentConfig->SearchElevation_stepCountSize = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchElevation_stepCountSize  Changed to; ") + String(_CurrentConfig->SearchElevation_stepCountSize)); _SearchPattern.SearchElevation._stepCountSize = _CurrentConfig->SearchElevation_stepCountSize;break;
			case 27: _CurrentConfig->SearchElevation_axisMax = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchElevation_axisMax  Changed to; ") + String(_CurrentConfig->SearchElevation_axisMax));  _SearchPattern.SearchElevation._axisMax = _CurrentConfig->SearchElevation_axisMax;break;
			case 28: _CurrentConfig->SearchElevation_axisMin = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchElevation_axisMin  Changed to; ") + String(_CurrentConfig->SearchElevation_axisMin)); _SearchPattern.SearchElevation._axisMin = _CurrentConfig->SearchElevation_axisMin;break;
			case 29: _CurrentConfig->SearchElevation_AxisBoundaryRollOver = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchElevation_AxisBoundaryRollOver  Changed to; ") + String(_CurrentConfig->SearchElevation_AxisBoundaryRollOver)); _SearchPattern.SearchElevation._AxisBoundaryRollOver = _CurrentConfig->SearchElevation_AxisBoundaryRollOver;break;
			case 30: _CurrentConfig->SearchAzimuth_stepCountSize = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchAzimuth_stepCountSize  Changed to; ") + String(_CurrentConfig->SearchAzimuth_stepCountSize)); _SearchPattern.SearchAzimuth._stepCountSize = _CurrentConfig->SearchAzimuth_stepCountSize;break;
			case 31: _CurrentConfig->SearchAzimuth_maxCounterBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchAzimuth_maxCounterBoundary  Changed to; ") + String(_CurrentConfig->SearchAzimuth_maxCounterBoundary)); _SearchPattern.SearchAzimuth._maxCounterBoundary = _CurrentConfig->SearchAzimuth_maxCounterBoundary;break;
			case 32: _CurrentConfig->SearchAzimuth_axisMax = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchAzimuth_axisMax  Changed to; ") + String(_CurrentConfig->SearchAzimuth_axisMax)); _SearchPattern.SearchAzimuth._axisMax = _CurrentConfig->SearchAzimuth_axisMax;break;
			case 33: _CurrentConfig->SearchAzimuth_axisMin = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchAzimuth_axisMin  Changed to; ") + String(_CurrentConfig->SearchAzimuth_axisMin));  _SearchPattern.SearchAzimuth._axisMin = _CurrentConfig->SearchAzimuth_axisMin;break;
			case 34: _CurrentConfig->SearchAzimuth_AxisBoundaryRollOver = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search SearchAzimuth_AxisBoundaryRollOver  Changed to; ") + String(_CurrentConfig->SearchAzimuth_AxisBoundaryRollOver)); _SearchPattern.SearchAzimuth._AxisBoundaryRollOver = _CurrentConfig->SearchAzimuth_AxisBoundaryRollOver;break;
			case 35: _CurrentConfig->_SearchPatternPointDelayTime = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search _SearchPatternPointDelayTime  Changed to; ") + String(_CurrentConfig->_SearchPatternPointDelayTime)); _SearchPattern._PointDelay = _CurrentConfig->_SearchPatternPointDelayTime; break;
			
			case 36: _CurrentConfig->UseOnBoardGyroWithCompass = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - UseOnBoardGyroWithCompass  Changed to; ") + String(_CurrentConfig->UseOnBoardGyroWithCompass)); break;
			case 37: _CurrentConfig->KGyroCompFilter = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - KGyroCompFilter  Changed to; ") + String(_CurrentConfig->KGyroCompFilter)); _YawCompFilter.setup(_CurrentConfig->KGyroCompFilter); break;
			
			case 38: _CurrentConfig->ElTargetAdjustmentDampening = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - ElTargetAdjustmentDampening  Changed to; ") + String(_CurrentConfig->ElTargetAdjustmentDampening)); break;
			case 39: _CurrentConfig->ElPitchRollCompBuffer = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - ElPitchRollCompBuffer  Changed to; ") + String(_CurrentConfig->ElPitchRollCompBuffer)); break;
			case 40: _CurrentConfig->UseSimplifiedStabilisation = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - UseSimplifiedStabilisation  Changed to; ") + String(_CurrentConfig->UseSimplifiedStabilisation));  break;
			

			case 41: _CurrentConfig->_FramePitchNumbAvg = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _FramePitchNumbAvg  Changed to; ") + String(_CurrentConfig->_FramePitchNumbAvg)); _FramePitchAvg.setup(_CurrentConfig->_FramePitchNumbAvg);break;
			case 42: _CurrentConfig->_FrameRollNumbAvg = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _FrameRollNumbAvg  Changed to; ") + String(_CurrentConfig->_FrameRollNumbAvg));_FrameRollAvg.setup(_CurrentConfig->_FrameRollNumbAvg); break;
			case 43: _CurrentConfig->_FramePitchDefaultBuffer = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _FramePitchDefaultBuffer  Changed to; ") + String(_CurrentConfig->_FramePitchDefaultBuffer)); _FramePitchAvg._ValueBuffer = _CurrentConfig->_FramePitchDefaultBuffer;break;
			case 44: _CurrentConfig->_FrameRollDefaultBuffer = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _FrameRollDefaultBuffer  Changed to; ") + String(_CurrentConfig->_FrameRollDefaultBuffer));_FrameRollAvg._ValueBuffer = _CurrentConfig->_FrameRollDefaultBuffer; break;

			case 45: _CurrentConfig->_FrameIMUEnabled = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _FrameIMUEnabled  Changed to; ") + String(_CurrentConfig->_FrameIMUEnabled)); _YEIFrame._enabled = _CurrentConfig->_FrameIMUEnabled; break;
			case 46: _CurrentConfig->Frame_RollTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Frame_RollTrim  Changed to; ") + String(_CurrentConfig->Frame_RollTrim));  _YEIFrame._RollTrim = _CurrentConfig->Frame_RollTrim; break;
			case 47: _CurrentConfig->Frame_PitchTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Frame_PitchTrim  Changed to; ") + String(_CurrentConfig->Frame_PitchTrim)); _YEIFrame._PitchTrim = _CurrentConfig->Frame_PitchTrim; break;
			case 48: _CurrentConfig->Frame_YEIUpdateRate = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Frame_YEIUpdateRate  Changed to; ") + String(_CurrentConfig->Frame_YEIUpdateRate)); _YEIFrame._YEIUpdateRate = _CurrentConfig->Frame_YEIUpdateRate; break;

			case 49: _CurrentConfig->Cross_RollTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_RollTrim  Changed to; ") + String(_CurrentConfig->Cross_RollTrim));MotorController._AntMotorCross._imuSensor->_RollTrim = _CurrentConfig->Cross_RollTrim; break;
			case 50: _CurrentConfig->Cross_PitchTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_PitchTrim  Changed to; ") + String(_CurrentConfig->Cross_PitchTrim));MotorController._AntMotorCross._imuSensor->_PitchTrim = _CurrentConfig->Cross_PitchTrim; break;
			case 51: _CurrentConfig->Cross_YEIUpdateRate = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_YEIUpdateRate  Changed to; ") + String(_CurrentConfig->Cross_YEIUpdateRate)); MotorController._AntMotorCross._imuSensor->_YEIUpdateRate = _CurrentConfig->Cross_YEIUpdateRate; break;

			case 52: _CurrentConfig->El_axisMoveReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisMoveReverse  Changed to; ") + String(_CurrentConfig->El_axisMoveReverse)); MotorController._AntMotorEl._axisMoveReverse = _CurrentConfig->El_axisMoveReverse; break;
			case 53: _CurrentConfig->El_axisReadReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisReadReverse  Changed to; ") + String(_CurrentConfig->El_axisReadReverse)); MotorController._AntMotorEl._axisReadReverse = _CurrentConfig->El_axisReadReverse;break;
			case 54: _CurrentConfig->El_axisTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisTrim  Changed to; ") + String(_CurrentConfig->El_axisTrim)); MotorController._AntMotorEl._axisTrim = _CurrentConfig->El_axisTrim; break;
			case 55: _CurrentConfig->El_axisSensorReverse = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisSensorReverse  Changed to; ") + String(_CurrentConfig->El_axisSensorReverse));  MotorController._AntMotorEl._axisSensorReverse = _CurrentConfig->El_axisSensorReverse; break;
			case 56: _CurrentConfig->El_axisError = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisError  Changed to; ") + String(_CurrentConfig->El_axisError)); MotorController._AntMotorEl._axisError = _CurrentConfig->El_axisError; break;
			case 57: _CurrentConfig->El_axisEnable = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisEnable  Changed to; ") + String(_CurrentConfig->El_axisEnable));   MotorController._AntMotorEl._axisEnable = _CurrentConfig->El_axisEnable; break;
			case 58: _CurrentConfig->El_axisPowerDown = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisPowerDown  Changed to; ") + String(_CurrentConfig->El_axisPowerDown)); MotorController._AntMotorEl._axisPowerDown = _CurrentConfig->El_axisPowerDown; break;
			case 59: _CurrentConfig->El_axisMinBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisMinBoundary  Changed to; ") + String(_CurrentConfig->El_axisMinBoundary)); MotorController._AntMotorEl._axisMinBoundary = _CurrentConfig->El_axisMinBoundary; break;
			case 60: _CurrentConfig->El_axisMaxBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - El_axisMaxBoundary  Changed to; ") + String(_CurrentConfig->El_axisMaxBoundary)); MotorController._AntMotorEl._axisMaxBoundary = _CurrentConfig->El_axisMaxBoundary; break;

			case 61: _CurrentConfig->Az_axisMoveReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisMoveReverse  Changed to; ") + String(_CurrentConfig->Az_axisMoveReverse)); MotorController._AntMotorAz._axisMoveReverse = _CurrentConfig->Az_axisMoveReverse; break;
			case 62: _CurrentConfig->Az_axisReadReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisReadReverse  Changed to; ") + String(_CurrentConfig->Az_axisReadReverse)); MotorController._AntMotorAz._axisReadReverse = _CurrentConfig->Az_axisReadReverse;break;
			case 63: _CurrentConfig->Az_axisTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisTrim  Changed to; ") + String(_CurrentConfig->Az_axisTrim)); MotorController._AntMotorAz._axisTrim = _CurrentConfig->Az_axisTrim; break;
			case 64: _CurrentConfig->Az_axisSensorReverse = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisSensorReverse  Changed to; ") + String(_CurrentConfig->Az_axisSensorReverse));  MotorController._AntMotorAz._axisSensorReverse = _CurrentConfig->Az_axisSensorReverse; break;
			case 65: _CurrentConfig->Az_axisError = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisError  Changed to; ") + String(_CurrentConfig->Az_axisError)); MotorController._AntMotorAz._axisError = _CurrentConfig->Az_axisError; break;
			case 66: _CurrentConfig->Az_axisEnable = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisEnable  Changed to; ") + String(_CurrentConfig->Az_axisEnable));  MotorController._AntMotorAz._axisEnable = _CurrentConfig->Az_axisEnable; break;
			case 67: _CurrentConfig->Az_axisPowerDown = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisPowerDown  Changed to; ") + String(_CurrentConfig->Az_axisPowerDown)); MotorController._AntMotorAz._axisPowerDown = _CurrentConfig->Az_axisPowerDown; break;
			case 68: _CurrentConfig->Az_axisMinBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisMinBoundary  Changed to; ") + String(_CurrentConfig->Az_axisMinBoundary)); MotorController._AntMotorAz._axisMinBoundary = _CurrentConfig->Az_axisMinBoundary; break;
			case 69: _CurrentConfig->Az_axisMaxBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Az_axisMaxBoundary  Changed to; ") + String(_CurrentConfig->Az_axisMaxBoundary)); MotorController._AntMotorAz._axisMaxBoundary = _CurrentConfig->Az_axisMaxBoundary; break;


			case 70: _CurrentConfig->Cross_axisMoveReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisMoveReverse  Changed to; ") + String(_CurrentConfig->Cross_axisMoveReverse)); MotorController._AntMotorCross._axisMoveReverse = _CurrentConfig->Cross_axisMoveReverse; break;
			case 71: _CurrentConfig->Cross_axisReadReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisReadReverse  Changed to; ") + String(_CurrentConfig->Cross_axisReadReverse)); MotorController._AntMotorCross._axisReadReverse = _CurrentConfig->Cross_axisReadReverse;break;
			case 72: _CurrentConfig->Cross_axisTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisTrim  Changed to; ") + String(_CurrentConfig->Cross_axisTrim)); MotorController._AntMotorCross._axisTrim = _CurrentConfig->Cross_axisTrim; break;
			case 73: _CurrentConfig->Cross_axisSensorReverse = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisSensorReverse  Changed to; ") + String(_CurrentConfig->Cross_axisSensorReverse)); MotorController._AntMotorCross._axisSensorReverse = _CurrentConfig->Cross_axisSensorReverse; break;
			case 74: _CurrentConfig->Cross_axisError = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisError  Changed to; ") + String(_CurrentConfig->Cross_axisError)); MotorController._AntMotorCross._axisError = _CurrentConfig->Cross_axisError; break;
			case 75: _CurrentConfig->Cross_axisEnable = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisEnable  Changed to; ") + String(_CurrentConfig->Cross_axisEnable)); MotorController._AntMotorCross._axisEnable = _CurrentConfig->Cross_axisEnable; break;
			case 76: _CurrentConfig->Cross_axisPowerDown = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisPowerDown  Changed to; ") + String(_CurrentConfig->Cross_axisPowerDown)); MotorController._AntMotorCross._axisPowerDown = _CurrentConfig->Cross_axisPowerDown; break;
			case 77: _CurrentConfig->Cross_axisMinBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisMinBoundary  Changed to; ") + String(_CurrentConfig->Cross_axisMinBoundary)); MotorController._AntMotorCross._axisMinBoundary = _CurrentConfig->Cross_axisMinBoundary; break;
			case 78: _CurrentConfig->Cross_axisMaxBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Cross_axisMaxBoundary  Changed to; ") + String(_CurrentConfig->Cross_axisMaxBoundary)); MotorController._AntMotorCross._axisMaxBoundary = _CurrentConfig->Cross_axisMaxBoundary; break;

			case 79: _CurrentConfig->Pol_axisMoveReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisMoveReverse  Changed to; ") + String(_CurrentConfig->Pol_axisMoveReverse)); MotorController._AntMotorPol._axisMoveReverse = _CurrentConfig->Pol_axisMoveReverse; break;
			case 80: _CurrentConfig->Pol_axisReadReverse = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisReadReverse  Changed to; ") + String(_CurrentConfig->Pol_axisReadReverse)); MotorController._AntMotorPol._axisReadReverse = _CurrentConfig->Pol_axisReadReverse;break;
			case 81: _CurrentConfig->Pol_axisTrim = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisTrim  Changed to; ") + String(_CurrentConfig->Pol_axisTrim)); MotorController._AntMotorPol._axisTrim = _CurrentConfig->Pol_axisTrim; break;
			case 82: _CurrentConfig->Pol_axisSensorReverse = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisSensorReverse  Changed to; ") + String(_CurrentConfig->Pol_axisSensorReverse)); MotorController._AntMotorPol._axisSensorReverse = _CurrentConfig->Pol_axisSensorReverse; break;
			case 83: _CurrentConfig->Pol_axisError = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisError  Changed to; ") + String(_CurrentConfig->Pol_axisError)); MotorController._AntMotorPol._axisError = _CurrentConfig->Pol_axisError; break;
			case 84: _CurrentConfig->Pol_axisEnable = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisEnable  Changed to; ") + String(_CurrentConfig->Pol_axisEnable)); MotorController._AntMotorPol._axisEnable = _CurrentConfig->Pol_axisEnable; break;
			case 85: _CurrentConfig->Pol_axisPowerDown = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisPowerDown  Changed to; ") + String(_CurrentConfig->Pol_axisPowerDown)); MotorController._AntMotorPol._axisPowerDown = _CurrentConfig->Pol_axisPowerDown; break;
			case 86: _CurrentConfig->Pol_axisMinBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisMinBoundary  Changed to; ") + String(_CurrentConfig->Pol_axisMinBoundary)); MotorController._AntMotorPol._axisMinBoundary = _CurrentConfig->Pol_axisMinBoundary; break;
			case 87: _CurrentConfig->Pol_axisMaxBoundary = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Pol_axisMaxBoundary  Changed to; ") + String(_CurrentConfig->Pol_axisMaxBoundary)); MotorController._AntMotorPol._axisMaxBoundary = _CurrentConfig->Pol_axisMaxBoundary; break;

			case 88: _CurrentConfig->_BucPower = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _BucPower  Changed to; ") + String(_CurrentConfig->_BucPower));break;
			case 89: _CurrentConfig->_ModemTXMute = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _ModemTXMute  Changed to; ") + String(_CurrentConfig->_ModemTXMute)); break;

			case 90: _CurrentConfig->_LoopUpdateTime = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _LoopUpdateTime  Changed to; ") + String(_CurrentConfig->_LoopUpdateTime)); break;
			case 91: _CurrentConfig->_PrintUSBDebug = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _PrintUSBDebug  Changed to; ") + String(_CurrentConfig->_PrintUSBDebug)); _CurrentConfig->_PrintUSBDebug =  _CurrentConfig->_PrintUSBDebug; break;

			case 92: _CurrentConfig->_UpdatePiRate = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _UpdatePiRate  Changed to; ") + String(_CurrentConfig->_UpdatePiRate)); break;
			case 93: _CurrentConfig->_UpdatePiRate_MotorData = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _UpdatePiRate_MotorData  Changed to; ") + String(_CurrentConfig->_UpdatePiRate_MotorData)); break;
			case 94: _CurrentConfig->_DebugPacketOutputRate = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _DebugPacketOutputRate  Changed to; ") + String(_CurrentConfig->_DebugPacketOutputRate)); break;
			case 95: _CurrentConfig->_UpdatePi_MotorData_Enable = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _UpdatePi_MotorData_Enable  Changed to; ") + String(_CurrentConfig->_UpdatePi_MotorData_Enable)); break;

			case 96: _CurrentConfig->_AzMotorTimeout = USBSerial.parseUL(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _AzMotorTimeout  Changed to; ") + String(_CurrentConfig->_AzMotorTimeout)); MotorController._AzMotorTimeout = _CurrentConfig->_AzMotorTimeout; break;
			case 97: _CurrentConfig->_ElMotorTimeout = USBSerial.parseUL(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _ElMotorTimeout  Changed to; ") + String(_CurrentConfig->_ElMotorTimeout)); MotorController._ElMotorTimeout = _CurrentConfig->_ElMotorTimeout; break;
			case 98: _CurrentConfig->_CrossMotorTimeout = USBSerial.parseUL(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _CrossMotorTimeout  Changed to; ") + String(_CurrentConfig->_CrossMotorTimeout)); MotorController._CrossMotorTimeout = _CurrentConfig->_CrossMotorTimeout; break;
			case 99: _CurrentConfig->_PolMotorTimeout = USBSerial.parseUL(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - _PolMotorTimeout  Changed to; ") + String(_CurrentConfig->_PolMotorTimeout)); MotorController._PolMotorTimeout = _CurrentConfig->_PolMotorTimeout; break;
			
			case 100: _CurrentConfig->UseGPSCompass = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Use GPS Compass  Changed to; ") + String(_CurrentConfig->UseGPSCompass));_UseGPSCompass = _CurrentConfig->UseGPSCompass; break;
			case 101: _CurrentConfig->GPSCompassBaud = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - GPS Compass Baud  Changed to; ") + String(_CurrentConfig->GPSCompassBaud)); GPSSerial.end(); GPSSerial.begin(_CurrentConfig->GPSCompassBaud);break;
			
			case 102: _CurrentConfig->_useDBviaUSB = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Use DB via USB  Changed to; ") + String(_CurrentConfig->_useDBviaUSB)); SatSignalRecv1._isUSBSignal = _CurrentConfig->_useDBviaUSB; break;
			case 103: _CurrentConfig->_DBThreshold = USBSerial.parseFloat(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - DB Threshold  Changed to; ") + String(_CurrentConfig->_DBThreshold)); SatSignalRecv1.setDBThreshold(_CurrentConfig->_DBThreshold); break;
			
			case 104: _CurrentConfig->_GPSCompassDir = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Compass Dir  Changed to; ") + String(_CurrentConfig->_GPSCompassDir)); GPSCompass.setup(&GPSSerial, _CurrentConfig->GPSCompassBaud, _CurrentConfig->_GPSCompassDir); break;
			
			case 105: _CurrentConfig->_SearchCoGWideEnable = USBSerial.parseInt(PacketPosition2); if(_CurrentConfig->_PrintUSBDebug) Serial.println(F("+,USB Comms - Search CoG En  Changed to; ") + String(_CurrentConfig->_SearchCoGWideEnable)); break;
			


			default:PCSerial.println("~,USB Comms - Config Value index not recognised. No Values changed. Data Received-" + String(ConfigIndex)); ConfigIndexValid = false; break;
		}
		
		//Check Valid Config index has been used
		if(ConfigIndexValid)
		{
			//Check 3rd position in packet for Save value. If 1 save, otherwise ignore
			if(USBSerial.parseInt(PacketPosition3) == 1)
			{
				Serial.println("+,USB Comms - Config Data updated. Saving new config to eeprom.");
				_EEPROMConfig.SaveCurrent(); // Save latest config file
			}
		}

	}else
	{
		Serial.println("@,USB Comms - Config Values Settings Invalid No. Values. Expected 4, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}

//**********************************************************************************//**********************************************************************************


//**********************************************************************************
void AntennaManagerClass:: AntennaComms_Advanced_ConfigMotorSpeedSettings()
{
	//:Command, MotorSelect, Speed, Accel,
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Adjust Motor Value\n");
	
	const int CommandExpectedNoValues = 4;

	if(USBSerial._FinalSeperatorCount >= CommandExpectedNoValues)
	{
		int ConfigIndex = USBSerial.parseInt(PacketPosition1);

		boolean ConfigIndexValid = true;

		float MaxSpeed = USBSerial.parseFloat(PacketPosition2);
		float Accel = USBSerial.parseFloat(PacketPosition3);

		switch(ConfigIndex)
		{
			
			//TODO: Finish adding functions
			case 0: MotorController._AntMotorAz._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorAz._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - Az Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel)); break;
			case 1: MotorController._AntMotorEl._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorEl._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - El Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel)); break;
			case 2: MotorController._AntMotorCross._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorCross._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - Cross Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel)); break;

			default:PCSerial.println("~,USB Comms - Motor Setting Value index not recognised. No Values changed. Data Received-" + String(ConfigIndex)); ConfigIndexValid = false; break;
		}

	}else
	{
		Serial.println("@,USB Comms - Motor Values Settings Invalid No. Values. Expected 4, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}


//**********************************************************************************
//Az PID Debug K value control
void AntennaManagerClass:: AntennaComms_Advanced_ConfigAzPID()
{
	//:Command, MotorSelect, Speed, Accel,
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Adjust Az PID Values\n");
	
	const int CommandExpectedNoValues = 3;

	if(USBSerial._FinalSeperatorCount == CommandExpectedNoValues)
	{
		int ConfigIndex = USBSerial.parseInt(PacketPosition1);

		boolean ConfigIndexValid = true;

		float NewValue = USBSerial.parseFloat(PacketPosition2);
		

		switch(ConfigIndex)
		{
			
			//TODO: Finish adding functions
			case 0: MotorController._AntMotorAz.Kp = NewValue; Serial.println("+,USB Comms - _AntMotorAz.Kp;" + String(MotorController._AntMotorAz.Kp)); break;
			case 1: MotorController._AntMotorAz.Ki = NewValue; Serial.println("+,USB Comms - _AntMotorAz.Ki;" + String(MotorController._AntMotorAz.Ki)); break;
			case 2: MotorController._AntMotorAz.Kd = NewValue; Serial.println("+,USB Comms - _AntMotorAz.Kd;" + String(MotorController._AntMotorAz.Kd)); break;
			case 3: MotorController._AntMotorAz._AltStepMoveEnable = boolean(NewValue); Serial.println("+,USB Comms - _AntMotorAz._PIDEnable;" + String(MotorController._AntMotorAz._AltStepMoveEnable)); break;
			default:PCSerial.println("~,USB Comms - AZ PID Value index not recognised. No Values changed. Data Received-" + String(ConfigIndex)); ConfigIndexValid = false; break;
		}

		MotorController._AntMotorAz.setPIDValues();
	}else
	{
		Serial.println("@,USB Comms - Motor Values Settings Invalid No. Values. Expected 3, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}


//**********************************************************************************
void AntennaManagerClass:: AntennaComms_Advanced_ConfigPrintSetting()
{
	if(_CurrentConfig->_PrintUSBDebug)
	Serial.print("+,USB Comms - Print Config Value\n");
	
	const int CommandExpectedNoValues = 1;

	if(USBSerial._FinalSeperatorCount >= CommandExpectedNoValues)
	{
		int ConfigIndex = USBSerial.parseInt(PacketPosition1);

		boolean ConfigIndexValid = true;

		switch(ConfigIndex)
		{

			//TODO: Finish adding functions
			case 0: Serial.println("+,USB Comms - Peaking Mode Currently;" + String(_CurrentConfig->PeakingMode)); break;

			case 1: Serial.println("+,USB Comms - Peaking Delta El Currently;" + String(_CurrentConfig->DeltaEl)); break;
			case 2: Serial.println("+,USB Comms - Peaking Delta Az Currently;" + String(_CurrentConfig->DeltaAz)); break;
			case 3: Serial.print("+,USB Comms - Peaking KEl Currently;"); Serial.println(_CurrentConfig->KEl,4); break;
			case 4: Serial.print("+,USB Comms - Peaking KAz Currently;"); Serial.println(_CurrentConfig->KAz, 4); break;
			case 5: Serial.println("+,USB Comms - Peaking Lock Threshold Currently;" + String(_CurrentConfig->PeakingLockThreshold)); break;
			case 6: Serial.println("+,USB Comms - PeakingPauseOnUpperThreshold Currently;" + String(_CurrentConfig->PeakingPauseOnUpperThreshold)); break;
			case 7: Serial.println(F("+,USB Comms - PeakingPauseThreshold Currently;") + String(_CurrentConfig->PeakingPauseThreshold)); break;
			case 8: Serial.println(F("+,USB Comms - PeakingUnPauseThreshold Currently;") + String(_CurrentConfig->PeakingUnPauseThreshold)); break;
			case 9: Serial.println(F("+,USB Comms - _PeakingPatternPointDelayTime Currently;") + String(_CurrentConfig->_PeakingPatternPointDelayTime)); break;

			case 10: Serial.println(F("+,USB Comms - _DyDeltaElStart Currently;") + String(_CurrentConfig->_DyDeltaElStart));break;
			case 11: Serial.println(F("+,USB Comms - _DyDeltaAzStart Currently;") + String(_CurrentConfig->_DyDeltaAzStart));break;
			case 12: Serial.println(F("+,USB Comms - _DyCentreKAz Currently;") + String(_CurrentConfig->_DyCentreKAz));break;
			case 13: Serial.println(F("+,USB Comms - _DyCentreKEl Currently;") + String(_CurrentConfig->_DyCentreKEl));break;
			case 14: Serial.println(F("+,USB Comms - _DyDeltaAzMax Currently;") + String(_CurrentConfig->_DyDeltaAzMax)); break;
			case 15: Serial.println(F("+,USB Comms - _DyDeltaElMax Currently;") + String(_CurrentConfig->_DyDeltaElMax));break;
			case 16: Serial.println(F("+,USB Comms - _DyKEl Currently;") + String(_CurrentConfig->_DyKEl));break;
			case 17: Serial.println(F("+,USB Comms - _DyKAz Currently;") + String(_CurrentConfig->_DyKAz));break;
			case 18: Serial.println(F("+,USB Comms - _DySigStrOffset Currently;") + String(_CurrentConfig->_DySigStrOffset));break;
			case 19: Serial.println(F("+,USB Comms - _AutoDyOffset Currently;") + String(_CurrentConfig->_AutoDyOffset));break;
			case 20: Serial.println(F("+,USB Comms - _DyPeakingLockThreshold Currently;") + String(_CurrentConfig->_DyPeakingLockThreshold));break;
			case 21: Serial.println(F("+,USB Comms - _DyPeakingPatternPointDelayTime Minimum Currently;") + String(_CurrentConfig->_DyPeakingPatternPointDelayTime));break;

			case 22: Serial.println(F("+,USB Comms - Modem Read _LockThreshold Currently;") + String(_CurrentConfig->_LockThreshold)); break;
			case 23: Serial.println(F("+,USB Comms - Modem Read _NumberofReadAverages Currently;") + String(_CurrentConfig->_NumberofReadAverages));break;

			case 24: Serial.println(F("+,USB Comms - Search _ElExpandingBoundaryDelta Currently;") + String(_CurrentConfig->_ElExpandingBoundaryDelta)); break;
			case 25: Serial.println(F("+,USB Comms - Search _AbsMaxPatternElevationBoundary Currently;") + String(_CurrentConfig->_AbsMaxPatternElevationBoundary));break;
			case 26: Serial.println(F("+,USB Comms - Search SearchElevation_stepCountSize Currently;") + String(_CurrentConfig->SearchElevation_stepCountSize)); break;
			case 27: Serial.println(F("+,USB Comms - Search SearchElevation_axisMax Currently;") + String(_CurrentConfig->SearchElevation_axisMax));break;
			case 28: Serial.println(F("+,USB Comms - Search SearchElevation_axisMin Currently;") + String(_CurrentConfig->SearchElevation_axisMin));break;
			case 29: Serial.println(F("+,USB Comms - Search SearchElevation_AxisBoundaryRollOver Currently;") + String(_CurrentConfig->SearchElevation_AxisBoundaryRollOver));break;
			case 30: Serial.println(F("+,USB Comms - Search SearchAzimuth_stepCountSize Currently;") + String(_CurrentConfig->SearchAzimuth_stepCountSize));break;
			case 31: Serial.println(F("+,USB Comms - Search SearchAzimuth_maxCounterBoundary Currently;") + String(_CurrentConfig->SearchAzimuth_maxCounterBoundary));break;
			case 32: Serial.println(F("+,USB Comms - Search SearchAzimuth_axisMax Currently;") + String(_CurrentConfig->SearchAzimuth_axisMax)); break;
			case 33: Serial.println(F("+,USB Comms - Search SearchAzimuth_axisMin Currently;") + String(_CurrentConfig->SearchAzimuth_axisMin));break;
			case 34: Serial.println(F("+,USB Comms - Search SearchAzimuth_AxisBoundaryRollOver Currently;") + String(_CurrentConfig->SearchAzimuth_AxisBoundaryRollOver));break;
			case 35: Serial.println(F("+,USB Comms - Search _SearchPatternPointDelayTime Currently;") + String(_CurrentConfig->_SearchPatternPointDelayTime)); break;
			
			case 36: Serial.println(F("+,USB Comms - UseOnBoardGyroWithCompass Currently;") + String(_CurrentConfig->UseOnBoardGyroWithCompass)); break;
			case 37: Serial.println(F("+,USB Comms - KGyroCompFilter Currently;") + String(_CurrentConfig->KGyroCompFilter)); break;
			
			case 38: Serial.println(F("+,USB Comms - ElTargetAdjustmentDampening Currently;") + String(_CurrentConfig->ElTargetAdjustmentDampening)); break;
			case 39: Serial.println(F("+,USB Comms - ElPitchRollCompBuffer Currently;") + String(_CurrentConfig->ElPitchRollCompBuffer)); break;
			case 40: Serial.println(F("+,USB Comms - UseSimplifiedStabilisation Currently;") + String(_CurrentConfig->UseSimplifiedStabilisation));  break;
			

			case 41: Serial.println(F("+,USB Comms - _FramePitchNumbAvg Currently;") + String(_CurrentConfig->_FramePitchNumbAvg));break;
			case 42: Serial.println(F("+,USB Comms - _FrameRollNumbAvg Currently;") + String(_CurrentConfig->_FrameRollNumbAvg));break;
			case 43: Serial.println(F("+,USB Comms - _FramePitchDefaultBuffer Currently;") + String(_CurrentConfig->_FramePitchDefaultBuffer)); break;
			case 44: Serial.println(F("+,USB Comms - _FrameRollDefaultBuffer Currently;") + String(_CurrentConfig->_FrameRollDefaultBuffer));break;

			case 45: Serial.println(F("+,USB Comms - _FrameIMUEnabled Currently;") + String(_CurrentConfig->_FrameIMUEnabled));break;
			case 46: Serial.println(F("+,USB Comms - Frame_RollTrim Currently;") + String(_CurrentConfig->Frame_RollTrim)); break;
			case 47: Serial.println(F("+,USB Comms - Frame_PitchTrim Currently;") + String(_CurrentConfig->Frame_PitchTrim));break;
			case 48: Serial.println(F("+,USB Comms - Frame_YEIUpdateRate Currently;") + String(_CurrentConfig->Frame_YEIUpdateRate));break;

			case 49: Serial.println(F("+,USB Comms - Cross_RollTrim Currently;") + String(_CurrentConfig->Cross_RollTrim));break;
			case 50: Serial.println(F("+,USB Comms - Cross_PitchTrim Currently;") + String(_CurrentConfig->Cross_PitchTrim));break;
			case 51: Serial.println(F("+,USB Comms - Cross_YEIUpdateRate Currently;") + String(_CurrentConfig->Cross_YEIUpdateRate));break;

			case 52: Serial.println(F("+,USB Comms - El_axisMoveReverse Currently;") + String(_CurrentConfig->El_axisMoveReverse)); break;
			case 53: Serial.println(F("+,USB Comms - El_axisReadReverse Currently;") + String(_CurrentConfig->El_axisReadReverse));break;
			case 54: Serial.println(F("+,USB Comms - El_axisTrim Currently;") + String(_CurrentConfig->El_axisTrim));break;
			case 55: Serial.println(F("+,USB Comms - El_axisSensorReverse Currently;") + String(_CurrentConfig->El_axisSensorReverse));break;
			case 56: Serial.println(F("+,USB Comms - El_axisError Currently;") + String(_CurrentConfig->El_axisError));break;
			case 57: Serial.println(F("+,USB Comms - El_axisEnable Currently;") + String(_CurrentConfig->El_axisEnable)); break;
			case 58: Serial.println(F("+,USB Comms - El_axisPowerDown Currently;") + String(_CurrentConfig->El_axisPowerDown));break;
			case 59: Serial.println(F("+,USB Comms - El_axisMinBoundary Currently;") + String(_CurrentConfig->El_axisMinBoundary));break;
			case 60: Serial.println(F("+,USB Comms - El_axisMaxBoundary Currently;") + String(_CurrentConfig->El_axisMaxBoundary));break;

			case 61: Serial.println(F("+,USB Comms - Az_axisMoveReverse Currently;") + String(_CurrentConfig->Az_axisMoveReverse)); break;
			case 62: Serial.println(F("+,USB Comms - Az_axisReadReverse Currently;") + String(_CurrentConfig->Az_axisReadReverse)); break;
			case 63: Serial.println(F("+,USB Comms - Az_axisTrim Currently;") + String(_CurrentConfig->Az_axisTrim)); break;
			case 64: Serial.println(F("+,USB Comms - Az_axisSensorReverse Currently;") + String(_CurrentConfig->Az_axisSensorReverse)); break;
			case 65: Serial.println(F("+,USB Comms - Az_axisError Currently;") + String(_CurrentConfig->Az_axisError)); break;
			case 66: Serial.println(F("+,USB Comms - Az_axisEnable Currently;") + String(_CurrentConfig->Az_axisEnable));break;
			case 67: Serial.println(F("+,USB Comms - Az_axisPowerDown Currently;") + String(_CurrentConfig->Az_axisPowerDown));break;
			case 68: Serial.println(F("+,USB Comms - Az_axisMinBoundary Currently;") + String(_CurrentConfig->Az_axisMinBoundary));break;
			case 69: Serial.println(F("+,USB Comms - Az_axisMaxBoundary Currently;") + String(_CurrentConfig->Az_axisMaxBoundary));break;


			case 70: Serial.println(F("+,USB Comms - Cross_axisMoveReverse Currently;") + String(_CurrentConfig->Cross_axisMoveReverse)); break;
			case 71: Serial.println(F("+,USB Comms - Cross_axisReadReverse Currently;") + String(_CurrentConfig->Cross_axisReadReverse));break;
			case 72: Serial.println(F("+,USB Comms - Cross_axisTrim Currently;") + String(_CurrentConfig->Cross_axisTrim));break;
			case 73: Serial.println(F("+,USB Comms - Cross_axisSensorReverse Currently;") + String(_CurrentConfig->Cross_axisSensorReverse));break;
			case 74: Serial.println(F("+,USB Comms - Cross_axisError Currently;") + String(_CurrentConfig->Cross_axisError));break;
			case 75: Serial.println(F("+,USB Comms - Cross_axisEnable Currently;") + String(_CurrentConfig->Cross_axisEnable));break;
			case 76: Serial.println(F("+,USB Comms - Cross_axisPowerDown Currently;") + String(_CurrentConfig->Cross_axisPowerDown));break;
			case 77: Serial.println(F("+,USB Comms - Cross_axisMinBoundary Currently;") + String(_CurrentConfig->Cross_axisMinBoundary));break;
			case 78: Serial.println(F("+,USB Comms - Cross_axisMaxBoundary Currently;") + String(_CurrentConfig->Cross_axisMaxBoundary));break;

			case 79: Serial.println(F("+,USB Comms - Pol_axisMoveReverse Currently;") + String(_CurrentConfig->Pol_axisMoveReverse));break;
			case 80: Serial.println(F("+,USB Comms - Pol_axisReadReverse Currently;") + String(_CurrentConfig->Pol_axisReadReverse));break;
			case 81: Serial.println(F("+,USB Comms - Pol_axisTrim Currently;") + String(_CurrentConfig->Pol_axisTrim));break;
			case 82: Serial.println(F("+,USB Comms - Pol_axisSensorReverse Currently;") + String(_CurrentConfig->Pol_axisSensorReverse));break;
			case 83: Serial.println(F("+,USB Comms - Pol_axisError Currently;") + String(_CurrentConfig->Pol_axisError)); break;
			case 84: Serial.println(F("+,USB Comms - Pol_axisEnable Currently;") + String(_CurrentConfig->Pol_axisEnable));break;
			case 85: Serial.println(F("+,USB Comms - Pol_axisPowerDown Currently;") + String(_CurrentConfig->Pol_axisPowerDown)); break;
			case 86: Serial.println(F("+,USB Comms - Pol_axisMinBoundary Currently;") + String(_CurrentConfig->Pol_axisMinBoundary));break;
			case 87: Serial.println(F("+,USB Comms - Pol_axisMaxBoundary Currently;") + String(_CurrentConfig->Pol_axisMaxBoundary)); break;

			case 88: Serial.println(F("+,USB Comms - _BucPower Currently;") + String(_CurrentConfig->_BucPower)); break;
			case 89: Serial.println(F("+,USB Comms - _ModemTXMute Currently;") + String(_CurrentConfig->_ModemTXMute));break;

			case 90: Serial.println(F("+,USB Comms - _LoopUpdateTime Currently;") + String(_CurrentConfig->_LoopUpdateTime)); break;
			case 91: Serial.println(F("+,USB Comms - _PrintUSBDebug Currently;") + String(_CurrentConfig->_PrintUSBDebug));break;

			case 92: Serial.println(F("+,USB Comms - _UpdatePiRate Currently;") + String(_CurrentConfig->_UpdatePiRate)); break;
			case 93: Serial.println(F("+,USB Comms - _UpdatePiRate_MotorData Currently;") + String(_CurrentConfig->_UpdatePiRate_MotorData)); break;
			case 94: Serial.println(F("+,USB Comms - _DebugPacketOutputRate Currently;") + String(_CurrentConfig->_DebugPacketOutputRate)); break;
			case 95: Serial.println(F("+,USB Comms - _UpdatePi_MotorData_Enable Currently;") + String(_CurrentConfig->_UpdatePi_MotorData_Enable)); break;

			case 96: Serial.println(F("+,USB Comms - _AzMotorTimeout Currently;") + String(_CurrentConfig->_AzMotorTimeout)); break;
			case 97: Serial.println(F("+,USB Comms - _ElMotorTimeout Currently;") + String(_CurrentConfig->_ElMotorTimeout)); break;
			case 98: Serial.println(F("+,USB Comms - _CrossMotorTimeout Currently;") + String(_CurrentConfig->_CrossMotorTimeout)); break;
			case 99: Serial.println(F("+,USB Comms - _PolMotorTimeout Currently;") + String(_CurrentConfig->_PolMotorTimeout));break;
			
			
			case 100:  Serial.println(F("+,USB Comms - Use GPS Compass Currently - ") + String(_CurrentConfig->UseGPSCompass));break;
			case 101:  Serial.println(F("+,USB Comms - GPS Compass Baud Currently - ") + String(_CurrentConfig->GPSCompassBaud)); break;
			
			case 102:  Serial.println(F("+,USB Comms - Use DB via USB Currently - ") + String(_CurrentConfig->_useDBviaUSB));  break;
			case 103:  Serial.println(F("+,USB Comms - DB Threshold Currently - ") + String(_CurrentConfig->_DBThreshold)); break;
			
			case 104:  Serial.println(F("+,USB Comms - Compass Dir Currently - ") + String(_CurrentConfig->_GPSCompassDir)); break;
			
			case 105:  Serial.println(F("+,USB Comms - Search COG En Currently - ") + String(_CurrentConfig->_SearchCoGWideEnable)); break;
			
			

			default:PCSerial.println("~,USB Comms - Config Value index not recognised. No Values changed. Data Received-" + String(ConfigIndex)); ConfigIndexValid = false; break;
		}
		

	}else
	{
		Serial.println("@,USB Comms - Config Values Settings Invalid No. Values. Expected 4, Received-" + String(USBSerial._FinalSeperatorCount));
	}
}


//**********************************************************************************
void AntennaManagerClass:: AntennaComms_Advanced_SwitchStabilityMethod()
{
	int isCommand = USBSerial.parseInt(PacketPosition1);
	int commitSettings = USBSerial.parseInt(PacketPosition2);

	Serial.print("+,USB Comms - Switch Stability Method\n");
	Serial.print("+,USB Comms - New Method is - ");
	
	switch (isCommand)
	{
	case 0: newPeakingOldStabilitySettings();Serial.println(F("New Tracking Settings")); break;
	case 1: oldStabilitySettings(); Serial.println(F("Original Tracking Settings")); break;
	case 2: newStabilitySettings();Serial.println(F("Gyro Heading Compensation")); break;
	}
	
	
	if(commitSettings)
	{
		Serial.println(F("+, USB Comms - Saving config to eeprom."));
		_EEPROMConfig.SaveCurrent();
	}
}
//**********************************************************************************//**********************************************************************************
//**********************************************************************************//**********************************************************************************
