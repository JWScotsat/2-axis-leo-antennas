/*
* AntennaMotors.cpp
*
* Created: 01/07/2015 13:23:57
* Author: Jack Wells
*/


#include "AntennaMotors.h"

//******************************************************************
//******************************************************************

// default constructor
// Set initial values/assignments for the used Accel Stepper Motor and the intended Axis Sensor
AntennaMotors::AntennaMotors(DCMotors *MotorIn, int AxisSensor, String Name)
{
	_DCMotor = MotorIn;
	_AxisSensor = AxisSensor;
	_Name = Name;
	_axisEnable = true;
	_axisCheckRecordedMotorPos = 0.0;
	_axisCheckTimer = millis();
	_axisTrim = 0.0;
	_axisSensorReverse = 1.0;
	_axisMoveReverse = 0;
	_axisReadReverse = 1;
	_MotorIs = DCMotor;
	_axisError = 5.0;
	_DCStoppedCount = 0;

} //AntennaMotors


// Run Initialisation for Axis with Hall Effect Sensors and Encoders
int AntennaMotors::initialiseDC()
{
	Serial.print("+,Initialising " + _Name + " DC Axis with Rotary Encoder\n");
	
	_StatusMessage = Status_InitialisingMessage;
	_status = Status_Initialising;
	
	_axisEnable = true;
	
	
	_DCMotor->Enable(); // Motor Enabled
	
	
	unsigned long initTimeOut = 10000; // Timeout for motor movement
	int InitResult = Status_TimeOutFail; // int to record Init result
	
	unsigned long timerStart = millis(); // Timeout Timer
	
	//Get current position and set a target position 30 degrees away
	
	float AxisPosition = getAxisPos() - _axisTrim;
	
	_axisInPosition = false;
	
	if(AxisPosition <= 10.0 || abs(AxisPosition - 180.0) <= 10.0 || abs(AxisPosition - 360.0) <= 10.0)
	_axisTarget = 30.0;
	else
	_axisTarget  = 0.0;
	
	
	boolean MotorEncoderWorking = false; // flag used for error estimation - tell if motor or encoder is working
	
	_axisCheckTimer = millis();
	_axisCheckRecordedMotorPos = AxisPosition;
	

	//Get target updated into 90-270 degree range
	DCTargetUpdate();
	
	//Get start distance from current and target position
	float startDist = abs(_axisTargetTrimmed - getAxisPos());
	
	Serial.println("+, Pol Init - Start Encoder Pos;" + String (getAxisPos()));
	Serial.println("+, Pol Init - Start Target Pos;" + String (_axisTargetTrimmed));
	Serial.println("+, Pol Init - Start Distance to Init Pos;" + String (startDist));
	
	
	while(InitResult != Status_AxisInitialised && millis() - timerStart < initTimeOut)
	{
		ClearUSBBuffer();
		//DC Move to target
		moveToTarget();
		
		//Check if distance is getting smaller - true then flag Motor and encoder working
		if(abs(_axisTargetTrimmed - getAxisPos()) < startDist-5.0 && !MotorEncoderWorking)
		{
			Serial.print("+," + _Name + " Movement Detected. Motor & Encoder Working.\n");
			MotorEncoderWorking = true;
			_axisCheckTimer = millis() + 1000;
			initTimeOut = 40000;
		}

		//Check encoder isn't in a negative position - failure reading encoder
		if(getAxisPos() < -100.0)
		{
			Serial.println("+, Pol Init - Final Encoder Pos;" + String (getAxisPos()));
			Serial.println("+, Pol Init - Final Target Pos;" + String (_axisTarget));
			Serial.print("@,Pol Encoder Error. Value misread\n");
			_axisEnable = false;

			InitResult = Status_EncoderFailure;
			
			_StatusMessage = Status_EncoderFailure;
			_status = InitResult;
			
			_DCMotor->Disable();
			
			break;
		}
		
		//Check motor and encoder aren't moving in the wrong direction - target to current getting larger
		if(abs(_axisTargetTrimmed - getAxisPos()) > startDist+10.0)
		{
			Serial.println("+, Pol Init - Final Encoder Pos;" + String (getAxisPos()));
			Serial.println("+, Pol Init - Final Target Pos;" + String (_axisTarget));
			Serial.print("@,Pol Movement Error. Moving in wrong direction\n");
			_axisEnable = false;

			InitResult = Status_MotorFail;
			_StatusMessage = Status_MotorFailMessage;
			_status = InitResult;
			
			_DCMotor->Disable();
			
			break;
		}
		
		//End stop/failed motor check
		if(axisCheckDirStop() && !_axisInPosition)
		{
			Serial.println("+, Pol Init - Final Encoder Pos;" + String (getAxisPos()));
			Serial.println("+, Pol Init - Final Target Pos;" + String (_axisTarget));
			Serial.print("+,DC Motor No Movement Detected. Possible End Stop or motor fail\n");
			_axisEnable = false;
			InitResult = Status_MotorFail;
			
			_StatusMessage = Status_MotorFailMessage;
			_status = InitResult;
			
			_DCMotor->Disable();
			
			break;
		}
		
		//Succesful initialisation
		if(_axisInPosition)
		{
			Serial.println("+, Pol Init - Final Encoder Pos;" + String (getAxisPos()));
			Serial.println("+, Pol Init - Final Target Pos;" + String (_axisTarget));
			Serial.print("+,DC Motor and Encoder In Position. Init Passed \n");
			InitResult = Status_AxisInitialised;
			_StatusMessage = Status_AxisInitialisedMessage;
			_status = InitResult;
			_axisEnable = true;
			_DCMotor->Disable();
			break;
		}
		
		//Serial.println("Tar;" + String(_axisTarget) + ", Current;" + String(getAxisPos()) + ", In Pos;" + String(_axisInPosition));

	}
	
	Serial.println("+, Pol Init - Final Encoder Pos;" + String (getAxisPos()));
	Serial.println("+, Pol Init - Final Target Pos;" + String (_axisTarget));
	
	
	//Report time out failure and handle
	if(millis() - timerStart >= initTimeOut)
	{
		Serial.print("+,Axis Timeout Fail");
		_axisEnable = false;

		InitResult = Status_TimeOutFail;

		_StatusMessage = Status_TimeOutFailMessage;
		_status = InitResult;
		
		_DCMotor->Disable();
		
		_axisTarget = 0.0;
		
	}
	
	return InitResult;

}

//******************************************************************

void AntennaMotors::DCMoveToTarget()
{
	// Get absolute target relative to trim
	DCTargetUpdate();
	
	// Check for in position and  motor stopped
	if (abs(getAxisPos() - _axisTargetTrimmed) < _axisError)
	{
		_DCMotor->Stop();
		
		
		_axisInPosition = true;
	}
	else // Move motor in target direction
	{
		
		_DCMotor->Enable();
		_axisInPosition = false;
		
		if (getAxisPos() < _axisTargetTrimmed)
		{
			if(_axisMoveReverse == 0)
			_DCMotor->MoveForward();
			else
			_DCMotor->MoveBackward();
		}else
		{
			if(_axisMoveReverse == 1)
			_DCMotor->MoveForward();
			else
			_DCMotor->MoveBackward();
		}
		
		_DCStoppedCount = 0;
	}
	
}

void AntennaMotors::setup()
{
	if(_MotorIs != DCMotor)
	Serial.print("+,Antenna Library Error! Setup used for DC Motor incorrect for;" + _Name);
	else
	_DCMotor->setup();
}


//Unit Tested Code
// Updates a "Bounded" "Trimmed" Target position
// This way the current position is always tracked 0-360 as encoder shows
//  However the target position is adjusted to within the encoder range but also taking into account trimming and bounding to 90-270
void AntennaMotors::DCTargetUpdate()
{
	// Error Check Axis Trim isn't out of physical bounds - error value
	if(abs(_axisTrim) > _axisMaxBoundary)
	{
		_StatusMessage = Serial.print("~,DC Motor Error - Axis Trim Out of Bounds; " + String(_axisTrim) + ". Trim Ignored\n");
		_axisTargetTrimmed = _axisTarget;
	}else if(_axisTarget < -0.001 || _axisTarget > 360.001)
	{
		_StatusMessage = ("~,DC Motor Error - Axis Target Out of Bounds; " + String(_axisTarget) + ". Target Ignored\n");
		_axisTargetTrimmed = 0.0;
	}else
	{
		//Axis Target gets bound from 90-270 - Horizontal/Vertical Pol is symetrical accross 360 therefor all positions are acheivable at 0-180 - offset to 90-270
		_axisTargetBound = _axisTarget;
		
		// Unit testing if to ensure previous tests work
		if(_isDCAxisTargetBoundMidWay)
		{
			if(_axisTargetBound > 270.0) // Bound from 90-270
			_axisTargetBound = _axisTargetBound - 180.0;
			
			if(_axisTargetBound < 90.0)
			_axisTargetBound = _axisTargetBound + 180.0;
		}
		
		_axisTargetTrimmed = _axisTargetBound + _axisTrim; // Get new trimmed target position. Axis is absolute so we must use a trimmed "target" position rather than reading position
		
		//Boundary Checks for resulting target
		if(_axisTargetTrimmed > _axisMaxBoundary)
		_axisTargetTrimmed -= _axisMaxBoundary;
		
		if(_axisTargetTrimmed < _axisMinBoundary)
		_axisTargetTrimmed += _axisMaxBoundary;
		
		
		// Unit testing if to ensure previous tests work
		if(_isDCAxisTargetBoundMidWay)
		{
			if(_axisTargetTrimmed > 270.0) // Bound from 90-270
			_axisTargetTrimmed = _axisTargetTrimmed - 180.0;
			
			if(_axisTargetTrimmed < 90.0)
			_axisTargetTrimmed = _axisTargetTrimmed + 180.0;
		}

	}
	
	
}
//******************************************************************
//******************************************************************