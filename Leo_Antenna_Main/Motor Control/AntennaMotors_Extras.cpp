/*
* AntennaMotors.cpp
*
* Created: 01/07/2015 13:23:57
* Author: Jack Wells
*/


#include "AntennaMotors.h"

//******************************************************************
//******************************************************************

// Run Initialisation for Axis with Hall Effect Sensors and Encoders
int AntennaMotors::initialiseWithHallEncoder()
{
	Serial.print("+,Initialising " + _Name + "Axis with Hall & Encoder\n");
	
	_status = Status_Initialising;
	_StatusMessage = Status_InitialisingMessage;
	
	//Zero and allow axis for movement
	_axisEnable = true;
	motorPowerControl(_axisPowerDown);
	_MotorEncoder->zeroCounterAxisPosition(_AxisSensor);
	
	//Timeout
	const unsigned long initTimeOut = 20000;
	
	//Returned result
	int InitResult = Status_Initialising;
	
	//Init timeout stamp
	unsigned long timerStart = millis();
	
	//Initial postion and axis target. 30 degree target from current position ensures the elevation moves up before crossing the hall effect
	// Creates repeatable position for hall effect "0"
	float AxisPosition = getAxisPos();
	_axisTarget  = AxisPosition + 30.0;
	boolean initForward = true;

	//Init flags for logic
	boolean isFirstStart = true;
	boolean MotorEncoderWorking = false;

	//Timeout stamp for motor stuck in position or against physical end stop
	_axisCheckTimer = millis();
	_axisCheckRecordedMotorPos = AxisPosition;
	
	//Initial readings of Hall Sensor. Used to detected failed sensor
	//int initialHallValue = digitalReadFast(_hallPin);
	boolean HallTrusted = false;

	
	while(InitResult != Status_AxisInitialised && millis() - timerStart < initTimeOut)
	{
		ClearUSBBuffer();
		axisCalculateSteps();
		//**
		
		// If we've correctly moved forward, or the motors have stopped moving
		// No longer move forward
		if(initForward && !isFirstStart)
		{
			if(_Motor->distanceToGo()  == 0 || axisCheckDirStop())
			{
				initForward = false;
				Serial.print("+,Moving axis toward hall effect sensor\n");
				_axisCheckTimer = millis() + 2000;
			}
		}
		
		//**
		
		if(abs(_axisTarget - getAxisPos()) < 5.0 && !MotorEncoderWorking)
		{
			Serial.print("+," + _Name + " Movement Detected. Motor & Encoder Working.\n");
			MotorEncoderWorking = true;
			
		}
		
		//**
		
		//Hall effect works on negative logic. High is no trigger, low triggered
		// If hall effect is reading HIGH then it has power and is currently untriggered
		if(!HallTrusted)
		{
			if(digitalReadFast(_hallPin) == 1)
			HallTrusted = true;
		}
		//**
		
		// Move backwards towards the hall effect sensors
		// until either the hall effect sensor is switched or the motor stops moving/hits an end point
		if(!initForward)
		{
			
			_axisTarget = -10.0;
			
			//**
			
			if(axisCheckDirStop())
			{

				Serial.print("+,Encoder Stopped in Negative Direction.Issue with encoder tracking or Hall Effect Sensor\n");
				_axisEnable = false;
				//Motor Working, Hall Not
				if(MotorEncoderWorking && !HallTrusted)
				{
					InitResult = Status_HallFail;
					_StatusMessage = Status_HallFailMessage;
				}
				else
				{
					//Motor Not Working, Hall Working
					if(HallTrusted)
					{
						InitResult = Status_MotorFail;
						_StatusMessage = Status_MotorFailMessage;
					}else
					//Complete Failure case
					{
						InitResult = Status_CompleteFail;
						_StatusMessage = Status_CompleteFailMessage;
					}
				}
				
				_status = InitResult;
				
				break;
			}
			
			//**
			
			if(digitalReadFast(_hallPin) == 0)
			{
				//Hall Effect Sensor Trusted - Working
				//Zero Encoder, Flag Init Complete
				if(HallTrusted)
				{
					//Zero Position
					_MotorEncoder->zeroCounterAxisPosition(_AxisSensor);
					
					Serial.print("+,Axis Zero Trigger\n");
					_axisTarget = 0.0;
					InitResult = Status_AxisInitialised;
					
					_status = InitResult;
					_StatusMessage = Status_AxisInitialisedMessage;
					
					MotorEncoderWorking = true;
					axisCalculateSteps();
					
					break;
					//Hall Effect Untrusted. Error with Hall Effect Reading
				}else
				{
					_axisEnable = false;
					
					if(MotorEncoderWorking)
					{
						InitResult = Status_HallFail;
						_StatusMessage = Status_HallFailMessage;
					}
					else
					{
						InitResult = Status_CompleteFail;
						_StatusMessage = Status_CompleteFailMessage;
					}
					_status = InitResult;
					
					break;
				}
				
			}
			else
			_axisTarget = getAxisPos() - 10.0; // force the axis Target to be further away
			
			
		}
		//Stepper library run motor
		_Motor->run();
		
		//First start flag cleared
		isFirstStart = false;
	}
	
	//Timeout failure condition
	if(millis() - timerStart >= initTimeOut)
	{
		Serial.print("+,Axis Timeout Fail");
		_axisEnable = false;
		
		if(MotorEncoderWorking)
		{
			InitResult = Status_TimeOutFail;
			_StatusMessage = Status_TimeOutFailMessage;
		}
		else
		{
			InitResult = Status_MotorFail;
			_StatusMessage = Status_MotorFailMessage;
		}
		_status = InitResult;
	}
	
	//Set target to current - prevents further movement
	_axisTarget = getAxisPos();
	
	//Force library to recalculate remaining steps. Prevents axis from continueing movements after init
	axisCalculateSteps();
	
	return InitResult;

}

//******************************************************************
//******************************************************************

// Run Initialisation for Axis with IMU Axis Sensor
int AntennaMotors::initialiseWithIMU()
{
	Serial.print("+,Initialising " + _Name + "Axis with IMU\n");
	
	_StatusMessage = Status_InitialisingMessage;
	_status = Status_Initialising;
	
	int InitResult = 0;
	motorPowerControl(_axisPowerDown);
	
	//Check IMU Is intiliased and continue
	if(_imuSensor->_enabled)
	{
		_axisEnable = true;
		
		const unsigned long initTimeOut = 20000;
		
		InitResult = Status_MotorFail;
		
		unsigned long timerStart = millis();
		
		float AxisPosition = getAxisPos();
		
		_axisTarget  = 0.0;

		boolean isFirstStart = true;
		
		boolean MotorWorking = false;
		
		boolean initForward = true;

		_axisCheckTimer = millis() + 3000;
		_axisCheckRecordedMotorPos = AxisPosition;
		
		float StartPosition = AxisPosition;
		
		Serial.print("+,Start IMU Pos;" + String(StartPosition) + "\n");
		
		//**
		
		while(InitResult != Status_AxisInitialised && millis() - timerStart < initTimeOut)
		{
			ClearUSBBuffer();
			//Update Steps required for motor to close to target
			axisCalculateSteps();

			//**
			
			//Check for motor failure where motor is stopped
			if(axisCheckDirStop())
			{

				Serial.print("+," + _Name + " Axis Stopped - Motor Failure or Axis Reversed Direction\n");

				InitResult = Status_MotorFail;
				
				_StatusMessage = Status_MotorFailMessage;
				_status = InitResult;
				
				_axisEnable = false;
				
				break;
			}
			
			//**
			
			//Axis In Position - Passed Init!
			if(getAxisPos()  > -1.0*_axisError && getAxisPos()  < _axisError)
			{
				Serial.print("+," + _Name + " Axis Initialised!");
				InitResult = Status_AxisInitialised;
				
				_StatusMessage = Status_AxisInitialisedMessage;
				_status = InitResult;
				break;
			}

			//**
			
			//Check for motor movement toward target position
			if(abs(StartPosition - getAxisPos()) > 1.0 && !MotorWorking)
			{
				Serial.print("+," + _Name + " Axis Motor Movement Detected. Motor Working.\n");
				MotorWorking = true;
			}
			

			
			
			
			_Motor->run();
		}
		
		//**
		
		if(millis() - timerStart >= initTimeOut)
		{
			Serial.print("+,Axis Timeout Fail");
			_axisEnable = false;
			
			InitResult = Status_TimeOutFail;

			_axisTarget = 0.0;
			_status = InitResult;
			_StatusMessage = Status_TimeOutFailMessage;

		}
		
		
		//**
		
		_imuSensor->_ignoreSanityCheck = false;
	}else
	{
		//IMU Failed Initialisation. Cannot Continue with Motor Init
		InitResult = Status_IMUFailure;
		_StatusMessage = Status_IMUFailureMessage;
		_status = InitResult;
		_axisEnable = false;
	}
	
	
	
	return InitResult;
	
}

//Setup to use hall effect for initialisation
void AntennaMotors::useHallEffect(int hallPin)
{
	_hallPin = hallPin;
	pinMode(_hallPin, INPUT);
	
	_useHallEffect = true;
}


//******************************************************************

//*********************************************************************

float AntennaMotors::imuGetAxis()
{
	_imuSensor->getEularAngles();
	
	float axisVal = 0.0;
	
	switch(_imuAxis)
	{
		case Pitch:axisVal = _imuSensor->_Pitch; break;
		case Roll: axisVal = _imuSensor->_Roll; break;
		case Yaw: axisVal = _imuSensor->_Yaw; break;
	}
	

	
	return axisVal;
}


//******************************************************************
void AntennaMotors::axisCalculateSteps()
{
	if(_axisEnable)
	{
		//AxisTarget[AdjustedElTarget] = ElAdjustTarget(AxisTarget,ImuFrameVals);
		
		int StepperSteps = _axisMoveReverse*motorCalculateSteps();
		
		//PCSerial.print(", Adjust;"); PCSerial.print(NewTarget);
		
		_Motor->move(StepperSteps);
	}
}

//******************************************************************

int AntennaMotors::motorCalculateSteps()
{

	const float PosToStepY_El = 16.0*(1/1.8)*(72.00/12.000);// 12 to 100 belt ratio
	const float PosToStepX_AZ = 16.0*(1/1.8)*(100.0/12.000);// 12 to 112 belt ratio
	const float PosToStepX_Cr = 16.0*(1/1.8)*(72.0/12.000);// 12 to 100 belt ratio
	
	float PosToStep = 0.0;
	
	if(_AxisSensor == EncoderX)
	PosToStep = PosToStepX_AZ;
	else if(_AxisSensor == EncoderY)
	PosToStep = PosToStepY_El;
	else
	PosToStep = PosToStepX_Cr;
	
	_axisInPosition = false;
	
	float pTerm=0;
	
	float error = _axisTarget - getAxisPos();
	
	if(error > 180.00)
	error -= 360.0;
	
	if(error < -180.00)
	error += 360.0;
	
	if (abs(error) > _axisError )
	pTerm = PosToStep * error;
	else
	{
		_axisInPosition = true;
		pTerm = 0;
	}
	

	
	return -constrain(pTerm, -10240, 10240);
}


//********************************************************************

void AntennaMotors:: ClearUSBBuffer()
{
	while(Serial.available() > 10)
	Serial.read();
}