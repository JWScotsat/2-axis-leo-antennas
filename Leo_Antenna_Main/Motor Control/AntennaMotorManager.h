// AntennaMotorManager.h

#ifndef _ANTENNAMOTORMANAGER_h
#define _ANTENNAMOTORMANAGER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


//***************************************************************************************************************
// New Library Functions Created and Added
#include "..\AntennaProject_Defines.h"
#include "..\Motor Control/AccelStepper.h" // Online Library - creates signal pulses and handles Stepper Motor Acceleration and speed
#include "..\Motor Control/AntennaMotors.h" // Main library handles all Antenna Motors + Sensors - Links a Motor and Sensor - El + Encoder, Az + Encoder, Cross + IMU
#include "..\Sensor Classes/YeiSensor.h" // 3 Space Sensor Library for YEI IMUs
#include "..\Motor Control/AntennaEncoder.h" // Teensy Encoder Library - Hardware Quadrature Decoders (x2)
#include "..\Motor Control/DCMotors.h" // Custom library for handling DC Motor - for,back,stop etc.
#include "..\AntennaManager_ErrorTracking.h" // Error Tracking structure
//***************************************************************************************************************


//***************************************************************************************************************
//***************************************************************************************************************

class AntennaMotorManager
{
	protected:

	//Stepper Motor Instantiations for Accel Stepper Library
	AccelStepper *_AzStepper;
	AccelStepper *_ElStepper;
	AccelStepper *_CrossStepper;

	//Sensor Library Instantiations
	AntennaEncoder _TeensyEncoders;


	
	//Setup Sub functions
	void Setup_PinMode();
	void Setup_Motors_SpeedVals();
	void Setup_MotorClasses();
	void Setup_Motor_SensorPins();
	void Setup_IMUs();
	void Setup_AntMotorValues();
	
	//Initialisation Functions
	void InitialiseMotorPositions();
	void Init_IMUs();
	void Serial_Clear_USBForDelay(unsigned long DelayTime);
	
	//Debug LED flashes using RunMotors interrupt code
	void BlinkDebugLED();
	boolean _DebugLEDBlinkEnabled = true;
	unsigned long _DebugLEDCounter = 0;
	boolean _DebugLEDEn = true;
	int _DebugLEDPin = 13;
	
	boolean _DCPWMEn = false;
	
	
	//Status Timer Flags
	boolean Status_AznoTimerActive = true;
	boolean Status_CrossnoTimerActive = true;
	boolean Status_ElnoTimerActive = true;
	boolean Status_PolnoTimerActive = true;
	

	public:
	
	//Error Tracking from Init
	ErrorTracker _InitListArray[InitListSize];
	
	//Cross IMU Sensor
	YEISensor _YEICross;


	//Antenna Motor Objects - Controls each axis for positioning
	AntennaMotors _AntMotorAz;
	AntennaMotors _AntMotorEl;
	AntennaMotors _AntMotorCross;
	AntennaMotors _AntMotorPol;
	
	// Run function - creates Motor "clock" pulses for driving steppers. Also uses pwm speed control for DC Motor
	void RunMotors();

	
	// Updates all Axis target positions
	void UpdateMotorTargets(float UpdatedTargets[]);
	
	//Main Stop Command
	void AllStop();
	
	float _MotorTargetsAzElPol[3];
	
	// Returns state of axis if they are at their target position
	boolean isMotorsInPosition();
	
	// Initialisation
	void axisInitialisation();
	void axisInitialisationNew();
	
	//Main Setup
	void setup();
	
	//Motor status checking
	boolean CheckStatus();
	void ResetStatus();
	void setup_MotorStatusTimeouts(unsigned long AzTimeout, unsigned long ElTimeout, unsigned long CrossTimeout, unsigned long PolTimeout);
	
	String _Status_ErrorCondition = "+,Status Error - Default Value\n";

	unsigned long _AzMotorTimeout = 20000000;
	unsigned long _ElMotorTimeout = 20000000;
	unsigned long _CrossMotorTimeout = 20000000;
	unsigned long _PolMotorTimeout = 200000;
};


#endif

