/*
* AntennaMotors.cpp
*
* Created: 01/07/2015 13:23:57
* Author: Jack Wells
*/

#include "AntennaMotors.h"



//******************************************************************
//******************************************************************

// default constructor
// Set initial values/assignments for the used Accel Stepper Motor and the intended Axis Sensor
AntennaMotors::AntennaMotors(AccelStepper *MotorIn, int AxisSensor, String Name)
{
	_Motor = MotorIn;
	_AxisSensor = AxisSensor;
	_Name = Name;
	_axisEnable = true;
	_axisCheckRecordedMotorPos = 0.0;
	_axisCheckTimer = millis();
	_axisTrim = 0.0;
	_axisSensorReverse = 1.0;
	_axisMoveReverse = 1.0;
	_axisReadReverse = 1.0;
	_MotorIs = StepperMotor;
	
	
	

	//myPID = &MyPID;
	myPID.setup(PIDInput, PIDOutput, Setpoint, Kp, Ki, Kd, DIRECT);
	delay(1);
	myPID.mySetpoint = 0.000000;
	myPID.SetSampleTime(10);
	myPID.SetMode(MANUAL);
	myPID.SetOutputLimits(-4000.0, 4000.0);
	//myPID.SetControllerDirection(REVERSE);
} //AntennaMotors
//******************************************************************
// default destructor
AntennaMotors::~AntennaMotors()
{
} //~AntennaMotors

//******************************************************************
// default constructor
AntennaMotors::AntennaMotors()
{
} //~AntennaMotors


//******************************************************************
//******************************************************************
//******************************************************************
// Initialise Axis Motor
//  Also initialises the related axis sensor
void AntennaMotors::setup(AntennaEncoder *AxisEncoder)
{
	_MotorEncoder = AxisEncoder;
	
	switch (_AxisSensor)
	{
		case EncoderY:  _MotorEncoder->setup(EncoderY); _axisError = DefaultEncoderError; break;
		case EncoderX:  _MotorEncoder->setup(EncoderX); _axisError = DefaultEncoderError; break;
	}
	
}
//******************************************************************
// Initialise Axis Motor
//  Also initialises the related axis sensor
void AntennaMotors::setup(YEISensor *NewIMU, int SensorAxis)
{
	_imuSensor = NewIMU;
	_imuAxis = SensorAxis;
	_axisEnable = true;
	_AxisSensor = IMU;
	
	switch (_AxisSensor)
	{
		case IMU:  _axisError = 1.5; break;
		default: Serial.println(F("@,Antenna Motor Library Error - setup with IMU when IMU is not selected as axis sensor")); break;
	}
	
	
	
}

//******************************************************************
//Setup Configuration Values
void AntennaMotors::setupConfigValues(int AxisMoveReverse, int AxisReadReverse, float AxisTrim, float AxisErrorBuffer, boolean AxisEnable,
boolean AxisPowerDown, float AxisMaxBound, float AxisMinBound)
{
	_axisMoveReverse = AxisMoveReverse;
	_axisReadReverse = AxisReadReverse;
	_axisTrim = AxisTrim;
	_axisError = AxisErrorBuffer;
	_axisEnable = AxisEnable;
	_axisPowerDown = AxisPowerDown;
	_axisMaxBoundary = AxisMaxBound;
	_axisMinBoundary = AxisMinBound;
}

//******************************************************************
// Run Initialisation for Axis
// - Different functions depending on initialisation method
int AntennaMotors::initialise()
{

	int init = 0;
	
	if(_useHallEffect && (_AxisSensor == EncoderX || _AxisSensor == EncoderY))
	init = initialiseWithHallEncoder();
	else if(_AxisSensor == IMU)
	init = initialiseWithIMU();
	else if(_AxisSensor == RotaryPotEncoder)
	init = initialiseDC();
	else
	Serial.print("@,Antenna Motor Library Error - initialising with no sensor avaiable\n");
	
	return init;
}


//******************************************************************
//
//Returns Accel Stepper library position
int AntennaMotors::getPos()
{
	return _Motor->currentPosition();
}


//******************************************************************
//Returns Axis Encoder Position
float AntennaMotors::getAxisPos()
{
	float axisVal = 0.0;
	
	//Case statement to get different axis positional data based on sensor type
	switch (_AxisSensor)
	{
		case EncoderX : axisVal = _MotorEncoder->getEncPos(_AxisSensor) + _axisTrim;break;
		case EncoderY : axisVal = _MotorEncoder->getEncPos(_AxisSensor) + _axisTrim;break;
		case IMU : axisVal =  imuGetAxis() + _axisTrim ;break;
		case RotaryPotEncoder: axisVal = _DCMotor->getPos(); break;
		default : Serial.print("@,Antenna Motors Error - Reading non available sensor;" + String(_AxisSensor) + "\n"); break;
	}
	
	//Azimuth Condition - trim can put the encoder position out of 0-360 bounds. This brings it back in
	if(_Name.equalsIgnoreCase("Az Motor"))
	{
		if(axisVal > _axisMaxBoundary)
		axisVal -= _axisMaxBoundary;
		else if (axisVal < _axisMinBoundary)
		axisVal += _axisMaxBoundary;
	}

	
	if(_isUnitTesting)
	axisVal = _axisValUnitSpoof;
	
	//Reverse sensor data if required
	axisVal = axisVal*_axisReadReverse;
	
	return axisVal;
}


//******************************************************************
//Zero Axis Position for Encoders
void AntennaMotors::zeroAxisPos()
{
	_MotorEncoder->zeroCounterAxisPosition(_AxisSensor);
}

//******************************************************************
// Check if Axis Position has moved correctly, or if it's come to a stop/end/crashed
boolean  AntennaMotors::axisCheckDirStop()
{
	const unsigned long PositionCheck = 700UL;
	
	float axisPosition = getAxisPos();
	
	boolean MotorStopped = false;

	if(_axisCheckTimer < millis())
	{
		if(millis() - _axisCheckTimer > PositionCheck)
		{
			_axisCheckTimer = millis();
			
			if ((_axisCheckRecordedMotorPos <= (axisPosition + 1.5)) && (_axisCheckRecordedMotorPos >= (axisPosition - 1.5)))
			{
				MotorStopped = true;
				Serial.print("+," + _Name + " Axis Motor End Stopped! No Change in Motor Position\n");
				_axisCheckTimer = millis() + 5000UL;
			}

			_axisCheckRecordedMotorPos = axisPosition;
			
			
		}
	}
	return MotorStopped;
}


//*******************************************
//Update motor Target steps so that motors move to new target position
void AntennaMotors::moveToTarget()
{
	if(_axisEnable && !_axisPowerDown)
	{
		if(_MotorIs == StepperMotor)
		{
			if(_AltStepMoveEnable)
			ALTAxisCalculateStepsAndSpeed();
			else
			axisCalculateSteps();
		}
		else if (_MotorIs == DCMotor)
		DCMoveToTarget();
	}
	
}

//*******************************************
//Motor Power Down Control
void AntennaMotors::motorPowerControl(boolean isPowerDown)
{
	_axisPowerDown = isPowerDown;
	
	if(_MotorIs == StepperMotor)
	digitalWriteFast(_PowerDownPin, _axisPowerDown);
	else
	if(_MotorIs == DCMotor && _axisPowerDown)
	_DCMotor->Disable();
	
}

//*******************************************
//Run Motor Interrupt Call
void AntennaMotors::run()
{
	if(_AltStepMoveEnable && _usePID)
	_Motor->runSpeed();
	else
	_Motor->run();

}

//*******************************************
//Alternative Function for near PID calculation of speed and steps for stepper Motor
void AntennaMotors::ALTAxisCalculateStepsAndSpeed()
{
	static int PIDPrintCount = 0;
	
	_axisInPosition = false;
	
	float error = _axisTarget - getAxisPos();
	
	if(error > 180.00)
	error -= 360.0;
	
	if(error < -180.00)
	error += 360.0;
	
	if (abs(error) < _axisError)
	{
		_axisInPosition = true;
// 		if(_atZeroCounter >= 5)
// 		{
			myPID.ITerm = 0;
			error = 0;
// 		}
// 		else
// 		_atZeroCounter++;

	}else
	_atZeroCounter = 0;

	if(abs(error) >= 2.00001)
	{
		_usePID = false;
		myPID.SetMode(MANUAL);
		myPID.ITerm = 0;
		returnPID = 0;
	}
	else
	{
		
		if(returnPID >= 500)
		{
			
			if(!_usePID)
			{
				_atZeroCounter = 5;
				myPID.SetMode(AUTOMATIC);
			}

			_usePID = true;
		}else
		returnPID++;
	}

	if(_usePID)
	{
		myPID.myInput = (double)error;

		myPID.Compute();
		
		if((myPID._ItermVal > 0.01 && myPID._PropVal < -0.01) || (myPID._ItermVal < -0.01 && myPID._PropVal > 0.01)) // Overshoot Condition
		{
			myPID.ITerm = 0;	//Reset Iterm value during overshoot condition
			myPID.Compute();
		}
		
		float NewSpeed = constrain(myPID.myOutput, -4000.0, 4000.0);
		
		_Motor->setSpeed(_axisMoveReverse*NewSpeed);

		_AltSpeedValue = NewSpeed;
	}else
	{
		int StepperStepsError = _axisMoveReverse*motorCalculateSteps();
		_Motor->move(StepperStepsError);
	}
	
	PIDPrintCount++;
	
	if(PIDPrintCount >= 500)
	{
		Serial.print("~,");
		Serial.print(error,4);Serial.print(",");
		Serial.print(myPID.myInput);Serial.print(",");
		Serial.print(_AltSpeedValue);Serial.print(",");
		Serial.print(myPID.GetKp());Serial.print(",");
		Serial.print(myPID.GetKi());Serial.print(",");
		Serial.print(myPID.GetKd());Serial.print(",");
		
		Serial.print(myPID._PropVal);Serial.print(",");
		
		Serial.print(myPID._ItermVal);Serial.print(",");
		
		Serial.print(myPID._DVal);Serial.print(",");
		Serial.println("");
		
		PIDPrintCount = 0;
	}
	
	
	
	//_altMotorTimeStamp = micros();
}

//*******************************************
//Alternative Function setting PID values
void AntennaMotors::setPIDValues()
{
	myPID.SetTunings(Kp, Ki, Kd);
}