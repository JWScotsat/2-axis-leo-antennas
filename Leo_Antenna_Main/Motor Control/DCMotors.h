/*
* AntennaMotors.h
*
* Created: 01/07/2015 13:23:58
* Author: Jack Wells
*/
#ifndef __DCMOTORS_H__
#define __DCMOTORS_H__

#include <stdlib.h>
#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <wiring.h>
#endif

#include <core_pins.h>

// Main Antenna Motors Class.
//  Manages each axis motors; Including axis measurement and intialisation
class DCMotors
{
	//variables
	public:
	boolean _axisReverse;
	float _axisTrim;
	String _Name;
	float _axisSensorReverse;
	
	protected:

	private:
	int _PWMCounter;
	boolean _DriveEnabled;
		
	//functions
	public:
	DCMotors();// Constructor - currently unused
	
	DCMotors(int M1Pin, int M2Pin, int ENPin, int EncoderPin, String Name); // Setup call. 
	DCMotors(int DirPin, int ENPin, int EncoderPin, String Name); // Setup call for Driver with Direction-Enable control scheme.
	~DCMotors();// Destructor - currently unused
	
	
	
	void setup();// Initial setup. Assigns and initialises the related sensor.
	
	float getPos(); // Return Position
	
	void MoveForward();
	void Enable();
	void Disable();
	void Stop();
	void MoveBackward();
	void PWMPulse();
	
	protected:


	private:
	int _pinM1;
	int _pinM2;
	int _pinEna;
	int _pinEncoder;

	boolean _Driver_DirEn = false;
	// 	AntennaMotors( const AntennaMotors &c );
	// 	AntennaMotors& operator=( const AntennaMotors &c );

}; //AntennaMotors

#endif //__ANTENNAMOTORS_H__
