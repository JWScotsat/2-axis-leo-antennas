/*
* AntennaEncoder.cpp
*
* Created: 02/07/2015 08:42:41
* Author: Jack Wells
*/

#include "..\Sensor Classes/QuadDecode_def.h"
#include "..\Sensor Classes/QuadDecode.h"

#include "AntennaEncoder.h"

QuadDecode<1> Counter1;	// Template using FTM1
QuadDecode<2> Counter2;	// Template using FTM1

//default constructor
AntennaEncoder::AntennaEncoder()
{
} //AntennaEncoder

void AntennaEncoder::setup(int AssignedCounter)
{


	if(AssignedCounter == xCounter)
	{
		Counter2.setup();
	}else if(AssignedCounter == yCounter)
	{
		Counter1.setup();
	}
	else
	Serial.println("@,Antenna Encoder Library Error. Tried to Zero counter of unasigned counter;" + String(AssignedCounter));
	
	zeroCounter(AssignedCounter);
	
} //AntennaEncoder

// default destructor
AntennaEncoder::~AntennaEncoder()
{
} //~AntennaEncoder




float AntennaEncoder::getEncPos(int AssignedCounter)
{
	int32_t pos= GetCounterPos(AssignedCounter);

	float EncCount = 0.0;
	float trackedCounter = 0.0;
	
	if(AssignedCounter == xCounter)
	{
		if (pos > 33600)
		{
			_trackedCounterX += pos*EncoderCountsToDegsX;
			zeroCounter(AssignedCounter);
			

			pos=GetCounterPos(AssignedCounter);
		}
		
		if (pos < -33600)
		{
			_trackedCounterX += pos*EncoderCountsToDegsX;
			zeroCounter(AssignedCounter);
			

			pos=GetCounterPos(AssignedCounter);
		}
		
		if(_xCounterRolloverBoundaried)
		{
			if((pos*EncoderCountsToDegsX) + _trackedCounterX > 360.001)
			_trackedCounterX -= 360.001;
			else if ((pos*EncoderCountsToDegsX) + _trackedCounterX < -0.001)
			_trackedCounterX += 360.001;
		}
		

		trackedCounter = _trackedCounterX;
		
		EncCount = (pos*EncoderCountsToDegsX) + trackedCounter;
		
	}else if(AssignedCounter == yCounter)
	{
		if (pos > 33600)
		{
			_trackedCounterY += pos*EncoderCountsToDegsY;
			zeroCounter(AssignedCounter);
			

			pos=GetCounterPos(AssignedCounter);
		}
		
		if (pos < -33600)
		{
			_trackedCounterY += pos*EncoderCountsToDegsY;
			zeroCounter(AssignedCounter);
			

			pos=GetCounterPos(AssignedCounter);
		}
		
		if(_yCounterRolloverBoundaried)
		{
			if((pos*EncoderCountsToDegsY) + _trackedCounterY > 360.001)
			_trackedCounterY -= 360.001;
			else if ((pos*EncoderCountsToDegsY) + _trackedCounterY < -0.001)
			_trackedCounterY += 360.001;
		}
		
		trackedCounter = _trackedCounterY;
		EncCount = (pos*EncoderCountsToDegsY) + trackedCounter;
	}
	
	
	
	
	return EncCount;
}



//
// **********************************************************************************************
//**********************************************************************************************
void AntennaEncoder::zeroCounter(int AssignedCounter)
{
	if(AssignedCounter == yCounter)
	Counter1.zeroFTM();
	else if(AssignedCounter == xCounter)
	Counter2.zeroFTM();
	else
	Serial.println("@,Antenna Encoder Library Error. Tried to Zero counter of an unasigned counter;" + String(AssignedCounter));
}

float AntennaEncoder::GetCounterPos(int AssignedCounter)
{
	if(AssignedCounter == xCounter)
	return Counter2.calcPosn();
	else if(AssignedCounter == yCounter)
	return Counter1.calcPosn();
	else
	Serial.println("@,Antenna Encoder Library Error. Tried to read counter of an unasigned counter;" + String(AssignedCounter));
	
	return -255.0;
}

// **********************************************************************************************
//**********************************************************************************************
void AntennaEncoder::zeroCounterAxisPosition(int AssignedCounter)
{
	zeroCounter(AssignedCounter);
	
	if(AssignedCounter == xCounter)
	_trackedCounterX = 0.0;
	else if(AssignedCounter == yCounter)
	_trackedCounterY = 0.0;
	else
	Serial.println("@,Antenna Encoder Library Error. Tried to Zero Axis counter of an unasigned counter;" + String(AssignedCounter));
	
}
