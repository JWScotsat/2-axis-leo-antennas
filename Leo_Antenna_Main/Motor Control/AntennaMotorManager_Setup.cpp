#include "AntennaMotorManager.h"

/*
Antenna Motor Manager Setup
Created By: Jack Wells

Top Level Antenna functionality and Setup code for Antenna Motor Manager. Applies default settings to Motor and supporting sub classes.

Returns: Nothing, no current checks for successful setup

*/

// Motor Object Instantiations - Can not be instantiate inside a function as they are deleted on function end
AccelStepper AzStepper(AccelStepper::DRIVER, AzStepPin, AzDirPin);
AccelStepper ElStepper(AccelStepper::DRIVER, ElStepPin, ElDirPin);
AccelStepper CrossStepper(AccelStepper::DRIVER, CrossStepPin, CrossDirPin);
DCMotors PolMotor(MotorPolDir,MotorPolEnable,PolEncoderPin, "DC Pol");

//**********************************************************************************
//**********************************************************************************

/* Setup - Setup classes with default values
- Returns: Nothing
*/
void AntennaMotorManager::setup()
{
	Serial.print("+,AntennaMotorManager - Setup\n");
	
	Setup_PinMode();
	
	Setup_MotorClasses();
	
	Setup_Motor_SensorPins();
	
	//Custom YEI Library - Uses serial port to configure 3 Space Sensor for use
	Setup_IMUs();
	
	Setup_Motors_SpeedVals();
	
	
	
	Setup_AntMotorValues();

}
//*********************************************************************************************************
//Ensure all motor pins are in a defined state and not left floating
void AntennaMotorManager::Setup_PinMode()
{
		pinMode(CrMicro32Pin,OUTPUT);
		digitalWrite(CrMicro32Pin,LOW);
		pinMode(AzMicro32Pin,OUTPUT);
		digitalWrite(AzMicro32Pin,LOW);
		pinMode(ElMicro32Pin,OUTPUT);
		digitalWrite(ElMicro32Pin,LOW);
		
		pinMode(ElDirPin,OUTPUT);
		digitalWrite(ElDirPin,LOW);
		pinMode(ElEnablePin,OUTPUT);
		digitalWrite(ElEnablePin,HIGH);
		pinMode(ElStepPin,OUTPUT);
		digitalWrite(ElStepPin,LOW);
}

//*********************************************************************************************************
void AntennaMotorManager::Setup_MotorClasses()
{
	// Antenna Motor Instantiations for Each Axis Motor + Sensor pairing
	AntennaMotors AntMotorAz(&AzStepper, EncoderX, "Az Motor");
	AntennaMotors AntMotorEl(&ElStepper, EncoderY, "El Motor");
	AntennaMotors AntMotorCross(&CrossStepper, IMU, "Cross IMU Motor");
	AntennaMotors AntMotorPol(&PolMotor, RotaryPotEncoder, "Polarisation Motor");
	
	// Setup hall effect sensor pins for El and Az Axis
	AntMotorEl.useHallEffect(Sensor_ElHallPin);// Hall Effect setup
	AntMotorAz.useHallEffect(AzHallEffectPin); // Hall Effect setup
	
	// Setup Axis Sensors for Axis Position Tracking
	_TeensyEncoders._xCounterRolloverBoundaried = true;

	AntMotorAz.setup(&_TeensyEncoders); // Links Antenna Motor Az to Encoder Library to access the Az Quad Decode
	
	AntMotorEl.setup(&_TeensyEncoders); // Links Antenna Motor El to Encoder Library to access the El Quad Decode
	
	AntMotorCross.setup(&_YEICross, Pitch); // Links Antenna Motor Cross to YEI Library to access the Cross IMU and defines the Pitch Axis as the related axis to motor movement
	
	

	// Setup Pointers to Motor Objects to allow external class access
	_AzStepper = &AzStepper;
	_ElStepper = &ElStepper;
	_CrossStepper = &CrossStepper;
	//_PolMotor = &NewPolMotor;

	_AntMotorAz = AntMotorAz;
	_AntMotorCross = AntMotorCross;
	_AntMotorEl = AntMotorEl;
	_AntMotorPol = AntMotorPol;
	
	_AntMotorAz._PowerDownPin = AzEnablePin;
	_AntMotorCross._PowerDownPin = CrossEnablePin;
	_AntMotorEl._PowerDownPin = ElEnablePin;
}

//*********************************************************************************************************
//Setup Max/Min boundaries for Axes
void AntennaMotorManager::Setup_AntMotorValues()
{
	_AntMotorEl._axisMaxBoundary = MaxPhysical_Elevation;
	_AntMotorEl._axisMinBoundary = MinPhysical_Elevation;
	
	_AntMotorPol._axisMaxBoundary = MaxPhysical_Pol;
	_AntMotorPol._axisMinBoundary = MinPhysical_Pol;
}

//*********************************************************************************************************

void AntennaMotorManager::Setup_Motor_SensorPins()
{


	_AntMotorPol.setup();

	_AntMotorPol._axisMoveReverse = 1;
	

	analogReadResolution(12); //Analogue read setup for the Polarisation encoder
	
	
	pinMode(Sensor_ElHallPin,INPUT);
	pinMode(AzHallEffectPin,INPUT);
	
	pinMode(_DebugLEDPin, OUTPUT);
	digitalWriteFast(_DebugLEDPin,HIGH);
	
}

//*********************************************************************************************************

void AntennaMotorManager::Setup_Motors_SpeedVals()
{
	
	//Motor Setup
	PiSerial.print("+,Configuring Motor Speed Settings\n");
	ElStepper.setPinsInverted(true,false,false);
	ElStepper.setMaxSpeed(4000.0);
	ElStepper.setAcceleration(4000.0);
	ElStepper.setMinPulseWidth(4);
	_AntMotorEl._axisReadReverse = -1.0;
	
	
	AzStepper.setPinsInverted(true,false,false);
	AzStepper.setMaxSpeed(4000.0);
	AzStepper.setAcceleration(4000.0);
	AzStepper.setMinPulseWidth(4);
	
	CrossStepper.setPinsInverted(true,false,false);
	CrossStepper.setMaxSpeed(4000.0);
	CrossStepper.setAcceleration(4000.0);
	CrossStepper.setMinPulseWidth(4);
	
	
	pinMode(ElEnablePin,OUTPUT);
	pinMode(AzEnablePin,OUTPUT);
	pinMode(CrossEnablePin,OUTPUT);
	
}

//*********************************************************************************************************


void AntennaMotorManager::Setup_IMUs()
{
	//Custom YEI Library - Uses serial port to configure 3 Space Sensor for use
	_YEICross.begin(&ImuCrossSerial, ImuBaudRate, "Cross Sensor");
	
}


void AntennaMotorManager::RunMotors()
{
	if(_AntMotorAz._AltStepMoveEnable && _AntMotorAz._usePID)
	AzStepper.runSpeed();
	else
	AzStepper.run();
	
	ElStepper.run();
	CrossStepper.run();
	
	if(_DCPWMEn)
	PolMotor.PWMPulse();
	
	BlinkDebugLED();
}

//*********************************************************************************************************


//*********************************************************************************************************

void AntennaMotorManager::BlinkDebugLED()
{
	if(_DebugLEDBlinkEnabled)
	{
		_DebugLEDCounter++;
		
		if(_DebugLEDCounter > 50000)
		{
		_DebugLEDEn = !_DebugLEDEn;
		digitalWriteFast(_DebugLEDPin,_DebugLEDEn);
		_DebugLEDCounter = 0;
		}
	}
}

//Set antenna motor status timeouts
void AntennaMotorManager::setup_MotorStatusTimeouts(unsigned long AzTimeout, unsigned long ElTimeout, unsigned long CrossTimeout, unsigned long PolTimeout)
{
	_AzMotorTimeout = AzTimeout;
	_ElMotorTimeout = ElTimeout;
	_CrossMotorTimeout = CrossTimeout;
	_PolMotorTimeout = PolTimeout;
}

//************************************************************/\************************************************************\\