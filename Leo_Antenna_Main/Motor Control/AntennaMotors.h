/*
* AntennaMotors.h
*
* Created: 01/07/2015 13:23:58
* Author: Jack Wells
*/
#ifndef __ANTENNAMOTORS_H__
#define __ANTENNAMOTORS_H__


#include "..\Motor Control/AccelStepper.h"



// #include "YEISensor.h"
// #include "DCMotors.h"


#include "AntennaEncoder.h"

#include "..\PID Lib/PID_v1.h"

#include "..\Sensor Classes/YeiSensor.h" // 3 Space Sensor Library for YEI IMUs
#include "DCMotors.h"

//Defines for which Axis sensor - Encoder 1, Encoder 2, or IMU Serialx
#define EncoderY 1
#define EncoderX 2
#define IMU 3
#define RotaryPotEncoder 4


// Defines used for returning from initialisation
#define Status_MotorFail -1
#define Status_HallFail -2
#define Status_TimeOutFail -3
#define Status_IMUFailure -4
#define Status_CompleteFail -6
#define	Status_EncoderFailure -5
#define Status_Initialising 0
#define Status_AxisInitialised 1


#define Status_InitialisingMessage "+," + _Name + "  Initialising Condition\n"
#define Status_AxisInitialisedMessage "+," + _Name + "  Passed Init\n"
#define Status_MotorFailMessage "@," + _Name + " failed Init - Motor Error\n"
#define Status_HallFailMessage "@," + _Name + "  failed Init - Hall Error\n"
#define Status_IMUFailureMessage "@," + _Name + " failed Init - IMU Error\n"
#define Status_EncoderFailureMessage "@," + _Name + " failed Init - Encoder Error\n"
#define Status_TimeOutFailMessage "@," + _Name + " failed Init - Timeout Error\n"
#define Status_CompleteFailMessage "@," + _Name + " failed Init - Possible Power Issue, all sensors failed\n"

//Define for MotorIs - Either Stepper or DC
#define StepperMotor 1
#define DCMotor 2

//Defines for IMU axis positions
#define Yaw 0
#define Pitch 1
#define Roll 2

#define DefaultEncoderError 0.050



// Main Antenna Motors Class.
//  Manages each axis motors; Including axis measurement and intialisation
class AntennaMotors
{
	//variables
	public:
	int _axisMoveReverse;
	int _axisReadReverse;
	float _axisTrim;
	String _Name;
	float _axisSensorReverse;
	float _axisTarget;
	boolean _axisInPosition;
	float _axisError;
	boolean _axisEnable = true;
	boolean _axisPowerDown = false;
	float _axisMinBoundary;
	float _axisMaxBoundary;
	
	int _PowerDownPin;
	
	float _axisValUnitSpoof = 0.0;
	boolean _isUnitTesting = false;
	float _axisTargetTrimmed = 0.0;
	float _axisTargetBound = 0.0;
	boolean _isDCAxisTargetBoundMidWay = true;
	
	int _status = 0;
	String _StatusMessage = "+,Uninitialsed. Default String Value";
	

	protected:

	private:
	
	int _AxisSensor;		// Axis Sensor type
	int _hallPin;		// Hall effect pin in use
	boolean _useHallEffect; // Hall effect usage
	AntennaEncoder *_MotorEncoder; //Encoder Object from AntennaEncoder. Quadrature Encoder Sensor
	int _DCStoppedCount;

	unsigned long _axisCheckTimer;
	float _axisCheckRecordedMotorPos;
	
	
	int _imuAxis;
	
	int _MotorIs;
	
	
	//functions
	public:
	AccelStepper *_Motor; // Accel Stepper Motor Reference
	
	YEISensor *_imuSensor;
	
	DCMotors *_DCMotor;
	
	AntennaMotors(AccelStepper *MotorIn, int AxisSensor, String Name); // Setup call. Receives a AccelStepper Motor Object and value to assign the axis sensor
	AntennaMotors(DCMotors *MotorIn, int AxisSensor, String Name); // Setup call. Receives a AccelStepper Motor Object and value to assign the axis sensor
	~AntennaMotors();// Destructor - currently unused
	AntennaMotors();// Destructor - currently unused
	
	void setup(AntennaEncoder *AxisEncoder);// Initial setup. Assigns and initialises the related sensor.
	void setup(YEISensor *NewIMU, int SensorAxis);
	void setup();
	
	void useHallEffect(int hallPin); // Assign use of hall effect and Pin number
	int initialise();
	
	void zeroAxisPos();
	int getPos(); // Return Accel Stepper Library Position
	float getAxisPos(); // Return Axis Sensor Position

	
	void moveToTarget();
	
	void motorPowerControl(boolean isPowerDown); // Shutdown motor
	
	void setupConfigValues(int AxisMoveReverse, int AxisReadReverse, float AxisTrim, float AxisErrorBuffer, boolean AxisEnable,
	boolean AxisPowerDown, float AxisMaxBound, float AxisMinBound);
	
	void run();


	// Alternative Stepper Movement
	boolean _AltStepMoveEnable = false;
	float _AltSpeedValue = 0.0;
	void ALTAxisCalculateStepsAndSpeed(); //Alternative function for calculating speed as well as new target position
	unsigned long _altMotorTimeStamp = 0;


	//PID Control
	double Setpoint = 0.000000;
	double PIDInput = 0.000000;
	double PIDOutput = 0.000000;

	//Specify the links and initial tuning parameters
	double Kp=100;
	double Ki=10;
	double Kd=0;
	boolean _usePID = false;
	unsigned long returnPID = 0;
	PID myPID;//(&PIDInput, &PIDOutput, &Setpoint, Kp, Ki, Kd, DIRECT);
	void setPIDValues();
	unsigned long _atZeroCounter = 0;
	protected:


	private:
	int initialiseWithHallEncoder();
	int initialiseWithIMU();
	int initialiseDC();
	void axisCalculateSteps();
	int motorCalculateSteps();
	boolean axisCheckDirStop();
	float imuGetAxis();
	
	void DCMoveToTarget();
	void DCTargetUpdate();
	
	void ClearUSBBuffer();
	

	// 	AntennaMotors( const AntennaMotors &c );
	// 	AntennaMotors& operator=( const AntennaMotors &c );

}; //AntennaMotors

#endif //__ANTENNAMOTORS_H__
