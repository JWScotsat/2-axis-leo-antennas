//
//
//

#include "AntennaMotorManager.h"

// New Axis Initialisation
// Inputs _InitList memory location. Adds found errors to error list
void AntennaMotorManager::axisInitialisationNew()
{
	
	Serial.print("+,AntennaMotorManager - Initialising Axes Sensors\n");
	
	//IMU Cross axis Init and store of status messages
	//Currently disabled due to lack of design requirement
	//_YEICross.Initialise();
	_InitListArray[ListCrossIMU].StatusMessage = _YEICross._statusMessage;
	_InitListArray[ListCrossIMU].StatusValue = 1;//_YEICross._status;

	Serial.print(_InitListArray[ListCrossIMU].StatusMessage);
	
	boolean DebugPrint = true;
	
	if(DebugPrint)
	Serial.print("+,Initialising Motor Positions\n");
	


	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	// Currently disabled due to lack of design requirement
	_InitListArray[ListMotorCross].StatusValue = 1;//_AntMotorCross.initialise();
	_InitListArray[ListMotorCross].StatusMessage = _AntMotorCross._StatusMessage;
	
	Serial.print(_InitListArray[ListMotorCross].StatusMessage);
	
	Serial_Clear_USBForDelay(10); // USB Serial buffer must be kept clear in order to prevent USB lock up

	//**

	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	_InitListArray[ListMotorEl].StatusValue = _AntMotorEl.initialise();
	_InitListArray[ListMotorEl].StatusMessage = _AntMotorEl._StatusMessage;
	
	if(_InitListArray[ListMotorEl].StatusValue != Status_AxisInitialised)
	_AntMotorEl._axisEnable = false;
	
	Serial.print(_InitListArray[ListMotorEl].StatusMessage);

	
	Serial_Clear_USBForDelay(10);

	//**
	
	
	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	_InitListArray[ListMotorAz].StatusValue = _AntMotorAz.initialise();
	_InitListArray[ListMotorAz].StatusMessage = _AntMotorAz._StatusMessage;
	
	if(_InitListArray[ListMotorAz].StatusValue != Status_AxisInitialised)
	_AntMotorAz._axisEnable = false;
	
	Serial.print(_InitListArray[ListMotorAz].StatusMessage);

	Serial_Clear_USBForDelay(10);
	
	//**
	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	//Polarisation Is currently disabled due to lack of design requirement
	_InitListArray[ListMotorPol].StatusValue = _AntMotorPol.initialise();
	_InitListArray[ListMotorPol].StatusMessage = _AntMotorPol._StatusMessage;
	
	if(_InitListArray[ListMotorPol].StatusValue != Status_AxisInitialised)
	_AntMotorPol._axisEnable = false;
	
	Serial.print(_InitListArray[ListMotorPol].StatusMessage);

	


}

//*********************************************************************************************************//*********************************************************************************************************
//*********************************************************************************************************//*********************************************************************************************************


//Legacy code
void AntennaMotorManager::Init_IMUs()
{
	_YEICross.Initialise();
	//_YEIFrame.Initialise();

}

void AntennaMotorManager::axisInitialisation()
{
	Serial.print("+,AntennaMotorManager - Initialising Axes Sensors\n");
	Init_IMUs();
	
	InitialiseMotorPositions();

}

//*********************************************************************************************************
//*********************************************************************************************************

//Legacy Code
void AntennaMotorManager::InitialiseMotorPositions()
{
	boolean DebugPrint = true;
	
	if(DebugPrint)
	Serial.print("+,Initialising Motor Positions\n");
	


	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	int InitState_CrossMotorInitialised = _AntMotorCross.initialise();
	Serial.print(_AntMotorCross._StatusMessage);

	Serial_Clear_USBForDelay(1000);

	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	int InitState_ElMotorInitialised = _AntMotorEl.initialise();
	Serial.print(_AntMotorEl._StatusMessage);

	Serial_Clear_USBForDelay(1000);

	// Initialisation of axis. Auto assigns init based on attached sensors.
	// Returns Complete Initialisation or Error code after failure
	int InitState_AzMotorInitialised = _AntMotorAz.initialise();
	Serial.print(_AntMotorAz._StatusMessage);

	//
	
	int InitState_PolMotorInitialised = _AntMotorPol.initialise();
	Serial.print(_AntMotorPol._StatusMessage);
	// 	if(InitState_PolMotorInitialised != AxisInitialised)
	// 	_PolMotor.Disable();



}
