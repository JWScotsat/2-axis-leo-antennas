//
//
//

#include "AntennaMotorManager.h"




//*********************************************************************************************************

void AntennaMotorManager::UpdateMotorTargets(float UpdatedTargets[])
{
	// Update local targets of each motor
	if(UpdatedTargets[El] < _AntMotorEl._axisMinBoundary)
	_AntMotorEl._axisTarget = _AntMotorEl._axisMinBoundary;
	else if (UpdatedTargets[El] > _AntMotorEl._axisMaxBoundary)
	_AntMotorEl._axisTarget = _AntMotorEl._axisMaxBoundary;
	else
	_AntMotorEl._axisTarget = UpdatedTargets[El];
	
	_AntMotorAz._axisTarget = UpdatedTargets[Az];
	
	
	if(UpdatedTargets[Pol] < _AntMotorPol._axisMinBoundary)
	_AntMotorPol._axisTarget =_AntMotorPol._axisMinBoundary;
	else if (UpdatedTargets[Pol] > _AntMotorPol._axisMaxBoundary)
	_AntMotorPol._axisTarget = _AntMotorPol._axisMaxBoundary;
	else
	_AntMotorPol._axisTarget = UpdatedTargets[Pol];
	
	// Recalculate number of steps for each motor
	_AntMotorEl.moveToTarget();
	_AntMotorAz.moveToTarget();
	_AntMotorPol.moveToTarget();
	_AntMotorCross.moveToTarget();
}

//*********************************************************************************************************
// Returns boolean value for all motors completing movement
boolean AntennaMotorManager::isMotorsInPosition()
{
	int inPosCount = 0;
	
	
	if(_AntMotorEl._axisInPosition || !_AntMotorEl._axisEnable || _AntMotorEl._axisPowerDown)
	inPosCount++;

	//if(_AntMotorAz._axisInPosition || !_AntMotorAz._axisEnable || _AntMotorAz._axisPowerDown)
	//inPosCount++;

	
// 	if(_AntMotorCross._axisInPosition || !_AntMotorCross._axisEnable || _AntMotorCross._axisPowerDown)
// 	inPosCount++;

	
	if(_AntMotorPol._axisInPosition || !_AntMotorPol._axisEnable || _AntMotorPol._axisPowerDown)
	inPosCount++;

	
	if(inPosCount == 2)
	return true;
	else
	return false;
	
}

//*********************************************************************************************************




//******************************************************************************
//******************************************************************************
// Flush the selected serial port to ensure no remaining data in the buffer
void AntennaMotorManager::Serial_Clear_USBForDelay(unsigned long delayTime)
{
	unsigned long startTime = millis();
	// 	if(Advanced_PrintOuts.DebugInfoOut)
	// 	{
	// 		Serial.print("+,Clearing Serial USB Buffer for Delay;"); Serial.println(delayTime);
	// 	}
	
	while(millis() - startTime < delayTime)
	{
		if(Serial.available())
		byte  w = Serial.read();
	}
}


//*********************************************************************************************************
//All Stop Command
//  Tells all motors to hold current position. Shuts down Pol DC Motor
void AntennaMotorManager::AllStop()
{
	_AntMotorPol._DCMotor->Stop();
	_AntMotorPol._axisEnable = false;
	
	_AntMotorEl._Motor->moveTo(_AntMotorEl._Motor->currentPosition());
	
	_AntMotorAz._Motor->moveTo(_AntMotorAz._Motor->currentPosition());
	
	_AntMotorCross._Motor->moveTo(_AntMotorCross._Motor->currentPosition());
}

//*********************************************************************************************************
//Check Axis Status
//  Uses timers to check all axis are moving to target correctly
//  Timers reset on _axisInPosition flag
//  Timeouts create error condition which is returned on CheckStatus call
boolean AntennaMotorManager::CheckStatus()
{
	//Resulting bool - false reports Axis Failure
	boolean WorkingStatus = true;
	
	_Status_ErrorCondition = "";
	
	//Elevation Motor Axis
	static unsigned long Status_ElTimer = millis();
	
	if(!_AntMotorEl._axisInPosition && _AntMotorEl._axisEnable && !_AntMotorEl._axisPowerDown)
	{
		//Start timer and define timeout required based on distance traveled
		if(Status_ElnoTimerActive)
		{

			Status_ElTimer = millis();
			Status_ElnoTimerActive = false;
		}
		
		// Report Error Condition
		if(millis() - Status_ElTimer > _ElMotorTimeout)
		{
			_Status_ErrorCondition += ("@,Status Check - El Axis Timeout Condition. Possible El Axis Error\n");
			WorkingStatus = false;
		}
	}
	else
	Status_ElnoTimerActive = true; // Reset Timer Condition
	
	
	//Pol Motor Axis
	
// 	static unsigned long Status_PolTimer = millis();
// 
// 	if(!_AntMotorPol._axisInPosition && _AntMotorPol._axisEnable && !_AntMotorPol._axisPowerDown)
// 	{
// 		if(Status_PolnoTimerActive)
// 		{
// 		
// 			Status_PolTimer = millis();
// 			Status_PolnoTimerActive = false;
// 		}
// 		
// 		if(millis() - Status_PolTimer > _PolMotorTimeout)
// 		{
// 			_Status_ErrorCondition += ("@,Status Check - Pol Axis Timeout Condition. Possible Pol Axis Error\n");
// 			WorkingStatus = false;
// 		}
// 	}
// 	else
// 	Status_PolnoTimerActive = true;
	
	//Azimuth Motor Axis
	static unsigned long Status_AzTimer = millis();
	
	if(!_AntMotorAz._axisInPosition && _AntMotorAz._axisEnable && !_AntMotorAz._axisPowerDown)
	{
		if(Status_AznoTimerActive)
		{

			Status_AzTimer = millis();
			Status_AznoTimerActive = false;
		}
		
		if(millis() - Status_AzTimer > _AzMotorTimeout)
		{
			_Status_ErrorCondition += ("@,Status Check - Az Axis Timeout Condition. Possible Az Error\n");
			WorkingStatus = false;
		}
	}
	else
	Status_AznoTimerActive = true;



	//Cross Motor Axis
// 	static unsigned long Status_CrossTimer = millis();
// 
// 	
// 	if(!_AntMotorCross._axisInPosition && _AntMotorCross._axisEnable && !_AntMotorCross._axisPowerDown)
// 	{
// 		if(Status_CrossnoTimerActive)
// 		{
// 			
// 			Status_CrossTimer = millis();
// 			Status_CrossnoTimerActive = false;
// 		}
// 		
// 		if(millis() - Status_CrossTimer > _CrossMotorTimeout)
// 		{
// 			_Status_ErrorCondition += ("@,Status Check - Cross Axis Timeout Condition. Possible Cross Error\n");
// 			WorkingStatus = false;
// 		}
// 	}
// 	else
// 	Status_CrossnoTimerActive = true;
	
	return WorkingStatus;
}

//*********************************************************************************************************
// Reset Timers for CheckStatus 
void AntennaMotorManager::ResetStatus()
{
	Status_AznoTimerActive = true;
	Status_CrossnoTimerActive = true;
	Status_ElnoTimerActive = true;
	Status_PolnoTimerActive = true;
}