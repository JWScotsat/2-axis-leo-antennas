/*
* AntennaMotors.cpp
*
* Created: 01/07/2015 13:23:57
* Author: Jack Wells
*/


#include "DCMotors.h"

//******************************************************************
//******************************************************************

// default constructor
// Set initial values/assignments for the used Accel Stepper Motor and the intended Axis Sensor
DCMotors::DCMotors(int M1Pin, int M2Pin, int ENPin, int EncoderPin, String Name) // Setup call.
{
	_pinM1 = M1Pin;
	_pinM2 = M2Pin;
	_pinEna = ENPin;
	_pinEncoder = EncoderPin;
	_Name = Name;
	_axisReverse = false;
	_DriveEnabled = false;
	_Driver_DirEn = false;
} //AntennaMotors

// default constructor
// Set initial values/assignments for the used Accel Stepper Motor and the intended Axis Sensor
DCMotors::DCMotors(int DirPin, int ENPin, int EncoderPin, String Name) // Setup call.
{
	_pinM1 = DirPin;
	_pinEna = ENPin;
	_pinEncoder = EncoderPin;
	_Name = Name;
	_axisReverse = false;
	_DriveEnabled = false;
	_Driver_DirEn = true;

} //AntennaMotors

// default destructor
DCMotors::~DCMotors()
{
} //~AntennaMotors

// default constructor
DCMotors::DCMotors()
{
} //~AntennaMotors


//******************************************************************
//******************************************************************

// Initialise Axis Motor
//  Also initialises the related axis sensor
void DCMotors::setup()
{
	if(_Driver_DirEn)
	{
		pinMode(_pinM1, OUTPUT);
		pinMode(_pinEna, OUTPUT);
		pinMode(_pinEncoder, INPUT);
		digitalWriteFast(_pinEna, LOW);
	}else
	{
		pinMode(_pinM1, OUTPUT);
		pinMode(_pinM2, OUTPUT);
		pinMode(_pinEna, OUTPUT);
		pinMode(_pinEncoder, INPUT);
		digitalWrite(_pinEna, LOW);
	}
}

//****************************************************************************************

void  DCMotors::MoveForward()
{
	if(_Driver_DirEn)
	{
		digitalWriteFast(_pinM1, HIGH);

		if(_DriveEnabled)
		digitalWriteFast(_pinEna, HIGH);

	}
	else
	{
		digitalWrite(_pinM1, HIGH);
		digitalWrite(_pinM2,LOW);
	}
}


//****************************************************************************************

void  DCMotors::MoveBackward()
{
	if(_Driver_DirEn)
	{
		digitalWriteFast(_pinM1, LOW);

		if(_DriveEnabled)
		digitalWriteFast(_pinEna, HIGH);

	}
	else
	{
		digitalWrite(_pinM2, HIGH);
		digitalWrite(_pinM1,LOW);
	}
}
//****************************************************************************************

void  DCMotors::Enable()
{
	_DriveEnabled = true;

}

//****************************************************************************************

void  DCMotors::Disable()
{
	_DriveEnabled = false;
	digitalWriteFast(_pinEna, LOW);
	digitalWrite(_pinM1,LOW);

	if(!_Driver_DirEn)
	digitalWrite(_pinM2, LOW);
	
}


//****************************************************************************************

void DCMotors::PWMPulse()
{
	if(!_Driver_DirEn)
	{
		if(_DriveEnabled)
		{
			if(_PWMCounter > 6)
			{
				digitalWriteFast(_pinEna, HIGH);
				_PWMCounter = 0;
			}
			else if(_PWMCounter > 3)
			digitalWriteFast(_pinEna, LOW);
			
			_PWMCounter++;
		}
	}
}

//****************************************************************************************

float DCMotors:: getPos() // Return Position
{
	int i, v, average;

	average  = analogRead(_pinEncoder);
	/*PCSerial.print("First Val;"); PCSerial.print(average);*/
	average  = map(average , 1250, 3400, 0, 355);
	//average  = map(average , 0, 4096, 0, 360);
	
	//
	for (i = 0; i < 4; i++)
	{
		v = analogRead(_pinEncoder);
		v = map(v, 1250, 3400, 0, 345);
		//v = map(v, 0, 4096, 0, 360);
		
		average = average + v;
	}

	float pos = average/5.0;
	
	return pos;
}

//****************************************************************************************

void  DCMotors::Stop()
{
	if(_Driver_DirEn)
	{
		digitalWriteFast(_pinEna, LOW);
		digitalWrite(_pinM1,LOW);
	}else
	{
		digitalWrite(_pinM1,LOW);
		digitalWrite(_pinM2, LOW);
	}

	Disable();
}

//****************************************************************************************