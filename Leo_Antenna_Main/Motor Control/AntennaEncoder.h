/*
* AntennaEncoder.h
*
* Created: 02/07/2015 08:42:41
* Author: Jack Wells
*/


#ifndef __ANTENNAENCODER_H__
#define __ANTENNAENCODER_H__

// Defines for assigned counter for any instance of Antenna Encoder
#define yCounter 1
#define xCounter 2


// Antenna Encoder Class relies on Quad Decode from Teensy Forum
//  Wraps the used library in a class to be used for the Antenna
class AntennaEncoder
{
	//variables
	public:
	boolean _xCounterRolloverBoundaried = false;
	boolean _yCounterRolloverBoundaried = false;
	protected:
	
	private:
	
	const float EncoderCountsToDegs = 360.0/5000.0/(4*8.333); // Encoder Count to Degrees - Encoder Stats; 4000 CPR Quad(x4) 12:100 Motor/Antenna Ratio = 8.333
	const float EncoderCountsToDegsX = 360.0/5000.0/(4*(100.00/12.000)); // Encoder Count to Degrees - Encoder Stats; 5000 CPR Quad(x4) 12:112 Motor/Antenna Ratio = 9.3333
	const float EncoderCountsToDegsY = 360.0/5000.0/(4*(72.0/12.000)); // Encoder Count to Degrees - Encoder Stats; 5000 CPR Quad(x4) 12:72 Motor/Antenna Ratio = 6
		
	
	const float MotorCountsToDegs = 1.8/16/8.333; // Motor Step Counts to Antenna Degrees - Motor Stats; 1.8 Deg/Step / 16 Micro Steps / 12:100 Motor/Antenna Ratio

	float _trackedCounterX; // Roll Over tracker. Keeps values during roll over the so the counter can reset
	float _trackedCounterY; // Roll Over tracker. Keeps values during roll over the so the counter can reset


	//functions
	public:
	
	AntennaEncoder();
	~AntennaEncoder();
	
	float GetCounterPos(int AssignedCounter); //Absolute encoder position
	
	void setup(int AssignedCounter); // Initialise the assigned counter
	
	float getEncPos(int AssignedCounter); //Antenna Axis Position
	void zeroCounterAxisPosition(int AssignedCounter); //Zero Antenna Axis Position
	
	
	protected:
		
		
	private:
	void zeroCounter(int AssignedCounter); // Zero Hardware counter
	

}; //AntennaEncoder

#endif //__ANTENNAENCODER_H__
