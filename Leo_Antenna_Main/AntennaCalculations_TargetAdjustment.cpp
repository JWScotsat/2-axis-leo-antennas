//
//
//

#include "AntennaCalculations_TargetAdjustment.h"

void AntennaCalculations_TargetAdjustmentClass::init()
{


}

void AntennaCalculations_TargetAdjustmentClass::CalculateTargetAdjustment(float *IMUPitchRoll, float AntennaYaw, float *DesiredAzElPol, float *ReturnedVals)
{


	double direcVec[3];
	double correctedDirecVec[3];
	double YawTransform[3], PitchTransform[3], RollTransform[3];


	double TargetAzRads = DesiredAzElPol[Az]* (PI / 180.0);
	double TargetElRads = DesiredAzElPol[El]* (PI / 180.0);


	double YawRads = AntennaYaw* (PI / 180.0) ;
	double PitchRads = IMUPitchRoll[Pitch]* (PI / 180.0)  ;
	double RollRads = IMUPitchRoll[Roll]* (PI / 180.0)  ;
	

	direcVec[x] = cos(TargetElRads)*cos(TargetAzRads);      // x = (cos(El)*cos(Az))
	direcVec[y] = cos(TargetElRads)*sin(TargetAzRads);      // y = (cos(El)*sin(Az))
	direcVec[z] = -sin(TargetElRads);                      // z = (-sin(El))
	
	//Serial.print("Direction Vector XYZ;"); StndFncts.PrintArray(direcVec,3);
	
	YawTransform[x] = cos(YawRads)*direcVec[x] +sin(YawRads)*direcVec[y];
	YawTransform[y] = -1*sin(YawRads)*direcVec[x]+cos(YawRads)*direcVec[y];
	YawTransform[z] = direcVec[z];
	
	//Serial.print("\nYaw Tansform XYZ;"); StndFncts.PrintArray(YawTransform,3);
	
	RollTransform[x] = YawTransform[x];
	RollTransform[y] = cos(RollRads)*YawTransform[y]+sin(RollRads)*YawTransform[z];
	RollTransform[z] = -1*sin(RollRads)*YawTransform[y]+cos(RollRads)*YawTransform[z];
	
	//Serial.print("\nRoll Tansform XYZ;"); StndFncts.PrintArray(RollTransform,3);
	
	PitchTransform[x] = cos(PitchRads)*RollTransform[x] -sin(PitchRads)*RollTransform[z];
	PitchTransform[y] = RollTransform[y];
	PitchTransform[z] = sin(PitchRads)*RollTransform[x] +cos(PitchRads)*RollTransform[z];
	
	//Serial.print("\nPitch Tansform XYZ;"); StndFncts.PrintArray(PitchTransform,3);
	

	double CorAzChange = degrees(atan2(PitchTransform[y], PitchTransform[x]));
	double CorEl  = degrees(asin(PitchTransform[z]/-1));
	
	// 	Serial.print("\nCorDeltaAz;"); Serial.print(CorAzChange);
	//
	// 	Serial.println("");
	
	//Boundary Conditions
	

	ReturnedVals[El] = CorEl;
	
	ReturnedVals[Az] = float(CorAzChange);
	
	ReturnedVals[Pol] = DesiredAzElPol[Pol];//degrees(FinalPol);
	
}

//*********************************************************************************************

void AntennaCalculations_TargetAdjustmentClass::CalculateSimplfiedDirectAdjustments(float *IMUPitchRoll, float AntennaYaw, float *DesiredAzElPol, float *ReturnedVals)
{
	
	float CorEl = DesiredAzElPol[El] - IMUPitchRoll[Pitch];
	
	float CorAz = StndFncts.Bound(DesiredAzElPol[Az] - AntennaYaw, -0.01, 360.01);
	
	ReturnedVals[El] = CorEl;
	
	ReturnedVals[Az] = CorAz;
	
	ReturnedVals[Pol] = DesiredAzElPol[Pol];//degrees(FinalPol);
}

//*********************************************************************************************



void AntennaCalculations_TargetAdjustmentClass::CalculateCombinedStabilityAdjustments(float *IMUPitchRoll, float AntennaYaw, float *DesiredAzElPol, float *ReturnedVals, float FrameIMU_Pitch, float FrameIMU_Roll)
{
	
	float CorEl = DesiredAzElPol[El]- IMUPitchRoll[Pitch];//Direct Elevation Compensation
	
	double direcVec[3];
	double correctedDirecVec[3];
	double YawTransform[3], PitchTransform[3], RollTransform[3];


	double TargetAzRads = DesiredAzElPol[Az]* (PI / 180.0);
	double TargetElRads = 20.0* (PI / 180.0); //static value used to compensate for cross axis angle


	double YawRads = AntennaYaw * (PI / 180.0) ;
	double PitchRads = FrameIMU_Pitch * (PI / 180.0);//IMUPitchRoll[Pitch]* (PI / 180.0)  ;
	double RollRads = FrameIMU_Roll * (PI / 180.0)  ;//IMUPitchRoll[Roll]* (PI / 180.0)  ;
	//***
	

	direcVec[x] = cos(TargetElRads)*cos(TargetAzRads);      // x = (cos(El)*cos(Az))
	direcVec[y] = cos(TargetElRads)*sin(TargetAzRads);      // y = (cos(El)*sin(Az))
	direcVec[z] = -sin(TargetElRads);                      // z = (-sin(El))
	
	//Serial.print("Direction Vector XYZ;"); StndFncts.PrintArray(direcVec,3);
	
	YawTransform[x] = cos(YawRads)*direcVec[x] +sin(YawRads)*direcVec[y];
	YawTransform[y] = -1*sin(YawRads)*direcVec[x]+cos(YawRads)*direcVec[y];

// 	YawTransform[x] = cos(YawRads)*direcVec[x] +sin(YawRads)*direcVec[x];
// 	YawTransform[y] = -1*sin(YawRads)*direcVec[y]+cos(YawRads)*direcVec[y];
	YawTransform[z] = direcVec[z];
	
	//Serial.print("\nYaw Tansform XYZ;"); StndFncts.PrintArray(YawTransform,3);
	
	RollTransform[x] = YawTransform[x];
	RollTransform[y] = cos(RollRads)*YawTransform[y]+sin(RollRads)*YawTransform[z];
	RollTransform[z] = -1*sin(RollRads)*YawTransform[y]+cos(RollRads)*YawTransform[z];
	
	//Serial.print("\nRoll Tansform XYZ;"); StndFncts.PrintArray(RollTransform,3);
	
	PitchTransform[x] = cos(PitchRads)*RollTransform[x] -sin(PitchRads)*RollTransform[z];
	PitchTransform[y] = RollTransform[y];
	PitchTransform[z] = sin(PitchRads)*RollTransform[x] +cos(PitchRads)*RollTransform[z];
	
	//Serial.print("\nPitch Tansform XYZ;"); StndFncts.PrintArray(PitchTransform,3);
	

	double CorAzChange = degrees(atan2(PitchTransform[y], PitchTransform[x]));

	

	ReturnedVals[El] = CorEl;
	
	ReturnedVals[Az] = float(CorAzChange);
	
	ReturnedVals[Pol] = DesiredAzElPol[Pol];//degrees(FinalPol);
	
}
//*********************************************************************************************

// Axis Boundary Conditions
// Ensure all axis targets don't go outside of Antenna Physical Boundaries
void AntennaCalculations_TargetAdjustmentClass::BoundaryConditions(float *ReturnedVals)
{
	
	if(ReturnedVals[El] > MaxPhysical_Elevation)
	ReturnedVals[El] = MaxPhysical_Elevation;
	else if (ReturnedVals[El] < MinPhysical_Elevation)
	ReturnedVals[El] = MinPhysical_Elevation;
	
	if(ReturnedVals[Az] > MaxPhysical_Azimuth)
	ReturnedVals[Az] -= MaxPhysical_Azimuth;
	else if (ReturnedVals[Az] < MinPhysical_Azimuth)
	ReturnedVals[Az] += MaxPhysical_Azimuth;
	
	if(ReturnedVals[Pol] > MaxPhysical_Pol)
	ReturnedVals[Pol] -= MaxPhysical_Pol;
	else if (ReturnedVals[Pol] < MinPhysical_Pol)
	ReturnedVals[Pol] += MaxPhysical_Pol;
	
}


//*********************************************************************************************
//*********************************************************************************************

float AntennaCalculations_TargetAdjustmentClass::CalculateYaw(float CurrentAxisPos, float CompassYaw, float MountingOffset)
{
	float CombinedYaw; // Resulting combined Yaw
	float AzEncoderPos = CurrentAxisPos; // Temporary store of Az Encoder Val
	
	//Boundary Range Compass to +-180
	if(CompassYaw > 180.0)
	CompassYaw -= 360.0;
	
	if(CompassYaw < -180.0)
	CompassYaw += 360.0;
	
	//Boundary Range MountingOffset to +-180
	if(MountingOffset > 180.0)
	MountingOffset -= 360.0;
	
	if(MountingOffset < -180.0)
	MountingOffset += 360.0;
	
	//Boundary Range the Az Encoder to +-180
	if(AzEncoderPos > 180.0)
	AzEncoderPos -= 360.0;
	
	if(AzEncoderPos < -180.0)
	AzEncoderPos += 360.0;
	
	CombinedYaw = AzEncoderPos + CompassYaw;//- MountingOffset;
	
	// Boundary Range Result to +-180
	if(CombinedYaw < -180.0)
	CombinedYaw += 360.0;
	
	if(CombinedYaw > 180.0)
	CombinedYaw -= 360.0;
	
	return CombinedYaw;
}

//*******************************************************************************************
//*******************************************************************************************

void  AntennaCalculations_TargetAdjustmentClass::CalculateUncorrectedCurrentPosition(float AzEncoderPos, float ElEncoderPos, float CorrectedMotorTarget[], float SkyTargetPos[], float* ReturnedUncorrectedPositions, float SatAz)
{
	_SatAzimuth = SatAz; // Store SatAz for further calculation updates
	
	float CurrentUncorrectedAz = AzEncoderPos - (CorrectedMotorTarget[Az] - SkyTargetPos[Az]);// - (CompassHeading + AzMountingOffset));
	
	float CurrentUncorrectedEl = ElEncoderPos - (CorrectedMotorTarget[El] - SkyTargetPos[El]);
	
	if(CurrentUncorrectedAz > 360.0)
	CurrentUncorrectedAz -= (360.0*(int(CurrentUncorrectedAz/360.0)));
	
	if(CurrentUncorrectedAz > 180.0)
	CurrentUncorrectedAz -= 360.0;
	
	if(CurrentUncorrectedAz < -360.0)
	CurrentUncorrectedAz += (360.0*(int(CurrentUncorrectedAz/360.0)));
	
	if(CurrentUncorrectedAz < -180.0)
	CurrentUncorrectedAz += 360.0;
	
	if(CurrentUncorrectedEl < MinPhysical_Elevation)
	CurrentUncorrectedEl = MinPhysical_Elevation;


	if(CurrentUncorrectedEl > MaxPhysical_Elevation)
	CurrentUncorrectedEl = MaxPhysical_Elevation;
	
	ReturnedUncorrectedPositions[Az] = CurrentUncorrectedAz;
	ReturnedUncorrectedPositions[El] = CurrentUncorrectedEl;
}

//*******************************************************************************************
/*
Function; Update calculated heading based on Satellite Position and Current Motor Position
Returns; Nothing, updates class variables for recall later
*/
void AntennaCalculations_TargetAdjustmentClass::CalculateOffsetFromSat(float CentreSkyTarget, float CurrentHeading)
{
	//_DeltaHeadingFromSat = (_SatAzimuth - (CentreSkyTarget +_OffsetFromSat)); // Calculate the current position compared to the calculated satellite position in the sky to get an approximate heading
	
	static boolean FirstPeak = true;
	static float previousDeltaChange = 0.0;
	if(!isNewSatOffset)
	{
		float PeakingDeltaChange = _SatAzimuth - CentreSkyTarget;

		float newOffset =   CurrentHeading + (_SatAzimuth - CentreSkyTarget - _FirstOffset) + _PeakingOffset; //- (_OffsetFromSat - CurrentHeading);//_DeltaHeadingFromSat + _OffsetFromSat;
		
		newOffset = StndFncts.Bound(newOffset,-0.01,360.01); // ensure heading is 0-360 ranged
		
		float _DeltaHeadingFromSat = _OffsetFromSat - newOffset;
		//NewHeadingFromSat += _HeadingFromSat;
		
		//NewHeadingFromSat = StndFncts.Bound(NewHeadingFromSat,-0.01,360.01); // ensure heading is 0-360 ranged
		
		
		if(abs(_DeltaHeadingFromSat) > 0.4)
		{
			_OffsetFromSat = newOffset;
		}
		
		if(FirstPeak) //|| abs((PeakingDeltaChange) - _PeakingOffset) > 0.4)
		{
			_FirstOffset = PeakingDeltaChange;
			FirstPeak = false;
		}
		
		if(abs(PeakingDeltaChange - previousDeltaChange) > 0.4)
		{
			_PeakingOffset += previousDeltaChange - PeakingDeltaChange;
			previousDeltaChange = PeakingDeltaChange;
		}
		
	}else
	{
		previousDeltaChange = _FirstOffset;
		isNewSatOffset = false;
		FirstPeak = true;
	}
}
//*******************************************************************************************

//*******************************************************************************************
/*
Function; Update calculated heading based on Satellite Position and Current Motor Position
Returns; Nothing, updates class variables for recall later
*/
void AntennaCalculations_TargetAdjustmentClass::CalculateOffsetFromSatSearch(float CentreSkyTarget, float CurrentHeading)
{
	//_DeltaHeadingFromSat = (_SatAzimuth - (CentreSkyTarget +_OffsetFromSat)); // Calculate the current position compared to the calculated satellite position in the sky to get an approximate heading
	
	
	
	float PeakingDeltaChange = StndFncts.Bound(_SatAzimuth - CentreSkyTarget,-0.01,360.01);
	
	_HeadingError = StndFncts.Bound(CurrentHeading - _PeakingOffset,-0.01,360.01);
	
	_FirstOffset = PeakingDeltaChange;
	_OffsetFromSat = PeakingDeltaChange;
	_PeakingOffset = 0.0;
	
	isNewSatOffset = true;
	

}
//*******************************************************************************************
AntennaCalculations_TargetAdjustmentClass AntennaCalculations_TargetAdjustment;

//*******************************************************************************************//*******************************************************************************************
//*******************************************************************************************//*******************************************************************************************