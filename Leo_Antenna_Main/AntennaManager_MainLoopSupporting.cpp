/*
* AntennaManager_MainLoopSupporting.cpp
*
* Created: 26/04/2019 10:24:07
*  Author: Admin
*/
#include "AntennaManager.h"

//*******************************************
void AntennaManagerClass::newStabilitySettings()
{
	Serial.println("+, Using Gyro IMU Heading Compensation");
	int Accel = 18000;
	int MaxSpeed = 18000;
	int AccelAz = 4000;
	int MaxSpeedAz = 8000;
	MotorController._AntMotorAz._Motor->setMaxSpeed(MaxSpeedAz); MotorController._AntMotorAz._Motor->setAcceleration(AccelAz); Serial.println("+,USB Comms - Az Speed Changed To;" + String(MaxSpeedAz) + ", Accel Changed To;"+ String(AccelAz));
	MotorController._AntMotorEl._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorEl._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - El Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel));
	//MotorController._AntMotorCross._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorCross._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - Cross Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel));
	
	_PeakingPatternDynamicDelta._DeltaAzMax = 0.20; Serial.println(F("+,USB Comms - _DyDeltaAzMax  Changed to; ") + String( _PeakingPatternDynamicDelta._DeltaAzMax));
	_PeakingPatternDynamicDelta._DeltaElMax = 0.20; Serial.println(F("+,USB Comms - _DyDeltaElMax  Changed to; ") + String(_PeakingPatternDynamicDelta._DeltaElMax));
	
	_PeakingPatternDynamicDelta._KAz = 0.10; Serial.println(F("+,USB Comms - _DyCentreKAz  Changed to; ") + String(_PeakingPatternDynamicDelta._KAz));
	_PeakingPatternDynamicDelta._KEl = 0.10; Serial.println(F("+,USB Comms - _DyCentreKEl  Changed to; ") + String(_PeakingPatternDynamicDelta._KEl ));
	
	
	_FramePitchAvg.setup(1);
	_FrameRollAvg.setup(1);
	
	_CurrentConfig->UseOnBoardGyroWithCompass = true;
	_CurrentConfig->UseSimplifiedStabilisation = true;
	_CurrentConfig->_BucPower = 2; //setting for saving stability method
	
	
}

void AntennaManagerClass::newPeakingOldStabilitySettings()
{
	
	Serial.println("+, Using Best Peaking compensation");
	int Accel = 3000;
	int MaxSpeed = 8000;
	int AccelAz = 4000;
	int MaxSpeedAz = 6000;
	MotorController._AntMotorAz._Motor->setMaxSpeed(MaxSpeedAz); MotorController._AntMotorAz._Motor->setAcceleration(AccelAz); Serial.println("+,USB Comms - Az Speed Changed To;" + String(MaxSpeedAz) + ", Accel Changed To;"+ String(AccelAz));
	MotorController._AntMotorEl._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorEl._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - El Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel));
	//MotorController._AntMotorCross._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorCross._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - Cross Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel));
	
	_PeakingPatternDynamicDelta._DeltaAzMax = 0.10; Serial.println(F("+,USB Comms - _DyDeltaAzMax  Changed to; ") + String( _PeakingPatternDynamicDelta._DeltaAzMax));
	_PeakingPatternDynamicDelta._DeltaElMax = 0.10; Serial.println(F("+,USB Comms - _DyDeltaElMax  Changed to; ") + String(_PeakingPatternDynamicDelta._DeltaElMax));
	_PeakingPatternDynamicDelta._KAz = 0.2; Serial.println(F("+,USB Comms - _DyCentreKAz  Changed to; ") + String(_PeakingPatternDynamicDelta._KAz));
	_PeakingPatternDynamicDelta._KEl = 0.15; Serial.println(F("+,USB Comms - _DyCentreKEl  Changed to; ") + String(_PeakingPatternDynamicDelta._KEl ));
	
	_PeakingPatternDynamicDelta._PointDelayMin = 350; Serial.println(F("+,USB Comms - _PointDelay  Changed to; ") + String(_PeakingPatternDynamicDelta._PointDelayMin ));
	_CurrentConfig->SearchAzimuth_maxCounterBoundary = 8.0;Serial.println(F("+,USB Comms - SearchAzimuth_maxCounterBoundary  Changed to; ") + String(_CurrentConfig->SearchAzimuth_maxCounterBoundary ));
	_FramePitchAvg.setup(30);
	_FrameRollAvg.setup(30);
	
	_CurrentConfig->UseOnBoardGyroWithCompass = false;
	_CurrentConfig->UseSimplifiedStabilisation = false;
	_CurrentConfig->_BucPower = 0; //setting for saving stability method
}

void AntennaManagerClass::oldStabilitySettings()
{
	Serial.println("+, Using standard default compensation");
	int Accel = 4000;
	int MaxSpeed = 4000;
	int AccelAz = 4000;
	int MaxSpeedAz = 4000;
	MotorController._AntMotorAz._Motor->setMaxSpeed(MaxSpeedAz); MotorController._AntMotorAz._Motor->setAcceleration(AccelAz); Serial.println("+,USB Comms - Az Speed Changed To;" + String(MaxSpeedAz) + ", Accel Changed To;"+ String(AccelAz));
	MotorController._AntMotorEl._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorEl._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - El Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel));
	//MotorController._AntMotorCross._Motor->setMaxSpeed(MaxSpeed); MotorController._AntMotorCross._Motor->setAcceleration(Accel); Serial.println("+,USB Comms - Cross Speed Changed To;" + String(MaxSpeed) + ", Accel Changed To;"+ String(Accel));
	
	_PeakingPatternDynamicDelta._DeltaAzMax = 0.35; Serial.println(F("+,USB Comms - _DyDeltaAzMax  Changed to; ") + String( _PeakingPatternDynamicDelta._DeltaAzMax));
	_PeakingPatternDynamicDelta._DeltaElMax = 0.35; Serial.println(F("+,USB Comms - _DyDeltaElMax  Changed to; ") + String(_PeakingPatternDynamicDelta._DeltaElMax));
	_PeakingPatternDynamicDelta._KAz = 0.02; Serial.println(F("+,USB Comms - _DyCentreKAz  Changed to; ") + String(_PeakingPatternDynamicDelta._KAz));
	_PeakingPatternDynamicDelta._KEl = 0.02; Serial.println(F("+,USB Comms - _DyCentreKEl  Changed to; ") + String(_PeakingPatternDynamicDelta._KEl ));
	
	_FramePitchAvg.setup(30);
	_FrameRollAvg.setup(30);
	
	_CurrentConfig->UseOnBoardGyroWithCompass = false;
	_CurrentConfig->UseSimplifiedStabilisation = false;
	_CurrentConfig->_BucPower = 1; //setting for saving stability method
}



//************************************************************************************
// Check Modem Signal For Lock Scenario - Only important during search
// returns modem signal for peaking
// Changes Antenna Mode to peaking if lock is found
int AntennaManagerClass::CheckModemLock()
{
	boolean changeinAntennaMode = false;
	
	if(_CurrentAntennaMode == AntennaModeSearching)
	{
		if(SatSignalRecv1._isLock)
		{
			changeinAntennaMode = true;
			// Get current Az El in terms of Sky Position
			float AzEncoderPos = MotorController._AntMotorAz.getAxisPos();
			float ElEncoderPos = MotorController._AntMotorEl.getAxisPos();
			
			_TargetAdjustments.CalculateUncorrectedCurrentPosition(AzEncoderPos, ElEncoderPos, _CorrectedMotorTargetPos, _NewSkyPos, _CurrentSkyMotorsPos, _SatPoint.getSatAzPos());
			
			//Peaking Setup
			_CurrentAntennaMode = AntennaModePeaking;
			_PeakingPattern.SetCentreSkyPosition(_CurrentSkyMotorsPos);
			_PeakingPatternDynamicDelta.SetCentreSkyPosition(_CurrentSkyMotorsPos);
			_PeakingPatternStepTracking.SetStartPosition(_CurrentSkyMotorsPos, SatSignalRecv1.read());
			_PeakingPatternCircGrad.SetCentreSkyPosition(_CurrentSkyMotorsPos);
			
			_TargetAdjustments.CalculateOffsetFromSatSearch(_PeakingPatternDynamicDelta.getCentreAz(), _EstimatedHeading);//Recalculate new heading from sat
			//_TargetAdjustments.CalculateOffsetFromSat(_PeakingPatternDynamicDelta.getCentreAz(), _HeadingAvg.getCurrentAvg());//Recalculate new heading from sat
			
			_SatPoint.setAzOffset(_TargetAdjustments.getFirstOffset());
			
			_SearchPattern.resetSearchAttempt();
		}
	}
	
	return changeinAntennaMode;
}

//**********************************************************************************
//**********************************************************************************


//Live Status checking of sensors/motors
boolean AntennaManagerClass::StatusCheck()
{
	//Status flag for return
	boolean WorkingStatus = true;
	if(!_DebugInitBypass)
	{

		
		//Frame IMU Check
		if(_YEIFrame._status < 0 && _YEIFrame._enabled)
		{
			Status_ErrorCondition(_YEIFrame._statusMessage);
			WorkingStatus = false;
		}
		
		
		if(MotorController.CheckStatus() == false)
		{
			WorkingStatus = false;
			Status_ErrorCondition(MotorController._Status_ErrorCondition);
		}

	}
	_AntennaStatusWorking = WorkingStatus;

	return WorkingStatus;
}


void AntennaManagerClass::Status_ErrorCondition(String ErrorMessage)
{
	Serial.print("+,Handling Sensor Error Condition. Error Message Received;\n" + ErrorMessage);
	Serial.print("+,Reinitialising Antenna\n");
	_isInitilisation_Passed = false;
	MotorController.AllStop();
}