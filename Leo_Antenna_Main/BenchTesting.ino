/*
* CPPFile1.cpp
*
* Created: 23/10/2019 13:43:33
*  Author: JW
*/

//Code save for bench testing

//Single IMU speed reading test. Place before Antenna Init.
void BenchTest_SingleIMUReadRate()
{
	unsigned long loopTimer = micros();
	unsigned long newDataTimer = micros();
	int loopCounter = 0;
	int newDataCounter = 0;
	int newDataDelta = 0;
	
	// Initialise both IMUs
	AntennaManager._YEIFrame.Initialise();
	AntennaManager.MotorController._AntMotorCross._imuSensor->Initialise();
	
	// Setup IMUs in test mode
	Serial.println("+Start SW Setup");
	AntennaManager._YEIFrame.swSetGyroRange();
	AntennaManager._YEIFrame.swCalGyros();
	AntennaManager._YEIFrame.swTare();
	AntennaManager._YEIFrame.swCalGyros();
	AntennaManager._YEIFrame.swTare();
	AntennaManager.MotorController._AntMotorCross._imuSensor->swSetGyroRange();
	AntennaManager.MotorController._AntMotorCross._imuSensor->swCalGyros();
	AntennaManager.MotorController._AntMotorCross._imuSensor->swTare();
	AntennaManager.MotorController._AntMotorCross._imuSensor->swCalGyros();
	AntennaManager.MotorController._AntMotorCross._imuSensor->swTare();
	
	Serial.println("+RUN");

	while (true)
	{
		loopCounter++;
		AntennaManager._YEIFrame.getEularAngles();
		AntennaManager.MotorController._AntMotorCross._imuSensor->getEularAngles();
		
		if(AntennaManager._YEIFrame._isNewData)
		{
			newDataDelta = micros() - newDataTimer;
			if(newDataCounter % 10 == 0)
			{
				unsigned long loopDelta = micros() - loopTimer;
				Serial.print(newDataCounter); Serial.print(",");
				Serial.print(newDataDelta); Serial.print(",");
				Serial.print(AntennaManager._YEIFrame._Roll); Serial.print(",");
				Serial.print(AntennaManager._YEIFrame._Pitch); Serial.print(",");
				Serial.print(AntennaManager._YEIFrame._Yaw); Serial.print(",");
				Serial.print(AntennaManager.MotorController._AntMotorCross._imuSensor->_Roll); Serial.print(",");
				Serial.print(AntennaManager.MotorController._AntMotorCross._imuSensor->_Pitch); Serial.print(",");
				Serial.print(AntennaManager.MotorController._AntMotorCross._imuSensor->_Yaw); Serial.print(",");
				Serial.println("0");
			}
			newDataCounter++;
						
			//
			loopCounter = 0;
			loopTimer = micros();
		}
	}
}