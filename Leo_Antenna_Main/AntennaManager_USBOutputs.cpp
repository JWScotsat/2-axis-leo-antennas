#include "AntennaManager.h"

//***********************************************************************************
//***********************************************************************************
// Main Pi Serial Packet
//  Used by Raspberry Pi webserver to output webpage data
//   See evernote for full packet details
void AntennaManagerClass::AntennaComms_UpdatePi_MasterPacket()
{
	const String Pointing =		"1,0,0,0,0,";
	const String Searching =	"0,1,0,0,0,";
	const String Peaking =		"0,0,1,0,0,";
	const String Manual =		"0,0,0,1,0,";
	
	if(millis() - _CommsLastMasterStamp > _CurrentConfig->_UpdatePiRate)
	{
		Serial.print(":,");
		Serial.print(_USBConnectionStatus); Serial.print(",");
		
		switch (_CurrentAntennaMode)
		{
			case AntennaModePointing: Serial.print(Pointing);break;
			case AntennaModeSearching: Serial.print(Searching);break;
			case AntennaModePeaking: Serial.print(Peaking);break;
			case AntennaModeManualPos: Serial.print(Manual);break;
		}


		Serial.print(_ModemSignalReading); Serial.print(",");
		Serial.print(_NewSkyPos[El]);Serial.print(",");
		
		float SkyAzWithOffset = StndFncs.Bound(_NewSkyPos[Az] + _TargetAdjustments.getOffsetFromSat(), -0.01,360.01);
		
		Serial.print(SkyAzWithOffset);Serial.print(",");
		Serial.print(_NewSkyPos[Pol]);Serial.print(",");
		
		Serial.print(_CurrentGPS.Latitude,6);Serial.print(",");
		Serial.print(_CurrentGPS.Longitude,6);Serial.print(",");
		
		Serial.print(_CurrentSat.SatLng);Serial.print(",");
		Serial.print(_CurrentSat.SatPolHV);Serial.print(",");
		
		Serial.print(_EstimatedHeading,3);Serial.print(",");//Previously buck power
		Serial.print(_TargetAdjustments.getOffsetFromSat());Serial.print(",");
		
		Serial.print(GPSCompass._Status);Serial.print(",");
		Serial.print(_AntennaModePaused);Serial.print(",");
		
		Serial.print("c\n");
		
		
		//Serial.println(">, Dollar Test Packet,");
		
		_CommsLastMasterStamp = millis();
	}
}



//***********************************************************************************
//***********************************************************************************
// Secondary Pi Output Packet - Mostly for debugging Motor Positional Data
//  Used by Raspberry Pi webserver to output webpage data
//   See evernote for full packet details
void AntennaManagerClass::AntennaComms_UpdatePi_MotorPacket()
{


	if(millis() - _CommsLastMotorStamp > _CurrentConfig->_UpdatePiRate_MotorData && _CurrentConfig->_UpdatePi_MotorData_Enable)
	{
		Serial.print("*,");
		Serial.print(_NewSkyPos[El]);Serial.print(",");
		Serial.print(_NewSkyPos[Az]); Serial.print(",");
		
		Serial.print(_CorrectedMotorTargetPos[El]);Serial.print(",");
		Serial.print(_CorrectedMotorTargetPos[Az]); Serial.print(",");+
		
		
		Serial.print(MotorController._AntMotorEl.getAxisPos());Serial.print(",");
		Serial.print(MotorController._AntMotorAz.getAxisPos()); Serial.print(",");
		Serial.print(MotorController._AntMotorCross.getAxisPos()); Serial.print(",");
		Serial.print(MotorController._AntMotorPol.getAxisPos()); Serial.print(",");
		
		Serial.print(MotorController._AntMotorCross._imuSensor->_Roll); Serial.print(",");
		Serial.print(MotorController._AntMotorCross._imuSensor->_Pitch); Serial.print(",");

		Serial.print(-1.0*_YEIFrame._Pitch); Serial.print(",");
		Serial.print(-1.0*_YEIFrame._Roll); Serial.print(",");
		Serial.print(_CurrentCompassHeading); Serial.print(",");

		Serial.print(_YEIFrame._Yaw); Serial.print(",");
		Serial.print(_ModemSignalReading); Serial.print(",");
		Serial.print(MotorController._AntMotorCross._imuSensor->_Yaw); Serial.print(",");
		//Serial.print(MotorController._AntMotorAz._AltSpeedValue); Serial.print(",");
		
		Serial.print(_EstimatedHeading); Serial.print(",");
		//Serial.print(IMUYawPitchRoll[Yaw],2); Serial.print(",");
		//Serial.print(IMUTrackedHeading,2); Serial.print(",");
		//Serial.print(_HeadingAvg.getCurrentAvg(),2); Serial.print(",");
		Serial.print(GPSCompass._headingSpeed,2); Serial.print(",");
		//Serial.print(MotorController._AntMotorCross._imuSensor->_Yaw); Serial.print(",");
		//Serial.print(_SatPoint.getSatAzPos(),2); Serial.print(",");
		Serial.print(millis()); Serial.print(",");

		Serial.print("c\n");
		
		_MotorDebugPrinted = true;
		_CommsLastMotorStamp = millis();
	}
}



//Debug Output Packet
void AntennaManagerClass::AntennaComms_Debug_PrintOutputPacket(float *InputData)
{
	static unsigned long _CommsLastDebugStamp = millis();
	
	if(millis() - _CommsLastDebugStamp > _CurrentConfig->_DebugPacketOutputRate)
	{
		if(_PrintDebug_Loop)
		{
			//InputData[0] Az Encoder
			//InputData[1] IMU Yaw
			//InputData[2] - Encoder Heading
			
						
			//Serial.print("+,DebugOut, ");
			//Serial.print(", Time Update, ");
			//Serial.print(millis() - _CommsLastMasterStamp);
			//Serial.print(", Enc, ");
			//Serial.print(InputData[0]); Serial.print(", Yaw, ");
			//Serial.print(InputData[1]); Serial.print(", Target Az, ");
			//Serial.print(_CorrectedMotorTargetPos[Az]);  Serial.print(", Current Heading, ");
			//Serial.print(_CurrentCompassHeading);Serial.print(", Sig Str, ");
			//Serial.print(_ModemSignalReading);Serial.print(", Avg Compass, ");
			//Serial.print(_CompassHeadingRnAvg.getCurrentAvg());Serial.print(", KGyro, ");
			//Serial.print(_CurrentConfig->KGyroCompFilter);Serial.print(", Resulting Heading, ");
			//Serial.print(InputData[2]);
			
			// SIW Debug Format
			Serial.print("+++,");
			Serial.print(millis() - _CommsLastMasterStamp); Serial.print(",");
			Serial.print(_YEIFrame.imuX); Serial.print(",");
			Serial.print(_YEIFrame.imuY); Serial.print(",");
			Serial.print(_YEIFrame.imuZ); Serial.print(",");
			Serial.print(MotorController._AntMotorCross._imuSensor->imuX); Serial.print(",");
			Serial.print(MotorController._AntMotorCross._imuSensor->imuY); Serial.print(",");
			Serial.print(MotorController._AntMotorCross._imuSensor->imuZ); Serial.print(",");
			
			
			
			//debug data
			
			// 				Serial.print("+,Debug Out Logic- Mode;"); Serial.print(_CurrentAntennaMode);
			// 				Serial.print(", isTarget;"); Serial.print(isAtTarget);
			// 				Serial.print(", ant Signal;"); Serial.print(_ModemSignalReading);
			// 				Serial.print(", Loop Time;"); Serial.print(millis() - loopTimer);
			//
			// 				Serial.print("\n+,Motors- \tAz;"); Serial.print(MotorController._AntMotorAz.getAxisPos())	;
			// 				Serial.print(", El;"); Serial.print(MotorController._AntMotorEl.getAxisPos())	;
			// 				Serial.print(", Pol;"); Serial.print(MotorController._AntMotorPol.getAxisPos())	;
			// 				Serial.print(", Cross;"); Serial.print(MotorController._AntMotorCross.getAxisPos())	;
			// 				// 		//
			// 				Serial.print("\n+,Sky Targets - \tAz;"); Serial.print(_NewSkyPos[Az]);
			// 				Serial.print(", El;"); Serial.print(_NewSkyPos[El]);
			//
			// 				Serial.print("\n+,Motor Targets - \tAz;"); Serial.print(_CorrectedMotorTargetPos[Az]);
			// 				Serial.print(", El;"); Serial.print(_CorrectedMotorTargetPos[El]);
			// 				Serial.print(", Pol;"); Serial.print(_CorrectedMotorTargetPos[Pol]);
			//
			// 				Serial.print("\n+, \tAntenna Yaw;"); Serial.print(_AntennaYaw);
			//
			// 				// 		//
			// 				Serial.print("\n+,Frame IMU - \tYaw;"); Serial.print(FrameIMUYawPitchRoll[Yaw]);
			// 				Serial.print(", Pitch;"); Serial.print(FrameIMUYawPitchRoll[Pitch]);
			// 				Serial.print(", Roll;"); Serial.print(FrameIMUYawPitchRoll[Roll]);
			// 		//

			Serial.print("\n");
			
			_CommsLastDebugStamp = millis();
		}
	}
}


void AntennaManagerClass::AntennaComms_Debug_PrintLoopPacket(unsigned long loopTimer)
{
	//"$,MillisTime, Last Loop, Average Loop Time, Highest Loop Time, Lowest LoopTime, TimeForNumberOfLoops"

	unsigned long loopTime = millis() - loopTimer;

	if(loopTime < _LowestLoop)
	_LowestLoop = loopTime;

	if(loopTime > _HighestLoop)
	_HighestLoop = loopTime;

	_LoopsRecorded += loopTime;

	if(_MotorDebugPrinted)
	{
		Serial.print(">,");
		Serial.print(millis());Serial.print(",");
		Serial.print(loopTime);Serial.print(",");
		float AvgLoop = _LoopsRecorded/_counter;
		Serial.print(AvgLoop);Serial.print(",");
		Serial.print(_HighestLoop);Serial.print(",");
		Serial.print(_LowestLoop);Serial.print(",");
		Serial.print(millis() - _TotalTime);Serial.print(",");
		Serial.println("c");

		_counter = 0;
		_LoopsRecorded = loopTime;
		_HighestLoop = 0;
		_LowestLoop = 200;
		_TotalTime = millis();
		
		_MotorDebugPrinted = false;
	}



	_counter++;
	_previousLoop = loopTimer;
}


//**********************************************************************************//**********************************************************************************
//**********************************************************************************//**********************************************************************************
