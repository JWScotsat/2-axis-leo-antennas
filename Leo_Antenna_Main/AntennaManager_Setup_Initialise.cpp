#include "AntennaManager.h"



//**********************************************************************************
//**********************************************************************************
// Main Init
//   - YEI Frame Initialises the Frame IMU sensor
//   - Motor Manager initialises all motors and related sensors
void AntennaManagerClass::Initialise()
{
	MotorController.AllStop();
	MotorController.ResetStatus();
	_RGBLED.setColor(LEDColour_Blue);

	_InitRunning = true;
	_AntennaStatusWorking = 1;
	boolean isInitialising = true;
	_USBConnectionStatus = false;
	
	//	int InitCounter = 1;
	USBSerial.Clear_USB();

	_isInitilisation_Passed = false;
	

	Serial.print("+,Antenna Manager - Initialising Antenna; Attempt;" + String(_InitCounter) +"\n");
	Serial.print("+,Printing Current Config Settings\n");
	_EEPROMConfig.PrintCurrentConfig();
	
	//TODO:Retest frame imu failure
	//USBSerial.Clear_USBForDelay(10000);//*** Testing - trying to avoid IMU sensor mis-calibration - update 18/4/19 - issue appears to be fixed
	
	// Frame IMU Init
	_YEIFrame.Initialise();
	Serial.print(_YEIFrame._statusMessage);

	//**
	
	//Motor Init - Checks all axis and related sensors
	MotorController.axisInitialisationNew();
	
	//Array copy for all error messages/values
	for (int i = 0; i < InitListSize; i++)
	{
		_InitListArray[i].StatusValue = MotorController._InitListArray[i].StatusValue;
		_InitListArray[i].StatusMessage = MotorController._InitListArray[i].StatusMessage;
	}
	
	//**
	

	_InitListArray[ListFrameIMU].StatusMessage = _YEIFrame._statusMessage;
	_InitListArray[ListFrameIMU].StatusValue = _YEIFrame._status;
	

	
	//**
	//GPS Serial Init
	if(!_UseGPSCompass) //Default GPS Module
	{
		
		
		Serial.print("+,GPS Init - Checking GPS Data flowing\n");
		for (unsigned long timer = millis(); millis() - timer <= 1500; USBSerial.Clear_USBForDelay(10))
		{
		AntennaComms_SerialCommandHandle();
		_InitListArray[ListGPSSensor].StatusValue = GPSDecoder.GetGPSLocation(&_CurrentGPS);
		}

		_InitListArray[ListGPSSensor].StatusMessage = GPSDecoder._StatusMessage;
		
		Serial.print(GPSDecoder._StatusMessage);

	}else //Scotsat GPS Compass Initialisation method
	{
		Serial.print("+,Scotsat GPS Compass Init - Checking Data flowing\n");
		
		//Run initialisation and store result
		_InitListArray[ListGPSSensor].StatusValue = GPSCompass.CompassHandshake();
		//Store status message for class
		_InitListArray[ListGPSSensor].StatusMessage = GPSCompass._StatusMessage;
		//Debug print initialisation result
		Serial.print(GPSCompass._StatusMessage);
	}


	
	//Check error list for possible errors
	boolean isError = Initialise_CheckForErrors();
	
	// Handle error condition or pass Init
	if(isError && !_DebugInitBypass)
	{
		Intialise_PiReportError();
		isInitialising = false;
		_isInitilisation_Passed = false;
		// Keep track of failed Init attempts
		_InitCounter++;
	}
	else
	{
		Initialise_PiReportComplete();
		_CurrentAntennaMode = AntennaModeManualPos;
		_StoreManualPosition[El] = 30.0;
		_StoreManualPosition[Az] = 0.0;
		_StoreManualPosition[Pol] = MotorController._AntMotorPol.getAxisPos();
		Initialise_WaitforPiResponse();
		isInitialising = false;
		_isInitilisation_Passed = true;
		_InitCounter = 1;
	}
	
	
	// Wait for Pi to respond to initialisation
	_InitRunning = false;
}

//*****************************************************************************
//*****************************************************************************

// Initialise Report Errors to Pi
// Repeated send of all errors to pi for 20Seconds
// returns after prints to then restart init
void AntennaManagerClass::Intialise_PiReportError()
{
	unsigned long timer = millis() + 20000;
	
	MotorController._AntMotorAz._axisEnable = false;
	MotorController._AntMotorEl._axisEnable = false;
	MotorController._AntMotorPol._axisEnable = false;
	MotorController._AntMotorCross._axisEnable = false;
	
	PIACKFailedInit = false;

	_RGBLED.setColor(LEDColour_Red);
	
	while(!PIACKFailedInit)
	{

		
		if(millis() - timer > 20000)
		{
			//Create valid failed initialisation packet
			Initialise_UpdateFailedPacket();
			
			//Output to Pi
			Serial.print(_InitialisationPacket);
			
			
			Serial.print("@,Init Error. Reporting Errors. Waiting for Pi Ack;\n");
			
			for(int i = 0; i < InitListSize; i++)
			{
				
				Serial.print(_InitListArray[i].StatusMessage);
			}
			
			
			timer = millis();
		}
		
		
		AntennaComms_SerialCommandHandle();
		
	}
	
}


//*****************************************************************************

void AntennaManagerClass::Initialise_PiReportComplete()
{
	_RGBLED.setColor(LEDColour_Yellow);
	
	while(Serial.available() > 10)//Handle incomming serial commands
	AntennaComms_SerialCommandHandle();

	Serial.print("+,Initialisation Complete. All Functions Passed\n");
	
	Serial.print("+Init Results. Reporting Results*****;\n");
	
	for(int i = 0; i < InitListSize; i++)
	{
		
		Serial.print(_InitListArray[i].StatusMessage);
	}
	
	
}


//*****************************************************************************
// Checks Init list for errors
// return true if any Init function has returned an error condition
boolean AntennaManagerClass::Initialise_CheckForErrors()
{
	
	for(int i = 0; i < InitListSize; i++)
	{
		if(_InitListArray[i].StatusValue < 0)
		return true;
	}
	
	return false;
	
}

//**********************************************************************************
// Wait for Pi to respond to initialisation packet
// Resend Init packet until response
void AntennaManagerClass::Initialise_WaitforPiResponse()
{
	unsigned long resendTimer = millis() + 5000;
	const unsigned long InitPacketRetry = 5000;
	_USBConnectionStatus = false;
	
	//Until USB Comms receives a valid reply
	while(!_USBConnectionStatus)
	{
		if(millis() - resendTimer > InitPacketRetry)
		{

			Serial.print("+,Sending Init Packet. Waiting for Pi Init Response\n");
			//Create valid initialisation packet
			Initialise_UpdateInitPacket();
			
			//Output to Pi
			Serial.print(_InitialisationPacket);
			Serial.print("\n");
			Serial.flush();

			
			
			resendTimer = millis();
		}
		
		// Handle returned command data
		AntennaComms_SerialCommandHandle();
		
	}
	
	// Update class variable for latest init state - usb comms connected
	Initialise_UpdateInitPacket();
}

//**********************************************************************************
// Form Init packet - see evernote for better details
void AntennaManagerClass::Initialise_UpdateInitPacket()
{
	_InitialisationPacket = "#,0,";
	
	for (int i = 0; i < InitListSize; i++)
	_InitialisationPacket += String(_InitListArray[i].StatusValue) + ",";
	

	_InitialisationPacket += "\n";
}

// Form Init packet - see evernote for better details
void AntennaManagerClass::Initialise_UpdateFailedPacket()
{
	_InitialisationPacket = "#,1,";
	
	for (int i = 0; i < InitListSize; i++)
	_InitialisationPacket += String(_InitListArray[i].StatusValue) + ",";
	

	_InitialisationPacket += "\n";
}
//**********************************************************************************
//**********************************************************************************



//**********************************************************************************//**********************************************************************************
//**********************************************************************************//**********************************************************************************
