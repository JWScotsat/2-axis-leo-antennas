// AntennaManager.h

#ifndef _ANTENNAProject_Defines_h
#define _ANTENNAProject_Defines_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "TeensyChip_PinDefines.h" // Storage for pin number defines for custom teensy chip

//Soft Restart Code - Completely resets the Teensy MK20DX256
#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);



#define  PosArraySize 3

#define AntennaModeTest -100
#define AntennaModePointing 1
#define AntennaModeSearching 2
#define AntennaModePeaking 3
#define AntennaModeManualPos 4

//Serial Defines
#define PiSerial Serial //USB Serial
#define PiSerialBaud 115200

#define GPSSerial Serial1
#define GPSSerialBaud 230400

#define PCSerial Serial

#define CRCTrue true
#define CRCFalse false

// IMU Serial Defines
#define ImuFrameSerial Serial3
#define ImuCrossSerial Serial2
#define ImuBaudRate 115200
//***************************************************************************************************************
//Global Defines of pins and sensors - 3 Axis PCB Integrated V1
#ifndef Az
#define Az 0
#define El 1
#define Pol 2
#endif

//Motor Pins
#define ElMicro32Pin		ChipPin_D6
#define ElStepPin			ChipPin_D7
#define ElDirPin			ChipPin_E1
#define ElEnablePin			ChipPin_E0

#define AzMicro32Pin		ChipPin_D0
#define AzStepPin			ChipPin_D1
#define AzDirPin			ChipPin_D5
#define AzEnablePin			ChipPin_D4

#define CrMicro32Pin		ChipPin_C6
#define CrossStepPin		ChipPin_C7
#define CrossDirPin			ChipPin_C9
#define CrossEnablePin		ChipPin_C8

#define MotorPol1			ChipPin_C10 //En
#define MotorPolDir			ChipPin_C11 //Dir
#define MotorPolEnable		ChipPin_C10 //En
#define PolEncoderPin		ChipPin_A0_DP

//Sensor Pin
#define Sensor_ElHallPin	ChipPin_B2
#define AzHallEffectPin		ChipPin_B1


#define QuadCountX1 3
#define QuadCountX2 4

#define QuadCountY1 32
#define QuadCountY2 25

//** Digital Pins
#define ModemTXMutePin		ChipPin_B0
#define ModemPWRGoodPin		ChipPin_B3
#define PM_Input1		ChipPin_B0
#define PM_Input2		ChipPin_B3

// Modem Signal Strength Defines
#define ModemSigStrPin		ChipPin_A1_DM
#define ModemSigStrPin2		ChipPin_A1_DP


#define ModemX5SigStrPin	ChipPin_A0_DM


//LED Colour Pins
#define LEDRedPin			ChipPin_C2
#define LEDGreenPin			ChipPin_C1
#define LEDBBluePin			ChipPin_C0


// Antenna Physical Boundary Definitions
//#ifndef MaxPhysical_Elevation
#define MaxPhysical_Elevation 98.0
#define MinPhysical_Elevation -10.0
#define MaxPhysical_Azimuth 360.01
#define MinPhysical_Azimuth -0.01
#define MaxPhysical_Pol 360.0
#define MinPhysical_Pol 0.0
//#endif


#ifndef Yaw
#define Yaw 0
#define Pitch 1
#define Roll 2
#endif

//Command Function Defines
#define Command_PiInitialisation 99
#define Command_Ack_InitFail 98

#define Command_PointAtSat 11
#define Command_ManualPosition 12
#define Command_UpdateDBSignal 13

#define Command_PauseAntennaMode 20
#define Command_ManualStartSearch 22
#define Command_ManualStartPeaking 23

#define Command_ManualSwitchBucPower 31
#define Command_ManualSwitchModemTxMute 32

#define Command_AdjustPolAcc 41
#define Command_AdjustPolTrim 42
#define Command_CompassUpdate 43
#define Command_AdjustAzTrim 44
#define Command_ManualGPSSet 45
#define Command_AutoGPSSet 46

#define Command_GetGPSTime 51

#define Command_AllStop 55

//Advanced Settings Commands
#define Command_Adv_PeakingSettings 102
#define Command_Adv_SearchSettings 103
#define Command_Adv_PeakingSettingsDynamic 104
#define Command_Adv_PeakingSettingsStepTracking 105
#define Command_Adv_MotorDisable 115
#define Command_Adv_MotorLock 116

#define Command_Adv_MiscSettings 200
#define Command_Adv_SatSignalSettings 201
#define Command_Adv_TargetAdj 202
#define Command_Adv_IMUSettings 203
#define Command_Adv_UseOnboardGyroCompass 204
#define Command_Adv_CompFiltKVal 205
#define Command_Adv_SwitchStabilisationMethod 206
#define Command_Adv_ChangeAnyConfig 207
#define Command_Adv_PrintAnyConfig 208
#define Command_Adv_MotorSettings 223
#define Command_Adv_AzPIDConfig 224


//Debug extra commands
#define Command_Debug_SpoofSignalValueChange 209
#define Command_Debug_PeakingPatternPointDelayChange 210
#define Command_Debug_SearchPatternPointDelayChange 211
#define Command_Debug_FrameIMUDebugEnable 212
#define Command_Debug_ToggleMotorDataOutput 213
#define Command_Debug_PrintCurrentAxisData 214
#define Command_Debug_PeakingPatternModeChange 215
#define Command_Debug_PeakingPatternPrintSettings 216
#define Command_Debug_PrintUSBReplies 217
#define Command_Debug_PrintLoopData 218
#define Command_Debug_InitByPass 219
#define Command_Debug_PrintFWVersion 220
#define Command_Debug_PrintCompassReplies 221
#define Command_Debug_EnableLoopTimer 222
#define Command_Debug_IMUStabilityChoice 225
#define Command_Debug_GyroOffset 226

//EEPROM Config Storage Commands
#define Command_Storage_SaveCurrentConfig 300
#define Command_Storage_PrintConfigs 301
#define Command_Storage_RestoreFactoryConfig 302
#define Command_Storage_PrintCurrentConfigPacket 303
#define Command_Storage_PrintStoredConfigPacket 304


#define Command_Reset_Reinitialise 500
#define Command_Reset_SystemRestart 501
#define Command_ID_Response 502 // Command for querying current code name and version
#define Command_Reset_PMReset 503

#define Command_SwitchStabilityMethod 504

#define LEDColour_Green 0x008000
#define LEDColour_Blue 0x00000F
#define LEDColour_Off 0x000000
#define LEDColour_White 0xFFFFFF
#define LEDColour_Purple 0x800080
#define LEDColour_Red 0xF00000
#define LEDColour_Yellow 0xF0F000
#endif

