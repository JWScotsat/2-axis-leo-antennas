/*
Leo Antenna Main
Created By: Jack Wells

The main code is based on standard Arduino methodology - Setup and Loop. The subclasses are setup 
and the main loop runs the Antenna Manager main functionality. The main loop handles a return condition
from the Antenna Manager and can also handle a re-initialisation call. 

Returns: Nothing, loop runs forever

*/



#include "AntennaManager.h"
#line 16 "AntennaProject.ino"
#include "ArduinoUnit/ArduinoUnit.h"

//Unit Testing triggers
#define isTesting false
#define isUnitTesting false
#define isDebug false

#define ProjectCodeVersion "LEOANT-V1.13.35"



void setup()
{
	//Serial port is flushed due to previous issues with reprogramming
	Serial.begin(9600);

	Serial.end();
	
	delay(5000);

	Serial.begin(9600);

	delay(150);

	//Unit/Debug Testing. Holds here until PC serial connection
	if(isDebug)
	while(!Serial);
	
	//Antenna Manager Setup - Setup for all antenna functionality
	AntennaManager.setup();
	
	//Timer interrupt used for stepping motors
	Setup_TimerInterrupt();
	
	//Code version update
	AntennaManager._ProjectCodeVersion = ProjectCodeVersion;
	
	//Initialise Antenna - Sensors, Motors etc.
	AntennaManager.Initialise();


}



void loop()
{
	// Test must be run inside loop. Currently no way to complete testing then run main code ***
	if(isTesting)
	Test::run();
	else
	{

		if(isDebug)AntennaManager._PrintDebug_USB = true;

		AntennaManager.AntennaComms_SerialCommandHandle(); //Handle any received USB commands

		if(!AntennaManager._AntennaStatusWorking)	//Re-initialise antenna if requested
		AntennaManager.Initialise();
			
		if(AntennaManager._isInitilisation_Passed)	//Main Antenna Code - Runs forever
		AntennaManager.run();

	}
}


//Timer Interrupt Routine
//  Used to "tick" the motors - move each motor depending on steps available
IntervalTimer _MotorTimer;
// ISR Setup
void Setup_TimerInterrupt()
{
	_MotorTimer.begin(RunMotors,25); //interrupt run at 25uS
	//_MotorTimer.priority(0);
}

// ISR Antenna Manager Call for Run Motors
void RunMotors()
{
	AntennaManager.MotorController.RunMotors();
}


//************************************************************/\************************************************************\\