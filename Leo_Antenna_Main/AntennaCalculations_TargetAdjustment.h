// AntennaCalculations_TargetAdjustment.h
/*
Axis Control Target Adjustment Class

	Class is designed to output Correct Targets for Az and El based upon input axis and IMU data.
	Mathematics provided by JG, excel sheet confirms Calculate Target Adjustments results. 
	
	Created By Jack Wells
	
	Change Log:		21/8/15 - Class Created and Unit Tested - JW
					26/8/15 - Class updated to include Reverse Calculation
									- Returns Current Position without Roll/Pitch Correction


*/


#ifndef _AntennaCalculations_TargetAdjustment_h
#define _AntennaCalculations_TargetAdjustment_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif



// Antenna Physical Boundary Definitions
#ifndef MaxPhysical_Elevation
#define MaxPhysical_Elevation 85.0
#define MinPhysical_Elevation -10.0
#define MaxPhysical_Azimuth 360.01
#define MinPhysical_Azimuth -0.01
#define MaxPhysical_Pol 360.0
#define MinPhysical_Pol 0.0
#endif

#include "Control Classes/Supporting Libs/StandardFunctions_Array.h"

// Error Values

//Array Position Defines

#ifndef Az
#define Az 0
#define El 1
#define Pol 2
#endif

#define x 0
#define y 1
#define z 2

#ifndef Yaw
#define Yaw 0
#define Pitch 1
#define Roll 2
#endif

#include "Control Classes/Supporting Libs/StandardFunctions_Array.h"

class AntennaCalculations_TargetAdjustmentClass
{
 protected:
	StandardFunctions_Array StndFncts;
	
	float _OffsetFromSat = 0.0; //Calculated heading based on Satelitte Azimuth and azimuth found on Lock
	float _DeltaHeadingFromSat = 0.0;//Rate of change in heading based on Satellite azimuth and centre peak azimuth
	float _SatAzimuth;
	boolean isNewSatOffset = false;

	float _CombinedYaw = 0.0;
	float _FirstOffset = 0.0; //first offset which takes into account antenna mounting differences etc on first lock
	float _PeakingOffset = 0.0;// changes due to peaking pattern
	float _HeadingError = 0.0;
 public:
	void init();
	void CalculateTargetAdjustment(float *IMUPitchRoll, float AntennaYaw, float *DesiredAzElPol, float *ReturnedVals);

	void CalculateSimplfiedDirectAdjustments(float *IMUPItchRoll, float AntennaYaw, float *DesiredAzElPol, float *ReturnedVals);
	
	float CalculateYaw(float CurrentAxisPos, float CompassYaw, float MountingOffset);
	
	void BoundaryConditions(float *ReturnedVals);
	
	void CalculateCombinedStabilityAdjustments(float *IMUPitchRoll, float AntennaYaw, float *DesiredAzElPol, float *ReturnedVals, float FrameIMU_Pitch, float FrameIMU_Roll);//19-4-18 - Latest function to combine both simplified and complex stability ideas
	
	void CalculateUncorrectedCurrentPosition(float AzEncoder, float ElEncoderPos, float CorrectedMotorTarget[], float AxisTargetPosition[], float* ReturnedUncorrectedPositions, float SatAz);

	float getOffsetFromSat(){return _OffsetFromSat;};//Return stored heading value
		
	void CalculateOffsetFromSat(float CentreSkyTarget, float CurrentHeading);//Calculate a new estimated heading based on current pointed az and known satellite Az
	void CalculateOffsetFromSatSearch(float CentreSkyTarget, float CurrentHeading);//Calculate a new estimated heading based on current pointed az and known satellite Az
	
	boolean getIsNewOffset(){return isNewSatOffset;};//Return value of new offset
	
	float getCombinedYaw(){return _CombinedYaw;};// Return value of Combined Calculated Yaw
	float getFirstOffset(){return _FirstOffset;};// Return value of Combined Calculated Yaw	
	float getPeakingOffset(){return _PeakingOffset;};// Return value of peaking offset
};

extern AntennaCalculations_TargetAdjustmentClass AntennaCalculations_TargetAdjustment;

#endif

