/*
EEPROM Config Class
Created By: Jack Wells

Class Function: Used to store, retrieve and update configuration structure to EEPROM. Manages configuration state. Also serial prints
configuration data for debug viewing.
Dependencies: Uses EEPROMex library for large write/read functions
Returns: Updates Class Data to store latest configuration settings. Can also write/retrieve configuration from EEPROM.

*/

#include "Storage_EEPROMConfig.h"

/*Class setup
 Configures EEPROMex to teensy eeprom size
 Loads EEPROM config store to class variable
 If no config is found in EEPROM then writes default settings to EEPROM
 Returns: Nothing
*/
void Storage_EEPROMConfigClass::setup()
{
	EEPROM.setMemPool(0, 2048); // Defines memory size of EEProm
	
	Read(); // Reads Memory location 0 for configuration data - see configuration Struct.
	
	while(StoredConfig.EndMemoryChar != DefaultConfig.EndMemoryChar) // Checks first Config Value to ensure the stored configuration is valid
	{
		Serial.print("~Invalid Config/ No Config found in memory. Storing Default Config\n"); // Stores default settings to EEPROM
		delay(1000);
		FactoryReset();
		Read();
	}
	
	CurrentConfig = StoredConfig;
	CurrentConfig.ConfigName = _CurrentConfigName;
}

//**********************************************************************************
//**********************************************************************************

/*Class Save Current Config to EEPROM
	Writes class variable - CurrentConfig - to EEPROM. 
	Reads back EEPROM to update class variables to latest config settings
	Returns: Nothing
*/
void Storage_EEPROMConfigClass::SaveCurrent()
{
	if(_StorageDebug)
	{
		Serial.print("+,Writing Current Struct to EEPROM Mem Location;");
		Serial.println(ConfigData_MemAddress);
	}
	
	//Iterate each config version on every save - keeps track of number of changes until roll over and gives individual config values
	CurrentConfig.ConfigVersion++;
	
	//Ensure Saved/Current is larger than default on roll over
	if(CurrentConfig.ConfigVersion >= 0xFE)
	CurrentConfig.ConfigVersion = DefaultConfig.ConfigVersion;
	
	//Write to EEPROM
	EEPROM.writeBlock(ConfigData_MemAddress,CurrentConfig);
	
	//Update Class variable to latest stored EEPROM config
	Read();

}

//**********************************************************************************
//**********************************************************************************

/*Class Save Default to EEPROM
	Writes the default config to the EEPROM through the EEPROMex library large write
	Returns: Nothing
*/
void Storage_EEPROMConfigClass::FactoryReset()
{
	if(_StorageDebug)
	{
		Serial.print("+Writing Default Struct to EEPROM Mem Location;");
		Serial.println(ConfigData_MemAddress);
	}
	
	EEPROM.writeBlock(ConfigData_MemAddress,DefaultConfig);
}

//**********************************************************************************
//**********************************************************************************

/*Class Read from EEPROM
	Reads the EEPROM back into the class stored configuration
	Returns: Nothing
*/
void Storage_EEPROMConfigClass::Read()
{
	if(_StorageDebug)
	{
		Serial.print("+,Reading Stored Config Data from Mem Address;");
		Serial.println(ConfigData_MemAddress);
	}
	
	//EEPROMex Large Read Back
	EEPROM.readBlock(ConfigData_MemAddress,StoredConfig);
	
	StoredConfig.ConfigName = _ReadBackConfigName;

}

//**********************************************************************************
//**********************************************************************************

/*Class Print to USB 
	Uses Serial Print to output different class configs
	Returns: Nothing
*/
void Storage_EEPROMConfigClass::PrintCurrentConfig()
{
	Serial.print("+,Printing Current Config \n");
	PrintConfig(CurrentConfig);

}

void Storage_EEPROMConfigClass::PrintDefaultConfig()
{
	Serial.print("+,Printing Default Config \n");
	PrintConfig(DefaultConfig);

}

void Storage_EEPROMConfigClass::PrintStoredConfig()
{
	Serial.print("+Printing Stored Config \n");
	PrintConfig(StoredConfig);

}

void Storage_EEPROMConfigClass::PrintConfig(Configuration ConfigIn)
{
	Serial.print("+,<\n+,***\n");
	
	Serial.print("+,ConfigVersion; "); Serial.print(ConfigIn.ConfigVersion, HEX); Serial.print("\n");
	Serial.print("+,Peaking; Mode; "); Serial.print(ConfigIn.PeakingMode); Serial.print("\t");
	Serial.print("\n+,Default Peaking; DeltaEl; "); Serial.print(ConfigIn.DeltaEl,3); Serial.print("\t");
	Serial.print("DeltaAz; "); Serial.print(ConfigIn.DeltaAz,3); Serial.print("\t");
	Serial.print("KEl; "); Serial.print(ConfigIn.KEl,4); Serial.print("\t");
	Serial.print("KAz; "); Serial.print(ConfigIn.KAz,4); Serial.print("\t");
	Serial.print("SignalLostThreshold; "); Serial.print(ConfigIn.PeakingLockThreshold); Serial.print("\t");
	Serial.print("PeakingPauseOnUpperThreshold; "); Serial.print(ConfigIn.PeakingPauseOnUpperThreshold); Serial.print("\t");
	Serial.print("PeakingPauseThreshold; "); Serial.print(ConfigIn.PeakingPauseThreshold); Serial.print("\t");
	Serial.print("PeakingUnPauseThreshold; "); Serial.print(ConfigIn.PeakingUnPauseThreshold); Serial.print("\t");
	Serial.print("PatternPointDelay; "); Serial.print(ConfigIn._PeakingPatternPointDelayTime); Serial.print("\t");
	
	Serial.print("\n+,Peaking Settings - Dynamic");
	Serial.print(", DeltaElStart;"); Serial.print(ConfigIn._DyDeltaElStart);
	Serial.print(", DeltaAzStart;"); Serial.print(ConfigIn._DyDeltaAzStart);
	Serial.print(", CentreKEl;"); Serial.print(ConfigIn._DyCentreKEl,4);
	Serial.print(", CentreKAz;"); Serial.print(ConfigIn._DyCentreKAz,4);
	Serial.print(", DeltaEl Max;"); Serial.print(ConfigIn._DyDeltaElMax);
	Serial.print(", DeltaAz Max;"); Serial.print(ConfigIn._DyDeltaAzMax);
	Serial.print(", DyKEl;"); Serial.print(ConfigIn._DyCentreKEl,3);
	Serial.print(", DyKAz;,"); Serial.print(ConfigIn._DyCentreKAz,3);
	Serial.print(", DySigOffset;"); Serial.print(ConfigIn._DySigStrOffset);
	Serial.print(", DyAutoOffset;"); Serial.print(ConfigIn._AutoDyOffset);
	Serial.print(", SigLostThreshold;"); Serial.print(ConfigIn._DyPeakingLockThreshold);
	Serial.print(", Point Delay;"); Serial.print(ConfigIn._DyPeakingPatternPointDelayTime);
	
	Serial.print("\n+,Modem Reding; LockThreshold; "); Serial.print(ConfigIn._LockThreshold); Serial.print("\t");
	Serial.print("ReadAvg; "); Serial.print(ConfigIn._NumberofReadAverages); Serial.print("\t");
	
	Serial.print("\n+,Search; Delta El; "); Serial.print(ConfigIn._ElExpandingBoundaryDelta); Serial.print("\t");
	Serial.print("Max El; "); Serial.print(ConfigIn._AbsMaxPatternElevationBoundary); Serial.print("\t");
	Serial.print("ElStep; "); Serial.print(ConfigIn.SearchElevation_stepCountSize); Serial.print("\t");
	Serial.print("ElMax; "); Serial.print(ConfigIn.SearchElevation_axisMax); Serial.print("\t");
	Serial.print("ElMin; "); Serial.print(ConfigIn.SearchElevation_axisMin); Serial.print("\t");
	Serial.print("ElRollOver; "); Serial.print(ConfigIn.SearchElevation_AxisBoundaryRollOver); Serial.print("\t");
	
	Serial.print("AzStep; "); Serial.print(ConfigIn.SearchAzimuth_stepCountSize); Serial.print("\t");
	Serial.print("AzSweep; "); Serial.print(ConfigIn.SearchAzimuth_maxCounterBoundary); Serial.print("\t");
	Serial.print("AzMaxBoundary; "); Serial.print(ConfigIn.SearchAzimuth_axisMax); Serial.print("\t");
	Serial.print("AzMinBoundary; "); Serial.print(ConfigIn.SearchAzimuth_axisMin); Serial.print("\t");
	Serial.print("AzRollOver; "); Serial.print(ConfigIn.SearchAzimuth_AxisBoundaryRollOver); Serial.print("\t");
	Serial.print("PatternPointDelay; "); Serial.print(ConfigIn._SearchPatternPointDelayTime); Serial.print("\t");
	
	//IMU Yaw with Compass Compensation
	Serial.print("\n+,IMU Yaw Effects; Use Onboard Gyro With Compass; "); Serial.print(ConfigIn.UseOnBoardGyroWithCompass); Serial.print("\t");
	Serial.print("Compass Trust Factor; "); Serial.print(ConfigIn.KGyroCompFilter); Serial.print("\t");

	//Roll compensation settings
	Serial.print("\n+,PitchRollCom; ElDampening; "); Serial.print(ConfigIn.ElTargetAdjustmentDampening); Serial.print("\t");
	Serial.print("ElCompensationBuffer; "); Serial.print(ConfigIn.ElPitchRollCompBuffer); Serial.print("\t");
	Serial.print("Use Simplified Compensation; "); Serial.print(ConfigIn.UseSimplifiedStabilisation); Serial.print("\t");

	//IMU Averaging Settings
	Serial.print("\n+,IMU Averaging; PitchRnAvg;"); Serial.print(ConfigIn._FramePitchNumbAvg); Serial.print("\t");
	Serial.print("RollRnAvg; "); Serial.print(ConfigIn._FrameRollNumbAvg); Serial.print("\t");
	Serial.print("PitchBuffer; "); Serial.print(ConfigIn._FramePitchDefaultBuffer); Serial.print("\t");
	Serial.print("RollBuffer; "); Serial.print(ConfigIn._FrameRollDefaultBuffer); Serial.print("\t");
	
	
	//IMU Settings Frame
	Serial.print("\n+,FrameIMU; UpdateRate; "); Serial.print(ConfigIn.Frame_YEIUpdateRate); Serial.print("\t");
	Serial.print("IMU Enabled; "); Serial.print(ConfigIn._FrameIMUEnabled); Serial.print("\t");
	Serial.print("PitchTrim; "); Serial.print(ConfigIn.Frame_PitchTrim); Serial.print("\t");
	Serial.print("RollTrim; "); Serial.print(ConfigIn.Frame_RollTrim); Serial.print("\t");

	//IMU Settings Cross
	Serial.print("\n+,CrossIMU; UpdateRate; "); Serial.print(ConfigIn.Cross_YEIUpdateRate); Serial.print("\t");
	Serial.print("PitchTrim; "); Serial.print(ConfigIn.Cross_PitchTrim); Serial.print("\t");
	Serial.print("RollTrim; "); Serial.print(ConfigIn.Cross_RollTrim); Serial.print("\t");

	//Elevation Motor Settings
	Serial.print("\n+,El Motor; Trim; "); Serial.print(ConfigIn.El_axisTrim); Serial.print("\t");
	Serial.print("ErrorBuffer; "); Serial.print(ConfigIn.El_axisError); Serial.print("\t");
	Serial.print("Enabled; "); Serial.print(ConfigIn.El_axisEnable); Serial.print("\t");
	Serial.print("PowerDown; "); Serial.print(ConfigIn.El_axisPowerDown); Serial.print("\t");
	Serial.print("MaxBound; "); Serial.print(ConfigIn.El_axisMaxBoundary); Serial.print("\t");
	Serial.print("MinBound; "); Serial.print(ConfigIn.El_axisMinBoundary); Serial.print("\t");
	Serial.print("MoveRev; "); Serial.print(ConfigIn.El_axisMoveReverse); Serial.print("\t");
	Serial.print("SensorRev; "); Serial.print(ConfigIn.El_axisSensorReverse); Serial.print("\t");
	Serial.print("ReadRev; "); Serial.print(ConfigIn.El_axisReadReverse); Serial.print("\t");
	
	//Azimuth Motor Settings
	Serial.print("\n+,Az Motor; Trim; "); Serial.print(ConfigIn.Az_axisTrim); Serial.print("\t");
	Serial.print("ErrorBuffer; "); Serial.print(ConfigIn.Az_axisError); Serial.print("\t");
	Serial.print("Enabled; "); Serial.print(ConfigIn.Az_axisEnable); Serial.print("\t");
	Serial.print("PowerDown; "); Serial.print(ConfigIn.Az_axisPowerDown); Serial.print("\t");
	Serial.print("MaxBound; "); Serial.print(ConfigIn.Az_axisMaxBoundary); Serial.print("\t");
	Serial.print("MinBound; "); Serial.print(ConfigIn.Az_axisMinBoundary); Serial.print("\t");
	Serial.print("MoveRev; "); Serial.print(ConfigIn.Az_axisMoveReverse); Serial.print("\t");
	Serial.print("SensorRev; "); Serial.print(ConfigIn.Az_axisSensorReverse); Serial.print("\t");
	Serial.print("ReadRev; "); Serial.print(ConfigIn.Az_axisReadReverse); Serial.print("\t");
	
	
	//Cross Motor Settings
	Serial.print("\n+,Cross Motor; Trim; "); Serial.print(ConfigIn.Cross_axisTrim); Serial.print("\t");
	Serial.print("ErrorBuffer; "); Serial.print(ConfigIn.Cross_axisError); Serial.print("\t");
	Serial.print("Enabled; "); Serial.print(ConfigIn.Cross_axisEnable); Serial.print("\t");
	Serial.print("PowerDown; "); Serial.print(ConfigIn.Cross_axisPowerDown); Serial.print("\t");
	Serial.print("MaxBound; "); Serial.print(ConfigIn.Cross_axisMaxBoundary); Serial.print("\t");
	Serial.print("MinBound; "); Serial.print(ConfigIn.Cross_axisMinBoundary); Serial.print("\t");
	Serial.print("MoveRev; "); Serial.print(ConfigIn.Cross_axisMoveReverse); Serial.print("\t");
	Serial.print("SensorRev; "); Serial.print(ConfigIn.Cross_axisSensorReverse); Serial.print("\t");
	Serial.print("ReadRev; "); Serial.print(ConfigIn.Cross_axisReadReverse); Serial.print("\t");

	
	//Pol Motor Settings
	Serial.print("\n+,Pol Motor; Trim; "); Serial.print(ConfigIn.Pol_axisTrim); Serial.print("\t");
	Serial.print("ErrorBuffer; "); Serial.print(ConfigIn.Pol_axisError); Serial.print("\t");
	Serial.print("Enabled; "); Serial.print(ConfigIn.Pol_axisEnable); Serial.print("\t");
	Serial.print("PowerDown; "); Serial.print(ConfigIn.Pol_axisPowerDown); Serial.print("\t");
	Serial.print("MaxBound; "); Serial.print(ConfigIn.Pol_axisMaxBoundary); Serial.print("\t");
	Serial.print("MinBound; "); Serial.print(ConfigIn.Pol_axisMinBoundary); Serial.print("\t");
	Serial.print("MoveRev; "); Serial.print(ConfigIn.Pol_axisMoveReverse); Serial.print("\t");
	Serial.print("SensorRev; "); Serial.print(ConfigIn.Pol_axisSensorReverse); Serial.print("\t");
	Serial.print("ReadRev; "); Serial.print(ConfigIn.Pol_axisReadReverse); Serial.print("\t");


	Serial.print("\n+,Misc; BucPowerToggle; "); Serial.print(ConfigIn._BucPower); Serial.print("\t");
	Serial.print("ModemTxMuteToggle; "); Serial.print(ConfigIn._ModemTXMute); Serial.print("\t");
	Serial.print("MainLoopUpdateTime; "); Serial.print(ConfigIn._LoopUpdateTime); Serial.print("\t");
	Serial.print("USBDebug; "); Serial.print(ConfigIn._PrintUSBDebug); Serial.print("\t");
	Serial.print("PiMasterUpdateTime; "); Serial.print(ConfigIn._UpdatePiRate); Serial.print("\t");
	Serial.print("PiMotorUpdateTime; "); Serial.print(ConfigIn._UpdatePiRate_MotorData); Serial.print("\t");
	Serial.print("PiDebugUpdateTime; "); Serial.print(ConfigIn._DebugPacketOutputRate); Serial.print("\t");
	Serial.print("PiMotorDataEnable; "); Serial.print(ConfigIn._UpdatePi_MotorData_Enable); Serial.print("\t");
	
	Serial.print("\n+,Motor Error Timeouts; Az Timeout; "); Serial.print(ConfigIn._AzMotorTimeout); Serial.print("\t");
	Serial.print("El Timeout; "); Serial.print(ConfigIn._ElMotorTimeout); Serial.print("\t");
	Serial.print("Cross Timeout; "); Serial.print(ConfigIn._CrossMotorTimeout); Serial.print("\t");
	Serial.print("Pol Timeout; "); Serial.print(ConfigIn._PolMotorTimeout); Serial.print("\t");

	Serial.print("\n+,ConfigNumber; "); Serial.print(ConfigIn.ConfigName); Serial.print("\t");
	Serial.print("EndofMemoryChar; ");Serial.print(ConfigIn.EndMemoryChar); Serial.print("\t");
	
	Serial.println("\n+,***,\n+,>\n\n");
}


void Storage_EEPROMConfigClass::PrintCurrentConfigPacket()
{
	
	
	Serial.print("<,1,");
	
	Serial.print(CurrentConfig.ConfigVersion, HEX);Serial.print(",");
	Serial.print(CurrentConfig.DeltaEl,3); Serial.print(",");
	Serial.print(CurrentConfig.DeltaAz,3); Serial.print(",");
	Serial.print(CurrentConfig.KEl,4); Serial.print(",");
	Serial.print(CurrentConfig.KAz,4); Serial.print(",");
	Serial.print(CurrentConfig.PeakingLockThreshold); Serial.print(",");
	Serial.print(CurrentConfig.PeakingPauseOnUpperThreshold); Serial.print(",");
	Serial.print(CurrentConfig.PeakingPauseThreshold); Serial.print(",");
	Serial.print(CurrentConfig.PeakingUnPauseThreshold); Serial.print(",");
	Serial.print(CurrentConfig._PeakingPatternPointDelayTime); Serial.print(",");
	
	Serial.print(CurrentConfig._LockThreshold); Serial.print(",");
	Serial.print(CurrentConfig._NumberofReadAverages); Serial.print(",");
	
	Serial.print(CurrentConfig._ElExpandingBoundaryDelta); Serial.print(",");
	Serial.print(CurrentConfig._AbsMaxPatternElevationBoundary); Serial.print(",");
	Serial.print(CurrentConfig.SearchElevation_stepCountSize); Serial.print(",");
	Serial.print(CurrentConfig.SearchElevation_axisMax); Serial.print(",");
	Serial.print(CurrentConfig.SearchElevation_axisMin); Serial.print(",");
	Serial.print(CurrentConfig.SearchElevation_AxisBoundaryRollOver); Serial.print(",");
	
	Serial.print(CurrentConfig.SearchAzimuth_stepCountSize); Serial.print(",");
	Serial.print(CurrentConfig.SearchAzimuth_maxCounterBoundary); Serial.print(",");
	Serial.print(CurrentConfig.SearchAzimuth_axisMax); Serial.print(",");
	Serial.print(CurrentConfig.SearchAzimuth_axisMin); Serial.print(",");
	Serial.print(CurrentConfig.SearchAzimuth_AxisBoundaryRollOver); Serial.print(",");
	Serial.print(CurrentConfig._SearchPatternPointDelayTime); Serial.print("\n");
	
	Serial.print("<,2,");

	Serial.print(CurrentConfig.ElTargetAdjustmentDampening); Serial.print(",");
	Serial.print(CurrentConfig._FramePitchNumbAvg); Serial.print(",");
	Serial.print(CurrentConfig._FrameRollNumbAvg); Serial.print(",");
	Serial.print(CurrentConfig._FramePitchDefaultBuffer); Serial.print(",");
	Serial.print(CurrentConfig._FrameRollDefaultBuffer); Serial.print(",");
	Serial.print(CurrentConfig.ElPitchRollCompBuffer); Serial.print(",");
	
	
	Serial.print(CurrentConfig.Frame_YEIUpdateRate); Serial.print(",");
	Serial.print(CurrentConfig._FrameIMUEnabled); Serial.print(",");
	Serial.print(CurrentConfig.Frame_PitchTrim); Serial.print(",");
	Serial.print(CurrentConfig.Frame_RollTrim); Serial.print(",");

	Serial.print(CurrentConfig.Cross_YEIUpdateRate); Serial.print(",");
	Serial.print(CurrentConfig.Cross_PitchTrim); Serial.print(",");
	Serial.print(CurrentConfig.Cross_RollTrim); Serial.print(",");


	Serial.print(CurrentConfig.El_axisTrim); Serial.print(",");
	Serial.print(CurrentConfig.El_axisError); Serial.print(",");
	Serial.print(CurrentConfig.El_axisEnable); Serial.print(",");
	Serial.print(CurrentConfig.El_axisPowerDown); Serial.print(",");
	Serial.print(CurrentConfig.El_axisMaxBoundary); Serial.print(",");
	Serial.print(CurrentConfig.El_axisMinBoundary); Serial.print(",");
	Serial.print(CurrentConfig.El_axisMoveReverse); Serial.print(",");
	Serial.print(CurrentConfig.El_axisSensorReverse); Serial.print(",");
	Serial.print(CurrentConfig.El_axisReadReverse); Serial.print(",");
	
	Serial.print(CurrentConfig.Az_axisTrim); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisError); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisEnable); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisPowerDown); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisMaxBoundary); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisMinBoundary); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisMoveReverse); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisSensorReverse); Serial.print(",");
	Serial.print(CurrentConfig.Az_axisReadReverse); Serial.print("\n");
	
	Serial.print("<,3,");

	Serial.print(CurrentConfig.Cross_axisTrim); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisError); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisEnable); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisPowerDown); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisMaxBoundary); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisMinBoundary); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisMoveReverse); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisSensorReverse); Serial.print(",");
	Serial.print(CurrentConfig.Cross_axisReadReverse); Serial.print(",");

	Serial.print(CurrentConfig.Pol_axisTrim); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisError); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisEnable); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisPowerDown); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisMaxBoundary); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisMinBoundary); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisMoveReverse); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisSensorReverse); Serial.print(",");
	Serial.print(CurrentConfig.Pol_axisReadReverse); Serial.print(",");

	Serial.print(CurrentConfig._BucPower); Serial.print(",");
	Serial.print(CurrentConfig._ModemTXMute); Serial.print(",");
	Serial.print(CurrentConfig._LoopUpdateTime); Serial.print(",");
	Serial.print(CurrentConfig._PrintUSBDebug); Serial.print(",");
	Serial.print(CurrentConfig._UpdatePiRate); Serial.print(",");
	Serial.print(CurrentConfig._UpdatePiRate_MotorData); Serial.print(",");
	Serial.print(CurrentConfig._UpdatePi_MotorData_Enable); Serial.print(",");
	

	
	Serial.print(CurrentConfig.ConfigName); Serial.print(",");
	Serial.print(CurrentConfig.EndMemoryChar, HEX); Serial.print(",");
	Serial.println("\n");
	
}

void Storage_EEPROMConfigClass::PrintStoredConfigPacket()
{
	
	
	Serial.print("<,1,");
	
	Serial.print(StoredConfig.ConfigVersion, HEX);Serial.print(",");
	Serial.print(StoredConfig.DeltaEl,3); Serial.print(",");
	Serial.print(StoredConfig.DeltaAz,3); Serial.print(",");
	Serial.print(StoredConfig.KEl,4); Serial.print(",");
	Serial.print(StoredConfig.KAz,4); Serial.print(",");
	Serial.print(StoredConfig.PeakingLockThreshold); Serial.print(",");
	Serial.print(StoredConfig.PeakingPauseOnUpperThreshold); Serial.print(",");
	Serial.print(StoredConfig.PeakingPauseThreshold); Serial.print(",");
	Serial.print(StoredConfig.PeakingUnPauseThreshold); Serial.print(",");
	Serial.print(StoredConfig._PeakingPatternPointDelayTime); Serial.print(",");
	
	Serial.print(StoredConfig._LockThreshold); Serial.print(",");
	Serial.print(StoredConfig._NumberofReadAverages); Serial.print(",");
	
	Serial.print(StoredConfig._ElExpandingBoundaryDelta); Serial.print(",");
	Serial.print(StoredConfig._AbsMaxPatternElevationBoundary); Serial.print(",");
	Serial.print(StoredConfig.SearchElevation_stepCountSize); Serial.print(",");
	Serial.print(StoredConfig.SearchElevation_axisMax); Serial.print(",");
	Serial.print(StoredConfig.SearchElevation_axisMin); Serial.print(",");
	Serial.print(StoredConfig.SearchElevation_AxisBoundaryRollOver); Serial.print(",");
	
	Serial.print(StoredConfig.SearchAzimuth_stepCountSize); Serial.print(",");
	Serial.print(StoredConfig.SearchAzimuth_maxCounterBoundary); Serial.print(",");
	Serial.print(StoredConfig.SearchAzimuth_axisMax); Serial.print(",");
	Serial.print(StoredConfig.SearchAzimuth_axisMin); Serial.print(",");
	Serial.print(StoredConfig.SearchAzimuth_AxisBoundaryRollOver); Serial.print(",");
	Serial.print(StoredConfig._SearchPatternPointDelayTime); Serial.print("\n");
	
	Serial.print("<,2,");

	Serial.print(StoredConfig.ElTargetAdjustmentDampening); Serial.print(",");
	Serial.print(StoredConfig._FramePitchNumbAvg); Serial.print(",");
	Serial.print(StoredConfig._FrameRollNumbAvg); Serial.print(",");
	Serial.print(StoredConfig._FramePitchDefaultBuffer); Serial.print(",");
	Serial.print(StoredConfig._FrameRollDefaultBuffer); Serial.print(",");
	Serial.print(StoredConfig.ElPitchRollCompBuffer); Serial.print(",");
	
	
	Serial.print(StoredConfig.Frame_YEIUpdateRate); Serial.print(",");
	Serial.print(StoredConfig._FrameIMUEnabled); Serial.print(",");
	Serial.print(StoredConfig.Frame_PitchTrim); Serial.print(",");
	Serial.print(StoredConfig.Frame_RollTrim); Serial.print(",");

	Serial.print(StoredConfig.Cross_YEIUpdateRate); Serial.print(",");
	Serial.print(StoredConfig.Cross_PitchTrim); Serial.print(",");
	Serial.print(StoredConfig.Cross_RollTrim); Serial.print(",");


	Serial.print(StoredConfig.El_axisTrim); Serial.print(",");
	Serial.print(StoredConfig.El_axisError); Serial.print(",");
	Serial.print(StoredConfig.El_axisEnable); Serial.print(",");
	Serial.print(StoredConfig.El_axisPowerDown); Serial.print(",");
	Serial.print(StoredConfig.El_axisMaxBoundary); Serial.print(",");
	Serial.print(StoredConfig.El_axisMinBoundary); Serial.print(",");
	Serial.print(StoredConfig.El_axisMoveReverse); Serial.print(",");
	Serial.print(StoredConfig.El_axisSensorReverse); Serial.print(",");
	Serial.print(StoredConfig.El_axisReadReverse); Serial.print(",");
	
	Serial.print(StoredConfig.Az_axisTrim); Serial.print(",");
	Serial.print(StoredConfig.Az_axisError); Serial.print(",");
	Serial.print(StoredConfig.Az_axisEnable); Serial.print(",");
	Serial.print(StoredConfig.Az_axisPowerDown); Serial.print(",");
	Serial.print(StoredConfig.Az_axisMaxBoundary); Serial.print(",");
	Serial.print(StoredConfig.Az_axisMinBoundary); Serial.print(",");
	Serial.print(StoredConfig.Az_axisMoveReverse); Serial.print(",");
	Serial.print(StoredConfig.Az_axisSensorReverse); Serial.print(",");
	Serial.print(StoredConfig.Az_axisReadReverse); Serial.print("\n");
	
	Serial.print("<,3,");

	Serial.print(StoredConfig.Cross_axisTrim); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisError); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisEnable); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisPowerDown); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisMaxBoundary); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisMinBoundary); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisMoveReverse); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisSensorReverse); Serial.print(",");
	Serial.print(StoredConfig.Cross_axisReadReverse); Serial.print(",");

	Serial.print(StoredConfig.Pol_axisTrim); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisError); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisEnable); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisPowerDown); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisMaxBoundary); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisMinBoundary); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisMoveReverse); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisSensorReverse); Serial.print(",");
	Serial.print(StoredConfig.Pol_axisReadReverse); Serial.print(",");

	Serial.print(StoredConfig._BucPower); Serial.print(",");
	Serial.print(StoredConfig._ModemTXMute); Serial.print(",");
	Serial.print(StoredConfig._LoopUpdateTime); Serial.print(",");
	Serial.print(StoredConfig._PrintUSBDebug); Serial.print(",");
	Serial.print(StoredConfig._UpdatePiRate); Serial.print(",");
	Serial.print(StoredConfig._UpdatePiRate_MotorData); Serial.print(",");
	Serial.print(StoredConfig._UpdatePi_MotorData_Enable); Serial.print(",");
	

	
	Serial.print(StoredConfig.ConfigName); Serial.print(",");
	Serial.print(StoredConfig.EndMemoryChar); Serial.print(",");
	Serial.println("\n");
	
}

//************************************************************/\************************************************************\\