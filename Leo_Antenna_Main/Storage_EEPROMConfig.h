// Storage_EEPROMConfig.h

#ifndef _STORAGE_EEPROMCONFIG_h
#define _STORAGE_EEPROMCONFIG_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "EEPROMEx\EEPROMex.h"

#define ConfigData_MemAddress 0

#define _CurrentConfigName 1
#define _ReadBackConfigName 2
#define _DefaultConfigName 0

#ifndef CustomConfigStruct
struct Configuration {
	
	byte ConfigVersion = 0x01; // Config Version Marker - Marks number of times the config has been changed

	//Peaking Settings
	int PeakingMode = 1; // Peaking Mode - 1- Dynamic Box, 2 - Step Tracking, 3 Circular Gradient/box
	
	float DeltaEl = 0.25; // Peaking Pattern Elevation Step Size
	float DeltaAz = 0.25; // Peaking Pattern Azimuth Step Size
	float KEl = 0.001; // Peaking Pattern K Elevation Value - Effects the reaction to peaking pattern box results - how the centre moves
	float KAz = 0.001; // Peaking Pattern K Azimuth Value - Effects the reaction to peaking pattern box results - how the centre moves
	int PeakingLockThreshold = 230; // Minimum Peaking signal strength before restarting search
	boolean PeakingPauseOnUpperThreshold = false; // Stop peaking at top threshold value
	int PeakingPauseThreshold = 2200; // Top threshold value for stopping peaking movements
	int PeakingUnPauseThreshold = 1950; // Lower threshold value for resumming peaking movements
	unsigned long _PeakingPatternPointDelayTime = 350; // Peaking pause time/delay at pattern points
	
	//Dynamic Peaking Settings
	float _DyDeltaElStart = 0.25; //Dynamic Peaking Delta Elevation Initial value
	float _DyDeltaAzStart = 0.25; //Dynamic Peaking Delta Az Initial value
	float _DyCentreKAz = 0.02; //Dynamic Peaking K Azimuth Initial value
	float _DyCentreKEl = 0.02; //Dynamic Peaking K Elevation Initial value
	
	float _DyDeltaAzMax = 0.35; //Dynamic Peaking Delta Elevation Maximum Value
	float _DyDeltaElMax = 0.35; //Dynamic Peaking Delta Azimuth Maximum Value
	
	float _DyKEl = 0.01; //Dynamic Peaking Delta K Value for adjusting Elevation Delta based on signal strength
	float _DyKAz = 0.01; //Dynamic Peaking Delta K Value for adjusting Azimuth Delta based on signal strength
	float _DySigStrOffset = 2; //Dynamic Peaking Signal Strength value used in adjusting Az/El Delta values -This value will have a delta El/Az the same as the "DyDelta..Start" value
	boolean _AutoDyOffset = false; // Dis/Enable Automatic Signal Strength offset on complete search moving to peaking
	float _DyPeakingLockThreshold = -4.0; // Dynamic Peaking lock threshold - lowest value before signal loss causes restart of search
	unsigned long _DyPeakingPatternPointDelayTime = 100; // Peaking pause time/delay at pattern points
	
	
	
	//Modem Signal Read Settings
	int _LockThreshold = 3500; // Search threshold for signal found - Triggers peaking
	int _NumberofReadAverages = 3; //Number of signal reads averaged together
	
	//Search Settings
	float _ElExpandingBoundaryDelta = 0.50; // Search elevation size increase at each row change
	float _AbsMaxPatternElevationBoundary = 2.0; // Maximum elevation search size
	
	float	SearchElevation_stepCountSize = 20.0; // Search elevation step size - limited movement to El Expanding Boundary Delta
	float	SearchElevation_axisMax = 87.0; // Maximum elevation position during search
	float	SearchElevation_axisMin = -10.0; // Minimum elevation position during search
	boolean	SearchElevation_AxisBoundaryRollOver = false; // Elevation axis roll-over (moving from minimum to maximum position)
	
	float	SearchAzimuth_stepCountSize = 0.750; // Search Azimuth step size
	float	SearchAzimuth_maxCounterBoundary = 20.0; // Search Azimuth Sweep Size - Maximum Azimuth position away from start location (pointed location)
	float	SearchAzimuth_axisMax = 360.01; // Azimuth Maximum Axis Size
	float	SearchAzimuth_axisMin = -0.01; // Azimuth Minimum Axis Size
	boolean	SearchAzimuth_AxisBoundaryRollOver = true;// Azimuth axis roll-over (moving from minimum to maximum position)
	
	unsigned long _SearchPatternPointDelayTime = 0;// Search pause time/delay at pattern points
	
	//New CoG system effecting wide search toggle enable
	boolean _SearchCoGWideEnable = true;
	
	//IMU Yaw with Compass Compensation
	boolean UseOnBoardGyroWithCompass = false; //Dis/Enable Use of IMU Yaw values for Azimuth stabilisation of yaw movements
	float KGyroCompFilter = 0.98; // K Gyro value for IMU Yaw stabilisation
	
	//PitchRoll Compensation Variables
	float ElTargetAdjustmentDampening = 1.0;  // Legacy Setting - unused
	float ElPitchRollCompBuffer = 1.3; // Legacy Setting - unused
	boolean UseSimplifiedStabilisation = false; // Dis/Enable Simplified Stabilisation - Links IMU Frame Pitch to Elevation Movement, Azimuth ignores roll compensation
	
	//Frame IMU Averaging Settings
	int _FramePitchNumbAvg = 10; // IMU Running Average - No of averages used for Frame Pitch Value
	int _FrameRollNumbAvg = 10; // IMU Running Average - No of averages used for Frame Roll Value
	float _FramePitchDefaultBuffer = 0.150; // IMU Running Average buffer/threshold - Pitch value will be ignored below this value
	float _FrameRollDefaultBuffer = 0.150; // IMU Running Average buffer/threshold - Roll value will be ignored below this value
	
	//IMU Variables
	boolean _FrameIMUEnabled = true; //Frame IMU Settings - Enable use of IMU
	float Frame_RollTrim = 0.0; //Frame IMU Settings - Roll Trim/Offset for adjusting error in roll readings
	float Frame_PitchTrim = 0.0; //Frame IMU Settings - Pitch Trim/Offset for adjusting error in Pitch readings
	unsigned long Frame_YEIUpdateRate = 7; //Frame IMU Settings - mS time between requesting IMU data (7 default)
	
	float Cross_RollTrim = 0.0;  //Cross IMU Settings - Roll Trim/Offset for adjusting error in roll readings
	float Cross_PitchTrim = 0.0; //Cross IMU Settings - Pitch Trim/Offset for adjusting error in Pitch readings
	unsigned long Cross_YEIUpdateRate = 7;//Cross IMU Settings - mS time between requesting IMU data (7 default)
	
	//Motor Settings
	// Elevation Motor Settings
	int El_axisMoveReverse = -1; //Reverse movement direction of Axis motor 1/-1
	int El_axisReadReverse = 1;//Reverse reading direction of Axis sensor 1/-1
	float El_axisTrim = 0.0;//23.5; // Axis Positional Trim - fixes misalignment in sensor readings and actual position
	float El_axisSensorReverse = 1.0; // Legacy Setting - No longer used
	float El_axisError = 0.050; // Axis positional buffer between acheived sensor value and Target position
	boolean El_axisEnable = true; //Axis enable - False Locks Axis in Current Place / True Allows axis movement
	boolean El_axisPowerDown = false; // Axis Power Down - False Axis powered to move / True Axis motor power disabled - Stepper motors free moving
	float El_axisMinBoundary = -10.0; // Axis minimum position boundary
	float El_axisMaxBoundary = 98.0; // Axis Maximum Position boundary
	
	// Azimuth Motor Settings
	int Az_axisMoveReverse = 1;//Reverse movement direction of Axis motor 1/-1
	int Az_axisReadReverse = 1;//Reverse reading direction of Axis sensor 1/-1
	float Az_axisTrim = 0.0; // Axis Positional Trim - fixes misalignment in sensor readings and actual position
	float Az_axisSensorReverse = 1.0;// Legacy Setting - No longer used
	float Az_axisError = 0.050;// Axis positional buffer between acheived sensor value and Target position
	boolean Az_axisEnable = true;//Axis enable - False Locks Axis in Current Place / True Allows axis movement
	boolean Az_axisPowerDown = false;// Axis Power Down - False Axis powered to move / True Axis motor power disabled - Stepper motors free moving
	float Az_axisMinBoundary = -0.010;// Axis minimum position boundary
	float Az_axisMaxBoundary = 360.010;// Axis Maximum Position boundary
	
	// Cross Axis Motor Settings
	int Cross_axisMoveReverse = -1;//Reverse movement direction of Axis motor 1/-1
	int Cross_axisReadReverse = 1;//Reverse reading direction of Axis sensor 1/-1
	float Cross_axisTrim = 0.0; // Axis Positional Trim - fixes misalignment in sensor readings and actual position
	float Cross_axisSensorReverse = 1.0;// Legacy Setting - No longer used
	float Cross_axisError = 0.4;// Axis positional buffer between acheived sensor value and Target position
	boolean Cross_axisEnable = true;//Axis enable - False Locks Axis in Current Place / True Allows axis movement
	boolean Cross_axisPowerDown = false;// Axis Power Down - False Axis powered to move / True Axis motor power disabled - Stepper motors free moving
	float Cross_axisMinBoundary = -90.0;// Axis minimum position boundary
	float Cross_axisMaxBoundary = 90.0;// Axis Maximum Position boundary
	
	// Polarisation Motor Settings
	int Pol_axisMoveReverse = 0;//Reverse movement direction of Axis motor 1/0
	int Pol_axisReadReverse = 1;//Reverse reading direction of Axis sensor 1/-1
	float Pol_axisTrim = 0.0; // Axis Positional Trim - fixes misalignment in sensor readings and actual position
	float Pol_axisSensorReverse = 1.0;// Legacy Setting - No longer used
	float Pol_axisError = 3.0;// Axis positional buffer between acheived sensor value and Target position
	boolean Pol_axisEnable = true;//Axis enable - False Locks Axis in Current Place / True Allows axis movement
	boolean Pol_axisPowerDown = false;// Axis Power Down - False Axis powered to move / True Axis motor power disabled - Stepper motors free moving
	float Pol_axisMinBoundary = -10.0;// Axis minimum position boundary
	float Pol_axisMaxBoundary = 360.0;// Axis Maximum Position boundary
	
	//Power Controls
	int _BucPower = 0; // Buc power relay control - 0 Off - 1 On (**Check**)
	int _ModemTXMute = 0; // Modem Tx Mute - 0 Unmuted - 1 Muted
	
	//Debug Data
	unsigned long _LoopUpdateTime = 0; // Antenna Main Loop time - 0 Run's as fast as possible (~10ms) - Other values adjust update rate
	boolean _PrintUSBDebug = false; // Print debug data over usb
	
	//USB Data Output
	unsigned long _UpdatePiRate = 350; //USB Update Main Packet Rate (ms)
	unsigned long _UpdatePiRate_MotorData = 250; // USB update Motor Data Rate (ms)
	unsigned long _DebugPacketOutputRate = 300; // USB update Debug packet Rate (ms)
	boolean _UpdatePi_MotorData_Enable = false; // Motor Data Output Dis/Enable

	//Motor Timeout Settings
	unsigned long _AzMotorTimeout = 1800000; // Error Timeout for Azimuth motor not reaching it's target position.(ms)
	unsigned long _ElMotorTimeout = 100000;// Error Timeout for Elevation motor not reaching it's target position.(ms)
	unsigned long _CrossMotorTimeout = 100000;// Error Timeout for Cross Axis motor not reaching it's target position.(ms)
	unsigned long _PolMotorTimeout = 120000;// Error Timeout for Polarisation motor not reaching it's target position.(ms)

	//GPS Compass Settings
	boolean UseGPSCompass = true;
	int GPSCompassBaud = 115200;
	
	//Sat Signal DB via USB Settings
	boolean _useDBviaUSB = true;
	float _DBThreshold = -4.0;
	
	//GPS Compass Extra Config
	boolean _GPSCompassDir = 1;
	
	
	
	int ConfigName = _DefaultConfigName; // Config Name
	char EndMemoryChar = 0x66; // End of config data check
	
};
#endif

class Storage_EEPROMConfigClass
{
	protected:
	Configuration FactoryConfig;
	

	public:
	
	boolean _StorageDebug = false;
	
	Configuration StoredConfig;
	const Configuration DefaultConfig;
	Configuration CurrentConfig;
	
	void init();
	
	void PrintConfig(Configuration ConfigIn);
	
	void setup();
	void SaveCurrent();
	void FactoryReset();
	void Read();
	void PrintCurrentConfig();
	void PrintStoredConfig();
	void PrintDefaultConfig();
	void PrintCurrentConfigPacket();
	void PrintStoredConfigPacket();
	
	
};

extern Storage_EEPROMConfigClass Storage_EEPROMConfig;

#endif

