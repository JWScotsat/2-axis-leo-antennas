#include "YEISensor.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

boolean YEISensor::getConnected()
{
	const unsigned long IMUFailtTimeout = 5000;
	unsigned long TimeOut = millis();
	boolean isResponding = false;
	
	LEDColour(1.0,0.0,0.0); // Set LED Red
	
	if(DebugPrint)
	PiSerial.print("+,Checking " + _Name  + " Imu Connection\n+,Requesting Handshake\n");

	while (isResponding == false && millis() - TimeOut < IMUFailtTimeout )
	{

		
		isResponding = CheckLEDColourResponse();
		
		ClearUSBForDelay(200);
		
		if(isResponding)
		break;
	}
	
	if(!isResponding)
	PiSerial.print("@," + _Name + "IMU Connection Fail!\n");
	
	return isResponding;
}


boolean YEISensor::CheckLEDColourResponse()
{

	
	int validcount = 0;
	float rSent = 0.50000;
	float gSent = 0.00;
	float bSent = 1.00000;
	
	unsigned long clearTimer = millis();
	
	while(millis() - clearTimer < 2)
	char C = _Serial->read();
	
	LEDColour(rSent, gSent, bSent);
	
	_Serial->print(":239\n");
	
	ClearUSBForDelay(30);

	if( _Serial->available())
	{
		float rReceived = _Serial->parseFloat();
		float gReceived = _Serial->parseFloat();
		float bReceived = _Serial->parseFloat();
		
		if(isFloatEqual(rReceived,rSent, 1))
		validcount++;
		
		//PCSerial.print("Comparing Colour\n Valid R;");PCSerial.println(validcount);
		
		if(isFloatEqual(gReceived,gSent, 1))
		validcount++;
		
		//PCSerial.print("Comparing Colour\n Valid G;");PCSerial.println(validcount);
		
		if(isFloatEqual(bReceived,bSent, 1))
		validcount++;
		
		//PCSerial.print("Comparing Colour\n Valid B;");PCSerial.println(validcount);
		
		if(validcount == 3)
		{
			if(DebugPrint)
			PiSerial.println("+,IMU Connection Test Passed");
			
			return true;
		}
		else if(DebugPrint)
		{
		PiSerial.println("+,Invalid Values Received.Possible Issue with IMU.");
		PiSerial.print("+, Received:"); PiSerial.print(rReceived); PiSerial.print(", ");
		PiSerial.print(gReceived); PiSerial.print(", ");
		PiSerial.print(bReceived); PiSerial.print(", ");
		}
	}
	
	ClearUSBForDelay(150);
	
	LEDColour(1.0, 0.0, 0.0);
	
	ClearUSBForDelay(150);
	
	return false;
}

//**********************************************************************************************

void YEISensor::LEDColour(float RedLED, float GrnLED, float BluLED)
{
	_Serial->print(":238,");
	_Serial->print(RedLED);
	_Serial->print(",");
	_Serial->print(GrnLED);
	_Serial->print(",");
	_Serial->println(BluLED);
	
	delay(20);
}

// YEI Sensor Command to turn the LED Green
void YEISensor::SetLEDGreen()
{
	
	_Serial->print(":238,");
	_Serial->print(0.0);
	_Serial->print(",");
	_Serial->print(1.0);
	_Serial->print(",");
	_Serial->println(0.0);
	
	if(DebugPrint)
	PiSerial.print("+,Setting " + _Name + "'s LED Green \n");
}



//Called by: IMU_Config_CheckLEDColourResponse
//
//Function: General Function to compare the equality of 2 floats
//				Degree of accuracy can be controlled by the Number of Decimal Places to compare
//
//Returns: boolean result (equal or not equal)
boolean YEISensor::isFloatEqual(float f1, float f2, int NumberofDecPlaces){
	float decimalPlace = 1.0000;
	
	
	for (int i = 0; i < NumberofDecPlaces; i++)
	{
		decimalPlace /=10;
	}
	
	if(abs(f1 - f2) < decimalPlace)
	return true;
	else
	return false;
}


//***************************************************************************************
//Sensor Command
// YEI Auto Calibrate Gyros
void YEISensor::CalibrateGyros()
{
	if(DebugPrint)
	PiSerial.println("+,Calibrating " + _Name + " Gyros");
	
	_Serial->print(":165\n");
	ClearUSBForDelay(5000);
	

}


//***************************************************************************************
//Sensor Command
// YEI Set Filter Mode
void YEISensor::setFilter(int filterMode)
{
	PiSerial.println("+,IMU " + _Name + " Setting Filter Mode;" +String(filterMode));
	
	_Serial->print(":123,");_Serial->print(filterMode);_Serial->println();
	
	delay(50);
	
	_Serial->print(":152\n");
	
	delay(50);
	
	if(_Serial->available())
	{
		PiSerial.print("+,IMU Filter Feedback;");
		while(_Serial->available())
		{
			char c = _Serial->read();
			PiSerial.print(c);
		}
		PiSerial.println("");
	}
	
	
	//TODO: code changes can be made to increase the baud rate of the sensor. Sensor keeps baud rate indefinitely through restarts. Restart required to take effect. Requires further thought on port scanning etc to ensure sensor connected responds to correct baud rate
// 	
// 		_Serial->print(":231,460800\n");
// 		
// 		delay(50);
// 		
// 		_Serial->print(":232\n");
// 		
// 		delay(50);
// 		
// 		if(_Serial->available())
// 		{
// 			PiSerial.print("+,IMU Baud Rate Feedback;");
// 			while(_Serial->available())
// 			{
// 				char c = _Serial->read();
// 				PiSerial.print(c);
// 			}
// 			PiSerial.println("");
// 		}
// 		
// 		delay(1000);
// 		
// 		ResetSensor();
// 		
// 		_Serial->end();
// 		_Serial->begin(460800);
}


//******************************************************************************
//******************************************************************************
// Flush the selected serial port to ensure no remaining data in the buffer
void YEISensor::ClearUSBForDelay(unsigned long delayTime)
{
	unsigned long startTime = millis();
	// 	if(DebugPrint)
	// 	{
	// 		Serial.print("+,Clearing Serial USB Buffer for Delay;"); Serial.println(delayTime);
	// 	}
	
	while(millis() - startTime < delayTime)
	{
		if(Serial.available())
		byte  w = Serial.read();
	}
}


//Soft reset of IMU sensor
boolean YEISensor::ResetSensor()
{
	const unsigned long IMUFailtTimeout = 15000;
	unsigned long TimeOut = millis();
	boolean isResponding = false;
	
	_Serial->print(":226\n");
	
	ClearUSBForDelay(1000);
	


	//isResponding = getConnected();
	

	
	return isResponding;
	
}