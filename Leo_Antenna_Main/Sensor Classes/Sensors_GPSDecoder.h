// Sensors-GPSDecoder.h

#ifndef _SENSORS_GPSDECODER_h
#define _SENSORS_GPSDECODER_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef __TinyGPSPlus_h
#include "TinyGPS++.h"
#endif

#ifndef GPSTimeStruct
#define GPSTimeStruct
struct GPSTime{
	byte Day = 0;
	byte Month = 0;
	int Year = 0;
	byte Hr = 0;
	byte Min = 0;
	byte Sec = 0;
	boolean TimeValid = false;
};
#endif

#ifndef GPSLatLngStruct
#define GPSLatLngStruct
struct GPSLatLng
{
	double Latitude;
	double Longitude;
	
};
#endif

#ifndef OfficeGPSLat
#define OfficeGPSLat 55.953986
#define OfficeGPSLng -3.197231
#endif



// Defines for returned GPS data condition
#define GPSData_NewValid 2
#define GPSData_OutOfDate 3
#define GPSData_Manual 4
#define GPSData_ValidTimeOut 0
#define GPSData_DisconnectedError -1
#define GPSData_NoDataWaiting 1

class Sensors_GPSDecoderClass
{
	protected:
	TinyGPSPlus GPSLib;
	HardwareSerial *_SerialPort;
	
	int	_errorCounter_Timeout;
	int	_errorCounter_Disconnected;
	
	GPSTime _StoredTime;
	
	public:
	GPSLatLng _ManualLatLng;
	GPSLatLng _GPSLatLng;
	boolean _isGPSManual;
	GPSLatLng OfficeGPSLatLng = {OfficeGPSLat, OfficeGPSLng};
	unsigned long _GPSLockAge;
	unsigned long _ReadTimeout;
	boolean _debugPrintDataFlow;
	void printGPSPort();
	void init();
	void setup(HardwareSerial *SerialPort, uint32_t BaudRate);
	int GetGPSLocation(GPSLatLng* GPSLatLngVal);
	void SetGPSManual(boolean isGPSManual, GPSLatLng ManualLatLng);
	void ClearGPSManual();
	
	String _StatusMessage;
	int _Status;
	
	void GetGPSTime(GPSTime* ReturnedTime);
};

extern Sensors_GPSDecoderClass Sensors_GPSDecoder;

#endif

