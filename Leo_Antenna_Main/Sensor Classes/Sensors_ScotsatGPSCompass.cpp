//
//
//

#include "Sensors_ScotsatGPSCompass.h"


void Sensors_ScotsatGPSCompass::init()
{


}

// Main setup of serial port and initial values
void Sensors_ScotsatGPSCompass::setup(HardwareSerial *SerialPort, uint32_t BaudRate, boolean Direction)
{
	_SerialPort = SerialPort;
	
	_isGPSManual = false;
	
	_ReadTimeout = 0;

	_debugPrintDataFlow = true;
	_GPSUpdateTimeStamp = 0;

	_GPSCompassSerial.setup(SerialPort,BaudRate, CompassPacketLayout, 10);

	_GPSCompassSerial._PrintIncomming = false;
	
	_Dir = Direction;
}

//Check Port and update class values
int Sensors_ScotsatGPSCompass::updateData()
{

	
	
	//Check Port for Data
	if(_GPSCompassSerial.Read())
	{
		if(_PrintDebug_CompassUSB)
		Serial.println("+,GPS Command received");

		//Get command values
		int CommandReceived = _GPSCompassSerial.parseCommand();
		
		//Handle different command values
		switch(CommandReceived)
		{
			
			
			//***************


			//New Compass Heading
			case Command_CompassUpdate :
			_isConnected = true;
			_CompassHeading = _GPSCompassSerial.parseFloat(PacketPosition1);
			_headingSpeed = _GPSCompassSerial.parseFloat(PacketPosition3);
			
			if(_PrintDebug_CompassUSB)
			{
				Serial.print("+,43," + String(_CompassHeading) + ",");
				Serial.print(millis()); Serial.println(",");
			}
			break;

			//***************

			//New GPS Position
			case Command_ManualGPSSet :
			_isConnected = true;
			//Parse out GPS Lat Lng data from serial packet
			_GPSLatLng.Latitude = _GPSCompassSerial.parseFloat(1);
			_GPSLatLng.Longitude = _GPSCompassSerial.parseFloat(2);
			_GPSUpdateTimeStamp = millis();
			
			if(_PrintDebug_CompassUSB)
			Serial.print("+,USB Comms - New Compass GPS; Lat;" + String(_GPSLatLng.Latitude) + " Lng;"  + String(_GPSLatLng.Longitude) + "\n");

			break;

			//**************************************

			//Handshake Reply
			case Command_CompassHandshake:
			_isConnected = true;
			_isHandShake = GPSData_Active;
			_SerialPort->println(":1,1,c");//Handshake reply
 			Serial.print("+,USB Comms - Compass Handshake Received\n");

			break;
			
			//*************************************************************
			
			case Command_CoGThreshold:
			_GPSCompassSpeedStatus = _GPSCompassSerial.parseInt(1);
			
			if(_PrintDebug_CompassUSB)
			Serial.print("+,USB Comms - Compass Speed Updated\n");
			break;
			
			//**************************************

			//Unrecognised Command Error
			default:Serial.println("@,GPS Compass Comms Error. Unrecognised Command;" + String(CommandReceived));
			break;
		}

		return 1;
	}

	return 0;
}

// Prints and Clears any data sitting in the GPS port - useful for quick debug checking
void Sensors_ScotsatGPSCompass::printGPSPort()
{
	while(_SerialPort->available())
	{
		char c = _SerialPort->read();

		Serial.print(c);
	}
}

// Main Get Function.
//  Returns GPS Lat Lng struct of latest available Lat Long
//  Returns int for various return conditions- Valid New Data, Valid Old Data, Manual Data, No new Data, GPS Error Timeout
int Sensors_ScotsatGPSCompass:: GetGPSLocation(GPSLatLng* GPSLatLngVal)
{
	
	// Save age of currently stored data
	_GPSLockAge = millis()-_GPSUpdateTimeStamp;
	
	// Check for manual condition flag
	if(_isGPSManual)
	{

		GPSLatLngVal->Latitude = _ManualLatLng.Latitude;
		GPSLatLngVal->Longitude = _ManualLatLng.Longitude;
		
		_StatusMessage = "+,GPS Message - Currently In Manual\n";
		_Status = GPSData_Manual;
		return _Status;
	}else if(_isConnected)

	{
		//Check data age
		if(_GPSLockAge < 30000UL)
		{
			
			GPSLatLngVal->Latitude = _GPSLatLng.Latitude;
			GPSLatLngVal->Longitude = _GPSLatLng.Longitude;
			
			_Status = GPSData_NewValid;
			_StatusMessage = "+,GPS Message - Valid Data\n";
			return _Status;
		}
		// New GPS data is invalid - not enough satellites or out of date - return last valid and report issue
		else
		{
			GPSLatLngVal->Latitude = _GPSLatLng.Latitude;
			GPSLatLngVal->Longitude = _GPSLatLng.Longitude;
			_StatusMessage = "~,GPS Data Invalid - No Satellite Lock / Data Out of Date";
			_Status = GPSData_OutOfDate;
			return _Status;
		}
		
	}

	return _Status;

}


// Set GPS into manual. GetGPSPos returns manual position until ClearGPSManual
void Sensors_ScotsatGPSCompass::SetGPSManual(boolean isGPSManual, GPSLatLng ManualLatLng)
{
	_isGPSManual = isGPSManual;
	
	_ManualLatLng.Latitude = ManualLatLng.Latitude;
	_ManualLatLng.Longitude = ManualLatLng.Longitude;
	
}

// Resets manual GPS so that GPS Lat Lng comes from GPS Module
void Sensors_ScotsatGPSCompass::ClearGPSManual()
{
	_isGPSManual = false;
}

//Return Current Heading and Status
float Sensors_ScotsatGPSCompass::getCompassHeading()
{
	if(_Dir)
	return _CompassHeading;
	else
	return -1.0*_CompassHeading;
}

//Initialisation Handshake Method
// Handshake command request sent to gps compass. Expecting reply with-in a timeout period
// Return status depending on handshake results
int Sensors_ScotsatGPSCompass::CompassHandshake()
{
	//Handshake timeouts
	const unsigned long HandshakeTimeout = 5000UL;
	const unsigned long HandShakeRetryTimeout = 1000UL;

	//Default failed status
	_StatusMessage = "+,GPS Compass Failed Init. No GPS Data \n";

	//Timers for measuring time taken for handshake
	unsigned long StartTimer = millis();
	unsigned long HandshakeTimer = millis() + HandShakeRetryTimeout;
	
	_SerialPort->print(":1,0,c \n");
	
	//Send handshake at repeated intervals and check for data
	// Check class global value to see if data has been returned
	while(millis() - StartTimer < HandshakeTimeout)
	{
		ClearUSB();
		//Send handshake command
		if(millis() - HandshakeTimer > HandShakeRetryTimeout)
		{
			_SerialPort->print(":1,0,c \n");
			HandshakeTimer = millis();
		}

		//Check for new data
		updateData();

		
		//Handle positive handshake result
		if(_isHandShake == GPSData_Active)
		{
			_Status = GPSData_Active;
			_StatusMessage = "+, GPS Compass Passed Init. GPS Data Flowing \n";


			return _isHandShake;
		}
	}


	return _isHandShake;
}


//********
//During Init clear USB buffer to prevent lock up
void Sensors_ScotsatGPSCompass::ClearUSB()
{
	while(Serial.available() > 10)
	Serial.read();
}