// Sensors_SatSignal.h

#ifndef _SENSORS_SATSIGNAL_h
#define _SENSORS_SATSIGNAL_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class Sensors_SatSignalClass
{
 protected:


 public:
	// Main variables for running
	int _AnalogueReadPin;
	int _LockThreshold;
	int _NumberofReadAverages;
	boolean _isLock;
	int _StoredSignalVal;
	
	//******************************************************************************
	
	//Alternative variables for usb to override signal value. This is to be used with Pi reading sat signal via openamip
	float _usbDBSignal = -99.0;
	boolean _isUSBSignal = false;
	float _usbDBThresholdLock = 0.0;
	
	//******************************************************************************
	
	//debug/testing variables
	int _spoofSignalVal;
	boolean isSignalDebug = false;
	
	//******************************************************************************
	
	void setup(int AnalogueSignalPin, int LockThreshold, int NumberofAverages);
	
	void init();
	float read();
	boolean CheckForLock();
	
	//******************************************************************************
	//New DB USB Data functions for handling USB provided Signal values
	void setup(boolean isUSBMethod, float DBThreshold);
	void setDBSignal(float DBLevel);
	void setDBThreshold(float DBThreshold);
	
	//******************************************************************************
	
};

extern Sensors_SatSignalClass Sensors_SatSignal;

#endif

