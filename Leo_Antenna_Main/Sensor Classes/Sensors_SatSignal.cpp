//
//
//

#include "Sensors_SatSignal.h"

void Sensors_SatSignalClass::init()
{


}

//******************************************************************************

// Main Object setup
//  Sets initial conditions for analogue signal reading of teensy ADC
void Sensors_SatSignalClass::setup(int AnalogueSignalPin, int LockThreshold, int NumberofAverages)
{
	_AnalogueReadPin = AnalogueSignalPin;
	_LockThreshold = LockThreshold;
	_NumberofReadAverages = NumberofAverages;
	
	analogReadAveraging(NumberofAverages);
}

//******************************************************************************
//******************************************************************************

// Main Object setup
//  Sets initial conditions for signal strength provided via usb
void Sensors_SatSignalClass::setup(boolean isUSBMethod, float DBThreshold)
{
	_isUSBSignal = isUSBMethod;
	_usbDBThresholdLock = DBThreshold;
}

//******************************************************************************

// Main read function
//  Gets latest signal value and returns;
float Sensors_SatSignalClass::read()
{

	float OutputSignal = 0.0;
	
	if(_usbDBSignal) //Openamip provided value selection
	{
		OutputSignal = _usbDBSignal;
	}else
	{
	// Debug Output
	if(isSignalDebug)
	_StoredSignalVal = _spoofSignalVal;
	else
	//Teensy analog read classes includes averaging of ADC set in setup.
	_StoredSignalVal = analogRead(_AnalogueReadPin);
	
	OutputSignal = (float)_StoredSignalVal;
	}
	
	CheckForLock();
	
	return OutputSignal;
}

//******************************************************************************

//Boolean command to check for lock with signal level vs lock threshold
// Simple comparison and return of results
boolean Sensors_SatSignalClass::CheckForLock()
{
	if(_isUSBSignal)
	{
		if(_usbDBSignal >= _usbDBThresholdLock)
		_isLock = true;
		else
		_isLock = false;
		
	}else //Standard Analgue Pin Method
	{
		if(_StoredSignalVal >= _LockThreshold) //Compare Lock to Threshold value and return accordingly
		_isLock = true;
		else
		_isLock = false;
		
	}
	
	return _isLock;

}

//******************************************************************************
//USB Command function for passing new DBLevel value to class
void Sensors_SatSignalClass::setDBSignal(float DBLevel)
{
	_usbDBSignal = DBLevel;
}


//******************************************************************************
//USB Command function for passing new DBLevel value to class
void Sensors_SatSignalClass::setDBThreshold(float DBThreshold)
{
	_usbDBThresholdLock = DBThreshold;
}
Sensors_SatSignalClass Sensors_SatSignal;

