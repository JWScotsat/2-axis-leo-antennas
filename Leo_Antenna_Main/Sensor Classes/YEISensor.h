// YEISensor.h
/*
YEI Sensor Library
Created by: Jack Wells

Designed For: Handling YEI 3 Space Sensors. Inputs Serial Port and Baud Rate to then configure
the IMUs for use. Basic functionally for pulling axis data in degrees. Specifically designed for
Hydra Antenna - Clear Serial USB during initialisation to ensure no USB build up while waiting for
sensors.

Change Log: 

6/7/15 - Tested with Main Antenna
			+Initialisation and axis reading functions added

14/9/15 - Error/Status tracking on init
			+Clean up of code and better documentation

11/7/17 - Sanity check on imu values is recorded and resets on high error number 
			- Prevents IMU getting "stuck" in one location
			
02/08/17 - Added soft reset of sensor at the start of initialisation. Soft reset should prevent any software errors or poor imu performance

23/10/19 - Added change in filter mode to IMU initialisation. 
			Changed "waitForIMUData" to "checkForIMUData" - no longer blocking

*/


#ifndef _YEISENSOR_h
#define _YEISENSOR_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#include <wiring.h>
#endif

#include "HardwareSerial.h"

#ifndef PiSerial
#define PiSerial Serial
#endif

#define IMUConnected 1
#define IMUFailedInit -1
#define IMUUnresponsive -2
#define IMUInvalidData -3
#define IMUPacketTimeOut -4


class YEISensor //: public Stream
{

	//******* Public Class Data
	public:
	
	YEISensor(); // Constructor

	//~YEISensor() { end(); }
	
	void begin(HardwareSerial *serIn, uint32_t baud, String Name); // Pass Serial Port, Baud Rate and Axis/Sensor Name
	void setupConfig(float PitchTrim, float RollTrim, unsigned long UpdateRate);
	
	boolean Initialise(); // Main Initialisation Code - returns 1 for Init or 0 for failure
	
	int _status; // Main init status
	String _statusMessage = "+,Default IMU Status Message"; // Init and other messages for Serial Output/Error Output
	
	boolean getConnected(); // Returns connection status of IMU - same as Initialisation without setting up the sensor
	
	static void end(); // unused. eventually will end the sensor allocation
	// 	boolean init();
	
	float _AxisVals[3]; // Axis Values stored as Array
	
	// Sensor Yaw Pitch Roll in Degrees
	float _Yaw;
	float _Pitch;
	float _Roll;
	
	float _RollTrim = 0.0;
	float _PitchTrim = 0.0;
	
	float imuX = 0.0;
	float imuY = 0.0;
	float imuZ = 0.0;
		
	unsigned long _YEIUpdateRate; // mS time between requesting IMU data (7 default)
	
	String SensorName(); // Return sensor name
	
	void SetLEDGreen(); // Set Sensor LED to green
	

	boolean DebugPrint; // Serial Debug Output
	
	String _Name; // Sensor Name Storage
	
	boolean _enabled; // Sensor enable - can not request data when not-enabled
	boolean _ignoreSanityCheck;// Sanity check prevents IMU data from wildly changing (>10) - disabled during motor initialisation
	
	boolean _isNewData;//Marker for when new data has arrived
	
	void getEularAngles(); // Reuqest Eular Angles from IMU - updates class public variables
	
	void swSetGyroRange();
	void swCalGyros();
	void swTare();
	void AssignImuValsXYZ(float InputData[], float XYZ[]);
	boolean Euler_Read(float *XYZ);
	
	//IMU filter mode
	int _filterMode = 1; //4 is Qgrad2
	
	//******* Private Class Data
	private:
	//counting for error condition, 1 is a hiccup, 5 is an issue
	byte _ErrorCount = 0;
	// YEI Set Filter Mode
	void setFilter(int filterMode);
	
	HardwareSerial *_Serial; // Pointer for Serial Port Used

	unsigned long _UpdateTimeStamp; // Time of last update
	
	void LEDColour(float Red, float Grn, float Blu); //Changed LED Colour - used in initialisation
	boolean isFloatEqual(float f1, float f2, int NumberofDecPlaces); //Boolean if 2 floats are equal - used to compare returned values from IMU
	boolean CheckLEDColourResponse(); // Main function for checking reply to LED colour change command
	void CalibrateGyros(); // Recalibrate Gyros - takes 5 Seconds to complete
	void ClearUSBForDelay(unsigned long delayTime); // Stops USB serial port from becomming over loaded - simply clears the port
	

	
	
	
	boolean IMUSerialRead(float OutputVals[]); //Returns 3 floating point array data from IMU - Eular Read
	byte checkforImuData(); // checks without blocking for incomming data from IMU.
	void ConvertToDegrees(float InputArray[]); // Converts 3 point array of Radians to Degrees
	void IMUDataAssignValid(float TempImuVals[]); // Sanity Checks Eular Angles and assigns them to Class Values
	void AssignImuVals(float InputData[], float YawPitchRoll[]);
	boolean Eular_Read(float YawPitchRoll[]);// Main Eular Read Function - Sends Commands, Waits for response, Returns values
	boolean ReadImuData();// Main IMU data read - as part of Eular_Read
	
	boolean ResetSensor();//Reset IMU sensor
	
	int _sanityCheckError = 0; //IMU sanity check counter for managing new valid positions after big shock movement
	
	boolean _requestNewData = true; // used to limit request rate to IMU
	boolean _newCheck = true;
	unsigned long _timeoutTimer;
	unsigned long _resendTimer;
};

#endif

//***************************************************************************************************************//***************************************************************************************************************
//***************************************************************************************************************//***************************************************************************************************************
