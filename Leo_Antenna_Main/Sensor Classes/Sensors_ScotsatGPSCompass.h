// Sensors_ScotsatGPSCompass.h

#ifndef _SENSORS_SCOTSATGPSCOMPASS_h
#define _SENSORS_SCOTSATGPSCOMPASS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#ifndef GPSLatLngStruct
#define GPSLatLngStruct
struct GPSLatLng
{
	double Latitude = -999.0;
	double Longitude = -999.0;
	
};

// Defines for returned GPS data condition
#define GPSData_NewValid 2
#define GPSData_OutOfDate 3
#define GPSData_Manual 4
#define GPSData_ValidTimeOut 0
#define GPSData_DisconnectedError -1
#define GPSData_NoDataWaiting 1
#define GPSData_Active 1
#endif

#ifndef OfficeGPSLat
#define OfficeGPSLat 55.953986
#define OfficeGPSLng -3.197231
#define Command_CompassUpdate 43
#define Command_ManualGPSSet 45
#endif

#define Command_CompassHandshake 1
#define Command_CoGThreshold 2

#include "Supporting Libs/Comms_SerialExtended.h"

class Sensors_ScotsatGPSCompass
{
	protected:
	//Serial Settings
	Comms_SerialExtendedClass _GPSCompassSerial;
	HardwareSerial *_SerialPort;
	PacketLayout CompassPacketLayout = {':', '\n', ','};
		
	//New course over ground flag for when CoG is higher than speed threshold
	boolean _GPSCompassSpeedStatus = false;

	//Time Stamping for Data last received
	unsigned long _GPSUpdateTimeStamp;
	unsigned long _CompassUpdateTimeStamp;

	//Manual GPS Data
	boolean _isGPSManual;
	GPSLatLng _ManualLatLng;

	//Serial Data Status
	unsigned long _ReadTimeout;

	boolean _PrintDebug_CompassUSB = false;

	//Stored Heading
	float _CompassHeading = 0.00;
	boolean _Dir = 1;

	void init();

	// Check for Compass Connection/Init
	boolean _isConnected = false;
	int _isHandShake = GPSData_DisconnectedError;
	
	void ClearUSB();

	public:

	void setup(HardwareSerial *SerialPort, uint32_t BaudRate, boolean Direction);

	//Check Port and update class values
	int updateData();

	boolean _debugPrintDataFlow;
	void printGPSPort();

	//Compass Heading Functions
	float getCompassHeading();

	float _headingSpeed = 0;


	//Stored Last GPS Data Out
	GPSLatLng _GPSLatLng;

	//Time value for how old GPS Data is
	unsigned long _GPSLockAge;

	//Initialisation Check/Handshake method
	int CompassHandshake();
	
	//GPS Functions
	int GetGPSLocation(GPSLatLng* GPSLatLngVal);
	void SetGPSManual(boolean isGPSManual, GPSLatLng ManualLatLng);
	void ClearGPSManual();
	
	//Status State/Messages
	String _StatusMessage;
	int _Status = GPSData_DisconnectedError;

	boolean GetSpeedStatus(){ return _GPSCompassSpeedStatus;};
	
	
};

#endif

