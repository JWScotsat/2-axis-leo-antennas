//
//
//

#include "Sensors_GPSDecoder.h"

void Sensors_GPSDecoderClass::init()
{


}

// Main setup of serial port and initial values
void Sensors_GPSDecoderClass::setup(HardwareSerial *SerialPort, uint32_t BaudRate)
{
	_SerialPort = SerialPort;
	_SerialPort->begin(BaudRate);
	
	_isGPSManual = false;
	_GPSLatLng = OfficeGPSLatLng;
	
	_ReadTimeout = 0;
	_debugPrintDataFlow = false;
	_errorCounter_Timeout = 0;
	_errorCounter_Disconnected = 650;
	
}

// Prints and Clears any data sitting in the GPS port - useful for quick debug checking
void Sensors_GPSDecoderClass::printGPSPort()
{
	while(_SerialPort->available())
	{
		char c = _SerialPort->read();

		Serial.print(c);
	}
}

// Main Get Function.
//  Returns GPS Lat Lng struct of latest available Lat Long
//  Returns int for various return conditions- Valid New Data, Valid Old Data, Manual Data, No new Data, GPS Error Timeout
int Sensors_GPSDecoderClass:: GetGPSLocation(GPSLatLng* GPSLatLngVal)
{
	boolean newData = false;
	boolean isGPSError = true;
	_Status = GPSData_NoDataWaiting;
	
	// Save age of currently stored data
	_GPSLockAge = GPSLib.location.age();
	

	// Read latest GPS info in buffer. Return if no GPS data - no point in waiting
	if(_SerialPort->available())
	{
		_errorCounter_Disconnected = 0;
		// Non-manual GPS - read GPS device
		
		// For timeout we parse GPS data and report some key values
		for (unsigned long start = millis(); millis() - start <= _ReadTimeout;)
		{
			while (_SerialPort->available())
			{
				char c = _SerialPort->read();
				if(_debugPrintDataFlow)
				Serial.write(c); // Debug print of GPS data as seen
				
				if (GPSLib.encode(c)) // Did a new valid sentence come in?
				newData = true;
				
				// Flag GPS has reported data over serial - therefor GPS is connected
				isGPSError = false;
			}
			
			// If new data has been encoded, parse and handle
			if(newData)
			{
				// Reset error counters
				_errorCounter_Timeout = 0;
				_errorCounter_Disconnected = 0;
				
				// New Location save values for easy access, return new values
				if(GPSLib.location.isValid())
				{
					_GPSLatLng.Latitude = GPSLib.location.lat();
					_GPSLatLng.Longitude = GPSLib.location.lng();
					
					GPSLatLngVal->Latitude = _GPSLatLng.Latitude;
					GPSLatLngVal->Longitude = _GPSLatLng.Longitude;
					
					_Status = GPSData_NewValid;
					_StatusMessage = "+,GPS Message - New Valid Data\n";
					break;
				}
				// New GPS data is invalid - not enough satellites or out of date - return last valid and report issue
				else
				{
					GPSLatLngVal->Latitude = _GPSLatLng.Latitude;
					GPSLatLngVal->Longitude = _GPSLatLng.Longitude;
					_StatusMessage = "~,GPS Data Invalid - No Satellite Lock / Data Out of Date";
					_Status = GPSData_OutOfDate;
					break;
				}
			}
			
			
		}
		
		// Timeout condition.. No data has come through. Return error conditions and last valid data
		GPSLatLngVal->Latitude = _GPSLatLng.Latitude;
		GPSLatLngVal->Longitude = _GPSLatLng.Longitude;
		
		_errorCounter_Timeout++;
		
		if(_errorCounter_Timeout > 100)
		Serial.print("~,GPS Data Timeout Condition - using last valid Data\n");
		
		_Status = GPSData_ValidTimeOut;
		_StatusMessage = "~,GPS Data Timeout Condition - using last valid Data\n";
		
		
	}else
	{
		_errorCounter_Disconnected++;
		
		if(_errorCounter_Disconnected > 700)
		{
			
			_StatusMessage = "@,GPS Error - No Data detected. GPS Disconnected\n";
			_Status = GPSData_DisconnectedError;
		}
		
	}


	// Check for manual condition flag
	if(_isGPSManual)
	{
		_GPSLatLng = _ManualLatLng;

		GPSLatLngVal->Latitude = _GPSLatLng.Latitude;
		GPSLatLngVal->Longitude = _GPSLatLng.Longitude;
		
		_StatusMessage = "+,GPS Message - Currently In Manual\n";
		_Status = GPSData_Manual;
		return _Status;
	}

	if(_Status == GPSData_NoDataWaiting)
		_StatusMessage = "+,GPS Message - No Data Waiting\n";
	
	return _Status;

}

// Store ad return most valid time values
void Sensors_GPSDecoderClass:: GetGPSTime(GPSTime* ReturnedTime)
{
	_StoredTime.TimeValid = false;
	
	if((GPSLib.time.age() < 2000 && GPSLib.location.age() < 2000) && GPSLib.time.isValid())
	{
		_StoredTime.TimeValid = true;
		
		_StoredTime.Day = GPSLib.date.day();
		_StoredTime.Month = GPSLib.date.month();
		_StoredTime.Year = GPSLib.date.year();
		
		_StoredTime.Hr = GPSLib.time.hour();
		_StoredTime.Min = GPSLib.time.minute();
		_StoredTime.Sec = GPSLib.time.second();
		
	}
	
	ReturnedTime->Day =_StoredTime.Day;
	ReturnedTime->Month = _StoredTime.Month;
	ReturnedTime->Year = _StoredTime.Year;
	
	ReturnedTime->Hr = _StoredTime.Hr;
	ReturnedTime->Min = _StoredTime.Min;
	ReturnedTime->Sec = _StoredTime.Sec;
	ReturnedTime->TimeValid = _StoredTime.TimeValid;
}

// Set GPS into manual. GetGPSPos returns manual position until ClearGPSManual
void Sensors_GPSDecoderClass::SetGPSManual(boolean isGPSManual, GPSLatLng ManualLatLng)
{
	_isGPSManual = isGPSManual;
	
	_ManualLatLng.Latitude = ManualLatLng.Latitude;
	_ManualLatLng.Longitude = ManualLatLng.Longitude;
	
}

// Resets manual GPS so that GPS Lat Lng comes from GPS Module
void Sensors_GPSDecoderClass::ClearGPSManual()
{
	_isGPSManual = false;
}

Sensors_GPSDecoderClass Sensors_GPSDecoder;

