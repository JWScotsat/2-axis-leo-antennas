// YEISensor.h
/*
YEI Sensor Library
Created by: Jack Wells

Designed For: Handling YEI 3 Space Sensors. Inputs Serial Port and Baud Rate to then configure
the IMUs for use. Basic functionally for pulling axis data in degrees. Specifically designed for
Hydra Antenna - Clear Serial USB during initialisation to ensure no USB build up while waiting for
sensors.

Change Log:

6/7/15 - Tested with Main Antenna
+Initialisation and axis reading functions added


*/


#include "YEISensor.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


// Main Constructor
YEISensor::YEISensor()
{
	_Serial = NULL;
	DebugPrint = true;
	_Name = "YEISensor";
	_status = 0;
	_statusMessage = "";
}


//*****************************************************************************************************
//Sets up main variables for Class
void YEISensor::begin(HardwareSerial *serIn,uint32_t baud,String Name)
{
	_Serial = serIn;
	_Serial->begin(baud);
	_Name = Name;
	_enabled = true;
	_ignoreSanityCheck = true;
}

//Setup other Config Values
void YEISensor::setupConfig(float PitchTrim, float RollTrim, unsigned long UpdateRate)
{
	_PitchTrim = PitchTrim;
	_RollTrim = RollTrim;
	_YEIUpdateRate = UpdateRate;
}

//*****************************************************************************************************
//Returns Sensor Name - similar to JAVA use for classes - possibly not required
String YEISensor::SensorName()
{
	return _Name;
}

//*****************************************************************************************************
// Main Initialisation of Sensor
// Sends a colour change command for the LED. Requests values of LED stored and compares with sent
//  -This ensures the IMU is correctly connected and responding to commands
//   Times out if there's no response or incorrect data
boolean YEISensor::Initialise(){
	
	const unsigned long IMUFailtTimeout = 15000;
	unsigned long TimeOut = millis();
	boolean isResponding = false;
	
	_ignoreSanityCheck = true;
	_UpdateTimeStamp = millis();
	
	if(DebugPrint)
	Serial.print("+,Initialising " + _Name  + " Imu\n");
	
	if(DebugPrint)
	Serial.print("+,Checking IMU Active. Requesting Handshake\n");
	
	// IMU Colour change Handshake - timeout after incorrect response
	while (isResponding == false && millis() - TimeOut < IMUFailtTimeout )
	{

		isResponding = CheckLEDColourResponse();
		
		if(isResponding)
		break;
		
	}
	
	//if(isResponding)
	//Soft Reset sensor on init
	//isResponding = ResetSensor();
	
	ResetSensor();
	
	//Correct response received. Calibrate Sensor
	if(isResponding)
	{
		
		_enabled = true;
		
		setFilter(_filterMode);
		
		//TODO: check gyro calibrate
		// CalibrateGyros(); // no need to wait 5 seconds?
		swCalGyros();
		//swTare(); // see if its ok to tare or not? at least it might fix FWD & Left, if not roll

		//setFilter(_filterMode);  // move this before calibration
		
		
		
		if(DebugPrint)
		PiSerial.print("+," + _Name + " Default Config Complete\n");
		
		SetLEDGreen();
		_statusMessage = "+," + _Name + " Init Complete. Connected\n";
		_status = IMUConnected;
		
		
	}
	
	//No Response/Incorrect flag error
	if(!isResponding)
	{
		_enabled = false;
		
		_statusMessage = "@," + _Name + " Init Error! Not responding\n";
		_status = IMUFailedInit;
		
		
		LEDColour(1.0, 0.0, 0.0);
	}
	
	
	
	
	// 	IMU_Config_AccTrustVals(ImuSerial,ImuSettings.AccConfidenceLow,ImuSettings.AccConfidenceHigh);
	//
	// 	IMU_Config_FilterMode(ImuSerial,ImuSettings.FilterMode);
	//
	// 	IMU_Config_GyroRange(ImuSerial, ImuSettings.GyroRange);
	//
	// 	IMU_Config_ReferenceVectorMode(ImuSerial,ImuSettings.ReferenceVectorMode);
	//
	// 	IMU_Config_CompassActive(ImuSerial,ImuSettings.CompassActive);
	//
	// 	IMU_Config_SetRunningAvg(ImuSerial,ImuSettings.RunningAvg);
	//
	// 	IMU_Config_OverSampleRate(ImuSerial,ImuSettings.OverSampleRate);
	
	return isResponding;
}


//*****************************************************************************************************
// Main function for retrieving IMU Data
//  Updates main class variables with received data from IMU
//   Limits the request amount to ensure we're not requesting data before IMU is ready (typically 7 ms)
//   Prevents updating Class values if IMU is disabled
void YEISensor::getEularAngles()
{
	if(_enabled)
	{
		
		if(millis() - _UpdateTimeStamp > _YEIUpdateRate)
		{
			_isNewData = ReadImuData();
			_UpdateTimeStamp = millis();
			
			// 			Serial.print("New Angles;");
			// 			Serial.print("\tYaw;"); Serial.print(_Yaw);
			// 			Serial.print("\tPitch;"); Serial.print(_Pitch);
			// 			Serial.print("\tRoll;"); Serial.print(_Roll);
			
		}else
		_isNewData = false;
		
	}
	
	
}


void YEISensor::swSetGyroRange()
{
	_Serial->print(":125,0\n");  // Set range to 250deg/s
	ClearUSBForDelay(100);
}

void YEISensor::swCalGyros()
{
	// Begin Gyro Auto Cal
	_Serial->print(":165\n");
	ClearUSBForDelay(100);
}

void YEISensor::swTare()
{
	// Tare
	_Serial->print(":96\n");
	ClearUSBForDelay(100);
}



//***********************************************************************//***************************************************************************************************************
//**********************************************************************//***************************************************************************************************************