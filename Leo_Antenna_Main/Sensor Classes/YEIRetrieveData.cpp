#include "YEISensor.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


//************************************************

boolean YEISensor::ReadImuData()
{
	boolean isImuDataValid = false;
	
	float TempImuVals[3] = {0.0,0.0,0.0};
	
	if(_enabled)
	isImuDataValid = Eular_Read(TempImuVals);
	//isImuDataValid = Euler_Read(TempImuVals);  // SIW changed this to be XYZ values

	if(isImuDataValid)
	IMUDataAssignValid(TempImuVals);
	
	return isImuDataValid;
	

}

//**********************************************************************

boolean YEISensor::Eular_Read(float *YawPitchRoll)
{

	boolean newdat = false;
	//static boolean requestNewData = true; // remove static function variable
	float NewDataVals[3] = {0.0,0.0,0.0};
	
	//Only request new data once per cycle, prevents flooding of IMU serial port
	if(_requestNewData)
	{
		//Have to send packet twice to ensure IMU is constantly reading next packet to get data as soon as possible.
		_Serial->print(":7\n"); // SIW Changed to tared orientation - then back to untared (7)
		//_Serial->print(":1\n");
		_requestNewData = false;
	}
	
	byte DataWaiting = checkforImuData(); //Check for response
	
	if(DataWaiting == true)
	{
		newdat = IMUSerialRead(NewDataVals); //Read response
		_requestNewData = true;
	}

	if (newdat == true)
	{
		// Save the raw reading - note the order is ZYX
		imuZ = NewDataVals[0];
		imuY = NewDataVals[0];
		imuX = NewDataVals[0];
		// Then do the usual processing
		AssignImuVals(NewDataVals,YawPitchRoll); //Parse response
		ConvertToDegrees(YawPitchRoll);
	}
	
	
	return newdat;
	
}



//**********************************************************************

void YEISensor::AssignImuVals(float InputData[], float YawPitchRoll[])
{
	float YawVal = 0.0;
	float PitchVal = 0.0;
	float RollVal = 0.0;
	
	//Normal Setup
	RollVal = InputData[2];
	YawVal = InputData[1];
	YawVal = -1*YawVal;
	PitchVal = InputData[0];


	YawPitchRoll[0] = YawVal;
	YawPitchRoll[1] = PitchVal;
	YawPitchRoll[2]  = RollVal;
}

//********************************************************************
//**********************************************************************


void YEISensor::IMUDataAssignValid(float TempImuVals[])
{
	const double ImuSanityCheck = 10.000; // Value to ensure the IMU data is valid and not a wild changed value
	
	//Sanity check error counter. If the IMU keeps showing a larger error than expected it may have been shocked before it's new position. This allows the IMU to continue with it's new position
	//without being fooled by a packet/data/short shock situation
	if(_sanityCheckError > 10)
	{
		_ignoreSanityCheck = true;
		Serial.print("@ " + _Name + "IMU Sanity Check Error. IMU Values Stuck at;");
		Serial.print(_AxisVals[0]); Serial.print(",");
		Serial.print(_AxisVals[1]); Serial.print(",");
		Serial.print(_AxisVals[2]); Serial.print(",");
		Serial.print(", Resetting sanity check for new values;");
		Serial.print(TempImuVals[0]); Serial.print(",");
		Serial.print(TempImuVals[1]); Serial.print(",");
		Serial.print(TempImuVals[2]); Serial.print(",");
		Serial.println("");
		
		_sanityCheckError = 0;
	}
	
	for (int i = 0; i < 3; i++)
	{
		if(abs(_AxisVals[i] - TempImuVals[i]) < ImuSanityCheck || _ignoreSanityCheck)
		_AxisVals[i] = TempImuVals[i];
		else
		_sanityCheckError++;// Count the sanity check incase the values get stuck in a new valid position away from the previous
	}
	
	
	_Yaw = _AxisVals[0];
	_Pitch = _AxisVals[1] + _PitchTrim;
	_Roll = _AxisVals[2] + _RollTrim;

}

//***************************************************************************************************************
// Convert IMU array to degrees
void YEISensor::ConvertToDegrees(float InputArray[])
{
	
	for (int i = 0; i < 3; i++)
	{
		InputArray[i] = InputArray[i] *57.296;
	}
	return;
}

//Code Returns when expected IMU data has been buffered,
//  -Timeout conditions resend command incase of missed command data
//  - Failure condition is flagged when no data received
byte YEISensor::checkforImuData()
{
	const unsigned long IMUDataTimeOut = 10;
	static boolean newCheck = true;
	static unsigned long timeoutTimer = millis();
	static unsigned long resendTimer = millis();
	// SIW Got rid of these static function variables!!
	
	//Reset values on new check
	if(_newCheck)
	{
		_timeoutTimer = millis();
		_resendTimer = millis();
		_newCheck = false;
	}
	
	//check for IMU data
	if(_Serial->available() >= 24)
	{
		_ErrorCount = 0;
		_newCheck = true;
		return true;
	}
	else if (millis() - _resendTimer > 7)
	{
		_Serial->print(":1\n");  // SIW changed to Tared output - as spurious (untared?) data appearing in my tests!
		_resendTimer = millis();
	}
	
	
	//IMU disconnect timer
	if (millis() - _timeoutTimer > IMUDataTimeOut)
	{
		_ErrorCount++;
		
		if(_ErrorCount > 5)
		{
			Serial.print("~," + _Name +" Timeout. No Response from IMU for data request\n");
			
			_status = IMUUnresponsive;
			_statusMessage = "@," + _Name + " Timeout. No Response from IMU for data request\n";
		}
		return -1;
	}
	
	return false;
	
}
//**********************************************************************//**********************************************************************

boolean YEISensor::IMUSerialRead(float OutputVals[])
{
	const unsigned long IMUTimeout = 7;
	//const unsigned long IMUResend = 20;
	boolean newdat = false;
	char inData[80];
	char* ImuData;
	char delimiters[] = ",";
	byte index = 0;
	byte validCharCount = 0;
	
	//Serial.print("ImuDat;");
	unsigned long timeoutTimer = millis();
	
	while (millis() - timeoutTimer < IMUTimeout)
	{
		if(_Serial->available() > 0)
		{
			if (index <= 80)
			{
				char aChar = _Serial->read();
				if(aChar == '\n')
				{
					// End of record detected. Time to parse
					if(validCharCount == 5)
					{
						newdat = true;
						
						ImuData = strtok(inData, delimiters);
						
						for (byte l = 0; l < 3; l++)
						{
							OutputVals[l] = atof(ImuData);
							ImuData = strtok(NULL, delimiters);
						}
						
						//Error monitoring Status message
						_status = 1;
						_statusMessage = "+," + _Name + " Packet Received Complete. Status Green\n";
						
						
						return newdat;
						
					}
					else
					{
						index = 0;
						inData[index] = '\0';
						validCharCount = 0;
						//delayMicroseconds(100);
					}
					
				}
				else
				{
					//Checks for "." or "," characters. IMU always supplies packet format ASCII - 0.00,0.00,0.00\n - able to count extra characters
					if(aChar == 46 || aChar == 44)
					validCharCount++;
					
					inData[index] = aChar;
					index++;
					inData[index] = '\0'; // Keep the string NULL terminated

				}
			}
			else
			{
				//Debug Error State
				if(DebugPrint)
				{
					Serial.print("~," + _Name + " IMU Serial Instruction too long. Index over flow. Invalid Input\n");
				}
				
				//Error monitoring Status message
				_status = IMUInvalidData;
				_statusMessage = "@," + _Name + " IMU Serial Instruction too long. Index over flow. Invalid Input\n";
				
				return newdat;
			}
			
			//delayMicroseconds(5);
			timeoutTimer = millis();
		}
		
	}
	
	//Debug Error State
	if(DebugPrint)
	{
		Serial.print("~," + _Name + " Packet Timeout. Received Packet Incomplete\n");
		Serial.print("+,Packet Received;");
		for (int i = 0; i < index && i < 80; i++)
		Serial.print(inData[i]);
		Serial.println("");
	}
	
	//Error monitoring Status message
	_status = IMUPacketTimeOut;
	_statusMessage = "@," + _Name + " Packet Timeout. Received Packet Incomplete\n";




	return newdat;
}





//**********************************************************************

boolean YEISensor::Euler_Read(float *XYZ)
{

	boolean newdat = false;
	//static boolean requestNewData = true;  // Static variable in an instantiable class
	float NewDataVals[3] = {0.0,0.0,0.0};
	
	//Only request new data once per cycle, prevents flooding of IMU serial port
	if(_requestNewData)
	{
		//Have to send packet twice to ensure IMU is constantly reading next packet to get data as soon as possible.
		_Serial->print(":1\n"); // SIW CHanged to tared orientation
		//_Serial->print(":1\n");
		_requestNewData = false;
	}
	
	byte DataWaiting = checkforImuData(); //Check for response
	
	if(DataWaiting == true)
	{
		newdat = IMUSerialRead(NewDataVals); //Read response
		_requestNewData = true;
	}

	if (newdat == true)
	{
		// Save the raw reading - note the order is ZYX
		imuZ = NewDataVals[0];
		imuY = NewDataVals[0];
		imuX = NewDataVals[0];
		AssignImuValsXYZ(NewDataVals,XYZ); //Parse response
		ConvertToDegrees(XYZ);
	}
	
	
	return newdat;
	
}

//**********************************************************************

void YEISensor::AssignImuValsXYZ(float InputData[], float XYZ[])
{
	XYZ[0] = InputData[0];
	XYZ[1] = InputData[1];
	XYZ[2] = InputData[2];
}