/* 
	Editor: https://www.visualmicro.com/
			visual micro and the arduino ide ignore this code during compilation. this code is automatically maintained by visualmicro, manual changes to this file will be overwritten
			the contents of the _vm sub folder can be deleted prior to publishing a project
			all non-arduino files created by visual micro and all visual studio project or solution files can be freely deleted and are not required to compile a sketch (do not delete your own code!).
			note: debugger breakpoints are stored in '.sln' or '.asln' files, knowledge of last uploaded breakpoints is stored in the upload.vmps.xml file. Both files are required to continue a previous debug session without needing to compile and upload again
	
	Hardware: Teensy 3.2 / 3.1, Platform=teensy3, Package=teensy
*/

#define __HARDWARE_MK20dx256__
#define __HARDWARE_MK20DX256__
#define ARDUINO 10809
#define ARDUINO_MAIN
#define printf iprintf
#define __TEENSY3__
#define F_CPU 96000000
#define USB_SERIAL_HID
#define LAYOUT_US_ENGLISH
#define __MK20DX256__
#define TEENSYDUINO 146
#define ARDUINO 10809
//
//
void Setup_TimerInterrupt();
void RunMotors();
void BenchTest_SingleIMUReadRate();
void Array_Print(float InputArray[]);
void Array_SetEquals(float InputArray[], float *OutputArray, int ArraySize);
void Array_Randomise(float *ArrayRandomised);

#include "arduino.h"
#include "Leo_Antenna_Main.ino"
#include "BenchTesting.ino"
#include "Unit Testing INOs\UT-AntennaCalculations-TargetAdjustment_Tests.ino"
#include "Unit Testing INOs\UT-AntennaManager-AntennaModeControl_Tests.ino"
#include "Unit Testing INOs\UT-AxisControl-FindYaw_Tests.ino"
#include "Unit Testing INOs\UT-ControlPattern-Peaking_AltCircleGradient_Tests.ino"
#include "Unit Testing INOs\UT-ControlPattern-Peaking_AltDyDelta_Tests.ino"
#include "Unit Testing INOs\UT-ControlPattern-Peaking_AltStepTracking_Tests.ino"
#include "Unit Testing INOs\UT-ControlPattern-Peaking_Tests.ino"
#include "Unit Testing INOs\UT-ControlPattern_Search_Tests.ino"
#include "Unit Testing INOs\UT-Sensors_SatSignal_Tests.ino"
#include "Unit Testing INOs\UT_AntennaMotors_DCPol_Tests.ino"
#include "Unit Testing INOs\UT_CP_SatPoint_Tests.ino"
#include "Unit Testing INOs\UT_Storage_EEPROMConfig_Tests.ino"
