// ControlPattern_Peaking.h
/*
Control Pattern Peaking Code

Class is designed to output positional targets for Az and El for the antenna. These positions are
based on a square pattern ABCD. Modem signal readings are taken at each location and then used to
calculate a new center location that the rest of the positions are based on.

Created by Jack Wells

Change Log: 25/8/15
-Class created and unit tested as part of project UT-ControlPattern-Peaking_Main


*/



#ifndef _CONTROLPATTERN_PEAKINGAltCircleGradient_h
#define _CONTROLPATTERN_PEAKINGAltCircleGradient_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Supporting Libs/StandardFunctions_Array.h"


#ifndef Az
#define Az 0
#define El 1
#define Pol 2
#endif

#define A1 0
#define B1 1
#define C1 2
#define D1 3
#define D2 7
#define C2 6
#define B2 5
#define A2 4


#define NONE -1
#define SignalLost -2

class ControlPattern_PeakingAltCircleGradientClass
{
	protected:
	float _CentreSkyPosition[3];
	StandardFunctions_Array StndFncts;
	

	boolean _isFirstPass = true;
	
	boolean CheckisSignalLost();
	void HandleEndOfPattern();
	void CalculateNewCentreSkyTarget();
	void HandleSignalStrengthSave(float CurrentModemSignal);
	void calculateNextSkyTarget(float* NewSkyTarget);
	int _signalLostcounter;
	
	
	void HandleDBLossValues(); // Checks saved signal values and changes -100db values to better match the rest
	
	
	public:
	unsigned long _LastUpdateTime = 0;
	unsigned long _PointDelay = 250;
	
	float _KAz;
	float _KEl;
	float _CircRadi = 0.5;
	
	void SetCentreSkyPosition(float InputPosition[]);
	void GetCentreSkyPosition(float* ReturnedPosition);
	void SetPeakingSizes(float AzStepSize, float ElStepSize, float KAz, float KEl);
	void GetNewSkyTarget(float CurrentModemSignal, float* NewSkyTarget);
	float _signalLostThreshold = -4.0;
	int _CurrentPatternTargetPositionCounter;
	float _SavedPeakingSignalsAtPoint[8];
	boolean _isSignalLost;
	void Setup();
	void Setup(float AzStepSize, float ElStepSize, float KAz, float KEl, float signalLostThreshold);
	void init();
};

#endif

