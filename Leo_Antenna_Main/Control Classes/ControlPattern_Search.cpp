//
//
//

#include "ControlPattern_Search.h"



ControlPattern_SearchClass::ControlPattern_SearchClass()
{
}

void ControlPattern_SearchClass::init()
{


}

//Setup for Main Class definition/default settings
void ControlPattern_SearchClass::setup()
{
	_isSearchComplete = false;
	
	_FinishedPositionCounter = 0;
	
	SearchAzimuth._stepCountSize = 10.0;
	SearchAzimuth._maxCounterBoundary = 40.0;
	SearchAzimuth._atBoundary = false;
	SearchAzimuth._isReversed = false;
	SearchAzimuth._axisMax = 360.0;
	SearchAzimuth._axisMin = -0.0;
	SearchAzimuth._AxisBoundaryRollOver = false;
	
	SearchElevation._stepCountSize = 10.0;
	SearchElevation._maxCounterBoundary = 10.0;
	SearchElevation._atBoundary = false;
	SearchElevation._isReversed = false;
	SearchElevation._axisMax = 85.0;
	SearchElevation._axisMin = -10.0;
	SearchElevation._AxisBoundaryRollOver = false;
	
	_ResetBoundaryBufferEl = 2.0;
	_ResetBoundaryBufferAz = 2.0;
	_ElExpandingBoundaryFactor = 0.0;
	_AbsMaxPatternElevationBoundary = 40.0;
	
	_isError = 0;
	
	for (int i = 0; i < 3; i++)
	_NewTarget[i] = 0.0;
}


//Setup for Main Class definition/default settings
void ControlPattern_SearchClass::setupDefaultAntennaSearch()
{
	_isSearchComplete = false;
	
	_FinishedPositionCounter = 0;
	

	SearchAzimuth._stepCountSize = 10.0;
	SearchAzimuth._maxCounterBoundary = 20.0;
	SearchAzimuth._atBoundary = false;
	SearchAzimuth._isReversed = false;
	SearchAzimuth._axisMax = 360.0;
	SearchAzimuth._axisMin = -0.0;
	SearchAzimuth._AxisBoundaryRollOver = true;
	
	SearchElevation._stepCountSize = 10.0; // 10.0
	SearchElevation._maxCounterBoundary = 0.0;
	SearchElevation._atBoundary = false;
	SearchElevation._isReversed = false;
	SearchElevation._axisMax = 85.0;
	SearchElevation._axisMin = -10.0;
	SearchElevation._AxisBoundaryRollOver = false;
	
	_ResetBoundaryBufferEl = 2.0;
	_ResetBoundaryBufferAz = 2.0;
	_ElExpandingBoundaryFactor = 0.50;
	_AbsMaxPatternElevationBoundary = 10.0;
	
	_isError = 0;
	
	for (int i = 0; i < 3; i++)
	_NewTarget[i] = 0.0;
}


// Setup with Config Data
void ControlPattern_SearchClass::setupConfig(float ElDeltaSize, float ElMaxSweep, float ElStepSize, float ElMaxBound, float ElMinBound, float ElBoundRollOver,
float AzStepSize, float AzMaxSweep, float AzMaxBound, float AzMinBound, float AzBoundRollOver, unsigned long PointDelayTime)
{
	_isSearchComplete = false;
	
	_FinishedPositionCounter = 0;
	

	SearchAzimuth._stepCountSize = AzStepSize;
	SearchAzimuth._maxCounterBoundary = AzMaxSweep;
	SearchAzimuth._atBoundary = false;
	SearchAzimuth._isReversed = false;
	SearchAzimuth._axisMax = AzMaxBound;
	SearchAzimuth._axisMin = AzMinBound;
	SearchAzimuth._AxisBoundaryRollOver = AzBoundRollOver;
	
	SearchElevation._stepCountSize = ElStepSize; // 10.0
	SearchElevation._maxCounterBoundary = 0.0;
	SearchElevation._atBoundary = false;
	SearchElevation._isReversed = false;
	SearchElevation._axisMax = ElMaxBound;
	SearchElevation._axisMin = ElMinBound;
	SearchElevation._AxisBoundaryRollOver = ElBoundRollOver;
	
	_ResetBoundaryBufferEl = 2.0;
	_ResetBoundaryBufferAz = 2.0;
	_ElExpandingBoundaryFactor = ElDeltaSize;
	_AbsMaxPatternElevationBoundary = ElMaxSweep;
	
	_isError = 0;
	
	for (int i = 0; i < 3; i++)
	_NewTarget[i] = 0.0;
	
	_PointDelay = PointDelayTime;
}

// Main Algorithim
// Takes in Sat Point Array and Current Postion in order to calculate and update New Target Az El Pol
boolean ControlPattern_SearchClass::NextPosition(float SatTarget[], float CurrentPos[], float *ReturnedTargetPos)
{
	_StatusMessage = "";
	_isError = 0;
	
	HandleSatTargetSearchBounds(SatTarget);
	

	//Clear Search Complete Flag after last call of function
	// Set new target to centre of Sat Pointed - Allowing for restart of search
	if(_isSearchComplete)
	{
		ResetSearchComplete(SatTarget,CurrentPos,ReturnedTargetPos);
		return _isSearchComplete;
	}
	


	// Search Algorithim Class Provides Next Azimuth Position based on starting and current positions
	_NewTarget[Az] = SearchAzimuth.NextAxisPosition(SatTarget[Az], CurrentPos[Az]);
	
	
	//Only move Elevation when Azimuth is in a corner position
	if(SearchAzimuth._atBoundary)
	// Search Algorithm Class Provides Next Elevation Position
	_NewTarget[El] = SearchElevation.NextAxisPosition(SatTarget[El], CurrentPos[El]);
	
	// Corners of the Search Spiral - Changes direction of the Search Axis depending on current directions.
	HandleAtSearchCorners(SatTarget);
	
	//Handle Out of Search Array Boundary
	HandleNewOutofSearchBounds(SatTarget);
	
	// Handle Current Position Out of boundary case
	//HandleCurrentPosOutOfBounds(SatTarget, CurrentPos, ReturnedTargetPos);
	
	//Set Returned Target array to class calculated New Target array
	Array_SetEquals(_NewTarget,ReturnedTargetPos,3);
	
	_LastUpdateTime = millis();
	
	return _isSearchComplete;
}

//*****************************************************************************************************
//*****************************************************************************************************

// Reset Values for Completed Search
void ControlPattern_SearchClass::ResetSearchComplete(float SatTarget[], float CurrentPos[], float *ReturnedTargetPos)
{
	// Initial Search Conditions after complete search
	_isSearchComplete = false;
	SearchAzimuth._atBoundary = false;
	SearchAzimuth._isReversed = false;
	SearchElevation._atBoundary = false;
	SearchElevation._isReversed = false;
	SearchElevation._maxCounterBoundary = 0.0;
	_FinishedPositionCounter = 0;
	
	// Set New Target to Centre of Search
	Array_SetEquals(SatTarget,_NewTarget,3);
	
	//Set Returned Target array to New Target array
	Array_SetEquals(_NewTarget,ReturnedTargetPos,3);
}

//*****************************************************************************************************
//*****************************************************************************************************

// Check Sat Target isn't out of physical bounds
void ControlPattern_SearchClass::HandleSatTargetSearchBounds(float SatTarget[])
{
	// Compare each Axis to ensure they are within the Search Boundaries for the pattern. Otherwise set them at the start
	// and call error condition
	if(SatTarget[El] > SearchElevation._axisMax || SatTarget[El] < SearchElevation._axisMin)
	{
		_StatusMessage += "~,Sat Target El out of Physical Bounds. Target Was;" + String(SatTarget[El]) + ", Axis Max;" + String(SearchElevation._axisMax) + ", Axis Min;" + String(SearchElevation._axisMin) + "\n";
		_isError = Error_NewSearchElPhyBounds;
		
		_NewTarget[El] = 0.0;
	}
	
	if(SatTarget[Az] > SearchAzimuth._axisMax || SatTarget[Az] < SearchAzimuth._axisMin)
	{
		_StatusMessage += "~,Sat Target Az out of Physical Bounds Target Was;" + String(SatTarget[Az]) + ", Axis Max;" + String(SearchAzimuth._axisMax) + ", Axis Min;" + String(SearchAzimuth._axisMin) + "\n";
		_isError = Error_NewSearchAzPhyBounds;
		
		_NewTarget[Az] = 0.0;
	}
}

//*****************************************************************************************************
//*****************************************************************************************************

// Handle Conditions when Next Position produces results outside of the Search Boundary
void ControlPattern_SearchClass::HandleNewOutofSearchBounds(float SatTarget[])
{
	// Compare each Axis to ensure they are within the Search Boundaries for the pattern. Otherwise set them at the start
	// and call error condition
	if(abs(_NewTarget[El] - SatTarget[El]) > SearchElevation._maxCounterBoundary + 0.003)
	{
		_StatusMessage += "~,New Target El out of Search Bounds. Constrained in place\n";
		_isError = Error_NewSearchElBounds;
		
		// 		Serial.print("New Target;"); Serial.print(_NewTarget[El]);
		// 		Serial.print(", Sat Target;"); Serial.print(SatTarget[El]);
		// 		Serial.print(", Counter;"); Serial.print(SearchElevation._maxCounterBoundary,8);
		// 		Serial.print(", Diff;"); Serial.print(abs(_NewTarget[El] - SatTarget[El]),8);
		// 		Serial.println("");
		_NewTarget[El] = SatTarget[El] + SearchElevation._maxCounterBoundary;
	}
	
}

//*****************************************************************************************************
//*****************************************************************************************************

//Handle Conditions for change in direction and elevation expansion when at corners of spiral pattern
void ControlPattern_SearchClass::HandleAtSearchCorners(float SatTarget[])
{
	// Corners of the Search Spiral - Changes direction of the Search Axis depending on current directions.
	// Also checks for repeated placement of top right corner which signifies a completed search
	if(SearchAzimuth._atBoundary && SearchElevation._atBoundary)
	{

		HandleAtSearchCorners_FinishedSearch(SatTarget);
		
		HandleAtSearchCorners_DirectionChange();

	}
}

//*****************************************************************************************************

// Completed Search Counter
//   Keeps track of visits to the top right hand corner of the search. 2 Visists signifies a completed search
//    - Completed search can only happen when the elevation has expanded to its maximum size
void ControlPattern_SearchClass::HandleAtSearchCorners_FinishedSearch(float SatTarget[])
{
	// Check if we're at the absolute maximum pattern boundary && that the elevation has grown to full size
	if(SearchElevation._maxCounterBoundary == _AbsMaxPatternElevationBoundary && (_NewTarget[El] == SatTarget[El]+_AbsMaxPatternElevationBoundary || _NewTarget[El] == SearchElevation._axisMax))
	_FinishedPositionCounter++; // First time visiting the position
	
	// Second visit to final position - this means the pattern has completed a loop of the maximum position. Time to
	//  flag complete and reset counter.
	if(_FinishedPositionCounter == 2)
	{
		_isSearchComplete = true;
		_FinishedPositionCounter = 0;
	}
}


//*****************************************************************************************************

// Direction Change Handles
//   Depending on corner of the search dictates the axis change in direction
//    -Current order produces a right hand based spiral
void ControlPattern_SearchClass::HandleAtSearchCorners_DirectionChange()
{
	// Change Direction of Axis Depending on Current Directions
	if(SearchElevation._isReversed)
	SearchAzimuth._isReversed = false; // Bottom left Corner
	else
	SearchAzimuth._isReversed = true; // Top Right Corner
	
	if(SearchAzimuth._isReversed)
	SearchElevation._isReversed = true; //Top Left Corner
	else // Bottom Right Corner
	{
		SearchElevation._isReversed = false;
		SearchElevation._maxCounterBoundary  = min(_ElExpandingBoundaryFactor + SearchElevation._maxCounterBoundary, _AbsMaxPatternElevationBoundary); // Increase the elevation Max Boundary to create an ever increasing elevation position (Spiralling out)
	}
	
}

//*****************************************************************************************************
//*****************************************************************************************************

// Handles Current Position Out Of Search Bounds
//  -Final safety condition. Prevents the search from being skewed by out of bounds current position
void ControlPattern_SearchClass::HandleCurrentPosOutOfBounds(float SatTarget[], float CurrentPos[], float *ReturnedTargetPos)
{
	// Elevation Check - Ensures Current Position isn't outside of current search boundary
	//  -flags error
	if(abs(CurrentPos[El] - SatTarget[El]) > SearchElevation._maxCounterBoundary + _ResetBoundaryBufferEl)
	{
		_NewTarget[El] = SatTarget[El];
		
		
		_StatusMessage += "~,Search Error; Elevation Current Position out of bounds. Restarting El Search";
		_isError = Error_ElCurrentOutOfBounds;
	}
	
	float SearchPosError = abs(CurrentPos[Az] - SatTarget[Az]);
	
	if(SearchPosError > 360.0)
	SearchPosError -= 360.0;
	
	// Azimuth Check - various conditions for azimuth to cover roll over conditions
	if(SearchPosError > SearchAzimuth._maxCounterBoundary + _ResetBoundaryBufferAz)
	{
		byte Trigger = 0;
		
		if(SatTarget[Az] < (SearchAzimuth._axisMin + SearchAzimuth._maxCounterBoundary) && CurrentPos[Az] < (SearchAzimuth._axisMax - (SearchAzimuth._maxCounterBoundary + _ResetBoundaryBufferAz)))
		{
			_NewTarget[Az] = SatTarget[Az];
			Trigger = 1;
		}
		if(SatTarget[Az] > (SearchAzimuth._axisMin + (SearchAzimuth._maxCounterBoundary)) && (SatTarget[Az] < (SearchAzimuth._axisMax - (SearchAzimuth._maxCounterBoundary))))
		{
			_NewTarget[Az] = SatTarget[Az];
			Trigger += 2;
		}
		
		if(SatTarget[Az] > (SearchAzimuth._axisMax - SearchAzimuth._maxCounterBoundary) && CurrentPos[Az] > (SearchAzimuth._axisMin + (SearchAzimuth._maxCounterBoundary + _ResetBoundaryBufferAz)))
		{
			_NewTarget[Az] = SatTarget[Az];
			Trigger += 4;
		}
		
		// Error Flag
		if(Trigger > 0)
		{
			_StatusMessage = "~,Search Error; Azimuth Current Position out of bounds. Restarting Az Search. Current Az;" + String(CurrentPos[Az]) + ", SatTarget Az;" + String(SatTarget[Az]) + ", Trigger; " + String(Trigger);
			_isError = Error_AzCurrentOutOfBounds;
		}
	}
}


//*****************************************************************************************************
//*****************************************************************************************************

//Support Function. Assign one array equal to the input array
void ControlPattern_SearchClass::Array_SetEquals(float InputArray[], float *OutputArray, int ArraySize)
{
	for (int i = 0; i < ArraySize; i++)
	{
		OutputArray[i] = InputArray[i];
	}
	
	
}





ControlPattern_SearchClass ControlPattern_Search;

