//
//
//

#include "ControlPattern_SatPoint.h"

void ControlPattern_SatPointClass::init()
{


}

// Main Class Setup
//  -Resets all values to default state
void ControlPattern_SatPointClass::setup()
{
	_GPSLat = 0.0;
	_GPSLng = 0.0;
	
	_isError = 0;
	_StatusMessage = "";

}

//Calculate Az El Pol for Given Sat Lng and GPS Position
// Basic pointing calc. Not to be used with main code
void ControlPattern_SatPointClass::CalculateSatPoint(float SatLng, int SatVHPol, float *ReturnedPos)
{
	_SatLng = SatLng;

	
	
	// Maths Supplied by JG
	// ****
	double AntennaLat = _GPSLat*(PI/180.0);                                    // Convert to radians.
	double AntennaLng = _GPSLng*(PI/180.0);
	double SatLngRad = _SatLng*(PI/180.0);
	double w = AntennaLng - SatLngRad;
	const double rgeo = 42164200.0;
	const double rearth = 6378000.0;
	double range = sqrt( sq(rgeo*cos(w) - rearth*cos(AntennaLat)) + sq(rgeo)*sq(sin(w)) + sq(rearth)*sq(sin(AntennaLat)) );

	double elevationRad = asin( (rgeo*cos(w)*cos(AntennaLat)-rearth)/range);
	double azimuthRad = atan2( -1*(rgeo*sin(w))/(range*cos(elevationRad))  , -1*(rgeo*sin(AntennaLat)*cos(w))/(range*cos(elevationRad)));

	// Convert to degrees
	float azimuthDeg = degrees(azimuthRad);
	float elevationDeg = degrees(elevationRad);
	
	
	// Ensure Az in range of 0 - 360
	if (azimuthDeg > 360.0)
	azimuthDeg = azimuthDeg - 360.0;

	if (azimuthDeg < -0.0)
	azimuthDeg = azimuthDeg + 360.0;
	
	
	// 	// ****
	//
	//  Pol Skew calculation - to give offset from horizon
	// ****
	double PolA = (-sin(AntennaLat)*1*sin(w));
	double cosAz = cos(azimuthRad);
	double sinEl = sin(elevationRad);
	double sinAz = sin(azimuthRad);
	double cosW = cos(w);
	
	double PolX = (cosAz*sinEl*(PolA))+((sinAz*sinEl*1*cosW)+(cos(elevationRad)*((-cos(AntennaLat)*1*sin(w)))));
	double PolY = -1*sinAz*(PolA)+(cosAz*1*cosW);

	double PolCalc = (atan2(PolY, PolX));
	// ****
	
	// Normalise the value around 180 - gives a greater space left and right of the Pol to allow for offset compensation
	PolCalc = -1*(degrees(PolCalc));


	//
	// 	// Polarisation of the feed/sat requires a 90 degree turn for Verticle or Horizontal
	if (SatVHPol == VerticalPol)
	PolCalc += 90.0;
	
	if(PolCalc < 0.00)
	PolCalc += 180.0;

	//Store Azimuth of Satellite for Geo Location
	_StoredSatAzPos = azimuthDeg;

	float PointingAz = azimuthDeg - _AzOffsetSat;
	
	// Ensure Az in range of 0 - 360
	if (PointingAz > 360.0)
	PointingAz = PointingAz - 360.0;

	if (PointingAz < -0.0)
	PointingAz = PointingAz + 360.0;
	
	// Returned Values
	ReturnedPos[El] = elevationDeg;
	ReturnedPos[Az] = PointingAz;
	ReturnedPos[Pol] = PolCalc;

}


//Calculate Az El Pol for Given Sat Lng and GPS Position
// Main Pointing Code to be used
void ControlPattern_SatPointClass::CalculateSatPoint(SatelliteDetails NewSat, float *ReturnedPos)
{
	_isError = 0;
	_StatusMessage = "";
	
	// Check input data for errors. Return on error found
	if(checkInputData(NewSat,ReturnedPos))
	return;
	
	_CurrentSat = NewSat;
	
	_SatLng = _CurrentSat.SatLng;

	
	
	// Maths Supplied by JG
	// ****
	double AntennaLat = _GPSLat*(PI/180.0);                                    // Convert to radians.
	double AntennaLng = _GPSLng*(PI/180.0);
	double SatLngRad = _SatLng*(PI/180.0);
	double w = AntennaLng - SatLngRad;
	const double rgeo = 42164200.0;
	const double rearth = 6378000.0;
	double range = sqrt( sq(rgeo*cos(w) - rearth*cos(AntennaLat)) + sq(rgeo)*sq(sin(w)) + sq(rearth)*sq(sin(AntennaLat)) );

	double elevationRad = asin( (rgeo*cos(w)*cos(AntennaLat)-rearth)/range);
	double azimuthRad = atan2( -1*(rgeo*sin(w))/(range*cos(elevationRad))  , -1*(rgeo*sin(AntennaLat)*cos(w))/(range*cos(elevationRad)));

	// Convert Results to degrees
	float azimuthDeg = degrees(azimuthRad);
	float elevationDeg = degrees(elevationRad);
	
	// Azimuth Boundaries
	if (azimuthDeg < -0.0 && elevationDeg < -0.0)
	azimuthDeg = azimuthDeg + 180.0;
	else if (azimuthDeg < 0.0)
	azimuthDeg = azimuthDeg + 360.0;
	
	// Handle Physical Boundaries for possible elevation results
	elevationDeg = handleElevationBoundaries(elevationDeg);

	//  Pol Skew calculation - to give offset from horizon
	// ****
	double PolA = (-sin(AntennaLat)*1*sin(w));
	double cosAz = cos(azimuthRad);
	double sinEl = sin(elevationRad);
	double sinAz = sin(azimuthRad);
	double cosW = cos(w);
	
	double PolX = (cosAz*sinEl*(PolA))+((sinAz*sinEl*1*cosW)+(cos(elevationRad)*((-cos(AntennaLat)*1*sin(w)))));
	double PolY = -1*sinAz*(PolA)+(cosAz*1*cosW);

	double PolCalc = (atan2(PolY, PolX));
	// ****
	
	// Set Pol Calc to show from a clockwise position in degrees
	PolCalc = -1*(degrees(PolCalc));
	
	
	PolCalc = handlePolBoundaries(PolCalc, _CurrentSat.SatPolHV);
	
	//Store Azimuth of Satellite for Geo Location
	_StoredSatAzPos = azimuthDeg;
	
	float PointingAz = azimuthDeg - _AzOffsetSat;
	
	// Ensure Az in range of 0 - 360
	if (PointingAz > 360.0)
	PointingAz = PointingAz - 360.0;

	if (PointingAz < -0.0)
	PointingAz = PointingAz + 360.0;
	
	
	// Returned Values
	ReturnedPos[El] = elevationDeg;
	ReturnedPos[Az] = PointingAz;
	ReturnedPos[Pol] = PolCalc;

}

//**********************************************************************************
//Calculate Pol only for current Sat Lng and updated GPS Position
void ControlPattern_SatPointClass::CalculateSat_PolUpdate(float *ReturnedPos)
{
	_isError = 0;
	_StatusMessage = "";
	
	// Check input data for errors. Return on error found
	if(checkInputData(_CurrentSat,ReturnedPos))
	return;
	
	_SatLng = _CurrentSat.SatLng;

	
	
	// Maths Supplied by JG
	// ****
	double AntennaLat = _GPSLat*(PI/180.0);                                    // Convert to radians.
	double AntennaLng = _GPSLng*(PI/180.0);
	double SatLngRad = _SatLng*(PI/180.0);
	double w = AntennaLng - SatLngRad;
	const double rgeo = 42164200.0;
	const double rearth = 6378000.0;
	double range = sqrt( sq(rgeo*cos(w) - rearth*cos(AntennaLat)) + sq(rgeo)*sq(sin(w)) + sq(rearth)*sq(sin(AntennaLat)) );

	double elevationRad = asin( (rgeo*cos(w)*cos(AntennaLat)-rearth)/range);
	double azimuthRad = atan2( -1*(rgeo*sin(w))/(range*cos(elevationRad))  , -1*(rgeo*sin(AntennaLat)*cos(w))/(range*cos(elevationRad)));

	// Convert Results to degrees
	float azimuthDeg = degrees(azimuthRad);
	float elevationDeg = degrees(elevationRad);
	
	// Azimuth Boundaries
	if (azimuthDeg < -0.0 && elevationDeg < -0.0)
	azimuthDeg = azimuthDeg + 180.0;
	else if (azimuthDeg < 0.0)
	azimuthDeg = azimuthDeg + 360.0;
	
	// Handle Physical Boundaries for possible elevation results
	elevationDeg = handleElevationBoundaries(elevationDeg);

	//  Pol Skew calculation - to give offset from horizon
	// ****
	double PolA = (-sin(AntennaLat)*1*sin(w));
	double cosAz = cos(azimuthRad);
	double sinEl = sin(elevationRad);
	double sinAz = sin(azimuthRad);
	double cosW = cos(w);
	
	double PolX = (cosAz*sinEl*(PolA))+((sinAz*sinEl*1*cosW)+(cos(elevationRad)*((-cos(AntennaLat)*1*sin(w)))));
	double PolY = -1*sinAz*(PolA)+(cosAz*1*cosW);

	double PolCalc = (atan2(PolY, PolX));
	// ****
	
	// Set Pol Calc to show from a clockwise position in degrees
	PolCalc = -1*(degrees(PolCalc));
	
	
	PolCalc = handlePolBoundaries(PolCalc, _CurrentSat.SatPolHV);

	// Returned Values
	ReturnedPos[Pol] = PolCalc;

}

//**********************************************************************************

// Check Input Data for out of bounds of erroneous data
boolean ControlPattern_SatPointClass::checkInputData(SatelliteDetails NewSat, float *ReturnedPos)
{
	// Check incomming Sat Longitude is within boundaries
	if(NewSat.SatLng > MaxSatLng || NewSat.SatLng < MinSatLng)
	{
		_StatusMessage += "@,CP-SatPoint; New Sat Lng Out of Bounds;" + String(NewSat.SatLng) + ". Failed Input\n";
		
		_isError = SatelliteDetailsInputError;
		
		float FailurePosArray[3] = {0.0,0.0,180.0};
		
		StndFnct.SetEquals(FailurePosArray,ReturnedPos,3);
		
		return true;
	}
	
	// Check input Sat Custom Skew is within boundaries
	if(NewSat.SatCustomSkew > MaxSatSkew || NewSat.SatCustomSkew < MinSatSkew)
	{
		_StatusMessage += "@,CP-SatPoint; New Sat Skew Out of Bounds;" + String(NewSat.SatCustomSkew) + ". Failed Input\n";
		
		_isError = SatelliteDetailsInputError;
		
		float FailurePosArray[3] = {0.0,0.0,180.0};
		
		StndFnct.SetEquals(FailurePosArray,ReturnedPos,3);
		
		return true;
	}
	
	// Check input GPS Lng is within boundaries
	if(_GPSLng > MaxGPSLng || _GPSLng < MinGPSLng)
	{
		_StatusMessage += "@,CP-SatPoint; New GPS Longitude Out of Bounds;" + String(_GPSLng) + ". Failed Input\n";
		
		_isError = GPSInputError;
		
		float FailurePosArray[3] = {0.0,0.0,180.0};
		
		StndFnct.SetEquals(FailurePosArray,ReturnedPos,3);
		
		return true;
	}
	
	// Check input GPS Lat is within boundaries
	if(_GPSLat > MaxGPSLat || _GPSLat < MinGPSLat)
	{
		_StatusMessage += "@,CP-SatPoint; New GPS Latitude Out of Bounds;" + String(_GPSLng) + ". Failed Input\n";
		
		_isError = GPSInputError;
		
		float FailurePosArray[3] = {0.0,0.0,180.0};
		
		StndFnct.SetEquals(FailurePosArray,ReturnedPos,3);
		
		return true;
	}
	
	
	return false;
}

// Handle Pol Max/Min and Offset boundaries
float ControlPattern_SatPointClass::handlePolBoundaries(float PolCalc, int SatHVPol)
{
	// 	// Polarisation of the feed/sat requires a 90 degree turn for Verticle or Horizontal
	if (SatHVPol == VerticalPol)
	PolCalc += 90.0;
	
	// Custom Skew Addition
	PolCalc += _CurrentSat.SatCustomSkew;
	
	// Minimum boundary - general calculation tends to go below 0
	if(PolCalc < MinPhysical_Pol && PolCalc > -180.0)
	PolCalc += 180.0;
	else if (PolCalc < MinPhysical_Pol)
	PolCalc += (180.0 * (1+(int(-1*PolCalc) / 180)));
	
	
	// Completely out of range issue - major maths or offset issue
	if((PolCalc > MaxPhysical_Pol * 2) || (PolCalc < MinPhysical_Pol))
	{
		_StatusMessage += "@,CP-SatPoint; Pol Result Greatly Out of Bounds;" + String(PolCalc) + ". Constraining to Max Physical Pol\n";
		
		PolCalc = MaxPhysical_Pol;
		
		_isError = PolOOBError;
	}// Max Boundary
	else if(PolCalc > MaxPhysical_Pol )
	{
		_StatusMessage += "~,CP-SatPoint; Pol Result Out of Bounds;" + String(PolCalc) + ". -360 result\n";
		
		PolCalc -= MaxPhysical_Pol;
		
		_isError = PolOOBError;
	}
	
	return PolCalc;
}


// Handle Elevation Max/Min
float ControlPattern_SatPointClass::handleElevationBoundaries(float elevationDeg)
{
	// Max El Boundary
	if(elevationDeg > MaxPhysical_Elevation)
	{
		_StatusMessage += "~,CP-SatPoint; Elevaion Result Out of Bounds;" + String(elevationDeg) + ". Constraining to Max Physical El\n";
		
		elevationDeg = MaxPhysical_Elevation;
		
		_isError = ElevationOOBError;
	}
	
	// Min El Boundary
	if(elevationDeg < MinPhysical_Elevation)
	{
		_StatusMessage += "~,CP-SatPoint; Elevaion Result Out of Bounds;" + String(elevationDeg) + ". Constraining to Min Physical El\n";
		
		elevationDeg = MinPhysical_Elevation;
		
		_isError = ElevationOOBError;
	}
	
	return elevationDeg;
}


// Manualy Set GPS Values
void ControlPattern_SatPointClass::setGPS(float GPSLat, float GPSLng)
{
	_GPSLat = GPSLat;
	_GPSLng = GPSLng;
}

// Return GPS Values in Array
void ControlPattern_SatPointClass::returnGPS(float *ReturnedGPS)
{
	ReturnedGPS[Lat] = _GPSLat;
	ReturnedGPS[Lng] = _GPSLng;
}

