// ControlPattern_Peaking.h
/*
Control Pattern Peaking Code

Class is designed to output positional targets for Az and El for the antenna. These positions are
based on a square pattern ABCD. Modem signal readings are taken at each location and then used to
calculate a new center location that the rest of the positions are based on.

Created by Jack Wells

Change Log: 25/8/15
-Class created and unit tested as part of project UT-ControlPattern-Peaking_Main


*/



#ifndef _CONTROLPATTERN_PEAKING_h
#define _CONTROLPATTERN_PEAKING_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Supporting Libs/StandardFunctions_Array.h"




#ifndef Az
#define Az 0
#define El 1
#define Pol 2
#endif

#define A 0
#define B 1
#define C 2
#define D 3
#define NONE -1
#define SignalLost -2

class ControlPattern_PeakingClass
{
	protected:
	float _CentreSkyPosition[3];
	StandardFunctions_Array StndFncts;

	boolean CheckisSignalLost();
	void HandleEndOfPattern();
	void CalculateNewCentreSkyTarget();
	void HandleSignalStrengthSave(int CurrentModemSignal);
	void calculateNextSkyTarget(float* NewSkyTarget);

	
	public:
	int _signalLostcounter;
	float _AzStepSize;
	float _ElStepSize;
	float _KAz;
	float _KEl;
	
	unsigned long _LastUpdateTime;
	unsigned long _PointDelay = 350;
	
	void SetCentreSkyPosition(float InputPosition[]);
	void GetCentreSkyPosition(float* ReturnedPosition);
	void SetPeakingSizes(float AzStepSize, float ElStepSize, float KAz, float KEl);
	void GetNewSkyTarget(int CurrentModemSignal, float* NewSkyTarget);
	int _signalLostThreshold;
	int _CurrentPatternTargetPositionCounter;
	int _SavedPeakingSignalsAtPoint[4];
	boolean _isSignalLost;
	void Setup();
	void Setup(float AzStepSize, float ElStepSize, float KAz, float KEl, int signalLostThreshold);
	void init();
	
	
	boolean _isConstantPeaking = false;
	boolean _isStopOnPeaked;
	int _PeakedUpperThreshold;
	int _PeakedLowerThreshold;
};


#endif

