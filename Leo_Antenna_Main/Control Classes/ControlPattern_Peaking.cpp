//
//
//

#include "ControlPattern_Peaking.h"

void ControlPattern_PeakingClass::init()
{
	

}

//***************************************************************************************

void ControlPattern_PeakingClass::Setup()
{
	StndFncts.Clear(_CentreSkyPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = 0;
	
	_ElStepSize = 0.0;
	_AzStepSize = 0.0;
	
	_KAz = 0.0;
	_KEl = 0.0;
}

// Alternative Setup function for fast setup of peaking pattern
void ControlPattern_PeakingClass::Setup(float AzStepSize, float ElStepSize, float KAz, float KEl, int signalLostThreshold)
{
	StndFncts.Clear(_CentreSkyPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = signalLostThreshold;
	
	_ElStepSize = ElStepSize;
	_AzStepSize = AzStepSize;
	
	_KAz = KAz;
	_KEl = KEl;
}


//**************************************************************************************

// Get-Set Functions for Centre of Peaking Pattern
void ControlPattern_PeakingClass::SetCentreSkyPosition(float InputPosition[])
{
	// Sets Centre Sky Position to input
	StndFncts.SetEquals(InputPosition,_CentreSkyPosition,3);
	
	_signalLostcounter = 0;
	_isSignalLost = false;
	
	
	//Resets Current Pattern Position Counter for new peaking pattern
	_CurrentPatternTargetPositionCounter = NONE;
}

void ControlPattern_PeakingClass::GetCentreSkyPosition(float* ReturnedPosition)
{
	StndFncts.SetEquals(_CentreSkyPosition,ReturnedPosition,3);
}

//**************************************************************************************

// Set Function for Peaking Pattern Sizes
void ControlPattern_PeakingClass::SetPeakingSizes(float AzStepSize, float ElStepSize, float KAz, float KEl)
{
	_ElStepSize = ElStepSize;
	_AzStepSize = AzStepSize;
	
	_KAz = KAz;
	_KEl = KEl;
}

//***************************************************************************************

//***************************************************************************************
//***************************************************************************************
// Main Pattern Code
//  Handles incomming Modem Signal to plot New Sky Target positions based on square box peaking pattern
//  Checks signal for signal loss - resulting in flagging signal lost for main code to restart pointing
//  Records signal strength for each pattern position
//  Outputs appropriate position based on step sizes and a centre position
void ControlPattern_PeakingClass::GetNewSkyTarget(int CurrentModemSignal, float* NewSkyTarget)
{
	
	//Save Signal for Actual Current Position, check signal is above threshold or increase signal lost counter
	HandleSignalStrengthSave(CurrentModemSignal);
	
	// Update to Next Pattern Position
	_CurrentPatternTargetPositionCounter++;
	
	
	// Reset pattern on completion if Signal is found, otherwise flag issignal lost
	if(_CurrentPatternTargetPositionCounter > D)
	HandleEndOfPattern();


	calculateNextSkyTarget(NewSkyTarget);

}

//***************************************************************************************
// Save Signal Strength to Class Array
//  Increase counter if signal strength is less than Threshold - leads to signal lost flag
void ControlPattern_PeakingClass::HandleSignalStrengthSave(int CurrentModemSignal)
{
	if(_CurrentPatternTargetPositionCounter != NONE)
	{
		_SavedPeakingSignalsAtPoint[_CurrentPatternTargetPositionCounter] = CurrentModemSignal;
		
		if(CurrentModemSignal < _signalLostThreshold)
		_signalLostcounter++;
		else
		{
		_signalLostcounter = 0;
		_isSignalLost = false;
		}
	}
}

//***************************************************************************************

// Handle End of 4 Position Peaking Pattern
//   - Checks for signal lost
//   - Recalculates new Centre Sky Target based on saved signal strength values
void ControlPattern_PeakingClass::HandleEndOfPattern()
{
	
	// Check for lost signal
	if(CheckisSignalLost())
	return;
	else
	CalculateNewCentreSkyTarget();
	
	
	
}

// Calculate New Peaking Pattern Centre Sky Target Position
//  Uses technique from journal on satellite antenna peaking
//  Box square peaking pattern - requires values to setup on class usage
void ControlPattern_PeakingClass::CalculateNewCentreSkyTarget()
{
	// Recalculate New Centre Position
	float NewCentreSkyTargets[3];
	
	NewCentreSkyTargets[Az] = _CentreSkyPosition[Az] + _KAz*(((_SavedPeakingSignalsAtPoint[B] + _SavedPeakingSignalsAtPoint[C]) - (_SavedPeakingSignalsAtPoint[A] + _SavedPeakingSignalsAtPoint[D])));
	
	NewCentreSkyTargets[El] = _CentreSkyPosition[El] + _KEl*(((_SavedPeakingSignalsAtPoint[A] + _SavedPeakingSignalsAtPoint[B]) - (_SavedPeakingSignalsAtPoint[C] + _SavedPeakingSignalsAtPoint[D])));
	
	// 		Serial.println("New Sky AZ;" + String(NewCentreSkyTargets[Az]));
	// 		Serial.println("New Sky El;" + String(NewCentreSkyTargets[El]));
	StndFncts.SetEquals(NewCentreSkyTargets,_CentreSkyPosition,2);
	
}


//***************************************************************************************

// Check is Signal Lost Counter for lost signal situation
//   - if signal is lost at all peaking pattern points then flag isSignalLost
boolean ControlPattern_PeakingClass::CheckisSignalLost()
{
	if(_CurrentPatternTargetPositionCounter > D)
	{
		if(_signalLostcounter == 4 && !_isConstantPeaking)
		{
			_isSignalLost = true;
			Serial.print("+,Peaking Pattern Signal Lost- Flagging isSignalLost \n");
			
			//Set Current Pattern Target Position to SignalLost condition - points Antenna At Centre
			_CurrentPatternTargetPositionCounter = SignalLost;
			
		}else
		{
			_signalLostcounter = 0;
			_CurrentPatternTargetPositionCounter = A; // reset peaking back to start
		}
	}
	
	return _isSignalLost;
}

//***************************************************************************************
//***************************************************************************************

// Next Peaking Pattern Position for NewSkyTarget based on Current Pattern Target Position Counter
//   Outputs to NewSkyTarget array
void ControlPattern_PeakingClass::calculateNextSkyTarget(float* NewSkyTarget)
{
	//Switch statement for each pattern position. Creates the square peaking pattern - see flow chart for more details
	switch(_CurrentPatternTargetPositionCounter)
	{
		// Pattern Position A
		case A: NewSkyTarget[El] = _CentreSkyPosition[El] + _ElStepSize;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] - _AzStepSize;
		break;
		
		case B: NewSkyTarget[El] = _CentreSkyPosition[El] + _ElStepSize;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + _AzStepSize;
		break;
		
		case C: NewSkyTarget[El] = _CentreSkyPosition[El] - _ElStepSize;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + _AzStepSize;
		break;
		
		case D:	NewSkyTarget[El] = _CentreSkyPosition[El] - _ElStepSize;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] - _AzStepSize;
		break;
		
		//Signal Lost Condition
		case SignalLost:NewSkyTarget[El] = _CentreSkyPosition[El];
		NewSkyTarget[Az] = _CentreSkyPosition[Az];
		break;
		
		// Error usage of Peaking Pattern - out of bounds of switch case
		default: Serial.println("@,Peaking Pattern Miss-use Error - Current Pattern Position Invalid:" + String(_CurrentPatternTargetPositionCounter) + "\n"); break;
	}
}



