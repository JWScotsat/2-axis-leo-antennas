// ControlPattern_Peaking_AltDyDelta.h
/*
Control Pattern Peaking Code

Class is designed to output positional targets for Az and El for the antenna. These positions are
based on a square pattern ABCD. Modem signal readings are taken at each location and then used to
calculate a new center location that the rest of the positions are based on.

Created by Jack Wells

Change Log: 25/8/15
-Class created and unit tested as part of project UT-ControlPattern-Peaking_Main


*/



#ifndef _ControlPattern_Peaking_AltDyDelta_h
#define _ControlPattern_Peaking_AltDyDelta_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Supporting Libs/StandardFunctions_Array.h"


#ifndef Az
#define Az 0
#define El 1
#define Pol 2
#endif

#define A 0
#define B 1
#define C 2
#define D 3
#define NONE -1
#define SignalLost -2

class ControlPattern_Peaking_AltDyDeltaClass
{
	protected:
	float _CentreSkyPosition[3];
	StandardFunctions_Array StndFncts;

	boolean CheckisSignalLost();
	void HandleEndOfPattern();
	void CalculateNewCentreSkyTarget();
	void HandleSignalStrengthSave(float CurrentModemSignal);
	void calculateNextSkyTarget(float* NewSkyTarget);
	void DynamicDeltaUpdate();
	
	void HandleDBLossValues(); // Checks saved signal values and changes -100db values to better match the rest
	public:
	int _signalLostcounter;
	float _AzStepSize;
	float _ElStepSize;
	float _KAz;
	float _KEl;
	
	float _DeltaEl = 0.25;
	float _DeltaAz = 0.25;
	float _DeltaAzMax = 0.3;
	float _DeltaElMax = 0.3;
	
	unsigned long _PointDelayMin = 150; // Point delay minimum size due to dynamic delay time
	
	float _DyKEl = 2;
	float _DyKAz = 2;
	float _DySigStrOffset = 0.0;
	float _DySigStrAvg = 0;
	boolean _AutoDyOffset = false;
	boolean _FirstPeak = true;
	
	float _signalLostThreshold;
	int _CurrentPatternTargetPositionCounter;
	float _SavedPeakingSignalsAtPoint[4];
	
	unsigned long _LastUpdateTime;
	unsigned long _PointDelay = 350;
	
	void SetCentreSkyPosition(float InputPosition[]);
	void GetCentreSkyPosition(float* ReturnedPosition);
	void SetPeakingSizes(float AzStepSize, float ElStepSize, float KAz, float KEl);
	void GetNewSkyTarget(float CurrentModemSignal, float* NewSkyTarget);
	
	float getCentreAz(){return _CentreSkyPosition[Az];};//Return centre azimuth position
	void setCentreSkyAz(float NewAz){_CentreSkyPosition[Az] = NewAz;};

	boolean _isSignalLost;
	void Setup();
	void Setup(float AzStepSizeStart, float ElStepSizeStart, float CentreKAz, float CentreKEl, float signalLostThreshold,
	float DeltaAzMax, float DeltaElMax, float DyKEl, float DyKAz, float SigStrOffset, boolean AutoOffsetEn);
	
	void init();
	
	
	boolean _isConstantPeaking = false;
	boolean _isStopOnPeaked;
	int _PeakedUpperThreshold;
	int _PeakedLowerThreshold;
	
	void PrintSettings();
};


#endif

