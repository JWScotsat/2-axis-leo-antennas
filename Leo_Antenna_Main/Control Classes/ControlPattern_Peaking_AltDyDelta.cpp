//
//
//

#include "ControlPattern_Peaking_AltDyDelta.h"

void ControlPattern_Peaking_AltDyDeltaClass::init()
{
	

}

//***************************************************************************************

void ControlPattern_Peaking_AltDyDeltaClass::Setup()
{
	StndFncts.Clear(_CentreSkyPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = 0;
	
	_ElStepSize = 0.0;
	_AzStepSize = 0.0;
	
	_KAz = 0.0;
	_KEl = 0.0;
}

// Alternative Setup function for fast setup of peaking pattern
void ControlPattern_Peaking_AltDyDeltaClass::Setup(float AzStepSizeStart, float ElStepSizeStart, float CentreKAz, float CentreKEl, float signalLostThreshold,
float DeltaAzMax, float DeltaElMax, float DyKEl, float DyKAz, float SigStrOffset, boolean AutoOffsetEn)
{
	StndFncts.Clear(_CentreSkyPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = signalLostThreshold;
	
	_ElStepSize = ElStepSizeStart;
	_AzStepSize = AzStepSizeStart;
	
	_DeltaEl = ElStepSizeStart;
	_DeltaAz = AzStepSizeStart;
	
	_KAz = CentreKAz;
	_KEl = CentreKEl;
	
	_DeltaAzMax = DeltaAzMax;
	_DeltaElMax = DeltaElMax;
	
	_DyKAz = DyKAz;
	_DyKEl = DyKEl;
	_DySigStrOffset = SigStrOffset;
	_AutoDyOffset = AutoOffsetEn;
	
	
}


//**************************************************************************************

// Get-Set Functions for Centre of Peaking Pattern
void ControlPattern_Peaking_AltDyDeltaClass::SetCentreSkyPosition(float InputPosition[])
{
	// Sets Centre Sky Position to input
	StndFncts.SetEquals(InputPosition,_CentreSkyPosition,3);
	
	_signalLostcounter = 0;
	_isSignalLost = false;
	
	_FirstPeak = true;
	
	//Reset saved signal values to 0
	for (int i = 0; i < 4; i++)
	_SavedPeakingSignalsAtPoint[i] = 0;
	

	
	//Resets Current Pattern Position Counter for new peaking pattern
	_CurrentPatternTargetPositionCounter = NONE;
}

void ControlPattern_Peaking_AltDyDeltaClass::GetCentreSkyPosition(float* ReturnedPosition)
{
	StndFncts.SetEquals(_CentreSkyPosition,ReturnedPosition,3);
}

//**************************************************************************************

// Set Function for Peaking Pattern Sizes
void ControlPattern_Peaking_AltDyDeltaClass::SetPeakingSizes(float AzStepSize, float ElStepSize, float KAz, float KEl)
{
	_ElStepSize = ElStepSize;
	_AzStepSize = AzStepSize;
	
	_DeltaEl = _ElStepSize;
	_DeltaAz = _AzStepSize;
	
	_KAz = KAz;
	_KEl = KEl;
}

//***************************************************************************************

//***************************************************************************************
//***************************************************************************************
// Main Pattern Code
//  Handles incomming Modem Signal to plot New Sky Target positions based on square box peaking pattern
//  Checks signal for signal loss - resulting in flagging signal lost for main code to restart pointing
//  Records signal strength for each pattern position
//  Outputs appropriate position based on step sizes and a centre position
void ControlPattern_Peaking_AltDyDeltaClass::GetNewSkyTarget(float CurrentModemSignal, float* NewSkyTarget)
{
	
	//Save Signal for Actual Current Position, check signal is above threshold or increase signal lost counter
	HandleSignalStrengthSave(CurrentModemSignal);
	
	// Update to Next Pattern Position
	_CurrentPatternTargetPositionCounter++;
	
	
	// Reset pattern on completion if Signal is found, otherwise flag issignal lost
	//if(_CurrentPatternTargetPositionCounter > D && _FirstPeak)
	HandleEndOfPattern();


	calculateNextSkyTarget(NewSkyTarget);

}

//***************************************************************************************
// Save Signal Strength to Class Array
//  Increase counter if signal strength is less than Threshold - leads to signal lost flag
void ControlPattern_Peaking_AltDyDeltaClass::HandleSignalStrengthSave(float CurrentModemSignal)
{
	if(_CurrentPatternTargetPositionCounter != NONE)
	{
		_SavedPeakingSignalsAtPoint[_CurrentPatternTargetPositionCounter] = CurrentModemSignal;
		
		if(CurrentModemSignal < _signalLostThreshold)
		_signalLostcounter++;
		else
		{
			_signalLostcounter = 0;
			_isSignalLost = false;
		}
		
		if(_CurrentPatternTargetPositionCounter == A)
		_DySigStrAvg = CurrentModemSignal/4.0;
		else
		_DySigStrAvg += CurrentModemSignal/4.0;
	}else
	{
		if(_AutoDyOffset)
		_FirstPeak = true;
	}

}

//***************************************************************************************

// Handle End of 4 Position Peaking Pattern
//   - Checks for signal lost
//   - Recalculates new Centre Sky Target based on saved signal strength values
void ControlPattern_Peaking_AltDyDeltaClass::HandleEndOfPattern()
{
	
	// Check for lost signal
	if(CheckisSignalLost())
	return;
	else
	{
		HandleDBLossValues();//Scale -100db values for db signal reading
		
		//*** TODO: Test if peaking pattern improved with only updating at full positional
		if(!_FirstPeak || _CurrentPatternTargetPositionCounter > D)
		//if(_CurrentPatternTargetPositionCounter > D)
		{
			CalculateNewCentreSkyTarget();
			DynamicDeltaUpdate();
			_FirstPeak = false;
		}

		
		
		if(_FirstPeak && _AutoDyOffset)
		_DySigStrOffset = _DySigStrAvg;
		
		if(_CurrentPatternTargetPositionCounter > D)
		{
			DynamicDeltaUpdate();
			_CurrentPatternTargetPositionCounter = A; // reset peaking back to start

		}
	}
	
	
}

// Calculate New Peaking Pattern Centre Sky Target Position
//  Uses technique from journal on satellite antenna peaking
//  Box square peaking pattern - requires values to setup on class usage
void ControlPattern_Peaking_AltDyDeltaClass::CalculateNewCentreSkyTarget()
{
	// Recalculate New Centre Position
	static float NewCentreSkyTargets[3];
	
	//Peaking pattern Az and El centre sky targets are updated on the peaking pattern positions that relate to a single axis (Az or El) change
	if(_CurrentPatternTargetPositionCounter == B || _CurrentPatternTargetPositionCounter == D || _FirstPeak)
	NewCentreSkyTargets[Az] = _CentreSkyPosition[Az] + _KAz*(((_SavedPeakingSignalsAtPoint[B] + _SavedPeakingSignalsAtPoint[C]) - (_SavedPeakingSignalsAtPoint[A] + _SavedPeakingSignalsAtPoint[D])));
	
	if(_CurrentPatternTargetPositionCounter == A || _CurrentPatternTargetPositionCounter == C || _FirstPeak)
	NewCentreSkyTargets[El] = _CentreSkyPosition[El] + _KEl*(((_SavedPeakingSignalsAtPoint[A] + _SavedPeakingSignalsAtPoint[B]) - (_SavedPeakingSignalsAtPoint[C] + _SavedPeakingSignalsAtPoint[D])));
	
	// 		Serial.println("New Sky AZ;" + String(NewCentreSkyTargets[Az]));
	// 		Serial.println("New Sky El;" + String(NewCentreSkyTargets[El]));
	StndFncts.SetEquals(NewCentreSkyTargets,_CentreSkyPosition,2);
	
}


//***************************************************************************************

// Check is Signal Lost Counter for lost signal situation
//   - if signal is lost at all peaking pattern points then flag isSignalLost
boolean ControlPattern_Peaking_AltDyDeltaClass::CheckisSignalLost()
{
	if(_CurrentPatternTargetPositionCounter > D)
	{
		if(_signalLostcounter == 4 && !_isConstantPeaking)
		{
			_isSignalLost = true;
			Serial.print("+,Peaking Pattern Signal Lost- Flagging isSignalLost \n");
			
			//Reset Dynamic Variables for new peaking pattern
			_DeltaAz = _DeltaAzMax;
			_DeltaEl = _DeltaElMax;
			_PointDelay = 450;
			
			//Set Current Pattern Target Position to SignalLost condition - points Antenna At Centre
			_CurrentPatternTargetPositionCounter = SignalLost;
			
		}else
		{
			_signalLostcounter = 0;
		}
	}
	
	return _isSignalLost;
}

//***************************************************************************************
//***************************************************************************************

// Next Peaking Pattern Position for NewSkyTarget based on Current Pattern Target Position Counter
//   Outputs to NewSkyTarget array
void ControlPattern_Peaking_AltDyDeltaClass::calculateNextSkyTarget(float* NewSkyTarget)
{
	//Switch statement for each pattern position. Creates the square peaking pattern - see flow chart for more details
	switch(_CurrentPatternTargetPositionCounter)
	{
		// Pattern Position A
		case A: NewSkyTarget[El] = _CentreSkyPosition[El] + _DeltaEl;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] - _DeltaAz;
		break;
		
		case B: NewSkyTarget[El] = _CentreSkyPosition[El] + _DeltaEl;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + _DeltaAz;
		break;
		
		case C: NewSkyTarget[El] = _CentreSkyPosition[El] - _DeltaEl;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + _DeltaAz;
		break;
		
		case D:	NewSkyTarget[El] = _CentreSkyPosition[El] - _DeltaEl;
		NewSkyTarget[Az] = _CentreSkyPosition[Az] - _DeltaAz;
		break;
		
		//Signal Lost Condition
		case SignalLost:NewSkyTarget[El] = _CentreSkyPosition[El];
		NewSkyTarget[Az] = _CentreSkyPosition[Az];
		break;
		
		// Error usage of Peaking Pattern - out of bounds of switch case
		default: Serial.println("@,Peaking Pattern Miss-use Error - Current Pattern Position Invalid:" + String(_CurrentPatternTargetPositionCounter) + "\n"); break;
	}
}


//***************************************************************************************
//***************************************************************************************

//Dynamic El/Az Calculation - see flowchart or Excel sheet for details
void ControlPattern_Peaking_AltDyDeltaClass::DynamicDeltaUpdate()
{
	_DeltaEl = _ElStepSize * (1- ((_DySigStrAvg - _DySigStrOffset)/(_DyKEl*1000)));
	
	_DeltaAz = _AzStepSize * (1- (_DySigStrAvg - _DySigStrOffset)/(_DyKAz*1000));
	
	_DeltaEl = min(_DeltaElMax, _DeltaEl);
	_DeltaAz = min(_DeltaAzMax, _DeltaAz);
	
	//Point delay time is variable along with peaking pattern delta size.
	_PointDelay = _DeltaEl * 1000;
	_PointDelay = max(_PointDelayMin,_PointDelay); // prevents delay going too low
}


void ControlPattern_Peaking_AltDyDeltaClass::PrintSettings()
{
	Serial.print("+,Peaking Settings - Dynamic");
	Serial.print(", DeltaEl,"); Serial.print(_DeltaEl);
	Serial.print(", DeltaAz,"); Serial.print(_DeltaAz);
	Serial.print(", DeltaEl Min,"); Serial.print(_DeltaElMax);
	Serial.print(", DeltaAz Min,"); Serial.print(_DeltaAzMax);
	Serial.print(", DeltaElStart"); Serial.print(_ElStepSize);
	Serial.print(", DeltaAzStart"); Serial.print(_AzStepSize);
	Serial.print(", DyKEl,"); Serial.print(_DyKEl,3);
	Serial.print(", DyKAz,"); Serial.print(_DyKAz,3);
	Serial.print(", DySigAvg,"); Serial.print(_DySigStrAvg);
	Serial.print(", DySigOffset,"); Serial.print(_DySigStrOffset);
	Serial.print(", DyAutoOffset,"); Serial.print(_AutoDyOffset);
	Serial.print(", CentreKEl,"); Serial.print(_KEl,4);
	Serial.print(", CentreKAz,"); Serial.print(_KAz,4);
	Serial.print(", SigLostThreshold,"); Serial.print(_signalLostThreshold);
	Serial.print(", Point Delay,"); Serial.print(_PointDelay);
	Serial.print("\n");
}


//**************************************************************
//Handler for -100db signal values during peaking pattern
// Values take the average of the adjacent box positions to better estimate a signal value that will not produce a swing in new position
// Or it takes the minimum signal value used for detecting signal loss
void ControlPattern_Peaking_AltDyDeltaClass::HandleDBLossValues()
{

	if(_SavedPeakingSignalsAtPoint[A] <= -99.0)
	_SavedPeakingSignalsAtPoint[A] = max((_SavedPeakingSignalsAtPoint[B] + _SavedPeakingSignalsAtPoint[C])/2,_signalLostThreshold);

	if(_SavedPeakingSignalsAtPoint[B] <= -99.0)
	_SavedPeakingSignalsAtPoint[B] = max((_SavedPeakingSignalsAtPoint[A] + _SavedPeakingSignalsAtPoint[D])/2,_signalLostThreshold);


	if(_SavedPeakingSignalsAtPoint[C] <= -99.0)
	_SavedPeakingSignalsAtPoint[C] = max((_SavedPeakingSignalsAtPoint[A] + _SavedPeakingSignalsAtPoint[D])/2,_signalLostThreshold);


	if(_SavedPeakingSignalsAtPoint[D] <= -99.0)
	_SavedPeakingSignalsAtPoint[D] = max((_SavedPeakingSignalsAtPoint[B] + _SavedPeakingSignalsAtPoint[C])/2,_signalLostThreshold);


}