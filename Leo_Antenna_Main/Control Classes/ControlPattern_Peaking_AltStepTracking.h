// ControlPattern_Peaking_AltDyDelta.h
/*
Control Pattern Peaking Code

Class is designed to output positional targets for Az and El for the antenna. These positions are
based on a square pattern ABCD. Modem signal readings are taken at each location and then used to
calculate a new center location that the rest of the positions are based on.

Created by Jack Wells

Change Log: 25/8/15
-Class created and unit tested as part of project UT-ControlPattern-Peaking_Main


*/



#ifndef _ControlPattern_Peaking_AltStepTracking_h
#define _ControlPattern_Peaking_AltStepTracking_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#include "Supporting Libs/StandardFunctions_Array.h"


#ifndef Az
#define Az 0
#define El 1
#define Pol 2
#endif

#define A 0
#define B 1
#define C 2
#define D 3
#define NONE -1
#define SignalLost -2

class ControlPattern_Peaking_AltStepTrackingClass
{
	protected:
	float _StartPosition[3];
	StandardFunctions_Array StndFncts;

	boolean CheckisSignalLost(int CurrentModemSignal);
	void HandleEndOfPattern();
	void HandleSignalStrengthSave(int CurrentModemSignal);
	void calculateNextSkyTarget(float* NewSkyTarget);

	
	public:
	int _signalLostcounter;
	
	boolean	_ResetHighSig = false;
	float _CurrentSkyMaxPoint[3];
	float _CurrentSkyTarget[3];
		
	int	_SavedPatternTargetPositionCounter = 0;
	int	_PatternPositionCheckedCounter = 0;
	int	_SavedBestSignal = 0;
	
	float _DeltaEl = 0.5;
	float _DeltaAz = 0.5;
	int _NewSigBufferError = 30;
	
	int _signalLostThreshold;
	int _CurrentPatternTargetPositionCounter;
	int _SavedPeakingSignalsAtPoint[4];
	boolean _isSignalLost;
	
	unsigned long _LastUpdateTime;
	unsigned long _PointDelay = 350;
	
	void SetStartPosition(float InputPosition[], int CurrentPeakSignal);
	void GetCentreSkyPosition(float* ReturnedPosition);
	void GetNewSkyTarget(int CurrentModemSignal, float* NewSkyTarget);
	void Setup();
	void Setup(float ElStepSize, float AzStepSize, int signalLostThreshold, int SigBufferError);
	void init();
	
};


#endif

