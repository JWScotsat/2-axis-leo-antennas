// ControlPattern_Search.h

/*
Control Search Pattern.

Initial Development through TDD with Arduino Unit.

Class manages 2 search axis objects in order to produce a Spiral Search Pattern for
Azimuth and Elevation.

Simply initialise and feed in start conditions to request NextPosition(). Results are output
to a passed in Array Pointer.

Highly configurable for size, shape and step-size of pattern.


Created By Jack Wells
Change Log: 13/8/15 - Finished Devlopment for v1.0 - JW

*/

#ifndef _CONTROLPATTERN_SEARCH_h
#define _CONTROLPATTERN_SEARCH_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#define Az 0
#define El 1
#define Pol 2
#define Cr 3


#define Error_NewSearchAzPhyBounds 1
#define Error_NewSearchElPhyBounds 2
#define Error_NewSearchElBounds 3
#define Error_AzCurrentOutOfBounds -2
#define Error_ElCurrentOutOfBounds -1

#include "ControlPattern_SearchAxis.h"

class ControlPattern_SearchClass
{
	protected:
	float _NewTarget[3] = {0.0,0.0,0.0};
	
	
	
	int _FinishedPositionCounter;
	void HandleAtSearchCorners(float SatTarget[]);
	void HandleNewOutofSearchBounds(float SatTarget[]);
	void HandleSatTargetSearchBounds(float SatTarget[]);
	
	void HandleAtSearchCorners_DirectionChange();
	void HandleAtSearchCorners_FinishedSearch(float SatTarget[]);
	void HandleCurrentPosOutOfBounds(float SatTarget[], float CurrentPos[], float *ReturnedTargetPos);
	
	int _SearchAttempt = 0; //Counter for storing number of attempted searches
	public:
	
	void ResetSearchComplete(float SatTarget[], float CurrentPos[], float *ReturnedTargetPos);
	
	ControlPattern_SearchClass();
	
	ControlPattern_SearchAxisClass SearchAzimuth;
	ControlPattern_SearchAxisClass SearchElevation;
	
	float _ElExpandingBoundaryFactor;
	float _AbsMaxPatternElevationBoundary;
	
	float _ResetBoundaryBufferEl;
	float _ResetBoundaryBufferAz;
	
	unsigned long _LastUpdateTime;
	unsigned long _PointDelay = 0;
	
	String _StatusMessage;
	int _isError;
	
	boolean _isSearchComplete;
	
	void init();
	void setup();
	void setupDefaultAntennaSearch();
	void setupConfig(float ElDeltaSize, float ElMaxSweep, float ElStepSize, float ElMaxBound, float ElMinBound, float ElBoundRollOver, 
	float AzStepSize, float AzMaxSweep, float AzMaxBound, float AzMinBound, float AzBoundRollOver, unsigned long PointDelayTime);

	boolean NextPosition(float SatTarget[], float CurrentPos[],float *NewTargetPos);
	void Array_SetEquals(float InputArray[], float *OutputArray, int ArraySize);
	
	void addSearchAttempt(){_SearchAttempt++;};
	int getSearchAttempt(){return _SearchAttempt;};
	void resetSearchAttempt(){_SearchAttempt = 1;};
	void resetFirstEverSearchAttempt(){_SearchAttempt = 2;};
};

extern ControlPattern_SearchClass ControlPattern_Search;

#endif

