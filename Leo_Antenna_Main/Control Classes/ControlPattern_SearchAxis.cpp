//
//
//

#include "ControlPattern_SearchAxis.h"

void ControlPattern_SearchAxisClass::init()
{
}



// Main Algorithim
// Takes in Sat Point Array and Current Postion in order to calculate and update New Target Az El Pol
float ControlPattern_SearchAxisClass::NextAxisPosition(float SatTargetAxis, float CurrentPosAxis)
{
	
	float NewCalcTarget;
	
	// Check for Axis Roll Over/Rolling Boundaries. Defines which mathematical method to use when handling boundaries
	if(_AxisBoundaryRollOver)
	NewCalcTarget = NextAxisPositionRollOver(SatTargetAxis,CurrentPosAxis);
	// Main Functionality when ignoring rolling boundaries
	else
	NewCalcTarget = NextAxisPositionBoundaried(SatTargetAxis,CurrentPosAxis);
	
	
	return NewCalcTarget;
}

//*******************************************************************************************************************
//*******************************************************************************************************************



// Main Algorithim for Axis Roll Over
// Takes in Sat Point Array and Current Postion in order to calculate and update New Target Az El Pol
float ControlPattern_SearchAxisClass::NextAxisPositionRollOver(float SatTargetAxis, float CurrentPosAxis)
{
	
	float NewCalcTarget;
	
	// Check for Axis Roll Over/Rolling Boundaries. Defines which mathematical method to use when handling boundaries

	float topSearchRollOverBounds = _axisMax - (_maxCounterBoundary - SatTargetAxis); // Top End over hand of search boundary when Sat Pointed is close to 0
	float bottomSearchRollOverBounds = _maxCounterBoundary - (_axisMax - SatTargetAxis); // Bottom End over hang of search boundary when Sat Pointed is close to 360.
	
	// Check if Axis is forward or Reverse
	if(!_isReversed)
	{
		//Forward Movement
		
		// Stepper New Target Forward from Current Position
		NewCalcTarget = CurrentPosAxis + _stepCountSize;
		
		// Cross 360 boundary
		if (NewCalcTarget >= _axisMax)
		NewCalcTarget -= _axisMax;
		
		// Out of bounds check to make sure the Axis isn't going higher than the max size of the search
		//  flags when at the maximum search boundary to signify a change in direction
		if((NewCalcTarget >  SatTargetAxis + _maxCounterBoundary) && (NewCalcTarget < topSearchRollOverBounds))
		{
			NewCalcTarget = SatTargetAxis + _maxCounterBoundary; // Constrain axis output to the maximum boundary
			
			_atBoundary = true;
		}
		// Cross AxisMax boundary (360 boundary). This checks for crossing and handles new values
		else if ((abs(SatTargetAxis - NewCalcTarget) > _maxCounterBoundary) && (NewCalcTarget > bottomSearchRollOverBounds) && bottomSearchRollOverBounds >= _axisMin)
		{
			
			NewCalcTarget = bottomSearchRollOverBounds;
			_atBoundary = true;
		}
		// No boundary cross
		else
		_atBoundary = false;
	}
	else
	{
		//Reverse Movement
		
		// Stepping New Target in reverse
		NewCalcTarget = CurrentPosAxis - _stepCountSize;
		
		//Roll Over Condition at Lower Bounds
		if (NewCalcTarget < _axisMin)
		NewCalcTarget += _axisMax;
		
		// Out of bounds check to make sure the Axis isn't going higher than the lower size of the search
		//  flags when at the minimum search boundary to signify a change in direction
		if(NewCalcTarget <  SatTargetAxis - _maxCounterBoundary && (NewCalcTarget > bottomSearchRollOverBounds))
		{
			NewCalcTarget = SatTargetAxis - _maxCounterBoundary;
			
			_atBoundary = true;
			
		}
		// Cross AxisMin boundary (0 boundary). This checks for crossing and handles new values
		else if ((abs(SatTargetAxis - NewCalcTarget) > _maxCounterBoundary) && (NewCalcTarget < topSearchRollOverBounds) && topSearchRollOverBounds <= _axisMax)
		{
			NewCalcTarget = topSearchRollOverBounds;
			_atBoundary = true;
		}
		else
		_atBoundary = false;
	}
	
	return NewCalcTarget;
}

//*******************************************************************************************************************
//*******************************************************************************************************************


// Main Algorithim
// Takes in Sat Point Array and Current Postion in order to calculate and update New Target Az El Pol
float ControlPattern_SearchAxisClass::NextAxisPositionBoundaried(float SatTargetAxis, float CurrentPosAxis)
{
	
	float NewCalcTarget;
	
	if(CurrentPosAxis > SatTargetAxis + _maxCounterBoundary || CurrentPosAxis < SatTargetAxis - _maxCounterBoundary)
	return NewCalcTarget = SatTargetAxis;
	
	if(!_isReversed)
	{
		// Lower Minimum Axis Condition. Ensure the search continues through the same points by finding the nearest
		//	"step" away from the upper max.
		if(CurrentPosAxis == _axisMin)
		{
			NewCalcTarget = NextAxisPositionBoundaried_AxisAtMin(SatTargetAxis,CurrentPosAxis);
		}else
		// Stepper New Target Forward from Current Position
		NewCalcTarget = CurrentPosAxis + _stepCountSize;
		
		// Out of bounds check to make sure the Axis isn't going higher than the max size of the search or axis capable boundary
		if((NewCalcTarget >  SatTargetAxis + _maxCounterBoundary) || NewCalcTarget > _axisMax)
		{
			NewCalcTarget = min(_axisMax, SatTargetAxis + _maxCounterBoundary);
			_atBoundary = true;
		}else
		_atBoundary = false;
		
		
		
	}else
	{
		// Upper Max Axis Condition. Ensure the search continues through the same points by finding the nearest
		//	"step" away from the upper max.
		if(CurrentPosAxis == _axisMax)
		{
			NewCalcTarget = NextAxisPositionBoundaried_AxisAtMax(SatTargetAxis,CurrentPosAxis);
		}else
		// Stepping New Target in reverse
		NewCalcTarget = CurrentPosAxis - _stepCountSize;
		
		// Out of bounds check to make sure the Axis isn't going lower than the negative max size of the search
		if((NewCalcTarget <  SatTargetAxis - _maxCounterBoundary) || NewCalcTarget < _axisMin)
		{
			NewCalcTarget = max(SatTargetAxis - _maxCounterBoundary, _axisMin);
			_atBoundary = true;
		}else
		_atBoundary = false;
	}

	return NewCalcTarget;
}

//*******************************************************************************************************************
//*******************************************************************************************************************


// Sub Algorithim
// Takes in Sat Point Array and Current Postion in order to calculate and update New Target Az El Pol
// Considers top boundary movements for Elevation
float ControlPattern_SearchAxisClass::NextAxisPositionBoundaried_AxisAtMax(float SatTargetAxis, float CurrentPosAxis)
{
	float NewCalcTarget;
	
	// Find the degrees distance between the target and max
	float diffInMaxFromTarget = _axisMax - SatTargetAxis;
	// Find the equivalent number of steps for the difference
	float numberOfStepsAway = diffInMaxFromTarget/_stepCountSize;
	// use int to consistently round down the number of steps - this gives the next position below the axisMax
	int roundedSteps = (int)numberOfStepsAway;
	
	if(abs(numberOfStepsAway - (float)roundedSteps) > 0.001)
	// Multiply up the number of steps away by the step size to get the degrees position.
	NewCalcTarget = (roundedSteps*_stepCountSize) + SatTargetAxis;
	else
	NewCalcTarget = CurrentPosAxis - _stepCountSize;
	
	return NewCalcTarget;
}

//*******************************************************************************************************************
//*******************************************************************************************************************

// Sub Algorithim
// Takes in Sat Point Array and Current Postion in order to calculate and update New Target Az El Pol
// Considers top boundary movements for Elevation
float ControlPattern_SearchAxisClass::NextAxisPositionBoundaried_AxisAtMin(float SatTargetAxis, float CurrentPosAxis)
{
	float NewCalcTarget;
	
	// Find the degrees distance between the target and max
	float diffInMaxFromTarget = _axisMin - SatTargetAxis;
	// Find the equivalent number of steps for the difference
	float numberOfStepsAway = diffInMaxFromTarget/_stepCountSize;
	// use int to consistently round down the number of steps - this gives the next position below the axisMax
	int roundedSteps = (int)numberOfStepsAway;
	
	if(abs(numberOfStepsAway - (float)roundedSteps) > 0.001)
	// Multiply up the number of steps away by the step size to get the degrees position.
	NewCalcTarget = (roundedSteps*_stepCountSize) + SatTargetAxis;
	else
	NewCalcTarget = CurrentPosAxis + _stepCountSize;
	
	return NewCalcTarget;
}
ControlPattern_SearchAxisClass ControlPattern_SearchAxis;

