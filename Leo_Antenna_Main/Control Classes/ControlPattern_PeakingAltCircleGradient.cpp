//
//
//

#include "ControlPattern_PeakingAltCircleGradient.h"

void ControlPattern_PeakingAltCircleGradientClass::init()
{
	

}

//***************************************************************************************

void ControlPattern_PeakingAltCircleGradientClass::Setup()
{
	StndFncts.Clear(_CentreSkyPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = 0;
	
	_CircRadi = 0.0;
	_CircRadi = 0.0;
	
	_KAz = 0.0;
	_KEl = 0.0;
}

// Alternative Setup function for fast setup of peaking pattern
void ControlPattern_PeakingAltCircleGradientClass::Setup(float AzStepSize, float ElStepSize, float KAz, float KEl, float signalLostThreshold)
{
	StndFncts.Clear(_CentreSkyPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = signalLostThreshold;
	
	_CircRadi = ElStepSize;
	_CircRadi = AzStepSize;
	
	_KAz = KAz;
	_KEl = KEl;
}


//**************************************************************************************

// Get-Set Functions for Centre of Peaking Pattern
void ControlPattern_PeakingAltCircleGradientClass::SetCentreSkyPosition(float InputPosition[])
{
	// Sets Centre Sky Position to input
	StndFncts.SetEquals(InputPosition,_CentreSkyPosition,3);
	
	//Resets Current Pattern Position Counter for new peaking pattern
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isFirstPass = true;
	
		_signalLostcounter = 0;
		_isSignalLost = false;
}

void ControlPattern_PeakingAltCircleGradientClass::GetCentreSkyPosition(float* ReturnedPosition)
{
	StndFncts.SetEquals(_CentreSkyPosition,ReturnedPosition,3);
}

//**************************************************************************************

// Set Function for Peaking Pattern Sizes
void ControlPattern_PeakingAltCircleGradientClass::SetPeakingSizes(float AzStepSize, float ElStepSize, float KAz, float KEl)
{
	_CircRadi = ElStepSize;
	_CircRadi = AzStepSize;
	
	_KAz = KAz;
	_KEl = KEl;
}

//***************************************************************************************

//***************************************************************************************
//***************************************************************************************
// Main Pattern Code
//  Handles incomming Modem Signal to plot New Sky Target positions based on square box peaking pattern
//  Checks signal for signal loss - resulting in flagging signal lost for main code to restart pointing
//  Records signal strength for each pattern position
//  Outputs appropriate position based on step sizes and a centre position
void ControlPattern_PeakingAltCircleGradientClass::GetNewSkyTarget(float CurrentModemSignal, float* NewSkyTarget)
{
	
	//Save Signal for Actual Current Position, check signal is above threshold or increase signal lost counter
	HandleSignalStrengthSave(CurrentModemSignal);
	
	// Update to Next Pattern Position
	_CurrentPatternTargetPositionCounter++;
	
	
	// Reset pattern on completion if Signal is found, otherwise flag issignal lost
	//if(_CurrentPatternTargetPositionCounter > D2 || !_isFirstPass)
	HandleEndOfPattern();


	calculateNextSkyTarget(NewSkyTarget);

}

//***************************************************************************************
// Save Signal Strength to Class Array
//  Increase counter if signal strength is less than Threshold - leads to signal lost flag
void ControlPattern_PeakingAltCircleGradientClass::HandleSignalStrengthSave(float CurrentModemSignal)
{
	if(_CurrentPatternTargetPositionCounter != NONE)
	{
		_SavedPeakingSignalsAtPoint[_CurrentPatternTargetPositionCounter] = CurrentModemSignal;
		
		if(CurrentModemSignal < _signalLostThreshold)
		_signalLostcounter++;
		else
		_signalLostcounter = 0;
	}else
	_signalLostcounter = 0;
}

//***************************************************************************************

// Handle End of 4 Position Peaking Pattern
//   - Checks for signal lost
//   - Recalculates new Centre Sky Target based on saved signal strength values
void ControlPattern_PeakingAltCircleGradientClass::HandleEndOfPattern()
{
	
	// Check for lost signal
	if(CheckisSignalLost())
	return;
	else
	{
	
	HandleDBLossValues();//Scales DB-100 values from Modem to stop massive skew due to change from -4 to -100	
	
	CalculateNewCentreSkyTarget();
	}
	
	
}

// Calculate New Peaking Pattern Centre Sky Target Position
//  Uses technique from journal on satellite antenna peaking
//  Box square peaking pattern - requires values to setup on class usage
void ControlPattern_PeakingAltCircleGradientClass::CalculateNewCentreSkyTarget()
{
	// Recalculate New Centre Position
	float NewCentreSkyTargets[3];
	
	NewCentreSkyTargets[Az] = _CentreSkyPosition[Az] + _KAz*(((_SavedPeakingSignalsAtPoint[D1] + _SavedPeakingSignalsAtPoint[A2] + _SavedPeakingSignalsAtPoint[B2]) - (_SavedPeakingSignalsAtPoint[A1] + _SavedPeakingSignalsAtPoint[B1] + _SavedPeakingSignalsAtPoint[D2])));
	
	NewCentreSkyTargets[El] = _CentreSkyPosition[El] + _KAz*(((_SavedPeakingSignalsAtPoint[B1] + _SavedPeakingSignalsAtPoint[C1] + _SavedPeakingSignalsAtPoint[D1]) - (_SavedPeakingSignalsAtPoint[B2] + _SavedPeakingSignalsAtPoint[C2] + _SavedPeakingSignalsAtPoint[D2])));
	
	// 		Serial.println("New Sky AZ;" + String(NewCentreSkyTargets[Az]));
	// 		Serial.println("New Sky El;" + String(NewCentreSkyTargets[El]));
	StndFncts.SetEquals(NewCentreSkyTargets,_CentreSkyPosition,2);
	
}


//***************************************************************************************

// Check is Signal Lost Counter for lost signal situation
//   - if signal is lost at all peaking pattern points then flag isSignalLost
boolean ControlPattern_PeakingAltCircleGradientClass::CheckisSignalLost()
{
	if(_CurrentPatternTargetPositionCounter > D2)
	{
		_isFirstPass = false;
		
		if(_signalLostcounter == 8)
		{
			_isSignalLost = true;
			Serial.print("+,Peaking Pattern Signal Lost- Flagging isSignalLost \n");
			
			//Set Current Pattern Target Position to SignalLost condition - points Antenna At Centre
			_CurrentPatternTargetPositionCounter = SignalLost;
			
		}else
		{
			_signalLostcounter = 0;
			_CurrentPatternTargetPositionCounter = A1; // reset peaking back to start
		}
	}
	
	return _isSignalLost;
}

//***************************************************************************************
//***************************************************************************************

// Next Peaking Pattern Position for NewSkyTarget based on Current Pattern Target Position Counter
//   Outputs to NewSkyTarget array
void ControlPattern_PeakingAltCircleGradientClass::calculateNextSkyTarget(float* NewSkyTarget)
{
	const float B1AzAngle = cos(radians(135.0));
	const float B1ElAngle = sin(radians(135.0));
	
	const float D1AzAngle = cos(radians(45.0));
	const float D1ElAngle = sin(radians(45.0));
	
	
	const float B2AzAngle = cos(radians(315.0));
	const float B2ElAngle = sin(radians(315.0));
	
	const float D2AzAngle = cos(radians(225.0));
	const float D2ElAngle = sin(radians(225.0));
	//Switch statement for each pattern position. Creates the square peaking pattern - see flow chart for more details
	switch(_CurrentPatternTargetPositionCounter)
	{
		// Pattern Position A
		case A1: NewSkyTarget[El] = _CentreSkyPosition[El];
		NewSkyTarget[Az] = _CentreSkyPosition[Az] - _CircRadi;
		break;
		
		case B1: NewSkyTarget[El] = _CentreSkyPosition[El] + (_CircRadi * B1ElAngle);
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + (_CircRadi * B1AzAngle);
		break;
		
		case C1: NewSkyTarget[El] = _CentreSkyPosition[El] + _CircRadi;
		NewSkyTarget[Az] = _CentreSkyPosition[Az];
		break;
		
		case D1: NewSkyTarget[El] = _CentreSkyPosition[El] + (_CircRadi * D1ElAngle);
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + (_CircRadi * D1AzAngle);
		break;

		case A2: NewSkyTarget[El] = _CentreSkyPosition[El];
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + _CircRadi;
		break;
		
		case B2:NewSkyTarget[El] = _CentreSkyPosition[El] + (_CircRadi * B2ElAngle);
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + (_CircRadi * B2AzAngle);
		break;
		
		
		case C2: NewSkyTarget[El] = _CentreSkyPosition[El] - _CircRadi;
		NewSkyTarget[Az] = _CentreSkyPosition[Az];
		break;
		
		case D2:NewSkyTarget[El] = _CentreSkyPosition[El] + (_CircRadi * D2ElAngle);
		NewSkyTarget[Az] = _CentreSkyPosition[Az] + (_CircRadi * D2AzAngle);
		break;
		
		//Signal Lost Condition
		case SignalLost:NewSkyTarget[El] = _CentreSkyPosition[El];
		NewSkyTarget[Az] = _CentreSkyPosition[Az];
		break;
		
		// Error usage of Peaking Pattern - out of bounds of switch case
		default: Serial.println("@,Peaking Pattern Miss-use Error - Current Pattern Position Invalid:" + String(_CurrentPatternTargetPositionCounter) + "\n"); break;
	}
}




//**************************************************************
//Handler for -100db signal values during peaking pattern
// Values take the average of the adjacent box positions to better estimate a signal value that will not produce a swing in new position
// Or it takes the minimum signal value used for detecting signal loss
void ControlPattern_PeakingAltCircleGradientClass::HandleDBLossValues()
{

	if(_SavedPeakingSignalsAtPoint[A1] <= -99.0)
		_SavedPeakingSignalsAtPoint[A1] = max((_SavedPeakingSignalsAtPoint[B1] + _SavedPeakingSignalsAtPoint[D2])/2,_signalLostThreshold);

	if(_SavedPeakingSignalsAtPoint[B1] <= -99.0)
	_SavedPeakingSignalsAtPoint[B1] = max((_SavedPeakingSignalsAtPoint[A1] + _SavedPeakingSignalsAtPoint[C1])/2,_signalLostThreshold);


	if(_SavedPeakingSignalsAtPoint[C1] <= -99.0)
	_SavedPeakingSignalsAtPoint[C1] = max((_SavedPeakingSignalsAtPoint[B1] + _SavedPeakingSignalsAtPoint[D1])/2,_signalLostThreshold);


	if(_SavedPeakingSignalsAtPoint[D1] <= -99.0)
	_SavedPeakingSignalsAtPoint[D1] = max((_SavedPeakingSignalsAtPoint[C1] + _SavedPeakingSignalsAtPoint[A2])/2,_signalLostThreshold);

	if(_SavedPeakingSignalsAtPoint[A2] <= -99.0)
	_SavedPeakingSignalsAtPoint[A2] = max((_SavedPeakingSignalsAtPoint[B2] + _SavedPeakingSignalsAtPoint[D1])/2,_signalLostThreshold);

	if(_SavedPeakingSignalsAtPoint[B2] <= -99.0)
	_SavedPeakingSignalsAtPoint[B2] = max((_SavedPeakingSignalsAtPoint[A2] + _SavedPeakingSignalsAtPoint[C2])/2,_signalLostThreshold);


	if(_SavedPeakingSignalsAtPoint[C2] <= -99.0)
	_SavedPeakingSignalsAtPoint[C2] = max((_SavedPeakingSignalsAtPoint[B2] + _SavedPeakingSignalsAtPoint[D2])/2,_signalLostThreshold);


	if(_SavedPeakingSignalsAtPoint[D2] <= -99.0)
	_SavedPeakingSignalsAtPoint[D2] = max((_SavedPeakingSignalsAtPoint[C2] + _SavedPeakingSignalsAtPoint[A1])/2,_signalLostThreshold);

}