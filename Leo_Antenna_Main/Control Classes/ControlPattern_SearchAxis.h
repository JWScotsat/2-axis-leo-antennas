// ControlPattern_SearchAxis.h

/*
Control Search Pattern Counter for Axis.

Initial Development through TDD with Arduino Unit.

Simple Small Counter class. Used in conjunction with ControlPattern_Search. This class returns a
positional value based on Current Position and Target Position inputs. Stores and handles certain
boundary conditions.

Simply initialise and feed in start conditions to request NextPosition(). Results are output
from function

Configurable for size, shape and step-size of pattern.


Created By Jack Wells
Change Log: 13/8/15 - Finished Devlopment for v1.0 - JW

*/
#ifndef _CONTROLPATTERN_SEARCHAXIS_h
#define _CONTROLPATTERN_SEARCHAXIS_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


class ControlPattern_SearchAxisClass
{
	protected:
	
	float NextAxisPositionRollOver(float SatTargetAxis, float CurrentPosAxis);
	float NextAxisPositionBoundaried(float SatTargetAxis, float CurrentPosAxis);
	float NextAxisPositionBoundaried_AxisAtMax(float SatTargetAxis, float CurrentPosAxis);
	float NextAxisPositionBoundaried_AxisAtMin(float SatTargetAxis, float CurrentPosAxis);
	public:

	float _stepCountSize; // Size between each position of the pattern
	float _maxCounterBoundary; // Counter Search Limit 
	boolean _AxisBoundaryRollOver; // Toggle whether the boundaries roll over to the opposite side (min-max)
	boolean _atBoundary; // Result of Next Position - provides details if next position is on a search or physical boundary
	boolean _isReversed; // Negative direction Toggle
	float _axisMax; // Physical Max of axis/counter
	float _axisMin; // Physical Min of axis/counter
	
	
	void init();

	float NextAxisPosition(float SatTarget, float CurrentPos);
	
};

extern ControlPattern_SearchAxisClass ControlPattern_SearchAxis;

#endif

