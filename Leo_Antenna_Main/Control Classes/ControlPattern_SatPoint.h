// ControlPattern_SatPoint.h
/*
 Main Control Code
  Calculates the Az El Pol for a given Satellite Longitude based on current GPS data
 Use Satellite Details Struct to package input data
 
 
 Change Log: V1 Created 18/8/15 - JW

*/

#ifndef _CONTROLPATTERN_SATPOINT_h
#define _CONTROLPATTERN_SATPOINT_h



#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif


// Antenna Physical Boundary Definitions
#ifndef MaxPhysical_Elevation
#define MaxPhysical_Elevation 85.0
#define MinPhysical_Elevation -10.0
#define MaxPhysical_Azimuth 360.01
#define MinPhysical_Azimuth -0.01
#define MaxPhysical_Pol 360.0
#define MinPhysical_Pol 0.0
#endif

#include "Supporting Libs/StandardFunctions_Array.h"

// Testing Defines
#define OfficeGPSLat 55.953986
#define OfficeGPSLng -3.197231
#define IS907Lng 332.5
//

// Input data boundaries
#define MaxSatLng 360.0
#define MinSatLng -360.0
#define MaxSatSkew 360.0
#define MinSatSkew -360.0
#define MaxGPSLng 360.0
#define MinGPSLng -360.0
#define MaxGPSLat 90.0
#define MinGPSLat -90.0
//

// Error Values
#define ElevationOOBError 4
#define PolOOBError 5
#define SatelliteDetailsInputError 6
#define GPSInputError 7


//Array Position Defines
#define Lng 1
#define Lat 0

#ifndef Az 
#define Az 0
#define El 1
#define Pol 2
#endif

#define HorizontalPol 1
#define VerticalPol 0
//

struct SatelliteDetails {
	float SatLng;
	int SatPolHV;
	float SatCustomSkew;
};



class ControlPattern_SatPointClass
{
	protected:
    float handlePolBoundaries(float PolCalc, int SatHVPol);
	float handleElevationBoundaries(float elevationDeg);
	boolean checkInputData(SatelliteDetails NewSat, float *ReturnedPos);
	StandardFunctions_Array StndFnct;
	
	float _StoredSatAzPos; //Storage of calculated Azimuth Position in the sky for Geo Location
	
	float _AzOffsetSat = 0.0;
	
	public:
	
	float _GPSLat;
	float _GPSLng;
	float _SatLng;
	SatelliteDetails _CurrentSat;
	int _isError;
	String _StatusMessage;
	
	
	
	void setup();
	void setGPS(float GPSLat, float GPSLng);
	void returnGPS(float *ReturnedGPS);
	void CalculateSatPoint(float SatLng, int SatVHPol, float *ReturnedPos);
	void CalculateSatPoint(SatelliteDetails NewSat, float *ReturnedPos);
	void init();
	
	float getSatAzPos(){return _StoredSatAzPos;}; //Return Stored Az Pos protecting variable from outer code changes
		
	void CalculateSat_PolUpdate(float *ReturnedPos);//Single function for updating Pol calculated position for new GEO Location

	void setAzOffset(float AzOffset){_AzOffsetSat = AzOffset;};
	float getAzOffset(){return _AzOffsetSat;};
		
};

extern ControlPattern_SatPointClass ControlPattern_SatPoint;

#endif

