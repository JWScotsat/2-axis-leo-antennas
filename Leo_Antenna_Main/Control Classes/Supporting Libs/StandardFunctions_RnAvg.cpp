//
//
//

#include "StandardFunctions_RnAvg.h"

void StandardFunctions_RnAvgClass::init()
{


}


//***************************************************************************

//Main Setup.
// Define number of averages to be used. Set all initial values
void StandardFunctions_RnAvgClass::setup(int numAvg)
{
	_NumberOfAverages = numAvg;
	
	for(int i = 0; i < _NumberOfAverages; i++)
	ValueStore[i] = 0.0;
	_index = 0;
}


//***************************************************************************
// Add value to list of current averages
// reset index to start of list to remove eldest value with lastest value
void StandardFunctions_RnAvgClass::addValue(float Val)
{
	if(abs(_CurrentAvg - Val) > _ValueBuffer)
	{
		ValueStore[_index] = Val;
		
		_index++;
	}
	
	if(_index >= _NumberOfAverages)
	_index = 0;
}


//***************************************************************************
// Return total average
float StandardFunctions_RnAvgClass::getCurrentAvg()
{
	float total = 0.0;
	
	for (int i = 0; i < _NumberOfAverages; i++)
	total += ValueStore[i];
	
	float avg = total/ (float)_NumberOfAverages;
	
	_CurrentAvg = avg;
	
	return _CurrentAvg;
}

//***************************************************************************
// Set current average
void StandardFunctions_RnAvgClass::setValue(float Val)
{
	if(abs(_LastSetValue - Val) > 0.1)
	{
		for (int i = 0; i < _NumberOfAverages; i++)
		ValueStore[i] = Val;
		_LastSetValue = Val;
	}
}

//***************************************************************************//***************************************************************************
//***************************************************************************//***************************************************************************

