//
//
//

#include "StandardFunctions_ComplFilter.h"

void StandardFunctions_ComplFilterClass::init()
{


}

void StandardFunctions_ComplFilterClass::setup(float KGyro)
{
	_KGyro = KGyro;
}


float StandardFunctions_ComplFilterClass::update(float AccurateSensor, float GyroDelta)
{
	
	//Complimentary filter equation
	float ResultingPosition = (_CurrentPosition + GyroDelta)*_KGyro + ((1-_KGyro) * AccurateSensor);
	
	_CurrentPosition = ResultingPosition;
	
	return ResultingPosition;
}

float StandardFunctions_ComplFilterClass::updateCircularFilter(float AccurateSensor, float GyroDelta)
{
	//Find when accurate sensor crosses a large border
	float AccurateSensorDelta = abs(AccurateSensor - _CurrentPosition);
	
	//Handle 360 -> 0 border
	if(_CurrentPosition >180.0 && AccurateSensor < 180.0 && AccurateSensorDelta > 180.0)
	_CurrentPosition -= 360.0;
	
	//Handle negative 0 <- 360 border
	if(_CurrentPosition < 180.0 && AccurateSensor > 180.0 && AccurateSensorDelta > 180.0)
	_CurrentPosition += 360.0;
	
	//Complimentary filter equation
	float ResultingPosition = (_CurrentPosition + GyroDelta)*_KGyro + ((1-_KGyro) * AccurateSensor);
	
	_CurrentPosition = ResultingPosition;
	
	//Bound Returned Value to 0-360 without effecting the filter
	if(ResultingPosition > 360.0)
	ResultingPosition -= 360.0;
	
	if(ResultingPosition < -0.0001)
	ResultingPosition += 360.0;
	
	return ResultingPosition;
}

StandardFunctions_ComplFilterClass ComplFilter;

