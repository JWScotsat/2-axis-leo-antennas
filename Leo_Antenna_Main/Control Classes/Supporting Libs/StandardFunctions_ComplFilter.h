// StandardFunctions_ComplFilter.h

#ifndef _STANDARDFUNCTIONS_COMPLFILTER_h
#define _STANDARDFUNCTIONS_COMPLFILTER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class StandardFunctions_ComplFilterClass
{
 protected:
	float _KGyro = 0.0;
	float _CurrentPosition = 0.0;
	

 public:
	float getValue(){return _CurrentPosition;};
	void init();
	void setup(float KGyro);
	float update(float AcurateSensor, float GyroDelta);
	float updateCircularFilter(float AcurateSensor, float GyroDelta);
};

extern StandardFunctions_ComplFilterClass ComplFilter;

#endif

