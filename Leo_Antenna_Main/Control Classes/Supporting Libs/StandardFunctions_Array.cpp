//
//
//

#include "StandardFunctions_Array.h"

void StandardFunctions_Array::init()
{


}
//Print simple positional Array
void StandardFunctions_Array::Print(float InputArray[])
{
	
	Serial.print(" {");
		
		//for (int i = 0; i < sizeof(InputArray)/sizeof(float); i++)
		for (int i = 0; i < 3; i++)
		{
			Serial.print(InputArray[i]);
			Serial.print(",");
		}
		
	Serial.print("}");
	
}

// Print Lat Lng Structure
void StandardFunctions_Array::Print(GPSLatLng InputLatLng)
{
	
	Serial.print(" {");
		
		Serial.print(InputLatLng.Latitude,6);
		Serial.print(",");
		Serial.print(InputLatLng.Longitude,6);
	Serial.print("}");
	
}


// Print GPS Time Structure
void StandardFunctions_Array::Print(GPSTime InputTime)
{
	
	Serial.print(" {dd,mm,yy,hh,mm,ss,isValid} {");
	
	Serial.print(InputTime.Day);
	Serial.print(",");
	Serial.print(InputTime.Month);
	Serial.print(",");
	Serial.print(InputTime.Year);
	Serial.print(",");
	Serial.print(InputTime.Hr);
	Serial.print(",");
	Serial.print(InputTime.Min);
	Serial.print(",");
	Serial.print(InputTime.Sec);
	Serial.print(",");
	Serial.print(InputTime.TimeValid);
Serial.print("}");

}

void StandardFunctions_Array::PrintArray(double InputArray[],int ArraySize)
{
	
	Serial.print(" {");
		
		//for (int i = 0; i < sizeof(InputArray)/sizeof(float); i++)
		for (int i = 0; i < ArraySize; i++)
		{
			Serial.print(InputArray[i]);
			Serial.print(",");
		}
		
	Serial.print("}");
	
}


void StandardFunctions_Array::Print(float InputArray[] ,int ArraySize)
{
	
	Serial.print(" {");
		
		//for (int i = 0; i < sizeof(InputArray)/sizeof(float); i++)
		for (int i = 0; i < ArraySize; i++)
		{
			Serial.print(InputArray[i]);
			Serial.print(",");
		}
		
	Serial.print("}");
	
}

void StandardFunctions_Array::SetEquals(float InputArray[], float *OutputArray, int ArraySize)
{
	for (int i = 0; i < ArraySize; i++)
	{
		OutputArray[i] = InputArray[i];
	}
	
	
}

void StandardFunctions_Array::Clear(float *InputArray, int ArraySize)
{
	for (int i = 0; i < ArraySize; i++)
	{
		InputArray[i] = 0.0;
	}
	
	
}


//Returns: boolean result (equal or not equal)
boolean StandardFunctions_Array::CompareFloats(float f1, float f2, int NumberofDecPlaces){
	float decimalPlace = 1.0000;
	
	
	for (int i = 0; i < NumberofDecPlaces; i++)
	{
		decimalPlace /=10;
	}
	
	if(abs(f1 - f2) < decimalPlace)
	return true;
	else
	return false;
}

void StandardFunctions_Array::Randomise(float *ArrayRandomised)
{
	for (int i = 0; i < 3; i++)
	{
		long randomUpper = random(0, 1000);
		long randomLower = random(0, 1000);
		float randomMapped = map(randomUpper, 0, 1000, -500, 500) +
		map(randomUpper, 0, 1000, -500, 500)/100.0;
		ArrayRandomised[i] = randomMapped;
	}
}

void StandardFunctions_Array::Constrain(float *InputArray, int ArraySize, float LowerLim, int UpperLim)
{
	float inStore;
	for (int i = 0; i < ArraySize; i++)
	{
		constrain(InputArray[i],LowerLim,UpperLim);
	}
}


//**************************************************************
/*
Function: Bound
Description: Bounds a vlue between an uppper and lower limit. Used for ensure heading is 0-360 where needed
Returns: Input bounded to range
By: Jack Wells
*/
float StandardFunctions_Array::Bound(float Input, float LowerLimit, float UpperLimit)
{
	if(Input > UpperLimit)
	Input -= (UpperLimit*(int(Input/UpperLimit)));
	
	if(Input < (LowerLimit-UpperLimit))
	Input += (UpperLimit*abs((int(Input/UpperLimit))));

	if(Input < LowerLimit)
	Input += UpperLimit;
	
	return Input;
}


//**************************************************************
//**************************************************************//**************************************************************
//**************************************************************//**************************************************************
