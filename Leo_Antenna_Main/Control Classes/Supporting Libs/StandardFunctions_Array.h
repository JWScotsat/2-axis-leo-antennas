// StandardFunctions_Array.h

#ifndef _STANDARDFUNCTIONS_ARRAY_h
#define _STANDARDFUNCTIONS_ARRAY_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif

#ifndef GPSLatLngStruct
#define GPSLatLngStruct
struct GPSLatLng
{
	double Latitude;
	double Longitude;
	
};
#endif

#ifndef GPSTimeStruct
#define GPSTimeStruct
struct GPSTime{
	byte Day = 0;
	byte Month = 0;
	int Year = 0;
	byte Hr = 0;
	byte Min = 0;
	byte Sec = 0;
	boolean TimeValid = false;
};
#endif

class StandardFunctions_Array
{
	protected:


	public:
	void init();
	void Print(float InputArray[]);

	void Print(GPSLatLng InputLatLng);
	
	void Print(GPSTime InputTime);
		
	void Print(float InputArray[] ,int ArraySize);
	void PrintArray(double InputArray[] ,int ArraySize);

	void SetEquals(float InputArray[], float *OutputArray, int ArraySize);


	void Randomise(float *ArrayRandomised);


	void Clear(float *InputArray, int ArraySize);
	
	void Constrain(float *InputArray, int ArraySize, float LowerLim, int UpperLim);
	
	float Bound(float Input, float LowerLimit, float UpperLimit);// Bound a value between upper and lower limit
	
	boolean CompareFloats(float f1, float f2, int NumberofDecPlaces);

};

#endif

