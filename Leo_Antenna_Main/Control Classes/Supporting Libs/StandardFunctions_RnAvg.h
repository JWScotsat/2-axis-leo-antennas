// StandardFunctions_RnAvg.h

#ifndef _STANDARDFUNCTIONS_RNAVG_h
#define _STANDARDFUNCTIONS_RNAVG_h



//Running Average Standard Function
// Class handles a running average (Max of 10 floats)
// Add values to current average then use get function for result


#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

class StandardFunctions_RnAvgClass
{
 protected:
	int _index;
	
	
	float ValueStore[100];
	float _CurrentAvg;
	float _LastSetValue;
	
 public:
	float _ValueBuffer = 0.0;
	int _NumberOfAverages = 10;
	
	void init();
	void setup(int numAvg);
	void clear();
	void addValue(float Val);
	float getCurrentAvg();
	
	void setValue(float Val);//Sets running average value 
	
};

extern StandardFunctions_RnAvgClass StandardFunctions_RnAvg;

#endif

