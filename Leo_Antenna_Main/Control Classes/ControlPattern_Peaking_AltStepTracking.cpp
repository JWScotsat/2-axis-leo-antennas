//
//
//

#include "ControlPattern_Peaking_AltStepTracking.h"

void ControlPattern_Peaking_AltStepTrackingClass::init()
{
	

}

//***************************************************************************************

void ControlPattern_Peaking_AltStepTrackingClass::Setup()
{
	StndFncts.Clear(_StartPosition,3);
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;
	_signalLostThreshold = 0;

}

// Alternative Setup function for fast setup of peaking pattern
void ControlPattern_Peaking_AltStepTrackingClass::Setup(float ElStepSize, float AzStepSize, int signalLostThreshold, int SigBufferError)
{
	StndFncts.Clear(_StartPosition,3);
	
	_DeltaEl = ElStepSize;
	_DeltaAz = AzStepSize;
	_signalLostThreshold = signalLostThreshold;
	
	_CurrentPatternTargetPositionCounter = NONE;
	
	_isSignalLost = false;
	_signalLostcounter = 0;

	
	_ResetHighSig = false;
	StndFncts.Clear(_CurrentSkyMaxPoint,3);
	
	_SavedPatternTargetPositionCounter = 0;

	_PatternPositionCheckedCounter = 0;
	
	_SavedBestSignal = 0;
	
	_NewSigBufferError = SigBufferError;
	
}


//**************************************************************************************

// Get-Set Functions for Centre of Peaking Pattern
void ControlPattern_Peaking_AltStepTrackingClass::SetStartPosition(float InputPosition[], int CurrentPeakSignal)
{
	// Sets Centre Sky Position to input
	StndFncts.SetEquals(InputPosition,_StartPosition,3);
	
	_isSignalLost = false;
	_signalLostcounter = 0;

	
	_ResetHighSig = false;
	StndFncts.SetEquals(InputPosition,_CurrentSkyMaxPoint,3);
	StndFncts.SetEquals(InputPosition,_CurrentSkyTarget, 3);
	
	_CurrentPatternTargetPositionCounter = NONE;
	_SavedPatternTargetPositionCounter = 0;
	_PatternPositionCheckedCounter = 0;
	
	_SavedBestSignal = CurrentPeakSignal;
}


void ControlPattern_Peaking_AltStepTrackingClass::GetCentreSkyPosition(float* ReturnedPosition)
{
	StndFncts.SetEquals(_CurrentSkyMaxPoint,ReturnedPosition,3);
}


//***************************************************************************************

//***************************************************************************************
//***************************************************************************************
// Main Pattern Code
//  Handles incomming Modem Signal to plot New Sky Target positions based on square box peaking pattern
//  Checks signal for signal loss - resulting in flagging signal lost for main code to restart pointing
//  Records signal strength for each pattern position
//  Outputs appropriate position based on step sizes and a centre position
void ControlPattern_Peaking_AltStepTrackingClass::GetNewSkyTarget(int CurrentModemSignal, float* NewSkyTarget)
{
	
	//Save Signal for Actual Current Position, check signal is above threshold or increase signal lost counter
	HandleSignalStrengthSave(CurrentModemSignal);

	//Check Sig Lost
	CheckisSignalLost(CurrentModemSignal);
	
	//
	HandleEndOfPattern();
	
	calculateNextSkyTarget(NewSkyTarget);

}

//***************************************************************************************
// Save Signal Strength to Class Array
//  Increase counter if signal strength is less than Threshold - leads to signal lost flag
void ControlPattern_Peaking_AltStepTrackingClass::HandleSignalStrengthSave(int CurrentModemSignal)
{
	if(_CurrentPatternTargetPositionCounter != NONE)
	{
		if((CurrentModemSignal - _SavedBestSignal) > _NewSigBufferError)
		{
			_SavedBestSignal = CurrentModemSignal;
			_ResetHighSig = false;
			StndFncts.SetEquals(_CurrentSkyTarget,_CurrentSkyMaxPoint,3);
			_SavedPatternTargetPositionCounter = _CurrentPatternTargetPositionCounter;
			_PatternPositionCheckedCounter = 0;

		}else
		{
			_CurrentPatternTargetPositionCounter++;
			_PatternPositionCheckedCounter++;
		}

		
		// Update to Next Pattern Position
	}else
	{
		_CurrentPatternTargetPositionCounter++;
		_SavedBestSignal = CurrentModemSignal - _NewSigBufferError;
		_ResetHighSig = false;
		StndFncts.SetEquals(_CurrentSkyTarget,_CurrentSkyMaxPoint,3);
		_SavedPatternTargetPositionCounter = _CurrentPatternTargetPositionCounter;
		_PatternPositionCheckedCounter = 0;
	}
	
	
	
}

//***************************************************************************************

// Handle End of 4 Position Peaking Pattern
//   - Checks for signal lost
//   - Recalculates new Centre Sky Target based on saved signal strength values
void ControlPattern_Peaking_AltStepTrackingClass::HandleEndOfPattern()
{
	
	

	if(_CurrentPatternTargetPositionCounter > D)
	{
		if(_PatternPositionCheckedCounter >= 4)
		_CurrentPatternTargetPositionCounter = NONE;
		else
		_CurrentPatternTargetPositionCounter = A;
	}else
	{
		if(_PatternPositionCheckedCounter >= 4)
		_CurrentPatternTargetPositionCounter = NONE;
	}
}




//***************************************************************************************

// Check is Signal Lost Counter for lost signal situation
//   - if signal is lost at all peaking pattern points then flag isSignalLost
boolean ControlPattern_Peaking_AltStepTrackingClass::CheckisSignalLost(int CurrentModemSignal)
{
	if(CurrentModemSignal < _signalLostThreshold)
	_signalLostcounter++;
	else
	{
		_signalLostcounter = 0;
		_isSignalLost = false;
	}

	if(_signalLostcounter >= 5)
	{
		_isSignalLost = true;
		Serial.print("+,Peaking Pattern Signal Lost- Flagging isSignalLost \n");
		
		//Set Current Pattern Target Position to SignalLost condition - points Antenna At Centre
		_CurrentPatternTargetPositionCounter = SignalLost;
		
	}
	
	return _isSignalLost;
}

//***************************************************************************************
//***************************************************************************************

// Next Peaking Pattern Position for NewSkyTarget based on Current Pattern Target Position Counter
//   Outputs to NewSkyTarget array
void ControlPattern_Peaking_AltStepTrackingClass::calculateNextSkyTarget(float* NewSkyTarget)
{
	//Switch statement for each pattern position. Creates the square peaking pattern - see flow chart for more details
	switch(_CurrentPatternTargetPositionCounter)
	{
		// Pattern Position A
		case A: NewSkyTarget[El] = _CurrentSkyMaxPoint[El] + _DeltaEl;
		NewSkyTarget[Az] = _CurrentSkyMaxPoint[Az];
		break;
		
		case B: NewSkyTarget[El] = _CurrentSkyMaxPoint[El];
		NewSkyTarget[Az] = _CurrentSkyMaxPoint[Az] + _DeltaAz;
		break;
		
		case C: NewSkyTarget[El] = _CurrentSkyMaxPoint[El] - _DeltaEl;
		NewSkyTarget[Az] = _CurrentSkyMaxPoint[Az];
		break;
		
		case D:	NewSkyTarget[El] = _CurrentSkyMaxPoint[El];
		NewSkyTarget[Az] = _CurrentSkyMaxPoint[Az] - _DeltaAz;
		break;
		
		case NONE : NewSkyTarget[El] = _CurrentSkyMaxPoint[El];
		NewSkyTarget[Az] = _CurrentSkyMaxPoint[Az];
		break;
		
		//Signal Lost Condition
		case SignalLost:NewSkyTarget[El] = _CurrentSkyMaxPoint[El];
		NewSkyTarget[Az] = _CurrentSkyMaxPoint[Az];
		break;
		
		// Error usage of Peaking Pattern - out of bounds of switch case
		default: Serial.println("@,Peaking Pattern Miss-use Error - Current Pattern Position Invalid:" + String(_CurrentPatternTargetPositionCounter) + "\n"); break;
	}
	
	StndFncts.SetEquals(NewSkyTarget,_CurrentSkyTarget, 3);
}


//***************************************************************************************
//***************************************************************************************

