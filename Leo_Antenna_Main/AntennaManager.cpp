#include "AntennaManager.h"

// Main Run Function
//   - Antenna Main Code




void AntennaManagerClass::run()
{
	Serial.print("+,Antenna Run\n");
	_AntennaStatusWorking = 1;
	boolean Run = true;
	boolean debugPrint = true;
	
	int i = 0;
	
	
	USBSerial._PrintIncomming = false;

	
	boolean isFirstStart = true;
	boolean isNewModemLock = false;
	
	//Frame IMU Read - get latest Yaw Pitch Roll
	_YEIFrame.getEularAngles();
	_IMUYawTimer = micros();
	_RGBLED.setColor(LEDColour_Green);
	
	
// 	//*** Testing - Speed tests
// 	switch (_CurrentConfig->_BucPower)
// 	{
// 	case 0: newPeakingOldStabilitySettings(); break;
//	case 1: oldStabilitySettings(); break; 
// 	case 2: newStabilitySettings(); break;
// 	}

	 newPeakingOldStabilitySettings();
	//***
	
	while(_isInitilisation_Passed || _DebugInitBypass)
	{
		unsigned long loopTimer = millis();
		
		//***************************************************************************************************
		//Serial & USB Data
		// Handle Serial Commands from Pi/PC
		boolean isNewCommand = AntennaComms_SerialCommandHandle();
		
		LED_USB.SetPin(isNewCommand);
		
		if(!_isInitilisation_Passed && !_DebugInitBypass)
		break;
		
				
		//***************************************************************************************************
		//Modem Signal
		
		// Read Modem Signal and Handle Lock Condition on Searching
		_ModemSignalReading = SatSignalRecv1.read();
		isNewModemLock = CheckModemLock();


		//***************************************************************************************************
		//GPS Data Handling
		if(_UseGPSCompass)
		{
			LED_Compass.SetPin(GPSCompass.updateData());
			_CurrentCompassHeading = GPSCompass.getCompassHeading(); //+ 2*GPSCompass._headingSpeed;
			GPSCompass.GetGPSLocation(&_CurrentGPS);
		}else
		GPSDecoder.GetGPSLocation(&_CurrentGPS);


		
		//***************************************************************************************************
		//Motor Logic
		
		// Check if motors have reached their target position - all motors must be in position
		boolean isAtTarget = CheckMotorsAreInPosition();
		
		// Update _CurrentSkyMotors to NewSky as motors are in position - acheived target
		//if(isAtTarget)
		StndFncs.SetEquals(_NewSkyPos,_CurrentSkyMotorsPos,3);
		
		//***************************************************************************************************
		//Mode Control
		
		// At Target or New Command results in change in control logic and new target
		if(isAtTarget || isNewCommand || isFirstStart || isNewModemLock || _CurrentAntennaMode == AntennaModeSearching || _CurrentAntennaMode == AntennaModePeaking)// (_isWideSearch && _CurrentAntennaMode == AntennaModeSearching))
		// Main Antenna Logic - handles switching between different Antenna Modes and outputs _NewSkyPos for latest target position
		_CurrentAntennaMode = AntennaModeControl(_CurrentAntennaMode,_ModemSignalReading,_CurrentSkyMotorsPos,_NewSkyPos);
		
		_TargetAdjustments.BoundaryConditions(_NewSkyPos); // Bound Sky Targets to El,Az,Pol max min

		//***************************************************************************************************
		//IMU Frame Retrieve Data
		
		FrameIMUUpdate(IMUYawPitchRoll);
		
		float FrameIMU_Roll = _FrameRollAvg.getCurrentAvg();
		float FrameIMU_Pitch = _FramePitchAvg.getCurrentAvg();
		
		
		//***************************************************************************************************
		//Antenna Yaw Calculations
		
		if(_CurrentConfig->UseOnBoardGyroWithCompass)
		{
			_AntennaYaw = OnboardGyroHeadingFiltered(); //internally updates estimated heading and returns antenna yaw
			
		}
		else
		{
			//TODO: Re-add complexx stability as fall back
			
			_EstimatedHeading = _CurrentCompassHeading;
		}
		
		//***************************************************************************************************
		//Motor Target Calculations and Adjustments for Pitch/Roll

		float returnedCorrected[3];
		
		
		if(_CurrentConfig->UseSimplifiedStabilisation)
		{
			_TargetAdjustments.CalculateSimplfiedDirectAdjustments(IMUYawPitchRoll, _AntennaYaw, _NewSkyPos, returnedCorrected);
		}
		else
		{
			float currentAzEncoder = MotorController._AntMotorAz.getAxisPos();
			
			// Yaw Calculation
			float currentHeading = _EstimatedHeading;
			
			_AntennaYaw = _TargetAdjustments.CalculateYaw(currentAzEncoder, currentHeading, MotorController._AntMotorAz._axisTrim);
			
			_TargetAdjustments.CalculateCombinedStabilityAdjustments(IMUYawPitchRoll, _AntennaYaw, _NewSkyPos, returnedCorrected, FrameIMU_Pitch, FrameIMU_Roll);
			
			returnedCorrected[Az] += currentAzEncoder;
		}
		
		//***************************************************************************************************
		//Motor Target Calculations and Adjustments for Pitch/Roll
		
		float NewMotorTargetsPos[3] = {returnedCorrected[Az], returnedCorrected[El],returnedCorrected[Pol]};
		
		_TargetAdjustments.BoundaryConditions(NewMotorTargetsPos);
		
		_CorrectedMotorTargetPos[El] = NewMotorTargetsPos[El];
		_CorrectedMotorTargetPos[Az] = NewMotorTargetsPos[Az];
		_CorrectedMotorTargetPos[Pol] = NewMotorTargetsPos[Pol];
		
		
		// Update motors with latest absolute motor targets
		MotorController.UpdateMotorTargets(_CorrectedMotorTargetPos);
		
		
		//Sensor Status Check;
		if(!_DebugInitBypass)
		if(StatusCheck() == false) break;
		
		
		//***************************************************************************************************
		//Debug & Serial Outputs
		
		//While loop to force an update rate as required
		//while(millis() - loopTimer < _CurrentConfig->_LoopUpdateTime);
		
		float DebugPacket[3] = {0, IMUYawPitchRoll[Yaw], _EstimatedHeading};

		AntennaComms_Debug_PrintOutputPacket(DebugPacket);
		
		AntennaComms_UpdatePi_MasterPacket();
		
		
		AntennaComms_UpdatePi_MotorPacket();
		
		if(_LoopDebugEnable)
		AntennaComms_Debug_PrintLoopPacket(loopTimer);
		
		isFirstStart = false;
		
		//While loop to force an update rate as required
		//SIW: THis is just a delayUntil logic
		//     Lets ,move this to after debug so debug output doesn't affect loop timing
		while(millis() - loopTimer < _CurrentConfig->_LoopUpdateTime);

		//***************************************************************************************************

	}
}


//**********************************************************************************
//**********************************************************************************


