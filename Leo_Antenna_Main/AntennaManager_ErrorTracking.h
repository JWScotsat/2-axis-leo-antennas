#ifndef _ANTENNAMANAGER_ErrorTracking_h
#define _ANTENNAMANAGER_ErrorTracking_h


//Struct for Error Reporting/tracking
#define IMUInitialised 1
#define IMUConnectionError -1
struct ErrorTracker
{
	int StatusValue;
	String StatusMessage;
};

struct ErrorInitList {
	ErrorTracker FrameYEI;
	ErrorTracker CrossYEI;
	ErrorTracker MotorAxisPol;
	ErrorTracker MotorAxisAz;
	ErrorTracker MotorAxisEl;
	ErrorTracker MotorAxisCross;
	ErrorTracker GPSSensor;
};

#define ListFrameIMU 0
#define ListCrossIMU 1
#define ListMotorCross 2
#define ListMotorAz 3
#define ListMotorEl 4
#define ListMotorPol 5
#define ListGPSSensor 6

#define InitListSize 7

#endif